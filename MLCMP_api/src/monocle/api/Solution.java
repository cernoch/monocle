/*
 * Copyright (C) 2013 Petr Ryšavý
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package monocle.api;

import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;

/**
 * This class wraps ordering. Ordering can be then modified by a problem to a more
 * readable form.
 *
 * @author Petr Ryšavý
 */
public interface Solution
{
	/**
	 * Gets the wrapped ordering.
	 *
	 * @return Ordering object or {@code null} when solution was not found.
	 */
	Ordering getOrdering();
}
