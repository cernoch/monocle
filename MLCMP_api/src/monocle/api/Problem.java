/*
 * Copyright (C) 2013 Petr Ryšavý
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package monocle.api;

import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;

/**
 * This interface defines a problem. A problem wraps a {@code MLCMP} instance.
 * Problem can be solved using {@code Solver}.
 *
 * @author Petr Ryšavý
 */
public interface Problem
{
	/**
	 * Gets the wrapped problem.
	 *
	 * @return The problem to solve.
	 */
	MLCMP getMLCMP();

	/**
	 * Processes the calculated solution (wrapper of {@code Ordering} to a instance
	 * that better captures problem properties. This may be typically a hash map.
	 *
	 * @param solution The solution that is returned by the solver.
	 * @return A understandable variant of the given solution.
	 */
	public Object processSolution(Solution solution);
}
