/*
 * Copyright (C) 2013 Petr Ryšavý
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package monocle.api.impl;

import cz.cvut.fel.rysavpe1.mlcmp.input.osm.DataLoader;
import cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory.TouristTrailXMLConfig;
import cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory.TramXMLConfig;
import cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory.XMLConfig;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.ContractionEvent;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.GraphContractionListener;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.SetUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import monocle.api.ContractableProblem;
import monocle.api.Solution;
import org.xml.sax.SAXException;

/**
 * This class represents a problem that is stored in XML defined by OpenStreetMap.
 *
 * @author Petr Ryšavý
 */
public class OSMXMLProblem extends AbstractProblem implements ContractableProblem
{
	/**
	 * Path to the file that contains the given XML.
	 */
	private Path file;
	/**
	 * Configuration that is used for parsing XML.
	 */
	private XMLConfig config;
	/**
	 * Map from track to set of edges. This is build within contractions.
	 */
	private Map<Track, Set<Edge>> trackToEdgeMap;
	/**
	 * Map from line name to set of way ids in the XML. Sometimes one line is
	 * represented by more ways.
	 */
	private Map<String, Set<Integer>> lineToWaysIdsMap;
	/**
	 * Maps station to id in XML.
	 */
	private Map<Station, Integer> stationToIdMap;
	/**
	 * List of stations that are considered to be same, because they share their
	 * coordinates.
	 */
	private Collection<Set<Integer>> equivallentStations;
	/**
	 * Graph contraction listener.
	 */
	private GCL listener;

	/**
	 * Creates new problem given the specification. Problem is not loaded.
	 *
	 * @param file   The file that contains the XML.
	 * @param config Configuration that is used for parsing XML.
	 */
	@SuppressWarnings("UseOfSystemOutOrSystemErr")
	public OSMXMLProblem(Path file, XMLConfig config)
	{
		this.file = file;
		this.config = config;
	}

	/**
	 * Creates new problem given the specification. Problem can be loaded.
	 *
	 * @param file            The file that contains the XML.
	 * @param config          Configuration that is used for parsing XML.
	 * @param loadImmediately When {@code true}, then {@code loadProblem()} method is
	 *                        called within constructor.
	 * @throws org.xml.sax.SAXException Error parsing XML.
	 * @throws java.io.IOException      Error reading the file.
	 * @see #loadProblem()
	 */
	@SuppressWarnings("UseOfSystemOutOrSystemErr")
	public OSMXMLProblem(Path file, XMLConfig config, boolean loadImmediately) throws SAXException, IOException
	{
		this(file, config);

		if (loadImmediately)
			loadProblem();
	}

	/**
	 * Loads the problem from the file.
	 *
	 * @throws org.xml.sax.SAXException Error parsing XML.
	 * @throws java.io.IOException      Error reading the file.
	 */
	@Override
	@SuppressWarnings("UseOfSystemOutOrSystemErr")
	public final void loadProblem() throws SAXException, IOException
	{
		// it seems that there is an bug // hope that this comment is obsolete ;-)
		checkLoaded();
		MLCMP problem = null;
		try
		{
			DataLoader dl = new DataLoader(file, config);
			dl.enableMonitors(true);
			dl.setOutput(new PrintWriter(System.err));
			dl.phase1();
			dl.phase2();
			dl.phase3();
			dl.constructGraph();

			problem = dl.getProblem();

			lineToWaysIdsMap = dl.getLineToWaysIdsMap();
			stationToIdMap = dl.getStationToIdMap();
			trackToEdgeMap = new HashMap<>(problem.getEdgesNum());
			for (Track t : new IteratorWrapper<>(problem.trackIterator()))
				trackToEdgeMap.put(t,
					SetUtilities.newHashSet(
						new Edge(stationToIdMap.get(t.getVertex1()),
							stationToIdMap.get(t.getVertex2()))));
			equivallentStations = dl.getEquivallentStations();
		}
		finally
		{
			setMLCMP(problem);
		}
	}

	/**
	 * {@inheritDoc }
	 * @param solution {@inheritDoc }
	 * @return Map from edge to sorted list of line names.
	 */
	@Override
	public Map<Edge, String[]> processSolution(Solution solution)
	{
		final Ordering ord = solution.getOrdering();
		final ArrayList<String> lst = new ArrayList<>(lineToWaysIdsMap.size());
		final Map<Edge, String[]> retval = new HashMap<>(trackToEdgeMap.size());

		for (Entry<Track, Set<Edge>> e : trackToEdgeMap.entrySet())
		{
			lst.clear();
			final Track t = e.getKey();
			for (Line l : new IteratorWrapper<>(ord.finalizedOrderIterator(t, t.getVertex1())))
				lst.add(l.getName());
			final String[] arr = new String[lst.size()];
			lst.toArray(arr);

			for (Edge edg : e.getValue())
				retval.put(edg, arr);
		}

		return retval;
	}

	/**
	 * One line in MLCMP can be defined by many ways in the XML file. This returns
	 * mapping from line name to set of ids of given lines.
	 *
	 * @return Map from line name to set of way ids in the XML. Sometimes one line is
	 *         represented by more ways.
	 */
	public Map<String, Set<Integer>> getLineToWaysIdsMap()
	{
		return lineToWaysIdsMap;
	}

	/**
	 * If there are two points with same coordinate in the XML file, then we force
	 * them to represent the same crossing.
	 *
	 * @return List of stations that are considered to be same, because they share
	 *         their coordinates.
	 */
	public Collection<Set<Integer>> getEquivallentStations()
	{
		return equivallentStations;
	}

	/**
	 * Returns configuration that allows to load standard czech tourist trals.
	 * @return Tourist trails configuration.
	 */
	public static XMLConfig getCzechTouristTraislConfig()
	{
		return TouristTrailXMLConfig.getInstance();
	}

	/**
	 * Returns configuration that allows to load all trams in the Czech republic.
	 * @return Trams configuration.
	 */
	public static XMLConfig getCzechTramsConfig()
	{
		return TramXMLConfig.getInstance();
	}

	/**
	 * Adds all edges to the given list.
	 * @param target List to add to.
	 * @param source List to add.
	 * @param reversed Insert reversed edges instead.
	 */
	private static void addAll(Set<Edge> target, Set<Edge> source, boolean reversed)
	{
		if (reversed)
			for (Edge e : source)
				target.add(e.getReverse());
		else
			target.addAll(source);
	}

	@Override
	public GraphContractionListener getGraphContractionListener()
	{
		if (listener == null)
			listener = new GCL();
		return listener;
	}

	/**
	 * Simple edge defined by pair of IDs.
	 */
	public final class Edge
	{
		/**
		 * Source id.
		 */
		private final int sourceId;
		/**
		 * Target id.
		 */
		private final int targetId;

		/**
		 * Creates new edge.
		 * @param sourceId Source id.
		 * @param targetId Target id.
		 */
		private Edge(int sourceId, int targetId)
		{
			this.sourceId = sourceId;
			this.targetId = targetId;
		}

		/**
		 * Gets the source ID.
		 * @return The source ID.
		 */
		public int getSourceId()
		{
			return sourceId;
		}

		/**
		 * Gets the target ID.
		 * @return The target ID.
		 */
		public int getTargetId()
		{
			return targetId;
		}

		/**
		 * Returns an edge that goes in different direction.
		 * @return An edge with swapped endpoints.
		 */
		private Edge getReverse()
		{
			return new Edge(targetId, sourceId);
		}

		@Override
		public String toString()
		{
			return "Edge{" + "sourceId=" + sourceId + ", targetId=" + targetId + '}';
		}
	}

	/**
	 * Graph contraction listener.
	 */
	private class GCL implements GraphContractionListener
	{
		@Override
		public void edgeContracted(ContractionEvent evt)
		{
			final Track t1 = evt.getFirstTrack();
			final Track t2 = evt.getSecondTrack();
			final Track nt = evt.getNewTrack();
			final Station middle = evt.getRemovedStation();

			// we have to save the order
			final boolean firstReversed = t1.getVertex1().equals(middle);
			final boolean secondReversed = t2.getVertex2().equals(middle);

			final Set<Edge> firstTrackEdges = trackToEdgeMap.remove(t1);
			final Set<Edge> secondTrackEdges = trackToEdgeMap.remove(t2);

			final Set<Edge> newTrackEdges = new HashSet<>(firstTrackEdges.size() + secondTrackEdges.size());

			addAll(newTrackEdges, firstTrackEdges, firstReversed);
			addAll(newTrackEdges, secondTrackEdges, secondReversed);

			trackToEdgeMap.put(nt, newTrackEdges);
		}
	}
}
