/*
 * Copyright (C) 2013 Petr Ryšavý
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package monocle.api.impl;

import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import monocle.api.Problem;
import monocle.api.Solution;

/**
 * Simple problem that only wraps the MLCMP.
 *
 * @author Petr Ryšavý
 */
public class SimpleProblem implements Problem
{
	/**
	 * The problem to wrap.
	 */
	private final MLCMP problem;

	/**
	 * Creates new problem given the MLCMP.
	 * @param problem The MLCMP instance to wrap.
	 */
	public SimpleProblem(MLCMP problem)
	{
		if(problem == null)
			throw new NullPointerException("The MLMP cannot be null.");
		this.problem = problem;
	}

	@Override
	public MLCMP getMLCMP()
	{
		return problem;
	}

	/**
	 * Returns the ordering.
	 * @param solution {@inheritDoc }
	 * @return {@inheritDoc }
	 */
	@Override
	public Ordering processSolution(Solution solution)
	{
		return solution.getOrdering();
	}
}
