/*
 * Copyright (C) 2013 Petr Ryšavý
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package monocle.api.impl;

import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.ContractionEvent;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.GraphContractionListener;
import cz.cvut.fel.rysavpe1.utilities.ArrayUtilities;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import monocle.api.ContractableProblem;
import monocle.api.Solution;

/**
 * This class represents a problem that is stored in plain text file. For format of
 * the file see {@code PlainTextGraphReader}. This input can be read very quicky when
 * comparing to XML.
 *
 * @author Petr Ryšavý
 * @see PlainTextGraphReader
 * TODO fix JavaDocs
 */
public class PlainTextProblem extends AbstractProblem implements ContractableProblem
{
	/**
	 * TODO 
	 * Path to the file that contains the given graph.
	 */
	private Reader in;
	/**
	 * Map from track in contracted problem to tracks in original problem. Tracks can
	 * be reversed ad well.
	 */
	private Map<Track, Map<Track, Boolean>> trackMap;
	/**
	 * Graph contraction listener.
	 */
	private GCL listener;

	/**
	 * Creates new problem. Problem is not loaded.
	 *
	 * @param file The file that contains the graph.
	 */
	@SuppressWarnings("UseOfSystemOutOrSystemErr")
	public PlainTextProblem(Path file) throws IOException
	{
		this.in = Files.newBufferedReader(file, Charset.forName("utf-8"));
	}
	
	public PlainTextProblem(Reader in)
	{
		if(in == null)
			throw new NullPointerException("Input stream cannot be null.");
		
		this.in = in;
	}

	/**
	 * Creates new problem given the specification. Problem can be loaded.
	 *
	 * @param file            The file that contains the graph.
	 * @param loadImmediately When {@code true}, then {@code loadProblem()} method is
	 *                        called within constructor.
	 * @throws java.io.IOException   Error reading the file.
	 * @throws GraphReadingException If the file is not valid.
	 * @see #loadProblem()
	 */
	@SuppressWarnings("UseOfSystemOutOrSystemErr")
	public PlainTextProblem(Path file, boolean loadImmediately) throws IOException, GraphReadingException
	{
		this(file);

		if (loadImmediately)
			loadProblem();
	}

	/**
	 * Loads the problem from the file.
	 *
	 * @throws GraphReadingException If the file is not valid.
	 * @throws java.io.IOException   Error reading the file.
	 */
	@Override
	@SuppressWarnings("UseOfSystemOutOrSystemErr")
	public final void loadProblem() throws IOException, GraphReadingException
	{
		checkLoaded();
		MLCMP problem = null;
		try
		{
			problem = new PlainTextGraphReader(new PrintWriter(System.err)).readGraph(in);

			trackMap = new HashMap<>(problem.getEdgesNum());
			for (Track t : new IteratorWrapper<>(problem.trackIterator()))
				trackMap.put(t, newTrackMap(t));
		}
		finally
		{
			setMLCMP(problem);
		}
	}

	/**
	 * {@inheritDoc }
	 *
	 * @param solution {@inheritDoc }
	 * @return Map from edge to sorted list of line names.
	 */
	@Override
	public Map<Edge, String[]> processSolution(Solution solution)
	{
		final Ordering ord = solution.getOrdering();
		final ArrayList<String> lst = new ArrayList<>(getMLCMP().getLinesNum());
		final Map<Edge, String[]> retval = new HashMap<>(trackMap.size());
//		
//		System.err.println("tm size "+trackMap.size());
//		System.err.println("lines   "+getMLCMP().getEdgesNum());

		if (ord == null || !ord.isTotallyOrdered())
			return null;

		for (Entry<Track, Map<Track, Boolean>> e : trackMap.entrySet())
		{
			lst.clear();
			final Track t = e.getKey();
			for (Line l : new IteratorWrapper<>(ord.finalizedOrderIterator(t, t.getVertex1())))
				lst.add(l.getName());

			//TODO it is sometimes needles to have both copies. But if there is a big
			// number of tracks that were contracted (and it is random variable),
			// then we have a good chance that both arrays will be needed
			final String[] arr = new String[lst.size()];
			lst.toArray(arr);
			final String[] revArr = new String[lst.size()];
			ArrayUtilities.toReversedArray(lst, revArr);

			for (Entry<Track, Boolean> f : e.getValue().entrySet())
			{
				final boolean reversed = f.getValue();
				retval.put(new Edge(f.getKey(), reversed), reversed ? revArr : arr);
			}
		}
		return retval;
	}

	/**
	 * Adds all edges to the given list.
	 *
	 * @param target   List to add to.
	 * @param source   List to add.
	 * @param reversed Insert reversed edges instead.
	 */
	private static void addAll(Map<Track, Boolean> target, Map<Track, Boolean> source, boolean reversed)
	{
		if (reversed)
			for (Entry<Track, Boolean> e : source.entrySet())
				target.put(e.getKey(), !reversed);
		else
			for (Entry<Track, Boolean> e : source.entrySet())
				target.put(e.getKey(), reversed);
	}

	/**
	 * Creates new map of tracks.
	 *
	 * @param t The track to add.
	 * @return The map with the given track. This track is marked as reversed.
	 */
	private static Map<Track, Boolean> newTrackMap(Track t)
	{
		HashMap<Track, Boolean> map = new HashMap<>(1);
		map.put(t, Boolean.TRUE);
		return map;
	}

	@Override
	public GraphContractionListener getGraphContractionListener()
	{
		if (listener == null)
			listener = new GCL();
		return listener;
	}

	/**
	 * Simple edge defined by pair of Stations.
	 */
	public final class Edge
	{
		/**
		 * Source.
		 */
		private final Station source;
		/**
		 * Target.
		 */
		private final Station target;

		/**
		 * Creates new edge between given stations.
		 * @param source The source.
		 * @param target The target.
		 */
		public Edge(Station source, Station target)
		{
			this.source = source;
			this.target = target;
		}

		/**
		 * Creates new edge from given track.
		 * @param t The track.
		 * @param reversed Shall we preserve the order of vertices.
		 */
		private Edge(Track t, boolean reversed)
		{
			if (reversed)
			{
				source = t.getVertex2();
				target = t.getVertex1();
			}
			else
			{
				source = t.getVertex1();
				target = t.getVertex2();
			}
		}

		/**
		 * Returns the sources's name. If name is not provided, than generic value is used.
		 * @return The source vertex name.
		 */
		public String getSourceValue()
		{
			return source.getValue();
		}

		/**
		 * Gets the source X coordinate.
		 * @return X coordinate of source vertex.
		 */
		public double getSourceX()
		{
			return source.getX();
		}

	/**
		 * Gets the source Y coordinate.
		 * @return Y coordinate of source vertex.
		 */
		public double getSourceY()
		{
			return source.getY();
		}

		/**
		 * Returns the target's name. If name is not provided, than generic value is used.
		 * @return The target vertex name.
		 */
		public String getTargetValue()
		{
			return target.getValue();
		}

		/**
		 * Gets the target X coordinate.
		 * @return X coordinate of target vertex.
		 */
		public double getTargetX()
		{
			return target.getX();
		}

		/**
		 * Gets the target Y coordinate.
		 * @return Y coordinate of target vertex.
		 */
		public double getTargetY()
		{
			return target.getY();
		}

		@Override
		public String toString()
		{
			return "Edge{" + "source=" + source + ", target=" + target + '}';
		}
	}

	/**
	 * Graph contraction listener.
	 */
	private class GCL implements GraphContractionListener
	{
		@Override
		public void edgeContracted(ContractionEvent evt)
		{
			final Track t1 = evt.getFirstTrack();
			final Track t2 = evt.getSecondTrack();
			final Track nt = evt.getNewTrack();
			final Station middle = evt.getRemovedStation();

			// we have to save the order
			final boolean firstReversed = t1.getVertex1().equals(middle);
			final boolean secondReversed = t2.getVertex2().equals(middle);

			final Map<Track, Boolean> firstTrackEdges = trackMap.remove(t1);
			final Map<Track, Boolean> secondTrackEdges = trackMap.remove(t2);

			final Map<Track, Boolean> newTrackEdges = new HashMap<>(firstTrackEdges.size() + secondTrackEdges.size());

			addAll(newTrackEdges, firstTrackEdges, firstReversed);
			addAll(newTrackEdges, secondTrackEdges, secondReversed);

			trackMap.put(nt, newTrackEdges);
		}
	}
}
