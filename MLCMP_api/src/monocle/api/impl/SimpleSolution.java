/*
 * Copyright (C) 2013 Petr Ryšavý
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package monocle.api.impl;

import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.utilities.NotNullIterable;
import monocle.api.Solution;
import monocle.api.Solution;

/**
 * This class imeplements a {@code Solution} that only wraps {@code Ordering}.
 *
 * @author Petr Ryšavý
 */
public class SimpleSolution implements Solution
{
	/**
	 * The wrapped ordering.
	 */
	private final Ordering ordering;

	/**
	 * Creates new instance of class with the given ordering.
	 *
	 * @param ordering Ordering to wrap.
	 */
	public SimpleSolution(Ordering ordering)
	{
		this.ordering = ordering;
	}

	/**
	 * Creates new solution by concatenating the given orderings.
	 *
	 * @param orderingIterable Orderings to wrap. Those will be wrapped to single
	 *                         ordering instance. List can contain {@code null}
	 *                         values.
	 */
	public SimpleSolution(Iterable<LineOrdering> orderingIterable)
	{
		ordering = new LineOrdering(new NotNullIterable<>(orderingIterable));
	}

	@Override
	public Ordering getOrdering()
	{
		return ordering;
	}
}
