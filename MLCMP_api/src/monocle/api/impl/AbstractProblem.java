/*
 * Copyright (C) 2013 Petr Ryšavý
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package monocle.api.impl;

import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import monocle.api.Problem;
import monocle.api.Solution;

/**
 * Abstract implementation of problem. This class handles loading of the problem from
 * a file.
 *
 * @author Petr Ryšavý
 */
public abstract class AbstractProblem implements Problem
{
	/**
	 * Flag signalizing that someone has tried to load the problem.
	 */
	private boolean loaded = false;
	/**
	 * The wrapped problem. When {@code loaded} is {@code true} and this is
	 * {@code null}, then there was an error reading the problem.
	 */
	private MLCMP problem;

	/**
	 * {@inheritDoc }
	 *
	 * @throws IllegalStateException When problem was not loaded yet or there was a
	 *                               problem when loading the problem.
	 */
	@Override
	public MLCMP getMLCMP()
	{
		if (!loaded)
			throw new IllegalStateException("Load the problem first.");
		if (problem == null)
			throw new IllegalStateException("The problem was not loaded properly. You cannot use this problem.");
		return problem;
	}

	/**
	 * Checks whether the was an attempt to load the problem.
	 *
	 * @return {@code loaded} flag.
	 */
	protected boolean isLoaded()
	{
		return loaded;
	}

	/**
	 * Throws an exception when the problem was already loaded.
	 *
	 * @throws IllegalStateException When {@code loaded} is set.
	 */
	protected void checkLoaded()
	{
		if (isLoaded())
			throw new IllegalStateException("Problem was already loaded.");
	}

	/**
	 * Loads the problem using the properties given by the user. This method shall
	 * call {@code setMLCMP()} method.
	 *
	 * @throws Exception When there is an error when loading the problem.
	 * @see #setMLCMP(cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP)
	 */
	public abstract void loadProblem() throws Exception;

	/**
	 * Sets the problem value and {@code loaded} flag.
	 * @param problem Problem or {@code null} after exception.
	 */
	protected void setMLCMP(MLCMP problem)
	{
		this.problem = problem;
		loaded = true;
	}
}
