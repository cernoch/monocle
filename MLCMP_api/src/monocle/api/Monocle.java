/*
 * Copyright (C) 2013 Petr Ryšavý
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package monocle.api;

import cz.cvut.fel.rysavpe1.mlcmp.eval.MonocleRun;
import cz.cvut.fel.rysavpe1.mlcmp.eval.MonocleRun.Output;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import monocle.api.impl.SimpleSolution;

/**
 * This class an main implementation of Solver interface. Class Monocle allows to
 * solve an problem in following way.
 * <pre>
 * Monocle solver = Monocle.getInstance();
 *
 * // now we can set properties of the computation, for example
 * solver.setProperty("time.limit", "-1");
 *
 * // using ProblemFactory we can load the solution
 * OSMXMLProblem problem = new OSMXMLProblem(pathToOSMXML, OSMXMLProblem.getCzechTouristTraislConfig());
 *
 * // now solve the problem
 * Solution solution = Monocle.solve(problem);
 *
 * // and process solution to a mapping from its to order of trails
 * Map<OSMXMLProblem.Edge, String[]> map = problem.processSolution(s);
 * </pre>
 *
 * Solver can have several properties set. List of currently implemented is shown in
 * following table.
 * <table>
 * <tr> <td> {@code problem.split}
 * <td> Specifies whether a problem shall be divided into isolated components. This
 * option leads to faster computation, but sometimes it can lead to suboptimal
 * solution.
 *
 * <tr> <td> {@code time.limit}
 * <td> This is time limit that is applied to each individual greedy part and each
 * individual isolated component. This means that solver gives up solving given
 * isolated component after this time. {@code -1} stands for infinite time. Always
 * make sure that the greedy part is done within this time.
 *
 * <tr> <td> {@code threads.enforceStop}
 * <td> When computation consumes {@code time.limit} than it is needed to stop the
 * thread. In default this value is {@code false}. This meand that the solver can
 * process the current state and return the best solution seen so far. When
 * {@code true} is set, the solves's thread is stopped using {@code Thread.stop()}
 * method. Be cautios when setting this option, becuase {@code Thread.stop()} is
 * deprecated and may lead to unhandled exceptions.
 * </table>
 *
 * @author Petr Ryšavý
 */
public final class Monocle implements Solver
{
	/**
	 * The singleton instance.
	 */
	private static Monocle instance;
	/**
	 * The properties associated with the solver.
	 */
	private final Properties properties;
	/**
	 * Default properties.
	 */
	private final Properties defaults;

	/**
	 * Creates this singleton instance. Loads the default properties.
	 */
	private Monocle()
	{
		defaults = new Properties();
		try
		{
			defaults.load(Monocle.class.getResourceAsStream("resources/defaults.properties"));
		}
		catch (IOException ex)
		{
			System.err.println("Cannot load properties. The jar file seems to be corrupted or modified in unpermitted way.");
		}
		properties = new Properties(defaults);
	}

	/**
	 * Gets the instance of the {@code Monocle} solver.
	 *
	 * @return The only instance of this singleton.
	 */
	public static Monocle getInstance()
	{
		if (instance == null)
			instance = new Monocle();
		return instance;
	}

	/**
	 * Sets the given properties. This is equivallent to calling {@code putAll} on
	 * properties that are already set.F
	 *
	 * @param props The properties to set.
	 */
	public void setProperties(Properties props)
	{
		properties.putAll(props);
	}

	/**
	 * Sets individual property value. For list of properties see the class header.
	 *
	 * @param key Property key.
	 * @param value Property value.
	 */
	public void setProperty(String key, String value)
	{
		properties.setProperty(key, value);
	}

	/**
	 * Uses the best-effort solver to get the solution of given problem.
	 * 
	 * {@inheritDoc }
	 * @param input {@inheritDoc }
	 * @return {@inheritDoc }
	 */
	@Override
	public Solution solve(Problem input)
	{
		try
		{
			Iterable<Output> out = MonocleRun.run(input.getMLCMP(),
				Integer.parseInt(properties.getProperty("time.limit", "-1")),
				properties.getProperty("problem.split").equalsIgnoreCase("true"),
				properties.getProperty("threads.enforceStop").equalsIgnoreCase("true"),
				input instanceof ContractableProblem ? ((ContractableProblem) input).getGraphContractionListener() : null);
			System.err.println("The solution done.");
			ArrayList<LineOrdering> al = new ArrayList<>();
			for (Output o : out)
				al.add(o.getOrdering());
			System.err.println("al size " + al.size());
			System.err.println("al      " + al);
			Solution s = new SimpleSolution(al);
			return s;
		}
		catch (IOException | InterruptedException ex)
		{
			return null;
		}
	}
}
