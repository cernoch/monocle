/*
 * Copyright (C) 2013 Petr Ryšavý
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package monocle.api;

import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory.XMLConfig;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import java.io.IOException;
import java.nio.file.Path;
import monocle.api.impl.SimpleProblem;
import org.xml.sax.SAXException;

/**
 *
 * @author Petr Ryšavý
 */
public class ProblemFactory
{
	//singleton not needed now, but reduce api changes in future
	private static ProblemFactory instance;
	
	private ProblemFactory()
	{
		
	}
	
	public Problem createProblem(Path xmlFile, XMLConfig config) throws IOException, GraphReadingException, SAXException
	{
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
	public SimpleProblem createProblem(MLCMP mlcmp)
	{
		// TODO split problem if needed
		return new SimpleProblem(mlcmp);
	}
	
	public Problem createProblem(Path txtFile)
	{
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
	public static ProblemFactory getInstance()
	{
		if(instance == null)
			instance = new ProblemFactory();
		return instance;
	}
}
