/*
 * Copyright (C) 2013 Petr Ryšavý
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package monocle.api;

import cz.cvut.fel.rysavpe1.mlcmp.preprocess.GraphContractionListener;

/**
 * Problem that wants to be informed about edge contractions in the problem. When
 * graph needs to be contracted then the Problem class should implement this
 * interface.
 *
 * This may be useful as the solver ignores the original structure when graph is
 * contracted. The final solution then reflects this contracted structure. If it is
 * undesirable, than problem shall implement this interface to get information about
 * graph contractions.
 *
 * @author Petr Ryšavý
 */
public interface ContractableProblem extends Problem
{
	/**
	 * Returns the listener, that will handle graph contractions.
	 *
	 * @return Class that will be informed about graph contractions wihin solver.
	 */
	GraphContractionListener getGraphContractionListener();
}
