/*
 * Copyright (C) 2013 Petr Ryšavý
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package monocle.xxx;

import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.XMLTouristTrailGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.EdgeShortener;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.UnderlyingGraphSplitter;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;

/**
 *
 * @author Petr Ryšavý
 */
public class HKTest
{
	
	public HKTest()
	{
	}

	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void testSomeMethod() throws IOException, GraphReadingException, InterruptedException, SAXException
	{
		//Path p = Paths.get("C:", "Users", "Petr Ryšavý", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
//		Path p = Paths.get("C:", "Users", "Petr Rysavy", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
//		Path p = Paths.get("C:", "Users", "Petr Ryšavý", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
//		Path p = Paths.get("C:", "Users", "Petr Rysavy", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "trams.in");
//		Path p = Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "trams.in");
//		Path p = Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_order", "test", "badGraph.in");
		
//		Path pla = Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
		Path pla = Paths.get("C:", "Users", "Petr", "Documents", "BP", "touristTrails.in");
		Path xml = Paths.get("C:", "Users", "Petr", "Documents", "BP", "czech-republic-latest.osm");

//		PlainRun.run(new PlainTextGraphReader(new OutputStreamWriter(System.out)).readGraph(p), 10000, false);
		final MLCMP plain = new PlainTextGraphReader(new OutputStreamWriter(System.out)).readGraph(pla);
		final MLCMP osm = new XMLTouristTrailGraphReader(new OutputStreamWriter(System.out)).readGraph(xml);
		run(plain, 10000, true);
		run(osm, 10000, true);
	}
	
	public static Map<MLCMP, Ordering> run(MLCMP bigProblem, long timeLimit, boolean split) throws IOException, InterruptedException
	{
		EdgeShortener.contractTracks(bigProblem);
		MLCMP[] problems = split ? UnderlyingGraphSplitter.splitProblem(bigProblem) : new MLCMP[]
		{
			bigProblem
		};
		Map<MLCMP, Ordering> solsMap = new HashMap<>(problems.length);

		problemLoop:
			for (MLCMP problem : problems)
			{
				if(problem.getEdgesNum() == 50 && problem.getVerticesNum() == 44)
				{
					for(Station s : new IteratorWrapper<>(problem.stationIterator()))
						System.err.println(s);
					System.err.println("");
					System.err.println("");
					System.err.println("");
					for(Track t : new IteratorWrapper<>(problem.trackIterator()))
						System.err.println(t);
					System.err.println("");
					System.err.println("");
					System.err.println("");
					for(Line l : problem.getLines())
						System.err.println(l);
				}
			}
		return null;
	}
}
