/*
 * Copyright (C) 2013 Petr Ryšavý
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package monocle.api;

import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;
import monocle.api.impl.OSMXMLProblem;
import monocle.api.impl.OSMXMLProblem.Edge;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;

/**
 * Exaple how to use the {@code Monocle} solver when you have a OSM XML file.
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class XMLTest
{
	public XMLTest()
	{
	}

	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void testOnCZEPlain() throws IOException, GraphReadingException, SAXException
	{
		// Replace this with path to your OSM XML
		Path p = Paths.get("C:", "Users", "Petr", "Documents", "BP", "czech-republic-latest.osm");
//		Path p = Paths.get("C:", "Users", "Petr", "Documents", "BP", "map.osm");
		
		OSMXMLProblem problem = new OSMXMLProblem(p, OSMXMLProblem.getCzechTouristTraislConfig());
		problem.loadProblem();
		Solution s = Monocle.getInstance().solve(problem);
		System.err.println("The problem solved");
		Map<Edge, String[]> map = problem.processSolution(s);
		System.err.println("The solution processed");
		if (map != null)
		{
			System.err.println(map.size());
			Entry<Edge, String[]> e = map.entrySet().iterator().next();
			System.err.println("edge "+e.getKey());
			System.err.println("line "+Arrays.toString(e.getValue()));
		}
		else
			System.err.println("was not successfull within the given time");
	}
}