/*
 * Copyright (C) 2013 Petr Ryšavý
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package monocle.api;

import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import monocle.api.impl.PlainTextProblem;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Exaple how to use the {@code Monocle} solver.
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class MonocleTest
{
	public MonocleTest()
	{
	}

	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void testOnCZEPlain() throws IOException, GraphReadingException
	{
		// Replace this with a path to your graph file
		Path p = Paths.get("C:", "Users", "Petr Ryšavý", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
		//Path p = Paths.get("C:", "Users", "Petr Ryšavý", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
//		Path p = Paths.get("C:", "Users", "Petr Rysavy", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
//		Path p = Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
//		Path p = Paths.get("C:", "Users", "Petr Rysavy", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "trams.in");
//		Path p = Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "trams.in");
//		Path p = Paths.get("C:", "Users", "Petr Ryšavý", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "trams.in");
//		Path p = Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_order", "test", "badGraph.in");
		
//		Path p = Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
//		Path p = Paths.get("C:", "Users", "Petr", "Documents", "BP", "aTouristTrailsNC.in");

		PlainTextProblem problem = new PlainTextProblem(p);
		problem.loadProblem();
		Monocle.getInstance().setProperty("time.limit", Integer.toString(1000));
		Solution s = Monocle.getInstance().solve(problem);
		Map<PlainTextProblem.Edge, String[]> map = problem.processSolution(s);
		if (map != null)
			System.err.println(map.size());
		else
			System.err.println("was not successfull within the given time");
	}
}
