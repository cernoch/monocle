/*
 */

package cz.cvut.fel.rysavpe1.mlcmp.orderoption;

import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import java.util.Iterator;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class OneCrossingOrderOptionTest {

    public OneCrossingOrderOptionTest() {
    }

	@Test
	public void testSomeMethod()
	{
		OneCrossingOrderOption oo = new OneCrossingOrderOption(TrackLineOrder.FOLLOWS, 2, 5);
		Iterator<Integer> i = oo.iterator();
		assertEquals(TrackLineOrder.FOLLOWS, i.next().intValue());
		assertEquals(TrackLineOrder.FOLLOWS, i.next().intValue());
		assertEquals(TrackLineOrder.PRECEDES, i.next().intValue());
		assertEquals(TrackLineOrder.PRECEDES, i.next().intValue());
		assertEquals(TrackLineOrder.PRECEDES, i.next().intValue());
	}

}