/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.rysavpe1.mlcmp.ordermodel;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.UnderlyingGraph;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
public class UndoableOrderingTest
{
	public UndoableOrderingTest()
	{
	}

	@Test
	public void testSomeMethod()
	{
		Station a = new Station("a", 0, 0);
		Station b = new Station("b", 1, 1);
		Track t = new Track(a, b);
		LinkedList<Track> ll = new LinkedList<>();
		ll.add(t);
		Line l1 = new Line("l1", ll);
		Line l2 = new Line("l2", ll);
		UnderlyingGraph ugr = new UnderlyingGraph();
		ugr.addVertex(a);
		ugr.addVertex(b);
		ugr.addEdge(t);
		MLCMP problem = new MLCMP(ugr, new Line[]
		{
			l1, l2
		});
		LineOrdering ord = new LineOrdering(problem);
		ord.setOrderOfLines(l1, l2, 3, t, a);
		UndoableOrdering uo = new UndoableOrdering(ord);
		assertEquals(3, uo.getOrderOfLines(l1, l2, t, b));
		uo.newBreakpoint();
		assertEquals(3, uo.getOrderOfLines(l1, l2, t, b));
		uo.setOrderOfLines(l1, l2, 2, t, b);
		assertEquals(2, uo.getOrderOfLines(l1, l2, t, b));
		assertEquals(1, uo.getOrderOfLines(l1, l2, t, a));
		assertTrue(uo.isTotallyOrdered(t));
		uo.undo();
		assertEquals(3, uo.getOrderOfLines(l1, l2, t, b));
		assertEquals(3, ord.getOrderOfLines(l1, l2, t, b));
		assertFalse(uo.isTotallyOrdered(t));


		uo.newBreakpoint();
		assertEquals(3, uo.getOrderOfLines(l1, l2, t, b));
		uo.setOrderOfLines(l1, l2, 2, t, b);
		assertEquals(2, uo.getOrderOfLines(l1, l2, t, b));
		assertEquals(1, uo.getOrderOfLines(l1, l2, t, a));
		assertTrue(uo.isTotallyOrdered(t));
		Set<Track> s = new HashSet<Track>();
		s.add(t);
		uo.storeToRoot(uo.storeTrack(s));
		assertTrue(uo.isTotallyOrdered(t));
		assertEquals(2, uo.getOrderOfLines(l1, l2, t, b));
		assertEquals(1, uo.getOrderOfLines(l1, l2, t, a));
		assertEquals(2, ord.getOrderOfLines(l1, l2, t, b));
		assertEquals(1, ord.getOrderOfLines(l1, l2, t, a));
	}
	@Test
	public void test2()
	{
		Station a = new Station("a", 0, 0);
		Station b = new Station("b", 1, 1);
		Track t = new Track(a, b);
		LinkedList<Track> ll = new LinkedList<>();
		ll.add(t);
		Line l1 = new Line("l1", ll);
		Line l2 = new Line("l2", ll);
		UnderlyingGraph ugr = new UnderlyingGraph();
		ugr.addVertex(a);
		ugr.addVertex(b);
		ugr.addEdge(t);
		MLCMP problem = new MLCMP(ugr, new Line[]
		{
			l1, l2
		});
		LineOrdering ord = new LineOrdering(problem);
		UndoableOrdering uo = new UndoableOrdering(ord);
		ord.setOrderOfLines(l1, l2, 5, t, a);
		assertEquals(5, uo.getOrderOfLines(l1, l2, t, b));
		uo.newBreakpoint();
		assertEquals(5, uo.getOrderOfLines(l1, l2, t, b));
		uo.setOrderOfLines(l1, l2, 3, t, b);
		assertEquals(3, uo.getOrderOfLines(l1, l2, t, b));
		assertEquals(3, uo.getOrderOfLines(l1, l2, t, a));
		assertFalse(uo.isTotallyOrdered(t));
		uo.newBreakpoint();
		uo.setOrderOfLines(l1, l2, 2, t, b);
		assertEquals(2, uo.getOrderOfLines(l1, l2, t, b));
		assertEquals(1, uo.getOrderOfLines(l1, l2, t, a));
		assertTrue(uo.isTotallyOrdered(t));
		uo.undo();
		assertEquals(3, uo.getOrderOfLines(l1, l2, t, b));
		assertEquals(3, uo.getOrderOfLines(l1, l2, t, a));
		assertFalse(uo.isTotallyOrdered(t));
		uo.undo();		
		assertEquals(5, uo.getOrderOfLines(l1, l2, t, b));
		assertEquals(5, uo.getOrderOfLines(l1, l2, t, a));
	}
}