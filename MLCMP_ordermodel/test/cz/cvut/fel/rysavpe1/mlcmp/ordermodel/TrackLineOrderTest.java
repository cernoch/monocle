/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.ordermodel;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import java.util.Arrays;
import java.util.HashSet;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Petr Ryšavý
 */
public class TrackLineOrderTest
{
	static Line l1 = new Line("l1", null);
	static Line l2 = new Line("l2", null);
	static Line l3 = new Line("l3", null);
	static Line l4 = new Line("l4", null);
	static Line l5 = new Line("l5", null);
	static Line l6 = new Line("l6", null);
	static TrackLineOrder instance = null;

	public TrackLineOrderTest()
	{
	}

	@BeforeClass
	public static void setUpClass()
	{
		instance = new TrackLineOrder(new Line[]
		{
			l1, l2, l3, l4, l5, l6
		});
//		instance.setLineOrder(l5, l3, TrackLineOrder.FOLLOWS);
//		instance.setLineOrder(l1, l4, TrackLineOrder.PRECEDES);
//		instance.setLineOrder(l5, l6, TrackLineOrder.FOLLOWS);
//		instance.setLineOrder(l3, l6, TrackLineOrder.FOLLOWS);
//		instance.setLineOrder(l4, l2, TrackLineOrder.PRECEDES);
		instance.setLineOrder(l1, l5, TrackLineOrder.PRECEDES);
		instance.setLineOrder(l4, l5, TrackLineOrder.FOLLOWS);
		instance.setLineOrder(l3, l6, TrackLineOrder.FOLLOWS);
		instance.setLineOrder(l3, l1, TrackLineOrder.PRECEDES);
		instance.setLineOrder(l2, l4, TrackLineOrder.FOLLOWS);
	}

	@Test
	public void testSetLineOrder()
	{
	}

	@Test
	public void testGetLineOrder()
	{
	}

	@Test
	public void testIsIsOrderDefinite()
	{
	}

	@Test
	public void testCanBeOrderDefinite()
	{
		System.out.println("canBeOrderDefinite");
		boolean expResult = true;
		boolean result = instance.canBeOrderDefinite();
		assertEquals(expResult, result);
	}

	@Test
	public void testTryFinalizeOrder()
	{
		System.out.println("testTryFinalizeOrder");
		boolean expResult = true;
		boolean result = instance.tryFinalizeOrder();
		assertEquals(expResult, result);
	}

	@Test
	public void testGetFinalizedOrder()
	{
		System.out.println("testGetFinalizedOrder");
		Line[] expResult = new Line[]
		{
			l2, l4, l5, l1, l3, l6
		};
		// expected result must be from biggest
		//Line[] expResult = new Line[] {l6, l3, l1, l5, l4, l2};
		//System.out.println(Arrays.toString(expResult));
		instance.tryFinalizeOrder();
		Line[] result = instance.getFinalizedOrder();
		//System.out.println(Arrays.toString(result));
		for (int i = 0; i < 6; i++)
			assertSame(expResult[i], result[i]);
	}

	@Test
	public void testAssignAnOrder()
	{
//		instance = new TrackLineOrder(new Line[]{l1, l2, l3, l4, l5, l6});
		instance = new TrackLineOrder(new Line[]
		{
			l6, l3, l2, l4, l1, l5
		});
		instance.setLineOrder(l2, l4, TrackLineOrder.FOLLOWS);
		instance.setLineOrder(l2, l5, TrackLineOrder.PRECEDES);
		instance.setLineOrder(l3, l1, TrackLineOrder.PRECEDES);
		instance.setLineOrder(l6, l1, TrackLineOrder.FOLLOWS);
		//instance.setLineOrder(l3, l5, TrackLineOrder.FOLLOWS);
		instance.assignAnOrder();
		System.err.println("Assigned order " + Arrays.toString(instance.getFinalizedOrder()));
	}

	@Test
	public void testAssignRandomOrder()
	{
		for (int i = 0; i < 10; i++)
		{
//		instance = new TrackLineOrder(new Line[]{l1, l2, l3, l4, l5, l6});
			instance = new TrackLineOrder(new Line[]
			{
				l6, l3, l2, l4, l1, l5
			});
			instance.setLineOrder(l2, l4, TrackLineOrder.FOLLOWS);
			instance.setLineOrder(l2, l5, TrackLineOrder.PRECEDES);
			instance.setLineOrder(l3, l1, TrackLineOrder.PRECEDES);
			instance.setLineOrder(l6, l1, TrackLineOrder.FOLLOWS);
			//instance.setLineOrder(l3, l5, TrackLineOrder.FOLLOWS);
			instance.assignRandomOrder();
			System.err.println("Assigned random order " + Arrays.toString(instance.getFinalizedOrder()));
		}
	}

	@Test
	public void testGetPrecedingLines()
	{
	}

	@Test
	public void testGetFollowingLines()
	{
	}
}