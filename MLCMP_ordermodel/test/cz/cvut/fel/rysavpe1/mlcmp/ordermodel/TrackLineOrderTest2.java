/*
 */

package cz.cvut.fel.rysavpe1.mlcmp.ordermodel;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrderTest.instance;
import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class TrackLineOrderTest2 {
	static Line l1 = new Line("l1", null);
	static Line l2 = new Line("l2", null);
	static Line l3 = new Line("l3", null);
	static Line l4 = new Line("l4", null);
	static Line l5 = new Line("l5", null);
	static Line l6 = new Line("l6", null);
	static TrackLineOrder instance = null;

    public TrackLineOrderTest2() {
    }

		
	@Test
	public void testOrder()
	{
		instance = new TrackLineOrder(new Line[]{l1, l2, l3, l4});
		instance.setLineOrder(l1, l4, TrackLineOrder.FOLLOWS);
		instance.setLineOrder(l2, l3, TrackLineOrder.FOLLOWS);
		System.err.println("arr "+Arrays.deepToString(instance.getLineOrder()));
		
		instance.setLineOrder(l4, l2, TrackLineOrder.FOLLOWS);
		System.err.println("arr "+Arrays.deepToString(instance.getLineOrder()));
	}
}
