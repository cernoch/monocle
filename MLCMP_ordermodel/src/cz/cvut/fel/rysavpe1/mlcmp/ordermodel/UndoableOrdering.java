/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.ordermodel;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * The undoable ordering implementation. This ordering is a copy-on-write structure
 * that enables backtracking the order. After a breakpoint the order can be set. Then
 * if we want to backtrack, then we erase the breakpoint and all the values will be
 * erased.
 *
 * @author Petr Ryšavý
 */
public class UndoableOrdering extends AbstractOrdering
{
	/**
	 * The ordering that contains the common subpath preferred order.
	 */
	private final LineOrdering root;
	/**
	 * The copy-on-write map. When new layer is created then a new map is put on the
	 * top of the list. This map maps from tracks to the ordering on specified
	 * tracks.
	 */
	private final ArrayList<Map<Track, TrackLineOrder>> cowmap;
	/**
	 * Shortcut to the current map that is on the top.
	 */
	private Map<Track, TrackLineOrder> top;

	/**
	 * Creates new undoable ordering.
	 *
	 * @param root The ordering that contains the common subpath preferred order.
	 *             This ordering is not modified unless the metod
	 *             {@literal storeToRoot} is called.
	 */
	public UndoableOrdering(LineOrdering root)
	{
		this.root = root;
		cowmap = new ArrayList<>(32);
		newBreakpoint();
	}

	/**
	 * Creates new breakpoint. Nothing before this breakpoint will be modified (only
	 * hidden) unless the breakpoint is deleted.
	 */
	public final void newBreakpoint()
	{
		top = new HashMap<>(32);
		cowmap.add(top);
	}

	/**
	 * Reelases the breakpoint on the top of the copy-on-write structure. All the
	 * changed will be discarded. Now the changes will be written to the older layer.
	 *
	 * @throws IllegalStateException When reached the bottom of the structure, i. e.
	 *                               as no {@literal newBreakpoint} was called yet.
	 */
	public void undo()
	{
		if (cowmap.size() == 1)
			throw new IllegalStateException("No breakpoint to remove ...");

		cowmap.remove(cowmap.size() - 1);
		top = cowmap.get(cowmap.size() - 1);
	}

	/**
	 * Reelases all the breakpoints of the copy-on-write structure. All the changed
	 * will be discarded. Now the changes will be written to the initial layer.
	 */
	public void reset()
	{
		// no breakpoints to remove
		if (cowmap.size() == 1)
			return;

		Map<Track, TrackLineOrder> bottom = cowmap.get(0);
		cowmap.clear();
		cowmap.add(bottom);
		top = bottom;
	}

	/**
	 * Returns the depth of breakpoints. I.e. how many layers are there. The default
	 * layer is not counted. This is exactly the number of {@literal newBreakpoint}
	 * calls minus the number of {@literal undo} calls.
	 *
	 * @return The depth of the breakpoints.
	 */
	public int getBreakpointDepth()
	{
		return cowmap.size() - 1;
	}

	@Override
	public void setOrderOfLines(Line l1, Line l2, int order, Track t, Station s)
	{
		if (root.isTotallyOrdered(t))
			root.setOrderOfLines(l1, l2, order, t, s);
		else if (t.getVertex1().equals(s))
			getForWrite(t).setLineOrder(l1, l2, order);
		else if (t.getVertex2().equals(s))
			getForWrite(t).setLineOrder(l2, l1, order);
		else
			throw new IllegalArgumentException("The given station must be endpoint of track at which lines shall be ordered.");
	}

	@Override
	public void setOrderOfLinesIfPossible(Line l1, Line l2, int order, Track t, Station s)
	{
		if (root.isTotallyOrdered(t))
			root.setOrderOfLinesIfPossible(l1, l2, order, t, s);
		else if (t.getVertex1().equals(s))
			getForWrite(t).setLineOrderIfPossible(l1, l2, order);
		else if (t.getVertex2().equals(s))
			getForWrite(t).setLineOrderIfPossible(l2, l1, order);
		else
			throw new IllegalArgumentException("The given station must be endpoint of track at which lines shall be ordered.");
	}

	@Override
	public int getOrderOfLines(Line l1, Line l2, Track t, Station s)
	{
		if (!t.isIncidentWith(s))
			throw new IllegalArgumentException("The given station must be endpoint of track at which lines shall be ordered.");

		TrackLineOrder order = getForRead(t);
		if (order == null)
			return root.getOrderOfLines(l1, l2, t, s);

		if (t.getVertex1().equals(s))
			return order.getLineOrder(l1, l2);
		else//  if (t.getVertex2().equals(s)) ... we know that it holds true
			return order.getLineOrder(l2, l1);
	}

	@Override
	public void assignAnOrder(Track t)
	{
		if (root.isTotallyOrdered(t))
			return;
		getForWrite(t).assignAnOrder();
	}

	@Override
	public void swapLines(Track t, Line l1, Line l2)
	{
		getForWrite(t).swapLines(l1, l2);
	}

	@Override
	public void assignRandomOrder(Track t)
	{
		if (root.isTotallyOrdered(t))
			return;
		getForWrite(t).assignRandomOrder();
	}

	@Override
	public boolean isTotallyOrdered(Track t)
	{
		TrackLineOrder order = getForRead(t);
		if (order == null)
			return root.isTotallyOrdered(t);

		return order.isIsOrderDefinite() || order.tryFinalizeOrder();
	}

	@Override
	public boolean areLinesCompared(Line l1, Line l2, Track t)
	{
		TrackLineOrder order = getForRead(t);
		if (order == null)
			return root.areLinesCompared(l1, l2, t);
		return order.areLinesCompared(l1, l2);
	}

	@Override
	public Iterator<Line> finalizedOrderIterator(Track t, Station s)
	{
		TrackLineOrder order = getForRead(t);
		if (order == null)
			return root.finalizedOrderIterator(t, s);

		if (t.getVertex1().equals(s))
			return order.finalizedOrderIterator(true);
		else if (t.getVertex2().equals(s))
			return order.finalizedOrderIterator(false);
		else
			throw new IllegalArgumentException("The given station must be endpoint of track at which lines shall be ordered.");
	}

	@Override
	public boolean isOrderSetPossible(Line l1, Line l2, int order, Track t, Station s)
	{
		final TrackLineOrder forRead = getForRead(t);

		if (forRead == null)
			return root.isOrderSetPossible(l1, l2, order, t, s);

		if (t.getVertex1().equals(s))
			return forRead.isOrderSetPossible(l1, l2, order);
		else if (t.getVertex2().equals(s))
			return forRead.isOrderSetPossible(l1, l2, order);
		else
			throw new IllegalArgumentException("The given station must be endpoint of track at which lines shall be ordered.");
	}

	/**
	 * Saves the current order to a order holder. This can be used for storing the
	 * optima.
	 *
	 * @param s The set of tracks whose order shall be saved. All tracks from this
	 *          set must have final order.
	 * @return The structure that stores the order of tracks from {@literal s}.
	 */
	@SuppressWarnings("NestedAssignment")
	public OrderHolder storeTrack(Set<Track> s)
	{
		OrderHolder oh = new OrderHolder(s.size());
		TrackLineOrder order;
		for (Track t : s)
			if ((order = getForRead(t)) != null)
				oh.orderMap.put(t, order.getFinalizedOrder());
		return oh;
	}

	/**
	 * Adds the current order to a given order holder. This can be used for storing
	 * the optima.
	 *
	 * @param s  The set of tracks whose order shall be saved. All tracks from this
	 *           set must have final order.
	 * @param oh The holder to that the current order will be saved.
	 * @throws IllegalArgumentException When {@literal oh} contains order of any
	 *                                  track from {@literal s}.
	 */
	@SuppressWarnings("NestedAssignment")
	public void storeTracksTo(Set<Track> s, OrderHolder oh)
	{
		for (Track t : s)
			if (oh.orderMap.containsKey(t))
				throw new IllegalArgumentException("Cannot store track that is already stored.");

		TrackLineOrder order;
		for (Track t : s)
			if ((order = getForRead(t)) != null)
				oh.orderMap.put(t, order.getFinalizedOrder());
	}

	/**
	 * Writes the given order to the root {@literal LineOrdering}.
	 *
	 * @param oh The order that shall be written.
	 */
	public void storeToRoot(OrderHolder oh)
	{
		for (Entry<Track, Line[]> e : oh.orderMap.entrySet())
		{
			final Track t = e.getKey();
			for (Map<Track, TrackLineOrder> m : cowmap)
				m.remove(t);
			root.storeOrder(t, t.getVertex1(), e.getValue());
		}
	}

	/**
	 * Returns the track line order for read. Searchs it but nothing copies.
	 *
	 * @param t The track to search the order.
	 * @return The order of {@literal null} if the order was not found in the
	 *         copy-on-write structure.
	 */
	@SuppressWarnings("NestedAssignment")
	private TrackLineOrder getForRead(Track t)
	{
		TrackLineOrder target;
		for (int i = cowmap.size() - 1; i > -1; i--)
			if ((target = cowmap.get(i).get(t)) != null)
				return target;
		return null;
	}

	/**
	 * Returns the track line order for write. Search the order and if it is in the
	 * top layer then returns. Otherwise clones that order and writes it to the top
	 * layer.
	 *
	 * @param t The track to search the order.
	 * @return The order to that can be written.
	 */
	@SuppressWarnings("NestedAssignment")
	private TrackLineOrder getForWrite(Track t)
	{
		TrackLineOrder target = null;
		if (top.containsKey(t))
			return top.get(t);

		for (int i = cowmap.size() - 2; i > -1; i--)
			if ((target = cowmap.get(i).get(t)) != null)
				break;

		if (target == null)
			target = copyFromRoot(t);
		else if (!target.isIsOrderDefinite())
			try
			{
				target = target.clone();
			}
			catch (CloneNotSupportedException ex)
			{
				// exception will be thrown only when cloning definite target and
				// that is prevented by the if condition
				throw new RuntimeException("This exception shall never arise.");
			}

		top.put(t, target);
		return target;
	}

	/**
	 * Copies the order from the root.
	 *
	 * @param t Track to copy.
	 * @return The copy of the root order.
	 * @throws IllegalStateException When the order on {@literal t} is finalized i.e.
	 *                               it is not copyable.
	 */
	private TrackLineOrder copyFromRoot(Track t)
	{
		try
		{
			return root.cloneTrackOrder(t);
		}
		catch (CloneNotSupportedException ex)
		{
			throw new IllegalStateException(ex.getMessage());
		}
	}

	@Override
	public boolean pureCSPRequired()
	{
		return root.pureCSPRequired();
	}

	@Override
	protected Iterable<Track> tracksIterable()
	{
		return root.tracksIterable();
	}

	/**
	 * The wrapper to the finalized state. The state can be then copied to the root.
	 */
	public static class OrderHolder
	{
		/**
		 * Map from track to the finalized order.
		 */
		private HashMap<Track, Line[]> orderMap;

		/**
		 * Creates new instance.
		 *
		 * @param initialCapacity The capacity of the map.
		 */
		private OrderHolder(int initialCapacity)
		{
			orderMap = new HashMap<>(initialCapacity);
		}

		/**
		 * Creates new instance with no {@literal orderMap}.
		 */
		private OrderHolder()
		{
		}

		/**
		 * Concatenate two order wrappers.
		 *
		 * @param oh1 First order wrapper.
		 * @param oh2 Second oder wrapper.
		 * @return Their union.
		 * @throws IllegalArgumentException When the {@literal oh1} and
		 *                                  {@literal oh2} are not disjoint. I.e.
		 *                                  when they contain same track.
		 */
		public static OrderHolder concat(OrderHolder oh1, OrderHolder oh2)
		{
			HashMap<Track, Line[]> orderMap = new HashMap<>(oh1.orderMap.size() + oh2.orderMap.size());
			orderMap.putAll(oh1.orderMap);

			for (Entry<Track, Line[]> e : oh2.orderMap.entrySet())
				if (orderMap.put(e.getKey(), e.getValue()) != null)
					throw new IllegalArgumentException("The order holders must be disjoint for concating.");

			OrderHolder oh = new OrderHolder();
			oh.orderMap = orderMap;
			return oh;
		}

		/**
		 * Returns the number of stored tracks.
		 *
		 * @return The number of track for the final order is remembered.
		 */
		public int getTracksNum()
		{
			return orderMap.size();
		}
	}
}
