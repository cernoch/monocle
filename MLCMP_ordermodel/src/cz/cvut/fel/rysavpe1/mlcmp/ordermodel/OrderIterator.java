/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.ordermodel;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import java.util.Iterator;

/**
 * This iterator is used for iterating order of two lines on their common subpath.
 *
 * @author Petr Ryšavý
 */
public final class OrderIterator implements Iterator<Integer>
{
	/**
	 * First line.
	 */
	private final Line l1;
	/**
	 * Second line.
	 */
	private final Line l2;
	/**
	 * Iterator of tracks of common subpath.
	 */
	private final Iterator<Track> tracks;
	/**
	 * Iterator of corresponding stations.
	 */
	private final Iterator<Station> stations;
	/**
	 * Line ordering.
	 */
	private final Ordering lo;

	/**
	 * Creates new order iterator.
	 *
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param tracks   Tracks iterator of common subpath.
	 * @param stations Stations iterator of common subpath. If stations don't
	 *                 correspond to tracks than using this iterator will cause an
	 *                 exception. This exception will be thrown on calling iterator
	 *                 methods, not in the constructor.
	 * @param lo       Line ordering.
	 */
	public OrderIterator(Line l1, Line l2, Iterator<Track> tracks, Iterator<Station> stations, Ordering lo)
	{
		this.l1 = l1;
		this.l2 = l2;
		this.tracks = tracks;
		this.stations = stations;
		this.lo = lo;
	}

	/**
	 * Creates new order iterator.
	 *
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param tracks   Tracks iterable of common subpath.
	 * @param stations Stations iterable of common subpath. If stations don't
	 *                 correspond to tracks than using this iterator will cause an
	 *                 exception. This exception will be thrown on calling iterator
	 *                 methods, not in the constructor.
	 * @param lo       Line ordering.
	 */
	public OrderIterator(Line l1, Line l2, Iterable<Track> tracks, Iterable<Station> stations, Ordering lo)
	{
		this(l1, l2, tracks.iterator(), stations.iterator(), lo);
	}

	/**
	 * Creates new order iterator.
	 *
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param tracks   Tracks iterator of common subpath.
	 * @param stations Stations iterable of common subpath. If stations don't
	 *                 correspond to tracks than using this iterator will cause an
	 *                 exception. This exception will be thrown on calling iterator
	 *                 methods, not in the constructor.
	 * @param lo       Line ordering.
	 */
	public OrderIterator(Line l1, Line l2, Iterator<Track> tracks, Iterable<Station> stations, Ordering lo)
	{
		this(l1, l2, tracks, stations.iterator(), lo);
	}

	/**
	 * Creates new order iterator.
	 *
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param tracks   Tracks iterable of common subpath.
	 * @param stations Stations iterator of common subpath. If stations don't
	 *                 correspond to tracks than using this iterator will cause an
	 *                 exception. This exception will be thrown on calling iterator
	 *                 methods, not in the constructor.
	 * @param lo       Line ordering.
	 */
	public OrderIterator(Line l1, Line l2, Iterable<Track> tracks, Iterator<Station> stations, Ordering lo)
	{
		this(l1, l2, tracks.iterator(), stations, lo);
	}

	@Override
	public boolean hasNext()
	{
		return tracks.hasNext();
	}

	@Override
	public Integer next()
	{
		return lo.getOrderOfLines(l1, l2, tracks.next(), stations.next());
	}

	/**
	 * This method is not supported.
	 *
	 * @throws UnsupportedOperationException This exception is thrown everytime this
	 *                                       method is called.
	 */
	@Override
	public void remove()
	{
		throw new UnsupportedOperationException("Not supported operation.");
	}
}
