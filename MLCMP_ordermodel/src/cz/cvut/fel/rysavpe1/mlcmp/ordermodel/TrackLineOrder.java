/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.ordermodel;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalLineException;
import cz.cvut.fel.rysavpe1.utilities.ArrayIterable;
import cz.cvut.fel.rysavpe1.utilities.ArrayUtilities;
import cz.cvut.fel.rysavpe1.utilities.BackwardArrayIterable;
import cz.cvut.fel.rysavpe1.utilities.MathUtils;
import cz.cvut.fel.rysavpe1.utilities.RandomAccessSet;
import cz.cvut.fel.rysavpe1.utilities.ReferencedHashSet;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

/**
 * This class defines model for ordering lines on a track. It allows to save fact
 * that a line precedes another and so on. One line can another precede, follow, they
 * may be uncomparable, their relative position may not matter or it doesn't need to
 * already have been calculated.
 *
 * If first line precedes the second, then the second follows the first. Situation
 * when two lines are uncomparable arises when they have to cross each other on a
 * common subpath. If order of two lines doesn't matter, it means that we won't
 * create any unnecessary crossing if we set l1 &lt; l2 or l2 &lt; l1 on common
 * subpath of these two lines.
 *
 * The whole ordering is done anticlockwise. If one line l1 precedes line l2, then l1
 * is right of l2. On the opposite the finalized order is in clockwise direction.
 *
 * @author Petr Ryšavý
 */
public class TrackLineOrder implements Cloneable
{
	/**
	 * The lines that are placed on this track.
	 */
	private Line[] lines;
	/**
	 * The array that contains the relative order of lines. First index points to
	 * first line and the second to second line, but the index decremented by one.
	 * I.e. {@code lineOrder[l1][l2 - 1]} refers to relative order of {@code l1} and
	 * {@code l2}. If the {@code lineOrder} is set to {@code FOLLOWS}, then the
	 * {@code l1} precedes line {@code l2}. The values are one of {@code NOT_DETERMINED},
	 * {@code FOLLOWS}, {@code PRECEDES}, {@code NOT_COMPARABLE} and
	 * {@code NOT_MATTER}.
	 */
	private int[][] lineOrder;
	/**
	 * Signalizes that the order is definite. The order is definite if all lines are
	 * compared or it doesn't matter on their relative position.
	 */
	private boolean isOrderDefinite;
	/**
	 * Signalizes that the order of two lines wasn't already determined.
	 */
	public static final int NOT_DETERMINED = 0;
	/**
	 * Signalizes that a line follows another.
	 */
	public static final int FOLLOWS = 1;
	/**
	 * Signalizes that a line precedes another.
	 */
	public static final int PRECEDES = 2;
	/**
	 * Signalizes that lines cross each other on the common subpath.
	 */
	public static final int NOT_COMPARABLE = 3;
	/**
	 * Signalizes that it doesn't matter on relative order of lines.
	 */
	public static final int NOT_MATTER = 4;
	/**
	 * Signalizes that we are not able to compare the lines now at the moment. It
	 * means that we need further information that might be helpful.
	 */
	public static final int NOT_ENOUGH_INFORMATION = 5;

	/**
	 * Creates new instance of this class.
	 *
	 * @param lines Lines to compare. The array cannot be modified in future,
	 *              otherwise this class will be buggy. But when the ordering is
	 *              final, then the array is swapped and hence the original array can
	 *              be modified.
	 */
	protected TrackLineOrder(Line[] lines)
	{
		this.lines = new Line[lines.length];
		System.arraycopy(lines, 0, this.lines, 0, lines.length);

		if (lines.length == 1)
			isOrderDefinite = true;
		else
		{
			lineOrder = new int[lines.length - 1][lines.length - 1];
			isOrderDefinite = false;
		}
	}

	/**
	 * Creates new instance with the given options.
	 *
	 * @param lines           Lines to compare. The array cannot be modified in
	 *                        future, otherwise this class will be buggy. But when
	 *                        the ordering is final, then the array is swapped and
	 *                        hence the original array can be modified.
	 * @param lineOrder       The line order array. This array is of (n-1)*(n-1) size
	 *                        where n is length of {@code lines} array. This array
	 *                        may contain only constants defined in this class.
	 * @param isOrderDefinite Signalizes that the order is definite. The order is
	 *                        definite if all lines are compared or it doesn't matter
	 *                        on their relative position.
	 */
	private TrackLineOrder(Line[] lines, int[][] lineOrder, boolean isOrderDefinite)
	{
		this.lines = new Line[lines.length];
		System.arraycopy(lines, 0, this.lines, 0, lines.length);
		this.lineOrder = lineOrder;
		this.isOrderDefinite = isOrderDefinite;
	}

	/**
	 * Sets the relative order of two lines, if possible. If the order is definite
	 * than the value to set is checked if it is not in contradiction with the final
	 * order. If the order is not definite that the {@code setLineOrder} method is
	 * called.
	 *
	 * @param l1    First line.
	 * @param l2    Second line.
	 * @param order The order.
	 * @see #setLineOrder(cz.cvut.fel.rysavpe1.mlcmp.model.Line,
	 * cz.cvut.fel.rysavpe1.mlcmp.model.Line, int)
	 */
	public void setLineOrderIfPossible(Line l1, Line l2, int order)
	{
		checkOrderSet(order);

		if (isOrderDefinite)
		{
			// other values don't matter because they say it doesn't matter, you don't
			// know or they cross somewhere (but you can already know the order thanks
			// to another line and transitivity
			if (order != PRECEDES && order != FOLLOWS)
				return;

			// setted order is PRECEDES or FOLLOWS if the order is definite
			final int settedOrder = getLineOrder(l1, l2);
			if (settedOrder != order)
				throw new IllegalOrderException("Trying to set order " + order + " on definite track that is different from the current value " + getLineOrder(l1, l2) + ".");
		}
		// the order is not definite, hence set it
		else
			setLineOrder(l1, l2, order);
	}

	/**
	 * Decides whether an order can be set. This method does not write any values
	 * into the order and won't throw any error.
	 *
	 * @param l1    First line.
	 * @param l2    Second line.
	 * @param order The order.
	 * @see #setLineOrderIfPossible(cz.cvut.fel.rysavpe1.mlcmp.model.Line,
	 * cz.cvut.fel.rysavpe1.mlcmp.model.Line, int)
	 * @return {
	 * @true} if the method {@code setLineOrderIfPossible} won't cause any
	 * exceptions, {@code false} otherwise.
	 */
	public boolean isOrderSetPossible(Line l1, Line l2, int order)
	{
		if (!isOrderSetPossible(order))
			return false;
		if (l1 == l2)
			return false;

		if (isOrderDefinite)
		{
			if (order != PRECEDES && order != FOLLOWS)
				return true;

			// setted order is PRECEDES or FOLLOWS if the order is definite
			final int settedOrder = getLineOrder(l1, l2);
			if (settedOrder != order)
				return false;
		}
		// the order is not definite, hence try to look what happen if you set it
		else
		{
			int l1index = -1, l2index = -1;
			for (int i = 0; i < lines.length; i++)
				if (lines[i] == l1)
					l1index = i;
				else if (lines[i] == l2)
					l2index = i;

			if (l1index == -1 || l2index == -1)
				return false;

			if (l1index < l2index)
			{
				int lastOrder = lineOrder[l1index][l2index - 1];
				if (!isOrderOverwritePossible(lastOrder, order)
					|| checkOrderOverwrite(lastOrder, order)
					&& !isTransitivityPossible(l1index, l2index, order))
					return false;
			}
			else
			{
				int lastOrder = lineOrder[l2index][l1index - 1];
				switch (order)
				{
					case PRECEDES:
						if (!isOrderOverwritePossible(lastOrder, FOLLOWS))
							return false;
						order = FOLLOWS;
						break;
					case FOLLOWS:
						if (!isOrderOverwritePossible(lastOrder, PRECEDES))
							return false;
						order = PRECEDES;
						break;
					default:
						if (!isOrderOverwritePossible(lastOrder, order))
							return false;
				}
				if (checkOrderOverwrite(order, lastOrder) && !isTransitivityPossible(l2index, l1index, order))
					return false;
			}
		}
		return true;
	}

	/**
	 * Sets the relative order of two lines.
	 *
	 * Note that calling this method with inapropriate order may cause an error.
	 * There are some properties that will be checked. At first not all values cannot
	 * be overwritten and in ordering cannot be created cycle. For example trying to
	 * set {@code PRECEDS} instead of {@code PRECEDES} or {@code NOT_COMPARABLE} to
	 * {@code NOT_MATTER} will lead to exception.
	 *
	 * @param l1    First line.
	 * @param l2    Second line.
	 * @param order The order.
	 * @throws IllegalArgumentException If the {@code order} is not one of
	 *                                  {@code FOLLOWS}, {@code PRECEDES}, {@code NOT_COMPARABLE}
	 *                                  and {@code NOT_MATTER}.
	 * @throws IllegalLineException     When the line is not handled by this order.
	 * @throws IllegalStateException    When the order was already determined.
	 */
	@SuppressWarnings("NestedAssignment")
	public void setLineOrder(Line l1, Line l2, int order)
	{
		checkOrderSet(order);

		if (l1 == l2)
			throw new IllegalLineException("Lines to compare cannot be equal.");

		if (isOrderDefinite)
			throw new IllegalStateException("The line order is now definite.");

		int l1index = -1, l2index = -1;
		for (int i = 0; i < lines.length; i++)
			if (lines[i] == l1)
				l1index = i;
			else if (lines[i] == l2)
				l2index = i;

		if (l1index == -1 || l2index == -1)
			throw new IllegalLineException("The line is not part of this track.");

		if (l1index < l2index)
		{
			int lastOrder = lineOrder[l1index][l2index - 1];
			if (checkOrderOverwrite(lastOrder, order))
			{
				lineOrder[l1index][l2index - 1] = order;
				// do transitivity check works only if new order is FOLLOWS or PRECEDES
//				if (lastOrder != order)
				doTransitivityCheck(l1index, l2index);
			}
		}
		else
		{
			int lastOrder = lineOrder[l2index][l1index - 1];
			// !!!!!!!!!!!!!!! order se přepisuje !!!!!!!!!!!!!!!!!!!!!!!!!!!
			switch (order)
			{
				case PRECEDES:
//					lastOrder = lineOrder[l2index][l1index - 1];
					if (checkOrderOverwrite(lastOrder, FOLLOWS))
						lineOrder[l2index][l1index - 1] = order = FOLLOWS;
					break;
				case FOLLOWS:
//					lastOrder = lineOrder[l2index][l1index - 1];
					if (checkOrderOverwrite(lastOrder, PRECEDES))
						lineOrder[l2index][l1index - 1] = order = PRECEDES;
					break;
				default:
//					lastOrder = lineOrder[l2index][l1index - 1];
					if (checkOrderOverwrite(lastOrder, order))
						lineOrder[l2index][l1index - 1] = order;
					break;
			}
			if (lastOrder != order)
				doTransitivityCheck(l2index, l1index);
		}
	}

	/**
	 * Decides all possible facts that can be gained by using transitivity. This
	 * method shall be called as reaction on value change in order.
	 *
	 * <b> The {@code l1index} must be smaller than {@code l2index}. Otherwise the
	 * method is buggy. </b>
	 *
	 * @param l1index Smaller index of line.
	 * @param l2index Bigger index of line.
	 */
	private void doTransitivityCheck(int l1index, int l2index)
	{
		@SuppressWarnings("UnusedAssignment")
		int[] linesO = null;
		switch (lineOrder[l1index][l2index - 1])
		{
			// this means that line l1 precedes the line l2, ie l1 < l2
			case FOLLOWS:
				linesO = new int[lines.length];
				// all lines that precedes l1 precede also l2
				for (int i = 0; i < l1index; i++)
					if (lineOrder[i][l1index - 1] == FOLLOWS)
					{
						if (lineOrder[i][l2index - 1] == PRECEDES)
							throw new IllegalOrderException("The cycle is created in the order.");
						lineOrder[i][l2index - 1] = FOLLOWS;
						linesO[i] = FOLLOWS;
					}
				for (int i = l1index; i < l2index - 1; i++)
					if (lineOrder[l1index][i] == PRECEDES)
					{
						if (lineOrder[i + 1][l2index - 1] == PRECEDES)
							throw new IllegalOrderException("The cycle is created in the order.");
						lineOrder[i + 1][l2index - 1] = FOLLOWS;
						linesO[i + 1] = FOLLOWS;
					}
				for (int i = l2index; i < lineOrder.length; i++)
					if (lineOrder[l1index][i] == PRECEDES)
					{
						if (lineOrder[l2index][i] == FOLLOWS)
							throw new IllegalOrderException("The cycle is created in the order.");
						lineOrder[l2index][i] = PRECEDES;
						linesO[i + 1] = FOLLOWS;
					}

				// all lines that follow l2 also follow the l1
				for (int i = 0; i < l1index; i++)
					if (lineOrder[i][l2index - 1] == PRECEDES)
					{
						if (lineOrder[i][l1index - 1] == FOLLOWS)
							throw new IllegalOrderException("The cycle is created in the order.");
						lineOrder[i][l1index - 1] = PRECEDES;
						linesO[i] = PRECEDES;
					}
				for (int i = l1index + 1; i < l2index; i++)
					if (lineOrder[i][l2index - 1] == PRECEDES)
					{
						if (lineOrder[l1index][i - 1] == PRECEDES)
							throw new IllegalOrderException("The cycle is created in the order.");
						lineOrder[l1index][i - 1] = FOLLOWS;
						linesO[i] = PRECEDES;
					}
				for (int i = l2index; i < lineOrder.length; i++)
					if (lineOrder[l2index][i] == FOLLOWS)
					{
						if (lineOrder[l1index][i] == PRECEDES)
							throw new IllegalOrderException("The cycle is created in the order.");
						lineOrder[l1index][i] = FOLLOWS;
						linesO[i + 1] = PRECEDES;
					}

				// and finally all lines that precede l1 also precede all lines that follow l2
				// TODO the speed is |L|^2, but it can be done faster - for example two hashsets
				for (int i = 0; i < lines.length; i++)
					if (linesO[i] == FOLLOWS)
						for (int j = 0; j < lines.length; j++)
							if (linesO[j] == PRECEDES)
								writeFollows(i, j);
				break;
			case PRECEDES:
				linesO = new int[lines.length];
				for (int i = 0; i < l1index; i++)
					if (lineOrder[i][l1index - 1] == PRECEDES)
					{
						if (lineOrder[i][l2index - 1] == FOLLOWS)
							throw new IllegalOrderException("The cycle is created in the order.");
						lineOrder[i][l2index - 1] = PRECEDES;
						linesO[i] = PRECEDES;
					}
				for (int i = l1index; i < l2index - 1; i++)
					if (lineOrder[l1index][i] == FOLLOWS)
					{
						if (lineOrder[i + 1][l2index - 1] == FOLLOWS)
							throw new IllegalOrderException("The cycle is created in the order.");
						lineOrder[i + 1][l2index - 1] = PRECEDES;
						linesO[i + 1] = PRECEDES;
					}
				for (int i = l2index; i < lineOrder.length; i++)
					if (lineOrder[l1index][i] == FOLLOWS)
					{
						if (lineOrder[l2index][i] == PRECEDES)
							throw new IllegalOrderException("The cycle is created in the order.");
						lineOrder[l2index][i] = FOLLOWS;
						linesO[i + 1] = PRECEDES;
					}

				for (int i = 0; i < l1index; i++)
					if (lineOrder[i][l2index - 1] == FOLLOWS)
					{
						if (lineOrder[i][l1index - 1] == PRECEDES)
							throw new IllegalOrderException("The cycle is created in the order.");
						lineOrder[i][l1index - 1] = FOLLOWS;
						linesO[i] = FOLLOWS;
					}
				for (int i = l1index + 1; i < l2index; i++)
					if (lineOrder[i][l2index - 1] == FOLLOWS)
					{
						if (lineOrder[l1index][i - 1] == FOLLOWS)
							throw new IllegalOrderException("The cycle is created in the order.");
						lineOrder[l1index][i - 1] = PRECEDES;
						linesO[i] = FOLLOWS;
					}
				for (int i = l2index; i < lineOrder.length; i++)
					if (lineOrder[l2index][i] == PRECEDES)
					{
						if (lineOrder[l1index][i] == FOLLOWS)
							throw new IllegalOrderException("The cycle is created in the order.");
						lineOrder[l1index][i] = PRECEDES;
						linesO[i + 1] = FOLLOWS;
					}

				// and finally all lines that precede l1 also precede all lines that follow l2
				// TODO the speed is |L|^2, but it can be done faster - for example two hashsets
				for (int i = 0; i < lines.length; i++)
					if (linesO[i] == FOLLOWS)
						for (int j = 0; j < lines.length; j++)
							if (linesO[j] == PRECEDES)
								writeFollows(i, j);
				break;
		}
	}

	/**
	 * Decides all possible facts that can be gained by using transitivity. This
	 * method only simulates all writes and decides whether they would lead to an
	 * error.
	 *
	 * <b> The {@code l1index} must be smaller than {@code l2index}. Otherwise the
	 * method is buggy. </b>
	 *
	 * @param l1index Smaller index of line.
	 * @param l2index Bigger index of line.
	 */
	private boolean isTransitivityPossible(int l1index, int l2index, int order)
	{
		@SuppressWarnings("UnusedAssignment")
		int[] linesO = null;
		switch (order)
		{
			// this means that line l1 precedes the line l2, ie l1 < l2
			case FOLLOWS:
				linesO = new int[lines.length];
				// all lines that precedes l1 precede also l2
				for (int i = 0; i < l1index; i++)
					if (lineOrder[i][l1index - 1] == FOLLOWS)
					{
						if (lineOrder[i][l2index - 1] == PRECEDES)
							return false;
						linesO[i] = FOLLOWS;
					}
				for (int i = l1index; i < l2index - 1; i++)
					if (lineOrder[l1index][i] == PRECEDES)
					{
						if (lineOrder[i + 1][l2index - 1] == PRECEDES)
							return false;
						linesO[i + 1] = FOLLOWS;
					}
				for (int i = l2index; i < lineOrder.length; i++)
					if (lineOrder[l1index][i] == PRECEDES)
					{
						if (lineOrder[l2index][i] == FOLLOWS)
							return false;
						linesO[i + 1] = FOLLOWS;
					}

				// all lines that follow l2 also follow the l1
				for (int i = 0; i < l1index; i++)
					if (lineOrder[i][l2index - 1] == PRECEDES)
					{
						if (lineOrder[i][l1index - 1] == FOLLOWS)
							return false;
						linesO[i] = PRECEDES;
					}
				for (int i = l1index + 1; i < l2index; i++)
					if (lineOrder[i][l2index - 1] == PRECEDES)
					{
						if (lineOrder[l1index][i - 1] == PRECEDES)
							return false;
						linesO[i] = PRECEDES;
					}
				for (int i = l2index; i < lineOrder.length; i++)
					if (lineOrder[l2index][i] == FOLLOWS)
					{
						if (lineOrder[l1index][i] == PRECEDES)
							return false;
						linesO[i + 1] = PRECEDES;
					}

				for (int i = 0; i < lines.length; i++)
					if (linesO[i] == FOLLOWS)
						for (int j = 0; j < lines.length; j++)
							if (linesO[j] == PRECEDES)
								if (!isWriteFollowsPossible(i, j))
									return false;
				break;
			case PRECEDES:
				linesO = new int[lines.length];
				for (int i = 0; i < l1index; i++)
					if (lineOrder[i][l1index - 1] == PRECEDES)
					{
						if (lineOrder[i][l2index - 1] == FOLLOWS)
							return false;
						linesO[i] = PRECEDES;
					}
				for (int i = l1index; i < l2index - 1; i++)
					if (lineOrder[l1index][i] == FOLLOWS)
					{
						if (lineOrder[i + 1][l2index - 1] == FOLLOWS)
							return false;
						linesO[i + 1] = PRECEDES;
					}
				for (int i = l2index; i < lineOrder.length; i++)
					if (lineOrder[l1index][i] == FOLLOWS)
					{
						if (lineOrder[l2index][i] == PRECEDES)
							return false;
						linesO[i + 1] = PRECEDES;
					}

				for (int i = 0; i < l1index; i++)
					if (lineOrder[i][l2index - 1] == FOLLOWS)
					{
						if (lineOrder[i][l1index - 1] == PRECEDES)
							return false;
						linesO[i] = FOLLOWS;
					}
				for (int i = l1index + 1; i < l2index; i++)
					if (lineOrder[i][l2index - 1] == FOLLOWS)
					{
						if (lineOrder[l1index][i - 1] == FOLLOWS)
							return false;
						linesO[i] = FOLLOWS;
					}
				for (int i = l2index; i < lineOrder.length; i++)
					if (lineOrder[l2index][i] == PRECEDES)
					{
						if (lineOrder[l1index][i] == FOLLOWS)
							return false;
						linesO[i + 1] = FOLLOWS;
					}

				for (int i = 0; i < lines.length; i++)
					if (linesO[i] == FOLLOWS)
						for (int j = 0; j < lines.length; j++)
							if (linesO[j] == PRECEDES)
								if (!isWriteFollowsPossible(i, j))
									return false;
				break;
		}
		return true;
	}

	/**
	 * Writes that line with index {@code l1} follows line with index {@code l2}.
	 *
	 * @param l1 First (bigger) line.
	 * @param l2 Second (smaller) line.
	 */
	private void writeFollows(int l1, int l2)
	{
		if (l2 < l1)
			writePrecedes(l2, l1);
		else
		{
			if (lineOrder[l1][l2 - 1] == PRECEDES)
				throw new IllegalOrderException("The cycle is created in the order.");

			lineOrder[l1][l2 - 1] = FOLLOWS;
		}
	}

	/**
	 * Writes that line with index {@code l1} precedes line with index {@code l2}.
	 *
	 * @param l1 First (smaller) line.
	 * @param l2 Second (bigger) line.
	 */
	private void writePrecedes(int l1, int l2)
	{
		if (l2 < l1)
			writeFollows(l2, l1);
		else
		{
			if (lineOrder[l1][l2 - 1] == FOLLOWS)
				throw new IllegalOrderException("The cycle is created in the order.");

			lineOrder[l1][l2 - 1] = PRECEDES;
		}
	}

	private boolean isWriteFollowsPossible(int l1, int l2)
	{
		if (l2 < l1)
			return isWritePrecedesPossible(l2, l1);
		else if (lineOrder[l1][l2 - 1] == PRECEDES)
			return false;
		return true;
	}

	private boolean isWritePrecedesPossible(int l1, int l2)
	{
		if (l2 < l1)
			return isWriteFollowsPossible(l2, l1);
		else if (lineOrder[l1][l2 - 1] == FOLLOWS)
			return false;
		return true;
	}

	/**
	 * Decides the relative order of two lines.
	 *
	 * @param l1 First line.
	 * @param l2 Second line.
	 * @return Their relative order.
	 * @throws IllegalLineException When the line is not handled by this order.
	 */
	public int getLineOrder(Line l1, Line l2)
	{
		if (l1 == l2)
			throw new IllegalLineException("Lines to compare cannot be equal.");

		int l1index = -1, l2index = -1;
		for (int i = 0; i < lines.length; i++)
			if (lines[i] == l1)
				l1index = i;
			else if (lines[i] == l2)
				l2index = i;

		if (l1index == -1 || l2index == -1)
			throw new IllegalLineException("The line is not part of this track.");

		if (isOrderDefinite)
		{
			if (l1index < l2index)
				return FOLLOWS;
			else
				return PRECEDES;
		}
		else
		{
			if (l1index < l2index)
				return lineOrder[l1index][l2index - 1];
			else
				switch (lineOrder[l2index][l1index - 1])
				{
					case PRECEDES:
						return FOLLOWS;
					case FOLLOWS:
						return PRECEDES;
					default:
						return lineOrder[l2index][l1index - 1];
				}
		}
	}

	/**
	 * Decides whether two lines have order value set as {@code FOLLOWS} or
	 * {@code PRECEDES}. This is true always when the order is definite.
	 *
	 * @param l1 First line.
	 * @param l2 Second line.
	 * @return {@code true} if the order of lines is known in this order.
	 */
	public boolean areLinesCompared(Line l1, Line l2)
	{
		if (l1 == l2)
			throw new IllegalLineException("Lines to compare cannot be equal.");

		int l1index = -1, l2index = -1;
		for (int i = 0; i < lines.length; i++)
			if (lines[i] == l1)
				l1index = i;
			else if (lines[i] == l2)
				l2index = i;

		if (l1index == -1 || l2index == -1)
			throw new IllegalLineException("The line is not part of this track.");

		if (isOrderDefinite)
			return true;
		else
		{
			int order;

			if (l1index < l2index)
				order = lineOrder[l1index][l2index - 1];
			else
				order = lineOrder[l2index][l1index - 1];

			return order == FOLLOWS || order == PRECEDES;
		}
	}

	/**
	 * Decides whether the order is definite. If the order is definite, no further
	 * modifications can't be done.
	 *
	 * @return True if whole line order is known.
	 */
	public boolean isIsOrderDefinite()
	{
		return isOrderDefinite;
	}

	/**
	 * Decides whether the line order can be supposed as definite. Line order can be
	 * supposed to be definite iff all pairs of lines are given order or if it
	 * doesn't matter on the order.
	 *
	 * @return If the line order can be definite.
	 */
	public boolean canBeOrderDefinite()
	{
		if (isOrderDefinite)
			return true;

		for (int i = 0; i < lineOrder.length; i++)
			for (int j = i; j < lineOrder.length; j++)
				if (lineOrder[i][j] != FOLLOWS && lineOrder[i][j] != PRECEDES)
					return false;
		return true;
	}

	/**
	 * Checks whether the order is complete. The order is complete when all lines are
	 * compared, i.e. holds that one preceses or follows second. If the order is
	 * complete, then the lines are sorted in decreasing order of &lt; relation.
	 * Hence the lines are sorted clockwise. The order is then locked and no further
	 * modifications can be done.
	 *
	 * @return Is the order specified for all pairs of lines.
	 */
	public boolean tryFinalizeOrder()
	{
		if (isOrderDefinite)
			return true;

		for (int i = 0; i < lineOrder.length; i++)
			for (int j = i; j < lineOrder.length; j++)
				if (lineOrder[i][j] != FOLLOWS && lineOrder[i][j] != PRECEDES)
					return false;

		// now for each line count the number of preceding lines
		// we already know that the order is definite
		final int[] precCount = new int[lines.length];
		for (int i = 0; i < lines.length; i++)
			precCount[i] = countFollowingLines(i);
		// we are sorting from the biggest, i.e. from the line with 0 lines following it
		// as the values are 1-l, we can sort the lines in linear time using a buffer
		final Line[] sortedLines = new Line[lines.length];
		for (int i = 0; i < lines.length; i++)
			sortedLines[precCount[i]] = lines[i];

		// and swap the references and free the space
		isOrderDefinite = true; // this must be after calling count following lines
		lines = sortedLines;
		lineOrder = null;
		return true;
	}

	/**
	 * Gets the final order of the lines. If the final order hasn't been decided yet
	 * then this method tries to finalize the order using the
	 * {@code tryFinalizeOrder()} method. If it is not possible then an exception is
	 * thrown.
	 *
	 * @return The final order of the lines.
	 * @throws IllegalStateException If there is not enough information to decide if
	 *                               the order can be finalized.
	 */
	public Line[] getFinalizedOrder()
	{
		if (!isOrderDefinite && !tryFinalizeOrder())
			throw new IllegalStateException("The order is not definite.");

		return Arrays.copyOf(lines, lines.length);
	}

	/**
	 * Returns an iterator of finalized order.
	 *
	 * @param forward The direction. {@code true} for clockwise direction,
	 *                {@code false} for anticlockwise.
	 * @return The iterator of tinalized order.
	 * @throws IllegalStateException If the order is not finalized.
	 * @see #tryFinalizeOrder()
	 */
	public Iterator<Line> finalizedOrderIterator(boolean forward)
	{
		if (!isOrderDefinite && !tryFinalizeOrder())
			throw new IllegalStateException("The order is not definite.");

		if (forward)
			return new ArrayIterable<>(lines).iterator();
		else
			return new BackwardArrayIterable<>(lines).iterator();
	}

	/**
	 * Gets the set of lines that surely follow the given line.
	 *
	 * @param l The line to search.
	 * @return The set of lines that follow {@code l}.
	 */
	public HashSet<Line> getFollowingLines(Line l)
	{
		int i = getLineIndex(l);
		return getFollowingLines(i);
	}

	/**
	 * Gets the set of lines that surely follow the given line.
	 *
	 * @param index The index of line to search.
	 * @return The set of lines that follow the line with index {@code index}.
	 */
	private HashSet<Line> getFollowingLines(int index)
	{
		HashSet<Line> precedingLines = new HashSet<>();

		if (isOrderDefinite)
			for (int i = 0; i < index; i++)
				precedingLines.add(lines[i]);
		else
		{
			for (int i = 0; i < index; i++)
				if (lineOrder[i][index - 1] == FOLLOWS)
					precedingLines.add(lines[i]);
			for (int i = index; i < lineOrder.length; i++)
				if (lineOrder[index][i] == PRECEDES)
					precedingLines.add(lines[i + 1]);
		}

		return precedingLines;
	}

	/**
	 * Gets the set of indices of lines that surely follow the given line.
	 *
	 * @param index The index of line to search.
	 * @return The set of indices of lines that follow the line with index
	 *         {@code index}.
	 */
	private HashSet<Integer> getFollowingLinesIndices(int index)
	{
		HashSet<Integer> precedingLines = new HashSet<>();

		if (isOrderDefinite)
			for (int i = 0; i < index; i++)
				precedingLines.add(i);
		else
		{
			for (int i = 0; i < index; i++)
				if (lineOrder[i][index - 1] == FOLLOWS)
					precedingLines.add(i);
			for (int i = index; i < lineOrder.length; i++)
				if (lineOrder[index][i] == PRECEDES)
					precedingLines.add(i + 1);
		}

		return precedingLines;
	}

	/**
	 * Gets the count of lines that surely follow the given line.
	 *
	 * @param index The index of line to search.
	 * @return The count of lines that follow the line with index {@code index}.
	 */
	private int countFollowingLines(int index)
	{
		int count = 0;

		if (isOrderDefinite)
			return index - 1;

		for (int i = 0; i < index; i++)
			if (lineOrder[i][index - 1] == FOLLOWS)
				count++;
		for (int i = index; i < lineOrder.length; i++)
			if (lineOrder[index][i] == PRECEDES)
				count++;

		return count;
	}

	/**
	 * Gets the set of lines that surely precede the given line.
	 *
	 * @param l The line to search.
	 * @return The set of lines that precede {@code l}.
	 */
	public HashSet<Line> getPrecedingLines(Line l)
	{
		int i = getLineIndex(l);
		return getPrecedingLines(i);
	}

	/**
	 * Gets the set of lines that surely precede the given line.
	 *
	 * @param index The index of line to search.
	 * @return The set of lines that precede the line with index {@code index}.
	 */
	private HashSet<Line> getPrecedingLines(int index)
	{
		HashSet<Line> followingLines = new HashSet<>();

		if (isOrderDefinite)
			for (int i = index + 1; i < lines.length; i++)
				followingLines.add(lines[i]);
		else
		{
			for (int i = 0; i < index; i++)
				if (lineOrder[i][index - 1] == PRECEDES)
					followingLines.add(lines[i]);
			for (int i = index; i < lineOrder.length; i++)
				if (lineOrder[index][i] == FOLLOWS)
					followingLines.add(lines[i + 1]);
		}

		return followingLines;
	}

	/**
	 * Gets the set of indices of lines that surely precede the given line.
	 *
	 * @param index The index of line to search.
	 * @return The set of indices of lines that precede the line with index
	 *         {@code index}.
	 */
	private HashSet<Integer> getPrecedingLinesIndices(int index)
	{
		HashSet<Integer> followingLines = new HashSet<>();

		if (isOrderDefinite)
			for (int i = index + 1; i < lines.length; i++)
				followingLines.add(i);
		else
		{
			for (int i = 0; i < index; i++)
				if (lineOrder[i][index - 1] == PRECEDES)
					followingLines.add(i);
			for (int i = index; i < lineOrder.length; i++)
				if (lineOrder[index][i] == FOLLOWS)
					followingLines.add(i + 1);
		}

		return followingLines;
	}

	/**
	 * Gets the count of lines that surely precede the given line.
	 *
	 * @param index The index of line to search.
	 * @return The count of lines that precede the line with index {@code index}.
	 */
	private int countPrecedingLines(int index)
	{
		int count = 0;

		if (isOrderDefinite)
			return lines.length - index;

		for (int i = 0; i < index; i++)
			if (lineOrder[i][index - 1] == PRECEDES)
				count++;
		for (int i = index; i < lineOrder.length; i++)
			if (lineOrder[index][i] == FOLLOWS)
				count++;

		return count;
	}

	/**
	 * Returns the line index in array of lines.
	 *
	 * @param l Line to search.
	 * @return The line index.
	 * @throws IllegalLineException If the line is not part of the lines array.
	 */
	private int getLineIndex(Line l) throws IllegalLineException
	{
		int i;
		for (i = 0; i < lines.length; i++)
			if (lines[i] == l)
				break;
		if (i == lines.length)
			throw new IllegalLineException("The line is not part of this track.");
		return i;
	}

	/**
	 * Checks whether the order to be set is known.
	 *
	 * @param order The order argument.
	 * @throws IllegalArgumentException If order is not one of constants defined in
	 *                                  this class.
	 */
	private void checkOrderSet(int order) throws IllegalArgumentException
	{
		if (order <= NOT_DETERMINED || order > NOT_ENOUGH_INFORMATION)
			throw new IllegalArgumentException("Unknown line order: " + order);
	}

	/**
	 * Checks whether the order to be set is known.
	 *
	 * @param order The order argument.
	 * @return {@code false} if order is not one of constants defined in this class.
	 */
	private boolean isOrderSetPossible(int order)
	{
		if (order <= NOT_DETERMINED || order > NOT_ENOUGH_INFORMATION)
			return false;
		return true;
	}

	/**
	 * Returns copy of array with line order.
	 *
	 * @return Array with compare information of lines.
	 */
	protected int[][] getLineOrder()
	{
		return ArrayUtilities.deepCopyOf(lineOrder);
	}

	/**
	 * Checks whether the order shall be overwritten. If the overwritte is not
	 * possible - for example if it will lead to contradiction, than an exception is
	 * thrown.
	 *
	 * @param oldOrder Old order.
	 * @param newOrder New order.
	 * @return {@code true} if the value shall ve overwritten, {@code false} if not.
	 *         That signalizes possible loss of information or no change.
	 * @throws IllegalArgumentException If the order is not admissible.
	 */
	private static boolean checkOrderOverwrite(int oldOrder, int newOrder) throws IllegalArgumentException
	{
		// true for overwrite, false - loosing information
		// not determined and not enough information can be overwritten anytime
		if (oldOrder == NOT_DETERMINED || oldOrder == NOT_ENOUGH_INFORMATION)
			return true;
		// if the old order and new order are same, there it is nothing to check
		// but don't do pointless operations
		if (oldOrder == newOrder)
			return false;
		// oldOrder is FOLLOWS, PRECEDES, NOT_COMPARABLE or NOT_MATTER

		// FOLLOWS and PRECEDES cannot be overwritten
		if (oldOrder == FOLLOWS || oldOrder == PRECEDES)
			// setting different value, that's bug
			if (newOrder == FOLLOWS || newOrder == PRECEDES)
				throw new IllegalOrderException("The order is not admissible.");
			// setting not_enough_info, not_matter or not_comparable is not bug,
			// but we will losse information, so don't do that
			else
				return false;
		// oldOrder is NOT_COMPARABLE or NOT_MATER - the only possible new values
		// are FOLLOWS or PRECEDES
		if (newOrder != FOLLOWS && newOrder != PRECEDES)
			throw new IllegalOrderException("The order is not admissible.");

		// anything else will be ok if it is overwritten
		return true;
	}

	/**
	 * Checks whether the order can be overwritten. If the overwritte is not possible
	 * - for example if it will lead to contradiction, than the returned value is
	 * {@code false}. This method behaves in the same way as
	 * {@code checkOrderOverwrite} method, but returns {@code false} if an error
	 * would be thrown, {@code true} otherwise.
	 *
	 * @param oldOrder Old order.
	 * @param newOrder New order.
	 * @return {@code true} if the value could ve overwritten, {@code false} if not.
	 */
	private static boolean isOrderOverwritePossible(int oldOrder, int newOrder) throws IllegalArgumentException
	{
		if (oldOrder == NOT_DETERMINED || oldOrder == NOT_ENOUGH_INFORMATION)
			return true;
		if (oldOrder == newOrder)
			return true;

		if (oldOrder == FOLLOWS || oldOrder == PRECEDES)
			if (newOrder == FOLLOWS || newOrder == PRECEDES)
				return false;
			else
				return true;
		if (newOrder != FOLLOWS && newOrder != PRECEDES)
			return false;
		return true;
	}

	public void assignAnOrder()
	{
		if (tryFinalizeOrder())
			return;

		// what we need to do is only to find an topological ordering
		HashMap<Line, Set<Line>> relation = new HashMap<>(lines.length);
		for (int i = 0; i < lines.length; i++)
			relation.put(lines[i], getFollowingLines(i));

		// when there is zero lines that follow current line, than this line can be first
		for (int i = 0; i < lines.length; i++)
			for (Entry<Line, Set<Line>> e : relation.entrySet())
				if (e.getValue().isEmpty())
				{
					lines[i] = e.getKey();
					relation.remove(lines[i]);
					for (Set<Line> s : relation.values())
						s.remove(lines[i]);
					break;
				}

		// and swap the references and free the space
		isOrderDefinite = true; // this must be after calling count following lines
		lineOrder = null;
	}

	public void assignRandomOrder()
	{
		if (tryFinalizeOrder())
			return;

		// what we need to do is only to find an topological ordering
		HashMap<Line, Set<Line>> relation = new HashMap<>(lines.length);
		for (int i = 0; i < lines.length; i++)
			relation.put(lines[i], getFollowingLines(i));

		RandomAccessSet<Line> sinkVertices = new RandomAccessSet<>(lines.length);
		// when there is zero lines that follow current line, than this line can be first
		for (int i = 0; i < lines.length; i++)
		{
			sinkVertices.clear();

			for (Entry<Line, Set<Line>> e : relation.entrySet())
				if (e.getValue().isEmpty())
					sinkVertices.add(e.getKey());
			
			Line l = sinkVertices.get(MathUtils.random(sinkVertices.size()));
			lines[i] = l;
			relation.remove(l);
			for (Set<Line> s : relation.values())
				s.remove(l);
		}

		// and swap the references and free the space
		isOrderDefinite = true; // this must be after calling count following lines
		lineOrder = null;
	}

	public void swapLines(Line l1, Line l2)
	{
		if (l1 == l2)
			return;

		int l1index = -1;
		int l2index = -1;
		//int c = 0;
		for (int i = 0; i < lines.length; i++)
			if (lines[i] == l1)
				l1index = i;
			else if (lines[i] == l2)
				l2index = i;

		if (l1index == -1 || l2index == -1)
			throw new IllegalLineException("The line is not part of this track.");

		lines[l1index] = l2;
		lines[l2index] = l1;
	}

	public static boolean isOrderPrecedesOrFollows(int order)
	{
		return order == PRECEDES || order == FOLLOWS;
	}

	/**
	 * Returns a dummy clone. Dummy clone compares same set of lines, but all pair of
	 * lines don't have enough information.
	 *
	 * @return A dummy clone.
	 */
	protected TrackLineOrder dummyClone()
	{
		TrackLineOrder tlo = new TrackLineOrder(lines);
		final int stop = lines.length - 1;
		if (!tlo.isOrderDefinite)
			for (int i = 0; i < stop; i++)
				for (int j = i; j < stop; j++)
					tlo.lineOrder[i][j] = NOT_ENOUGH_INFORMATION;
		return tlo;
	}

	/**
	 * Clones not finalized order.
	 *
	 * @return Clone of this order that is independent.
	 * @throws CloneNotSupportedException When the order is finalized.
	 */
	@Override
	protected TrackLineOrder clone() throws CloneNotSupportedException
	{
		if (isIsOrderDefinite())
			throw new CloneNotSupportedException("Definite TrackLineOrder should not be cloned.");

		Line[] newLines = new Line[lines.length];
		System.arraycopy(lines, 0, newLines, 0, lines.length);
		final int linesOrderLength = lines.length - 1;
		int[][] newLineOrder = new int[linesOrderLength][linesOrderLength];
		for (int i = 0; i < linesOrderLength; i++)
			System.arraycopy(lineOrder[i], 0, newLineOrder[i], 0, linesOrderLength);
		return new TrackLineOrder(newLines, newLineOrder, isOrderDefinite);

	}
}
