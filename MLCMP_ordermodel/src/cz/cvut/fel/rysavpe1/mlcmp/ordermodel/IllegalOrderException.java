/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.ordermodel;

/**
 * Exception that is thrown when there is something bad with the order.
 *
 * @author Petr Ryšavý
 */
public class IllegalOrderException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates new exception.
	 */
	public IllegalOrderException()
	{
	}

	/**
	 * Creates new exception with the given message.
	 *
	 * @param msg The message.
	 */
	public IllegalOrderException(String msg)
	{
		super(msg);
	}
}
