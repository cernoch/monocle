/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.ordermodel;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.ListUtilities;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * This class defines basic implementation of ordering of lines. It provides
 * implementation of some methods of {@literal Ordering} interface.
 *
 * @author Petr Ryšavý
 */
public abstract class AbstractOrdering implements Ordering
{
	/**
	 * Sets order of two lines two lines on each of the given tracks.
	 *
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param order    Their order.
	 * @param tracks   Set of tracks where the lines shall be ordered.
	 * @param stations Stations that are incident to given tracks respectively and
	 *                 that specify the endpoint of each ordering that shall be used.
	 */
	public void setOrderOfLines(Line l1, Line l2, int order, Track[] tracks, Station[] stations)
	{
		for (int i = 0; i < tracks.length; i++)
			setOrderOfLines(l1, l2, order, tracks[i], stations[i]);
	}

	@Override
	public void setOrderOfLines(Line l1, Line l2, int order, Iterable<Track> tracks, Iterable<Station> stations)
	{
		Iterator<Station> sit = stations.iterator();
		for (Iterator<Track> it = tracks.iterator(); it.hasNext();)
			setOrderOfLines(l1, l2, order, it.next(), sit.next());
	}

	@Override
	public void setOrderOfLines(Line l1, Line l2, Iterable<Integer> order, Iterable<Track> tracks, Iterable<Station> stations)
	{
		Iterator<Station> sit = stations.iterator();
		Iterator<Integer> iit = order.iterator();
		for (Iterator<Track> it = tracks.iterator(); it.hasNext();)
			setOrderOfLines(l1, l2, iit.next(), it.next(), sit.next());
	}

	@Override
	public void setOrderOfLinesIfPossible(Line l1, Line l2, int order, Iterable<Track> tracks, Iterable<Station> stations)
	{
		Iterator<Station> sit = stations.iterator();
		for (Iterator<Track> it = tracks.iterator(); it.hasNext();)
			setOrderOfLinesIfPossible(l1, l2, order, it.next(), sit.next());
	}

	@Override
	public void setOrderOfLinesIfPossible(Line l1, Line l2, Iterable<Integer> order, Iterable<Track> tracks, Iterable<Station> stations)
	{
		Iterator<Station> sit = stations.iterator();
		Iterator<Integer> iit = order.iterator();
		for (Iterator<Track> it = tracks.iterator(); it.hasNext();)
			setOrderOfLinesIfPossible(l1, l2, iit.next(), it.next(), sit.next());
	}

	@Override
	public boolean isOrderSetPossible(Line l1, Line l2, Iterable<Integer> order, Iterable<Track> tracks, Iterable<Station> stations)
	{
		Iterator<Station> sit = stations.iterator();
		Iterator<Integer> iit = order.iterator();
		for (Iterator<Track> it = tracks.iterator(); it.hasNext();)
			if (!isOrderSetPossible(l1, l2, iit.next(), it.next(), sit.next()))
				return false;
		return true;
	}

	@Override
	public boolean precedes(Line l1, Line l2, Track t, Station s)
	{
		return getOrderOfLines(l1, l2, t, s) == TrackLineOrder.FOLLOWS;
	}

	@Override
	public boolean follows(Line l1, Line l2, Track t, Station s)
	{
		return getOrderOfLines(l1, l2, t, s) == TrackLineOrder.PRECEDES;
	}

	@Override
	public Iterator<Line> finalizedOrderIterator(Station s)
	{
		return new StationOrderIterator(s);
	}

	@Override
	public Line[] getFinalizedOrder(Station s)
	{
		final List<Line> lst = ListUtilities.toList(finalizedOrderIterator(s));
		final Line[] arr = new Line[lst.size()];
		return lst.toArray(arr);
	}
	
	@Override
	public boolean isTotallyOrdered(Station s)
	{
		for (Track t : new IteratorWrapper<>(s.incidenTracksIterator()))
			if (!isTotallyOrdered(t))
				return false;
		return true;
	}

	@Override
	public void assignAnOrder(Iterable<Track> set)
	{
		for (Track t : set)
			assignAnOrder(t);
	}

	@Override
	public void assignRandomOrder(Iterable<Track> set)
	{
		for (Track t : set)
			assignRandomOrder(t);
	}

	@Override
	public void storeOrder(Track t, Station s, Line[] order)
	{
		for (int i = 1; i < order.length; i++)
			setOrderOfLines(order[i - 1], order[i], TrackLineOrder.FOLLOWS, t, s);
	}

	@Override
	public void storeOrder(Track t, Station s, Iterable<Line> order)
	{
		Iterator<Line> it = order.iterator();
		if (!it.hasNext())
			return;

		Line last = it.next();
		Line current;
		while (it.hasNext())
		{
			current = it.next();
			setOrderOfLines(last, current, TrackLineOrder.FOLLOWS, t, s);
		}
	}
	
	@Override
	public boolean isTotallyOrdered()
	{
		return isTotallyOrdered(tracksIterable());
	}

	@Override
	public boolean isTotallyOrdered(Iterable<Track> it)
	{
		for(Track t : it)
			if(!isTotallyOrdered(t))
				return false;
		return true;
	}
	
	protected abstract Iterable<Track> tracksIterable();

	/**
	 * Iterator of the finalized order.
	 */
	private class StationOrderIterator implements Iterator<Line>
	{
		/**
		 * The station that we iterate.
		 */
		private final Station s;
		/**
		 * Iterator of incident tracks to station {@literal s}.
		 */
		private final Iterator<Track> trackIt;
		/**
		 * Iterator of finalized order on the current tracks.
		 */
		private Iterator<Line> current;

		/**
		 * Creates new iterator.
		 *
		 * @param s The station on that we iterate.
		 */
		public StationOrderIterator(Station s)
		{
			this.s = s;
			trackIt = s.incidenTracksIterator(true);
			if (trackIt.hasNext())
				current = finalizedOrderIterator(trackIt.next(), s);
			else
				current = Collections.emptyIterator();

			checkCurrentIterator();
		}

		@Override
		public boolean hasNext()
		{
			return current.hasNext();
		}

		@Override
		public Line next()
		{
			final Line retval = current.next();
			checkCurrentIterator();
			return retval;
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("Remove operation no supported.");
		}

		/**
		 * Checks the current iterator and if its empty, uses the next.
		 */
		private void checkCurrentIterator()
		{
			if (current.hasNext() || !trackIt.hasNext())
				return;

			current = finalizedOrderIterator(trackIt.next(), s);
			checkCurrentIterator();
		}
	}
}
