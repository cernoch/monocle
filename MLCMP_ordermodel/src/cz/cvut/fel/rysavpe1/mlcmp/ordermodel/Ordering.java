/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.ordermodel;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import java.util.Iterator;
import java.util.Set;

/**
 * Interface that defines the ordering of lines on the problem.
 *
 * @author Petr Ryšavý
 */
public interface Ordering
{
	/**
	 * Sets order of two lines on each of the given tracks.
	 *
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param order    Their order.
	 * @param tracks   The iterable of tracks where the lines shall be ordered.
	 * @param stations Stations that are incident to given tracks respectively and
	 *                 that specify the endpoint of each ordering that shall be used.
	 */
	public void setOrderOfLines(Line l1, Line l2, int order, Iterable<Track> tracks, Iterable<Station> stations);

	/**
	 * Sets order of two lines on each of the given tracks. The order is given for
	 * each track individually.
	 *
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param order    Iterator of the order - subsequent values for each track.
	 * @param tracks   The iterable of tracks where the lines shall be ordered.
	 * @param stations Stations that are incident to given tracks respectively and
	 *                 that specify the endpoint of each ordering that shall be used.
	 */
	public void setOrderOfLines(Line l1, Line l2, Iterable<Integer> order, Iterable<Track> tracks, Iterable<Station> stations);

	/**
	 * Sets order of two lines on each of the given tracks. This method have same
	 * behaviour as {@literal setOrderOfLines} with one difference. It checks whether
	 * the order can be setted and if not than it only compares the order with the
	 * new value.
	 *
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param order    Their order.
	 * @param tracks   The iterable of tracks where the lines shall be ordered.
	 * @param stations Stations that are incident to given tracks respectively and
	 *                 that specify the endpoint of each ordering that shall be used.
	 */
	public void setOrderOfLinesIfPossible(Line l1, Line l2, int order, Iterable<Track> tracks, Iterable<Station> stations);

	/**
	 * Sets order of two lines on each of the given tracks. The order is given for
	 * each track individually. This method have same behaviour as
	 * {@literal setOrderOfLines} with one difference. It checks whether the order can
	 * be setted and if not than it only compares the order with the new value.
	 *
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param order    Iterator of the order - subsequent values for each track.
	 * @param tracks   The iterable of tracks where the lines shall be ordered.
	 * @param stations Stations that are incident to given tracks respectively and
	 *                 that specify the endpoint of each ordering that shall be used.
	 */
	public void setOrderOfLinesIfPossible(Line l1, Line l2, Iterable<Integer> order, Iterable<Track> tracks, Iterable<Station> stations);

	/**
	 * Sets order of two lines two lines on the given track.
	 *
	 * @param l1    First line.
	 * @param l2    Second line.
	 * @param order Their order.
	 * @param t     The track where the order will be set.
	 * @param s     The endpoint of track that specifies the direction of the edge
	 *              that shall be used for ordering.
	 * @see
	 * cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder#setLineOrder(cz.cvut.fel.rysavpe1.mlcmp.model.Line,
	 * cz.cvut.fel.rysavpe1.mlcmp.model.Line, int)
	 */
	public void setOrderOfLines(Line l1, Line l2, int order, Track t, Station s);

	/**
	 * Sets order of two lines two lines on the given track. This method have same
	 * behaviour as {@literal setOrderOfLines} with one difference. It checks whether
	 * the order can be setted and if not than it only compares the order with the
	 * new value.
	 *
	 * @param l1    First line.
	 * @param l2    Second line.
	 * @param order Their order.
	 * @param t     The track where the order will be set.
	 * @param s     The endpoint of track that specifies the direction of the edge
	 *              that shall be used for ordering.
	 * @see
	 * cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder#setLineOrder(cz.cvut.fel.rysavpe1.mlcmp.model.Line,
	 * cz.cvut.fel.rysavpe1.mlcmp.model.Line, int)
	 */
	public void setOrderOfLinesIfPossible(Line l1, Line l2, int order, Track t, Station s);

	/**
	 * Returns the specified order of two lines on given track.
	 *
	 * @param l1 First line.
	 * @param l2 Second line.
	 * @param t  The common track of {@literal l1} and {@literal l2} where the order shall
	 *           be given.
	 * @param s  The endpoint of track that specifies the direction of the edge that
	 *           shall be used for ordering.
	 * @return The order of lines.
	 * @see
	 * cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder#getLineOrder(cz.cvut.fel.rysavpe1.mlcmp.model.Line,
	 * cz.cvut.fel.rysavpe1.mlcmp.model.Line)
	 */
	public int getOrderOfLines(Line l1, Line l2, Track t, Station s);

	/**
	 * Determines whether the track had benn assigned total ordering yet. If the
	 * order is final, then the order is locked.
	 *
	 * @param t The track.
	 * @return Are lines on the track ordered?
	 */
	public boolean isTotallyOrdered(Track t);

	/**
	 * Decides whether a station is totally ordered. A station is totally ordered iff
	 * all of incident tracks are totally ordered.
	 *
	 * @param s The station.
	 * @return {@literal true} if all tracks incident to {@literal s} are totally ordered.
	 */
	public boolean isTotallyOrdered(Station s);

	/**
	 * Decides whether two lines have order value set as {@literal PRECEDES} or
	 * {@literal FOLLOWS}. This is true always when the order is definite.
	 *
	 * @param l1 First line.
	 * @param l2 Second line.
	 * @param t  The common track of {@literal l1} and {@literal l2} where the order shall
	 *           be given.
	 * @return {@literal true} if the order of lines is known on the given track
	 *         {@literal t}.
	 */
	public boolean areLinesCompared(Line l1, Line l2, Track t);

	/**
	 * Decides whether one line precedes another on given track.
	 *
	 * @param l1 First line.
	 * @param l2 Second line.
	 * @param t  The common track of {@literal l1} and {@literal l2} where the order shall
	 *           be given.
	 * @param s  The endpoint of track that specifies the direction of the edge that
	 *           shall be used for ordering.
	 * @return {@literal true} if the value returned by {@literal getOrderOfLines()} is
	 *         {@literal TrackLineOrder.PRECEDES}, {@literal false} otherwise.
	 * @see
	 * cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder#getLineOrder(cz.cvut.fel.rysavpe1.mlcmp.model.Line,
	 * cz.cvut.fel.rysavpe1.mlcmp.model.Line)
	 * @see TrackLineOrder#PRECEDES
	 */
	public boolean precedes(Line l1, Line l2, Track t, Station s);

	/**
	 * Decides whether one line follows another on given track.
	 *
	 * @param l1 First line.
	 * @param l2 Second line.
	 * @param t  The common track of {@literal l1} and {@literal l2} where the order shall
	 *           be given.
	 * @param s  The endpoint of track that specifies the direction of the edge that
	 *           shall be used for ordering.
	 * @return {@literal true} if the value returned by {@literal getOrderOfLines()} is
	 *         {@literal TrackLineOrder.FOLLOWS}, {@literal false} otherwise.
	 * @see
	 * cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder#getLineOrder(cz.cvut.fel.rysavpe1.mlcmp.model.Line,
	 * cz.cvut.fel.rysavpe1.mlcmp.model.Line)
	 * @see TrackLineOrder#FOLLOWS
	 */
	public boolean follows(Line l1, Line l2, Track t, Station s);

	/**
	 * Returns an iterator of finalized order on the given track looking from given
	 * station. The finalized order is in clockwise direction. The order on track
	 * {@literal t} must be definite.
	 *
	 * @param t The track on that the order shall be given.
	 * @param s The endpoint on that we want to know the order.
	 * @return Iterator of the finalized order.
	 */
	public Iterator<Line> finalizedOrderIterator(Track t, Station s);

	/**
	 * Returns an iterator of finalized order on all tracks incident to given
	 * station. The finalized order is in clockwise direction starting from
	 * south-to-north direction. The order on all tracks incident to {@literal s} must
	 * be definite.
	 *
	 * @param s The station where we want to know the order.
	 * @return Iterator of the finaliyed order.
	 */
	public Iterator<Line> finalizedOrderIterator(Station s);

	/**
	 * Returns the finalized order on all tracks incident to given station. The
	 * finalized order is in clockwise direction starting from south-to-north
	 * direction. The order on all tracks incident to {@literal s} must be definite.
	 *
	 * @param s The station where we want to know the order.
	 * @return The array with the given order.
	 */
	public Line[] getFinalizedOrder(Station s);

	public boolean isOrderSetPossible(Line l1, Line l2, Iterable<Integer> order, Iterable<Track> tracks, Iterable<Station> stations);

	public boolean isOrderSetPossible(Line l1, Line l2, int order, Track t, Station s);

	public void assignAnOrder(Track t);

	public void assignRandomOrder(Track t);

	public void assignAnOrder(Iterable<Track> set);

	public void assignRandomOrder(Iterable<Track> set);

	public void storeOrder(Track t, Station s, Line[] order);

	public void storeOrder(Track t, Station s, Iterable<Line> order);

	public void swapLines(Track t, Line l1, Line l2);

	/**
	 * Checks whether the order is definite on all tracks of the given set.
	 *
	 * @return {@literal true} if the order is definite on all tracks of the given
	 *         iterator, {@literal false} if there is at least one track where the order
	 *         is not definite.
	 */
	public boolean isTotallyOrdered(Iterable<Track> it);

	/**
	 * Checks whether the order is definite on all tracks.
	 *
	 * @return {@literal true} if the order is definite on all tracks, {@literal false} if
	 *         there is at least one track where the order is not definite.
	 */
	public boolean isTotallyOrdered();

	/**
	 * Tests whether the pure CSP is required.
	 *
	 * @return When this is {@literal true} then the greedy part failed and we need to
	 *         clear the ordering and do everything again.
	 */
	public boolean pureCSPRequired();
}
