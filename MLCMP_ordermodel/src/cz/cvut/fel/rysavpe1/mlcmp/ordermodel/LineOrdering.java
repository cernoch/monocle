/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.ordermodel;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalEdgeException;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * The line ordering data structure for the whole problem. The inner representation
 * of ordering in class {@literal TrackLineOrdering} uses the ordering of lines from
 * station given by {@literal getVertex1()} to station returned by {@literal getVertex2}.
 *
 * @author Petr Ryšavý
 */
public class LineOrdering extends AbstractOrdering
{
	/**
	 * The map from tracks to the ordering on specified tracks.
	 */
	private HashMap<Track, TrackLineOrder> orderMap;
	/**
	 * When this is true then the greedy part failed and we need to clear the
	 * ordering and do everything again.
	 */
	private boolean requirePureCSP = false;

	/**
	 * Creates new line ordering instance. The problem can't be modified in future,
	 * otherwise this structure doesn't need to work correctly.
	 *
	 * @param problem The problem on which the ordering should be created.
	 */
	public LineOrdering(MLCMP problem)
	{
		orderMap = new HashMap<>(problem.getEdgesNum());

		for (Track t : new IteratorWrapper<>(problem.trackIterator()))
		{
			Set<Line> lineSet = problem.getLineSet(t);
			Line[] lines = new Line[lineSet.size()];
			lineSet.toArray(lines);
			orderMap.put(t, new TrackLineOrder(lines));
		}
	}

	/**
	 * Creates new instance of LineOrdering as a compound of several orderings. Those
	 * orderings are backed, i.e. changes done on this ordering can be seen on
	 * orderings of {@literal orderingIterable} and vice versa. If there is a track in
	 * two or more orderings from parameter, then a random from the orders is
	 * choosen. Hence it is recommended that the orderings from input are mutually
	 * exclusive.
	 *
	 * @param orderingIterable Iterable of input orderings.
	 */
	public LineOrdering(Iterable<LineOrdering> orderingIterable)
	{
		int size = 0;
		for (LineOrdering lo : orderingIterable)
			size += lo.orderMap.size();

		orderMap = new HashMap<>(size);
		for (LineOrdering lo : orderingIterable)
			orderMap.putAll(lo.orderMap);
		System.err.println("lo size "+size);
		System.err.println("ol size "+orderMap.size());
	}

	@Override
	public void setOrderOfLines(Line l1, Line l2, int order, Track t, Station s)
	{
		checkTrack(t);

		if (t.getVertex1().equals(s))
			orderMap.get(t).setLineOrder(l1, l2, order);
		else if (t.getVertex2().equals(s))
			orderMap.get(t).setLineOrder(l2, l1, order);
		else
			throw new IllegalArgumentException("The given station must be endpoint of track at which lines shall be ordered.");
	}

	@Override
	public void assignAnOrder(Track t)
	{
		checkTrack(t);
		orderMap.get(t).assignAnOrder();
	}

	@Override
	public void assignRandomOrder(Track t)
	{
		checkTrack(t);
		orderMap.get(t).assignRandomOrder();
	}

	@Override
	public void setOrderOfLinesIfPossible(Line l1, Line l2, int order, Track t, Station s)
	{
		checkTrack(t);

		if (t.getVertex1().equals(s))
			orderMap.get(t).setLineOrderIfPossible(l1, l2, order);
		else if (t.getVertex2().equals(s))
			orderMap.get(t).setLineOrderIfPossible(l2, l1, order);
		else
			throw new IllegalArgumentException("The given station must be endpoint of track at which lines shall be ordered.");
	}

	@Override
	public boolean isOrderSetPossible(Line l1, Line l2, int order, Track t, Station s)
	{
		checkTrack(t);

		if (t.getVertex1().equals(s))
			return orderMap.get(t).isOrderSetPossible(l1, l2, order);
		else if (t.getVertex2().equals(s))
			return orderMap.get(t).isOrderSetPossible(l1, l2, order);
		else
			throw new IllegalArgumentException("The given station must be endpoint of track at which lines shall be ordered.");
	}

	@Override
	public int getOrderOfLines(Line l1, Line l2, Track t, Station s)
	{
		checkTrack(t);
		if (t.getVertex1().equals(s))
			return orderMap.get(t).getLineOrder(l1, l2);
		else if (t.getVertex2().equals(s))
			return orderMap.get(t).getLineOrder(l2, l1);
		else
			throw new IllegalArgumentException("The given station must be endpoint of track at which lines shall be ordered.");
	}

	@Override
	public boolean isTotallyOrdered(Track t)
	{
		checkTrack(t);
		final TrackLineOrder order = orderMap.get(t);
		return order.isIsOrderDefinite() || order.tryFinalizeOrder();
	}

	/**
	 * Decides whether the track order can be modified. Order cannot be modified for
	 * example when it is already total ordering.
	 *
	 * @param t The track.
	 * @return Can be order modified?
	 */
	public boolean canBeTrackOrderModified(Track t)
	{
		checkTrack(t);
		return !orderMap.get(t).isIsOrderDefinite();
	}

	@Override
	public boolean areLinesCompared(Line l1, Line l2, Track t)
	{
		checkTrack(t);
		return orderMap.get(t).areLinesCompared(l1, l2);
	}

	@Override
	public Iterator<Line> finalizedOrderIterator(Track t, Station s)
	{
		checkTrack(t);
		if (t.getVertex1().equals(s))
			return orderMap.get(t).finalizedOrderIterator(true);
		else if (t.getVertex2().equals(s))
			return orderMap.get(t).finalizedOrderIterator(false);
		else
			throw new IllegalArgumentException("The given station must be endpoint of track at which lines shall be ordered.");
	}

	/**
	 * Checks whether track is part of this ordering.
	 *
	 * @param t The track.
	 * @throws IllegalEdgeException If the track is not in {@literal orderMap}.
	 */
	private void checkTrack(Track t) throws IllegalEdgeException
	{
		if (!orderMap.containsKey(t))
			throw new IllegalEdgeException("The track is not part of the problem.");
	}

	/**
	 * Creates a clone of the order on the given track.
	 *
	 * @param t The track on that the order interests us.
	 * @return The track line order.
	 * @throws CloneNotSupportedException When the order on {@literal t} is finalized
	 *                                    and hence cannot create the clone. The
	 *                                    finalized order shall be used instead.
	 */
	protected TrackLineOrder cloneTrackOrder(Track t) throws CloneNotSupportedException
	{
		checkTrack(t);
		return orderMap.get(t).clone();
	}
	
	/**
	 * Prints the current ordering to given output.
	 *
	 * @param out The output.
	 * @throws IOException Exception thrown by the writer {@literal out}.
	 */
	public void printToStream(Writer out) throws IOException
	{
		out.write("Line ordering *****************\n");

		for (Map.Entry<Track, TrackLineOrder> entry : orderMap.entrySet())
		{
			Track track = entry.getKey();
			TrackLineOrder trackLineOrder = entry.getValue();

			out.write("Track               : " + track);
			out.write('\n');
			out.write("    is definite     : " + trackLineOrder.isIsOrderDefinite());
			out.write('\n');
			if (trackLineOrder.isIsOrderDefinite())
				out.write("    order           : " + Arrays.toString(trackLineOrder.getFinalizedOrder()));
			else
				out.write("    partial order   : " + Arrays.deepToString(trackLineOrder.getLineOrder()));
			out.write('\n');
			out.write("-----------------------------------");
			out.write('\n');
			out.write('\n');
		}
	}

	/**
	 * Sets that the pure CSP will be required. When this is set then the greedy part
	 * failed and we need to clear the ordering and do everything again. Sets all
	 * orders to not enough information.
	 */
	public void requirePureCSP()
	{
		if (requirePureCSP)
			throw new IllegalStateException("The ordering was already forced to require pure CSP.");

		for (Entry<Track, TrackLineOrder> e : orderMap.entrySet())
			e.setValue(e.getValue().dummyClone());

		requirePureCSP = true;
	}

	@Override
	public boolean pureCSPRequired()
	{
		return requirePureCSP;
	}

	@Override
	public void swapLines(Track t, Line l1, Line l2)
	{
		checkTrack(t);
		orderMap.get(t).swapLines(l1, l2);
	}

	@Override
	protected Iterable<Track> tracksIterable()
	{
		return orderMap.keySet();
	}
}
