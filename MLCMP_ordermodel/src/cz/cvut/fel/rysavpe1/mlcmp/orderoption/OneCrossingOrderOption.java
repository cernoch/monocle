/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.orderoption;

import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Order option for two lines that cross exactly once.
 *
 * @author Petr Ryšavý
 */
public class OneCrossingOrderOption implements OrderOption
{
	/**
	 * The order at the beginning.
	 */
	private final int startOrder;
	/**
	 * The order at the end.
	 */
	private final int endOrder;
	/**
	 * The vertex at that the lines cross.
	 */
	private final int crossingVertex;
	/**
	 * The number of tracks on the common subpath.
	 */
	private final int tracksNum;

	/**
	 * Creates new one crossing order option.
	 *
	 * @param startOrder     The order at the beginning.
	 * @param crossingVertex The index of vertex where the lines cross. The numbering
	 *                       goes from zero.
	 * @param tracksNum      The number of tracks.
	 * @throws IllegalArgumentException When the start order is not {@code PRECEDES}
	 *                                  nor {@code FOLLOWS}. Also when the number of
	 *                                  tracks is less than one or the vertex of
	 *                                  crossing has index that cannot be on the
	 *                                  track of given lenght.
	 */
	public OneCrossingOrderOption(int startOrder, int crossingVertex, int tracksNum)
	{
		if (startOrder == TrackLineOrder.FOLLOWS)
			endOrder = TrackLineOrder.PRECEDES;
		else if (startOrder == TrackLineOrder.PRECEDES)
			endOrder = TrackLineOrder.FOLLOWS;
		else
			throw new IllegalArgumentException("Unknown order " + startOrder);

		this.startOrder = startOrder;
		if (tracksNum < 1)
			throw new IllegalArgumentException("Illegal number of tracks " + tracksNum);
		this.tracksNum = tracksNum;
		if (crossingVertex < 0 || crossingVertex > tracksNum)
			throw new IllegalArgumentException("Illegal index of crossing vertex " + crossingVertex);
		this.crossingVertex = crossingVertex;
	}

	@Override
	public Iterator<Integer> iterator()
	{
		return new OrderIterator();
	}

	/**
	 * Iterator of the order.
	 */
	private class OrderIterator implements Iterator<Integer>
	{
		/**
		 * Current index of track.
		 */
		int currentTrack = 0;

		@Override
		public boolean hasNext()
		{
			return currentTrack < tracksNum;
		}

		@Override
		public Integer next()
		{
			if (currentTrack >= tracksNum)
				throw new NoSuchElementException("Iterated over all possible tracks.");

			final int retval;
			if (currentTrack < crossingVertex)
				retval = startOrder;
			else // currentTrack >= crossingVertex
				retval = endOrder;
			currentTrack++;
			return retval;
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("Not supported yet.");
		}
	}

	@Override
	public int hashCode()
	{
		int hash = 5;
		hash = 43 * hash + this.crossingVertex;
		hash = 43 * hash + this.tracksNum;
		return hash;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final OneCrossingOrderOption other = (OneCrossingOrderOption) obj;
		if (this.startOrder != other.startOrder)
			return false;
		if (this.endOrder != other.endOrder)
			return false;
		if (this.crossingVertex != other.crossingVertex)
			return false;
		if (this.tracksNum != other.tracksNum)
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "OneCrossingOrderOption{" + "startOrder=" + startOrder + ", endOrder=" + endOrder + ", crossingVertex=" + crossingVertex + ", tracksNum=" + tracksNum + '}';
	}
}
