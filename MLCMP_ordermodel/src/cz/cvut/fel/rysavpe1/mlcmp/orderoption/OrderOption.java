/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.orderoption;

import java.util.Iterator;

/**
 * The order option is the possible assignment on one common subpath. There is many
 * possibilities how to cross the lines on common subpath and instance of this type
 * wraps up one of this possibility.
 *
 * @author Petr Ryšavý
 */
public interface OrderOption extends Iterable<Integer>
{
	/**
	 * Iterates over the order on the tracks of common subpath.
	 *
	 * @return Iterator of the orders on tracs.
	 */
	@Override
	public Iterator<Integer> iterator();
}