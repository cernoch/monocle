/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.orderoption;

import cz.cvut.fel.rysavpe1.utilities.IntArrayIterable;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Order option that can contain arbitrary order.
 *
 * @author Petr Ryšavý
 */
public class CustomOrderOption implements OrderOption
{
	/**
	 * The order values.
	 */
	private final int[] order;

	/**
	 * Creates new order option with the given values.
	 *
	 * @param order The order that is set.
	 */
	public CustomOrderOption(int[] order)
	{
		this.order = new int[order.length];
		System.arraycopy(order, 0, this.order, 0, order.length);
	}

	/**
	 * Creates new order option that stores only one value. The
	 * {@code NoCrossingOrderOption} is recommended instead as it does not store any
	 * array.
	 *
	 * @param order  The order to set.
	 * @param length The number of path for that the order is set.
	 */
	public CustomOrderOption(int order, int length)
	{
		this.order = new int[length];
		Arrays.fill(this.order, order);
	}

	@Override
	public int hashCode()
	{
		int hash = 7;
		hash = 79 * hash + Arrays.hashCode(this.order);
		return hash;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final CustomOrderOption other = (CustomOrderOption) obj;
		if (!Arrays.equals(this.order, other.order))
			return false;
		return true;
	}

	@Override
	public Iterator<Integer> iterator()
	{
		return new IntArrayIterable(order).iterator();
	}
}
