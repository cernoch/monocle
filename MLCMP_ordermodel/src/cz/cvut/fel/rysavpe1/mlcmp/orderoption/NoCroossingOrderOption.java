/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.orderoption;

import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import java.util.Iterator;

/**
 * Order option for two lines that don't cross.
 *
 * @author Petr Ryšavý
 */
public class NoCroossingOrderOption implements OrderOption
{
	/**
	 * The order.
	 */
	private final int order;
	/**
	 * The iterator.
	 */
	private Iterator<Integer> it = null;
	/**
	 * Order option that returns {@code FOLLOWS} for all tracks.
	 */
	public static final NoCroossingOrderOption FOLLOWS_ORDER_OPTION = new NoCroossingOrderOption(TrackLineOrder.FOLLOWS);
	/**
	 * Order option that returns {@code PRECEDES} for all tracks.
	 */
	public static final NoCroossingOrderOption PRECEDES_ORDER_OPTION = new NoCroossingOrderOption(TrackLineOrder.PRECEDES);

	/**
	 * Creates new order option. This order option will return all the time the given
	 * order.
	 *
	 * @param order The order that shall given by the iterator.
	 */
	private NoCroossingOrderOption(int order)
	{
		this.order = order;
	}

	@Override
	public Iterator<Integer> iterator()
	{
		if (it == null)
			it = new Iterator<Integer>()
			{
				@Override
				public boolean hasNext()
				{
					return true;
				}

				@Override
				public Integer next()
				{
					return order;
				}

				@Override
				public void remove()
				{
					throw new UnsupportedOperationException("Not supported yet.");
				}
			};

		return it;
	}
}
