/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.graphalgos.KahnAlgorithm;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.UnderlyingGraph;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.DirectedEdge;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.SimpleDirectedGraph;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.EdgeShortener;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class DependencyGraphBuilderTest
{
	public DependencyGraphBuilderTest()
	{
	}

	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void testSomeMethod() throws IOException, GraphReadingException
	{
		//BufferedReader in = Files.newBufferedReader(Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in"), Charset.forName("utf-8"));
		BufferedReader in = Files.newBufferedReader(Paths.get("C:", "Users", "Petr Rysavy", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in"), Charset.forName("utf-8"));
		PlainTextGraphReader instance = new PlainTextGraphReader(new OutputStreamWriter(System.err));
		MLCMP result = instance.readGraph(in);
		EdgeShortener.contractTracks(result);
		LineOrdering ord = new LineOrdering(result);
		Map<CommonSubpath, Integer> toDo = GreedyOrderer.order(result, ord);

		//ord.printToStream(out);
		System.err.println("toDo : " + toDo.size() + " common subpaths on " + result.getEdgesNum());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		GreedyOrdererTest_CZE.printUnsolvedTracks(result, ord);
		//TailCutter.cutTails(result, ord, toDo);
		System.err.println("toDo : " + toDo.size() + " common subpaths on " + result.getEdgesNum());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		GreedyOrdererTest_CZE.printUnsolvedTracks(result, ord);

		HashMap<PairSet<Line>, List<SimpleDirectedGraph<CommonSubpath>>> graph = DependencyGraphBuilder.getDependencyGraph(result, ord, toDo);
		printDependencyGraphs(graph, toDo, ord);
	}

	public static void printDependencyGraphs(HashMap<PairSet<Line>, List<SimpleDirectedGraph<CommonSubpath>>> map, Map<CommonSubpath, Integer> toDo, LineOrdering lo)
	{
		for (Entry<PairSet<Line>, List<SimpleDirectedGraph<CommonSubpath>>> e : map.entrySet())
		{
			System.err.println("line1 : " +e.getKey().getValue1());
			System.err.println("line2 : " +e.getKey().getValue2());
			System.err.println("components : "+e.getValue().size());
			System.err.println("------------");
			for(SimpleDirectedGraph<CommonSubpath> graph : e.getValue())
			{
				printDependencyGraph(graph, toDo, lo);
				System.err.println("cycle : " + (KahnAlgorithm.sort(graph) == null));
				System.err.println("sort  : " + (KahnAlgorithm.sort(graph)));
				System.err.println("---------");
			}
			System.err.println("*******************************");
			System.err.println();
		}
	}

	public static void printDependencyGraph(SimpleDirectedGraph<CommonSubpath> graph, Map<CommonSubpath, Integer> toDo, LineOrdering lo)
	{
		System.err.println("dependency graph");
		System.err.println("vertices: " + graph.getVerticesNum());
		System.err.println("edges: " + graph.getEdgesNum());
		System.err.println("-----------");

		for (CommonSubpath cs : graph)
			System.err.println("vertex : " + cs.toPathString() + " order " + (toDo.containsKey(cs) ? toDo.get(cs) : GreedyOrderDecider.getPreferredOrder(cs.getFirstLine(), cs.getSecondLine(), cs.getFirstStation(), cs.getFirstTrack(), lo)));
		System.err.println("-----------");
		int i = 0;
		for (DirectedEdge<Void> de : new IteratorWrapper<>(graph.edgeIterator()))
		{
			final CommonSubpath h = (CommonSubpath) de.getHead().getValue();
			final CommonSubpath t = (CommonSubpath) de.getTail().getValue();
			System.err.println("edge " + de.getHead() + " to " + de.getTail());
			System.err.println("     from "
				+ (toDo.containsKey(h) ? toDo.get(h) : GreedyOrderDecider.getPreferredOrder(h.getFirstLine(), h.getSecondLine(), h.getFirstStation(), h.getFirstTrack(), lo))
				+ " to"
				+ (toDo.containsKey(t) ? toDo.get(t) : GreedyOrderDecider.getPreferredOrder(t.getFirstLine(), t.getSecondLine(), t.getFirstStation(), t.getFirstTrack(), lo)));
			i++;
		}
		System.err.println("i: " + i);
	}
}