/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.Map;
import org.junit.Test;
import org.junit.BeforeClass;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class GreedyOrdererTest
{
	static MLCMP problem1;
	static MLCMP problem2;
	static MLCMP problem3;
	static Writer out = new OutputStreamWriter(System.out);
	
	public GreedyOrdererTest()
	{
	}
	
	@BeforeClass
	public static void beforeClass() throws FileNotFoundException, IOException, GraphReadingException
	{
		BufferedReader in = new BufferedReader(new FileReader("test/graph1.in"));
		PlainTextGraphReader instance = new PlainTextGraphReader(new OutputStreamWriter(System.err));
		problem1 = instance.readGraph(in);
		in = new BufferedReader(new FileReader("test/graph2.in"));
		problem2 = instance.readGraph(in);
		in = new BufferedReader(new FileReader("test/graph3.in"));
		problem3 = instance.readGraph(in);
		//problem.printToStream(new OutputStreamWriter(System.out));
	}

	@Test
	public void test1() throws IOException
	{
		System.err.println("test1");
		LineOrdering ord = new LineOrdering(problem1);
		Map<CommonSubpath, Integer> toDo = GreedyOrderer.order(problem1, ord);
		System.err.println("toDo : "+toDo.size());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		ord.printToStream(out);
	}

	@Test
	public void test2() throws IOException
	{
		System.err.println("test2");
		LineOrdering ord = new LineOrdering(problem2);
		Map<CommonSubpath, Integer> toDo = GreedyOrderer.order(problem2, ord);
		System.err.println("toDo : "+toDo.size());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		ord.printToStream(out);
	}

	@Test
	public void test3() throws IOException
	{
		System.err.println("test3");
		LineOrdering ord = new LineOrdering(problem3);
		Map<CommonSubpath, Integer> toDo = GreedyOrderer.order(problem3, ord);
		System.err.println("toDo : "+toDo.size());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		ord.printToStream(out);
	}
}