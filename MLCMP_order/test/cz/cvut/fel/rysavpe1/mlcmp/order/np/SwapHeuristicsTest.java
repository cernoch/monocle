/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.order.np;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.SimpleDirectedGraph;
import cz.cvut.fel.rysavpe1.mlcmp.order.BundleOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.DependencyGraphBuilder;
import cz.cvut.fel.rysavpe1.mlcmp.order.ExtendedGreedyOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.GreedyOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.NotEnoughInformationResolver;
import cz.cvut.fel.rysavpe1.mlcmp.order.TailCutter;
import cz.cvut.fel.rysavpe1.mlcmp.order.utililies.CrossingsCounter;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.EdgeShortener;
import cz.cvut.fel.rysavpe1.mlcmp.utilities.TrackMapper;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class SwapHeuristicsTest
{
	public SwapHeuristicsTest()
	{
	}

	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void testSomeMethod() throws IOException, GraphReadingException
	{
		//Path p = Paths.get("C:", "Users", "Petr Ryšavý", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
//		Path p = Paths.get("C:", "Users", "Petr Rysavy", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
		Path p = Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
//		Path p = Paths.get("C:", "Users", "Petr Ryšavý", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
//		Path p = Paths.get("C:", "Users", "Petr Rysavy", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "trams.in");
//		Path p = Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "trams.in");
//		Path p = Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_order", "test", "badGraph.in");

		MLCMP problem = new PlainTextGraphReader(new OutputStreamWriter(System.out)).readGraph(p);

		EdgeShortener.contractTracks(problem);

		LineOrdering ordering = new LineOrdering(problem);
		Map<CommonSubpath, Integer> toDo = GreedyOrderer.order(problem, ordering);
		TailCutter.cutTails(problem, ordering, toDo);
		ExtendedGreedyOrderer.order(problem, ordering, toDo);
		HashMap<PairSet<Line>, List<SimpleDirectedGraph<CommonSubpath>>> graph = DependencyGraphBuilder.getDependencyGraph(problem, ordering, toDo);
		NotEnoughInformationResolver.resolve(problem, ordering, toDo, graph);
		BundleOrderer.order(problem, ordering, toDo);

		SwapHeuristics.findRandomSolution(ordering, TrackMapper.trackMap(toDo.keySet()).keySet(), toDo.keySet());
		System.err.println("crossings : "+CrossingsCounter.countCrossings(problem, ordering));
	}
}