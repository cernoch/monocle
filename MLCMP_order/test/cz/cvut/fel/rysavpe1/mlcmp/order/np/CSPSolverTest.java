/*
 */

package cz.cvut.fel.rysavpe1.mlcmp.order.np;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.SimpleDirectedGraph;
import cz.cvut.fel.rysavpe1.mlcmp.order.BundleOrderer;
import static cz.cvut.fel.rysavpe1.mlcmp.order.BundleOrdererTest.testTODOMap;
import cz.cvut.fel.rysavpe1.mlcmp.order.DependencyGraphBuilder;
import cz.cvut.fel.rysavpe1.mlcmp.order.ExtendedGreedyOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.GreedyOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.GreedyOrdererTest_CZE;
import cz.cvut.fel.rysavpe1.mlcmp.order.NotEnoughInformationResolver;
import cz.cvut.fel.rysavpe1.mlcmp.order.TailCutter;
import cz.cvut.fel.rysavpe1.mlcmp.order.utililies.CrossingsCounter;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.UndoableOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.EdgeShortener;
import cz.cvut.fel.rysavpe1.mlcmp.utilities.TrackMapper;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class CSPSolverTest {

    public CSPSolverTest() {
    }

	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void testSomeMethod() throws IOException, GraphReadingException
	{
		BufferedReader in = Files.newBufferedReader(Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in"), Charset.forName("utf-8"));
//		BufferedReader in = Files.newBufferedReader(Paths.get("C:", "Users", "Petr Ryšavý", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in"), Charset.forName("utf-8"));
//		BufferedReader in = Files.newBufferedReader(Paths.get("C:", "Users", "Petr Rysavy", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in"), Charset.forName("utf-8"));
		PlainTextGraphReader instance = new PlainTextGraphReader(new OutputStreamWriter(System.err));
		MLCMP result = instance.readGraph(in);
		EdgeShortener.contractTracks(result);
		LineOrdering ord = new LineOrdering(result);
		Map<CommonSubpath, Integer> toDo = GreedyOrderer.order(result, ord);
		TailCutter.cutTails(result, ord, toDo);
		ExtendedGreedyOrderer.order(result, ord, toDo);
		HashMap<PairSet<Line>, List<SimpleDirectedGraph<CommonSubpath>>> graph = DependencyGraphBuilder.getDependencyGraph(result, ord, toDo);
		NotEnoughInformationResolver.resolve(result, ord, toDo, graph);
		BundleOrderer.order(result, ord, toDo);

		System.err.println();
		System.err.println("after");
		System.err.println("***************************");
		System.err.println("");
		System.err.println("toDo : " + toDo.size() + " common subpaths on " + result.getEdgesNum());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		GreedyOrdererTest_CZE.printUnsolvedTracks(result, ord);

		testTODOMap(result, ord, toDo);
		
		Map<Track, List<CommonSubpath>> trackMap = TrackMapper.trackMap(toDo.keySet());
		for (Iterator<Track> it = trackMap.keySet().iterator(); it.hasNext();)
			if(ord.isTotallyOrdered(it.next()))
				it.remove();
		
		System.err.println("not solved "+trackMap.size());
		List<Set<Track>> components = IsolatedComponentsFinder.findIsolatedComponents(trackMap.keySet());
		
//		System.err.println("*******************************");
//		System.err.println("the isolated components sizes :");
//		for (Set<Track> s : components)
//		{
//			System.err.println("\tnumber of tracks " + (s.size()));
//			System.err.println("\t\tmincut of size    " + MinCutFinder.findMinCut(s, trackMap).size());
//			System.err.println("\t\twith common sups. " + trackMap.get(MinCutFinder.findMinCut(s, trackMap).iterator().next()).size());
//		}
		
		UndoableOrdering uord = new UndoableOrdering(ord);
//		for(Set<Track> s : components)
//			CSPSolver.solve(result, uord, s, trackMap);
		CSPSolver.solve(result, ord, components, trackMap);
		
		int crossings = 0;
		for(Station s : new IteratorWrapper<>(result.stationIterator()))
			crossings += CrossingsCounter.countCrossings(ord.getFinalizedOrder(s));
		System.err.println("Total number of crossings "+crossings);
	}

}