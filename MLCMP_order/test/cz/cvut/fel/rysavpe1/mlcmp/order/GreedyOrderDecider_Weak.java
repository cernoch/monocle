package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.order.*;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.*;
import java.util.Arrays;

/**
 * The decider that defines the greedy order of two lines on common subpath.
 *
 * @author Petr Ryšavý
 */
public final class GreedyOrderDecider_Weak
{
	/**
	 * Weak prefference of precedes. This means that the order doesn't matter or it
	 * is {@literal FOLLOWS}.
	 */
	private static final int WEAK_PRECEDES = -FOLLOWS;
	/**
	 * Weak prefference of follows. This means that the order doesn't matter or it is
	 * {@literal PRECEDES}.
	 */
	private static final int WEAK_FOLLOWS = -PRECEDES;

	/**
	 * Don't let anybody to create the class instance.
	 */
	private GreedyOrderDecider_Weak()
	{
	}

	/**
	 * Returns the greedy order of two lines.
	 *
	 * @param problem  The problem.
	 * @param ordering The given ordering.
	 * @param path     Common subpath of two lines.
	 * @return The order constant given by {@literal TrackLineOrder} class.
	 * @see cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder
	 */
	public static int orderLines(MLCMP problem, Ordering ordering, CommonSubpath path)
	{
		final Line l1 = path.getFirstLine();
		final Line l2 = path.getSecondLine();

		final Station start = path.getFirstStation();
		final Station end = path.getLastStation();

		final Track first = path.getFirstTrack();
		final Track last = path.getLastTrack();

//		System.err.println("l1 "+l1);
//		System.err.println("l2 "+l2);
//		System.err.println("pa "+path.toPathString());

		final int startPreferredOrder = getPreferredOrder(l1, l2, start, first, ordering);

		// if you don't have enough information on one side, you don't have enough
		// information at all
		if (startPreferredOrder == NOT_ENOUGH_INFORMATION)
			return NOT_ENOUGH_INFORMATION;

		final int endPreferredOrder = getPreferredOrder(l2, l1, end, last, ordering);

		
//		if (start.getValue().equals("S241839") || start.getValue().equals("S107100")
//			|| first.getOtherVertex(start).getValue().equals("S91134") || first.getOtherVertex(start).getValue().equals("S107100"))
//		{
//			System.err.println("l1     " + l1);
//			System.err.println("l2     " + l2);
//			System.err.println("start  " + startPreferredOrder);
//			System.err.println("end    " + endPreferredOrder);
//			System.err.println("t      " +first);
//			System.err.println("s      " + start);
//			System.err.println("e      " + end);
//			System.err.println("endgreedy");
//		}

		if (endPreferredOrder == NOT_ENOUGH_INFORMATION)
			return NOT_ENOUGH_INFORMATION;

		// if one side doesn't matter, then it depends only on the second side
		// it may be not matter, prefference or weak prefference
		// weak preference may be made strong - if not matter we decided for one
		// strong prefference will win anyway
		if (startPreferredOrder == NOT_MATTER)
			return Math.abs(endPreferredOrder);
		if (endPreferredOrder == NOT_MATTER)
			return Math.abs(startPreferredOrder);

		// if preferred order is same, than it is our value
		// the only possible values are now PRECEDES or FOLLOWS or their weak equivallents
		if (startPreferredOrder == endPreferredOrder || startPreferredOrder == -endPreferredOrder)
			return Math.abs(startPreferredOrder);

		// one relation is weak (ex. WEAK_FOLLOWS) and the other opposite (FOLLOWS)
		// hence the whole result is NOT_COMPARABLE or that strong value (FOLLOWS)
		// i.e. we don't know
		if (startPreferredOrder < 0 || endPreferredOrder < 0)
			return NOT_ENOUGH_INFORMATION;

		// otherwise they are different, one is FOLLOWS and other PRECEDES
		return NOT_COMPARABLE;
	}

	/**
	 * Returns preferred order of two lines on common subpath end.
	 *
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param s        End station of supath.
	 * @param t        Last track of subpath.
	 * @param ordering The ordering already known.
	 * @return The preferred order.
	 */
	public static int getPreferredOrder(Line l1, Line l2, Station s, Track t, Ordering ordering)
	{
		if (l1.isEndpoint(s) || l2.isEndpoint(s))
			return NOT_MATTER;

		// here are tracks sorted clockwise so that t is on index 0
		Track[] tracksArr = s.getIncidentTracks(t);
		//Map<Track, Integer> tracksMap = s.getIncidentTracks();

		// index of next l1 line clockwise
		int l1nextClockwise = findNextClockwise(tracksArr, l1);
		int l1nextAnticlockwise = findNextAnticlockwise(tracksArr, l1);
		int l2nextClockwise = findNextClockwise(tracksArr, l2);
		int l2nextAnticlockwise = findNextAnticlockwise(tracksArr, l2);
		
//		if (s.getValue().equals("S241839") || s.getValue().equals("S107100"))
//		{
//			System.err.println("l1     " + l1);
//			System.err.println("l2     " + l2);
//			System.err.println("arr    "+Arrays.toString(tracksArr));
//			System.err.println("l1nc   "+l1nextClockwise);
//			System.err.println("l2nc   "+l2nextClockwise);
//			System.err.println("l1nac  "+l1nextAnticlockwise);
//			System.err.println("l2nac  "+l2nextAnticlockwise);
//			System.err.println("endgreedy");
//		}

//		System.err.println("l1 "+l1);
//		System.err.println("l2 "+l2);
//		System.err.println("tr "+t);
//		System.err.println("i1 "+l1nextClockwise);
//		System.err.println("i2 "+l2nextClockwise);
//		System.err.println("t1 "+tracksArr[l1nextClockwise]);
//		System.err.println("t2 "+tracksArr[l2nextClockwise]);
//		System.err.println("c1 "+l1.containsTrack(tracksArr[l1nextClockwise]));
//		System.err.println("c2 "+l2.containsTrack(tracksArr[l1nextClockwise]));
//		System.err.println("");

		// if two lines lie on same line that you have already ordered, you can use the order
		if (l1nextClockwise == l2nextClockwise)
			switch (ordering.getOrderOfLines(l1, l2, tracksArr[l1nextClockwise], s))
			{
				case FOLLOWS:
					l1nextClockwise--;
					break;
				case PRECEDES:
					l1nextClockwise++;
					break;
			}
		// similarly anticlockwise
		if (l1nextAnticlockwise == l2nextAnticlockwise)
			switch (ordering.getOrderOfLines(l1, l2, tracksArr[l1nextAnticlockwise], s))
			{
				case FOLLOWS:
					l1nextAnticlockwise--;
					break;
				case PRECEDES:
					l1nextAnticlockwise++;
					break;
			}

		// follows condition
		if (l1nextClockwise < l2nextClockwise && l1nextAnticlockwise < l2nextAnticlockwise)
			return PRECEDES;
		// precedes condition
		if (l1nextClockwise > l2nextClockwise && l1nextAnticlockwise > l2nextAnticlockwise)
			return FOLLOWS;

		// not matter if there are both surrounding tracks contain same line
		if (l1nextClockwise < l2nextClockwise && l1nextAnticlockwise > l2nextAnticlockwise
			|| l1nextClockwise > l2nextClockwise && l1nextAnticlockwise < l2nextAnticlockwise)
			return NOT_MATTER;

		// and hence
		// l1nextAnticlockwise == l2nextAnticlockwise || l1nextClockwise == l2nextClockwise
		// what means weak prefference - NOT_MATTER or PRECEDES
		if (l1nextClockwise < l2nextClockwise || l1nextAnticlockwise < l2nextAnticlockwise)
			return WEAK_FOLLOWS;
		if (l1nextClockwise > l2nextClockwise || l1nextAnticlockwise > l2nextAnticlockwise)
			return WEAK_PRECEDES;

		// we have tried all sharp inequalities hence it means that two lines share
		// same track, but they are not ordered yet
		return NOT_ENOUGH_INFORMATION;
	}

	/**
	 * Finds index of next track (in clockwise direction) that contains the given
	 * line.
	 *
	 * @param tracksArr Tracks that constains the tracks sorted clockwise. The first
	 *                  track is skipped from the search.
	 * @param l         The line to search.
	 * @return Index of first track that contains {@literal l} or {@literal -1}.
	 */
	protected static int findNextClockwise(Track[] tracksArr, Line l)
	{
		for (int i = 1; i < tracksArr.length; i++)
			if (l.containsTrack(tracksArr[i]))
				return i;
		return -1;
	}

	/**
	 * Finds index of next track (in anticlockwise direction) that contains the given
	 * line.
	 *
	 * @param tracksArr Tracks that constains the tracks sorted clockwise.
	 * @param l         The line to search.
	 * @return Index of last track that contains {@literal l} or {@literal -1}.
	 */
	protected static int findNextAnticlockwise(Track[] tracksArr, Line l)
	{
		for (int i = tracksArr.length - 1; i > 0; i--)
			if (l.containsTrack(tracksArr[i]))
				return i;
		return -1;
	}
}
