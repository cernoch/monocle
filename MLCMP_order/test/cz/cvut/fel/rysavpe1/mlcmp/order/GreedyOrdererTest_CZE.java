/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.EdgeShortener;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class GreedyOrdererTest_CZE
{
	static Writer out = new OutputStreamWriter(System.out);

	public GreedyOrdererTest_CZE()
	{
	}

	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void test1() throws IOException, GraphReadingException
	{
		//BufferedReader in = new BufferedReader(new FileReader("test/touristTrails.in"));
		BufferedReader in = Files.newBufferedReader(Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in"), Charset.forName("utf-8"));
		PlainTextGraphReader instance = new PlainTextGraphReader(new OutputStreamWriter(System.err));
		MLCMP result = instance.readGraph(in);
		EdgeShortener.contractTracks(result);
		LineOrdering ord = new LineOrdering(result);
		Map<CommonSubpath, Integer> toDo = GreedyOrderer.order(result, ord);
		ord.printToStream(out);
		System.err.println("toDo : " + toDo.size() + " common subpaths on " + result.getEdgesNum());
		printTODOmap(toDo);
		printUnsolvedTracks(result, ord);
	}

	public static void printTODOmap(Map<CommonSubpath, Integer> toDo)
	{
		int not_comparable = 0;
		int not_matter = 0;
		int not_enough_information = 0;

		for (int i : toDo.values())
			switch (i)
			{
				case TrackLineOrder.NOT_COMPARABLE:
					not_comparable++;
					break;
				case TrackLineOrder.NOT_MATTER:
					not_matter++;
					break;
				case TrackLineOrder.NOT_ENOUGH_INFORMATION:
					not_enough_information++;
					break;
			}

		System.err.println("unsolved common subpaths totally : " + toDo.size());
		System.err.println("not_comparable                   : " + not_comparable);
		System.err.println("not_matter                       : " + not_matter);
		System.err.println("not_enough_information           : " + not_enough_information);
	}

	public static void printUnsolvedTracks(MLCMP pr, LineOrdering ord)
	{
		int definite = 0;
		for(Track t : new IteratorWrapper<>(pr.trackIterator()))
			if(ord.isTotallyOrdered(t))
				definite++;
		System.err.println("remains "+(pr.getEdgesNum() - definite) + " of "+pr.getEdgesNum() + " tracks.");
	}
}