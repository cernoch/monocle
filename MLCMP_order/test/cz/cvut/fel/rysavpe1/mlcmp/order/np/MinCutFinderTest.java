/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order.np;

import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
public class MinCutFinderTest
{
	public MinCutFinderTest()
	{
	}

	@Test
	public void oneTrackMincut()
	{
		System.err.println("cut one track");
		int i = 0;
		Track t1 = new Track(new Station("a", i++,0), new Station("b",i++,0));
		Set<Track> s = new HashSet<>();
		s.add(t1);
		Map<Track, Set> map = new HashMap<>();
		for(Track t : s)
			map.put(t, Collections.EMPTY_SET);
		
		Set<Track> mincut  = MinCutFinder.findMinCut(s, map);
		assertEquals(mincut, s);
	}

	@Test
	public void twoTrackMincut()
	{
		System.err.println("cut two track");
		int i = 0;
		final Station a = new Station("a", i++,0);
		final Station b = new Station("b",i++,0);
		final Station c = new Station("c",i++,0);
		Track t1 = new Track(a, b);
		Track t2 = new Track(c, b);
		Set<Track> s = new HashSet<>();
		s.add(t1);
		s.add(t2);
		Map<Track, Set<Integer>> map = new HashMap<>();
		for(Track t : s)
			map.put(t, new HashSet<Integer>());
		
		Set<Track> mincut  = MinCutFinder.findMinCut(s, map);
		System.err.println("mincut "+mincut);
		assertEquals(mincut.size(), 1);
	}

	@Test
	public void three()
	{
		System.err.println("cut three");
		int i = 0;
		final Station a = new Station("a", i++,0);
		final Station b = new Station("b",i++,0);
		final Station c = new Station("c",i++,0);
		final Station d = new Station("d",i++,0);
		final Station e = new Station("e",i++,0);
		final Station f = new Station("f",i++,0);
		Track t1 = new Track(a, f);
		Track t2 = new Track(a, b);
		Track t3 = new Track(c, b);
		Track t4 = new Track(c, a);
		Track t5 = new Track(f, e);
		Track t6 = new Track(e, d);
		Track t7 = new Track(d, f);
		Set<Track> s = new HashSet<>();
		s.add(t1);
		s.add(t2);
		s.add(t3);
		s.add(t4);
		s.add(t5);
		s.add(t6);
		s.add(t7);
		Map<Track, Set> map = new HashMap<>();
		for(Track t : s)
			map.put(t, Collections.EMPTY_SET);
		
		Set<Track> mincut  = MinCutFinder.findMinCut(s, map);
		System.err.println("mincut "+mincut);
		assertEquals(mincut.size(), 1);
		assertEquals(mincut.iterator().next(), t1);
	}

	@Test
	public void four()
	{
		System.err.println("cut four");
		int i = 0;
		final Station a = new Station("a", i++,0);
		final Station b = new Station("b",i++,0);
		final Station c = new Station("c",i++,0);
		final Station d = new Station("d",i++,0);
		final Station e = new Station("e",i++,0);
		final Station f = new Station("f",i++,0);
		final Station g = new Station("g",i++,0);
		final Station h = new Station("h",i++,0);
		Track t1 = new Track(b, e);
		Track t2 = new Track(d, g);
		Track t3 = new Track(a, b);
		Track t4 = new Track(b, d);
		Track t5 = new Track(c, d);
		Track t6 = new Track(a, d);
		Track t7 = new Track(c, b);
		Track t8 = new Track(a, c);
		Track t9 = new Track(e, g);
		Track t10 = new Track(e, f);
		Track t11 = new Track(f, h);
		Track t12 = new Track(g, h);
		Track t13 = new Track(g, f);
		Track t14 = new Track(e, h);
		Set<Track> s = new HashSet<>();
		s.add(t11);
		s.add(t5);
		s.add(t6);
		s.add(t8);
		s.add(t4);
		s.add(t9);
		s.add(t10);
		s.add(t7);
		s.add(t2);
		s.add(t12);
		s.add(t13);
		s.add(t1);
		s.add(t3);
		s.add(t14);
		Map<Track, Set> map = new HashMap<>();
		for(Track t : s)
			map.put(t, Collections.EMPTY_SET);
		
		Set<Track> mincut  = MinCutFinder.findMinCut(s, map);
		System.err.println("mincut 4 "+mincut);
		assertEquals(mincut.size(), 2);
		Iterator<Track> it = mincut.iterator();
		Track x1 = it.next();
		Track x2 = it.next();
		assertEquals(new PairSet<>(x1, x2), new PairSet<>(t1, t2));
	}

	@Test
	public void five()
	{
		System.err.println("cut five");
		int i = 0;
		final Station a = new Station("a", i++,0);
		final Station b = new Station("b",i++,0);
		final Station c = new Station("c",i++,0);
		final Station d = new Station("d",i++,0);
		final Station e = new Station("e",i++,0);
		Track t1 = new Track(a, c);
		Track t2 = new Track(a, b);
		Track t3 = new Track(e, b);
		Track t4 = new Track(b, c);
		Track t5 = new Track(e, d);
		Track t6 = new Track(c, d);
		Track t7 = new Track(c, e);
		Track t8 = new Track(b, d);
		Set<Track> s = new HashSet<>();
		s.add(t3);
		s.add(t4);
		s.add(t5);
		s.add(t2);
		s.add(t6);
		s.add(t8);
		s.add(t1);
		s.add(t7);
		Map<Track, Set> map = new HashMap<>();
		for(Track t : s)
			map.put(t, Collections.EMPTY_SET);
		
		Set<Track> mincut  = MinCutFinder.findMinCut(s, map);
		System.err.println("mincut 5 "+mincut);
		assertEquals(mincut.size(), 2);
		Iterator<Track> it = mincut.iterator();
		Track x1 = it.next();
		Track x2 = it.next();
		assertEquals(new PairSet<>(x1, x2), new PairSet<>(t1, t2));
	}
}