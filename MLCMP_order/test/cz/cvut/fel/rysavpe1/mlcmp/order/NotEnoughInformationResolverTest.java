/*
 */

package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.SimpleDirectedGraph;
import static cz.cvut.fel.rysavpe1.mlcmp.order.DependencyGraphBuilderTest.printDependencyGraphs;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.EdgeShortener;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class NotEnoughInformationResolverTest {

    public NotEnoughInformationResolverTest() {
    }

	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void testSomeMethod() throws IOException, GraphReadingException
	{
		//BufferedReader in = Files.newBufferedReader(Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in"), Charset.forName("utf-8"));
		BufferedReader in = Files.newBufferedReader(Paths.get("C:", "Users", "Petr Rysavy", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in"), Charset.forName("utf-8"));
		PlainTextGraphReader instance = new PlainTextGraphReader(new OutputStreamWriter(System.err));
		MLCMP result = instance.readGraph(in);
		EdgeShortener.contractTracks(result);
		LineOrdering ord = new LineOrdering(result);
		Map<CommonSubpath, Integer> toDo = GreedyOrderer.order(result, ord);

		//ord.printToStream(out);
		System.err.println("toDo : " + toDo.size() + " common subpaths on " + result.getEdgesNum());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		GreedyOrdererTest_CZE.printUnsolvedTracks(result, ord);
		//TailCutter.cutTails(result, ord, toDo);
		System.err.println("toDo : " + toDo.size() + " common subpaths on " + result.getEdgesNum());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		GreedyOrdererTest_CZE.printUnsolvedTracks(result, ord);

		HashMap<PairSet<Line>, List<SimpleDirectedGraph<CommonSubpath>>> graph = DependencyGraphBuilder.getDependencyGraph(result, ord, toDo);
		NotEnoughInformationResolver.resolve(result, ord, toDo, graph);
		
		System.err.println("");
		System.err.println("after");
		System.err.println("***************************");
		System.err.println("");
		System.err.println("toDo : " + toDo.size() + " common subpaths on " + result.getEdgesNum());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		GreedyOrdererTest_CZE.printUnsolvedTracks(result, ord);

	}

}