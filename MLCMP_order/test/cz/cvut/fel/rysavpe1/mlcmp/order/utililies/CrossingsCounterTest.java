package cz.cvut.fel.rysavpe1.mlcmp.order.utililies;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.UnderlyingGraph;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
public class CrossingsCounterTest
{
	private static final Line l1 = new Line("line 1", null);
	private static final Line l2 = new Line("line 2", null);
	private static final Line l3 = new Line("line 3", null);
	private static final Line l4 = new Line("line 4", null);
	private static final Line l5 = new Line("line 5", null);
	
	public CrossingsCounterTest()
	{
	}

	@Test
	public void testSomeMethod()
	{
		MLCMP problem = new MLCMP(new UnderlyingGraph(), new Line[]{l1, l2, l3, l4});
		assertEquals(1, CrossingsCounter.countCrossings(new Line[]{l1, l2, l1, l2, l2, l2, l1}));
		assertEquals(2, CrossingsCounter.countCrossings(new Line[]{l1, l2, l1, l2, l2, l2, l1, l2}));
		assertEquals(2, CrossingsCounter.countCrossings(new Line[]{l2, l1, l2, l1, l2, l2, l2, l1}));
		assertEquals(10, CrossingsCounter.countCrossings(new Line[]{l1, l2, l1, l3, l4, l2, l5, l3, l1, l4, l3, l1, l1, l2, l2, l2, l4}));
		assertEquals(0, CrossingsCounter.countCrossings(new Line[]{l1, l2, l1, l1}));
		assertEquals(0, CrossingsCounter.countCrossings(new Line[]{l1, l2, l2}));
		assertEquals(0, CrossingsCounter.countCrossings(new Line[]{l2, l1, l1}));
		assertEquals(0, CrossingsCounter.countCrossings(new Line[]{l2, l1, l2}));
		assertEquals(1, CrossingsCounter.countCrossings(new Line[]{l2, l1, l2, l1}));
		assertEquals(1, CrossingsCounter.countCrossings(new Line[]{l1, l2, l1, l2}));
	}
	
	@Test
	public void testXX()
	{
		assertEquals(0, CrossingsCounter.countCrossings(new Line[]{}));
		assertEquals(0, CrossingsCounter.countCrossings(new Line[]{l1}));
		assertEquals(0, CrossingsCounter.countCrossings(new Line[]{l1, l1, l1}));
	}
}