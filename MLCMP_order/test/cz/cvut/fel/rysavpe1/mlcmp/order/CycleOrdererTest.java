/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.UnderlyingGraph;
import cz.cvut.fel.rysavpe1.mlcmp.order.utililies.CrossingsCounter;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class CycleOrdererTest
{
	public CycleOrdererTest()
	{
	}

	@Test
	public void testSomeMethod()
	{
		for(int i = 0; i < 100; i++)
		{
		Station s1 = new Station("1", 1, 1);
		Station s2 = new Station("2", 2, 0);
		Station s3 = new Station("3", 0, 0);
		Station s4 = new Station("4", 1, 2);
		Station s5 = new Station("5", 3, 0);
		Station s6 = new Station("6", -1, 0);

		Track t1 = Math.random() > 0.5 ? new Track(s1, s2) : new Track(s2, s1);
		Track t2 = Math.random() > 0.5 ? new Track(s2, s3) : new Track(s3, s2);
		Track t3 = Math.random() > 0.5 ? new Track(s1, s3) : new Track(s3, s1);
		Track t4 = new Track(s1, s4);
		Track t5 = new Track(s5, s2);
		Track t6 = new Track(s6, s3);

		Line l1 = new Line("l1", Arrays.asList(new Track[]
		{
			t1, t2, t3, t4, t5, t6
		}));
		Line l2 = new Line("l2", Arrays.asList(new Track[]
		{
			t1, t2, t3, t5, t6
		}));
		Line l3 = new Line("l3", Arrays.asList(new Track[]
		{
			t1, t2, t3, t6
		}));

		Set<Track> l = new HashSet<>(8);
		l.add(t1);
		l.add(t2);
		l.add(t3);
		l.add(t4);
		l.add(t5);
		l.add(t6);

		ArrayList<Station> s = new ArrayList<>(8);
		s.add(s1);
		s.add(s2);
		s.add(s3);
		s.add(s4);
		s.add(s5);
		s.add(s6);

		UnderlyingGraph ugl = new UnderlyingGraph(s, l);
		MLCMP problem = new MLCMP(ugl, Arrays.asList(new Line[]
		{
			l1, l2, l3
		}));
		LineOrdering lo = new LineOrdering(problem);

		CycleOrderer.decideOnCycles(problem, l, lo);

		System.err.println("t1");
//		lo.printTrackToStream(t1);
		System.err.println("t2");
//		lo.printTrackToStream(t2);
		System.err.println("t3");
//		lo.printTrackToStream(t3);

		lo.setOrderOfLines(l1, l2, TrackLineOrder.FOLLOWS, t5, s5);
		lo.setOrderOfLines(l1, l2, TrackLineOrder.FOLLOWS, t6, s6);
		lo.setOrderOfLines(l2, l3, TrackLineOrder.FOLLOWS, t6, s6);
		lo.isTotallyOrdered(t5);
		lo.isTotallyOrdered(t6);
		
//		lo.printTrackToStream(t4);
//		lo.printTrackToStream(t5);
//		lo.printTrackToStream(t6);

		int crossings = 0;
		for(Station st : new IteratorWrapper<>(problem.stationIterator()))
			crossings += CrossingsCounter.countCrossings(lo.getFinalizedOrder(st));
		assertEquals(4, crossings);
		}
	}
}