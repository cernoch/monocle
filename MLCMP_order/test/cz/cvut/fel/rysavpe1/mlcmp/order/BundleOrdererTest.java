/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpathFinder;
import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.SimpleDirectedGraph;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.EdgeShortener;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class BundleOrdererTest
{
	public BundleOrdererTest()
	{
	}

	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void testSomeMethod() throws IOException, GraphReadingException
	{
		BufferedReader in = Files.newBufferedReader(Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in"), Charset.forName("utf-8"));
		//BufferedReader in = Files.newBufferedReader(Paths.get("C:", "Users", "Petr Rysavy", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in"), Charset.forName("utf-8"));
		PlainTextGraphReader instance = new PlainTextGraphReader(new OutputStreamWriter(System.err));
		MLCMP result = instance.readGraph(in);
		EdgeShortener.contractTracks(result);
		LineOrdering ord = new LineOrdering(result);
		Map<CommonSubpath, Integer> toDo = GreedyOrderer.order(result, ord);
		TailCutter.cutTails(result, ord, toDo);

		System.err.println("greedy order");
		testTODOMap(result, ord, toDo);

		ExtendedGreedyOrderer.order(result, ord, toDo);

		System.err.println("extended greedy");
		testTODOMap(result, ord, toDo);

		HashMap<PairSet<Line>, List<SimpleDirectedGraph<CommonSubpath>>> graph = DependencyGraphBuilder.getDependencyGraph(result, ord, toDo);
		NotEnoughInformationResolver.resolve(result, ord, toDo, graph);

		System.err.println("not enough resolver");
		testTODOMap(result, ord, toDo);

		System.err.println("before");
		System.err.println("***************************");
		System.err.println("");
		System.err.println("toDo : " + toDo.size() + " common subpaths on " + result.getEdgesNum());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		GreedyOrdererTest_CZE.printUnsolvedTracks(result, ord);

		BundleOrderer.order(result, ord, toDo);

		System.err.println();
		System.err.println("after");
		System.err.println("***************************");
		System.err.println("");
		System.err.println("toDo : " + toDo.size() + " common subpaths on " + result.getEdgesNum());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		GreedyOrdererTest_CZE.printUnsolvedTracks(result, ord);

		System.err.println("bundle orderer");
		testTODOMap(result, ord, toDo);
	}

	public static void testTODOMap(MLCMP problem, LineOrdering lo, Map<CommonSubpath, Integer> map)
	{
		for (Track t : new IteratorWrapper<>(problem.trackIterator()))
			if (!lo.isTotallyOrdered(t))
				for (Line l1 : problem.getLineSet(t))
					for (Line l2 : problem.getLineSet(t))
						if (l1 != l2 && !map.containsKey(CommonSubpathFinder.findCommonSubpath(problem, l1, l2, t)))
						{
							final int order = lo.getOrderOfLines(l1, l2, t, t.getVertex1());
							if (order != TrackLineOrder.FOLLOWS && order != TrackLineOrder.PRECEDES)
							{
								System.err.println("cs : " + CommonSubpathFinder.findCommonSubpath(problem, l1, l2, t));
								System.err.println("or : " + lo.getOrderOfLines(l1, l2, t, t.getVertex1()));
								System.err.println("io : " + lo.isTotallyOrdered(t));
								throw new RuntimeException("There is not totally ordered track in the map.");
							}
						}
		for (CommonSubpath cs : map.keySet())
		{
			boolean ordered = true;
			for (Track t : cs)
				ordered &= lo.isTotallyOrdered(t);
			if (ordered)
			{
				System.err.println("Warning : There is a totally ordered common subpath in toDo map.");
				System.err.println("cs      : "+cs);
				System.err.println("order   : "+map.get(cs));
			}
		}
	}
}