/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import static cz.cvut.fel.rysavpe1.mlcmp.order.GreedyOrdererTest_CZE.out;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.EdgeShortener;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class TailCutterTest
{
	public TailCutterTest()
	{
	}

	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void testSomeMethod() throws IOException, GraphReadingException
	{
		BufferedReader in = Files.newBufferedReader(Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in"), Charset.forName("utf-8"));
		PlainTextGraphReader instance = new PlainTextGraphReader(new OutputStreamWriter(System.err));
		MLCMP result = instance.readGraph(in);
		EdgeShortener.contractTracks(result);
		LineOrdering ord = new LineOrdering(result);
		Map<CommonSubpath, Integer> toDo = GreedyOrderer.order(result, ord);
		//ord.printToStream(out);
		System.err.println("toDo : " + toDo.size() + " common subpaths on " + result.getEdgesNum());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		GreedyOrdererTest_CZE.printUnsolvedTracks(result, ord);
		TailCutter.cutTails(result, ord, toDo);
		System.err.println("toDo : " + toDo.size() + " common subpaths on " + result.getEdgesNum());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		GreedyOrdererTest_CZE.printUnsolvedTracks(result, ord);

		int i = 0;
		for (CommonSubpath s : toDo.keySet())
			if (toDo.get(s) == TrackLineOrder.NOT_ENOUGH_INFORMATION
				&& GreedyOrderDecider.orderLines(result, ord, s) != TrackLineOrder.NOT_ENOUGH_INFORMATION)
			{
				i++;
				System.err.println("not_enough_info is no more true");
				final int order = GreedyOrderDecider.orderLines(result, ord, s);
				System.err.println("order "+order);
				ord.setOrderOfLinesIfPossible(s.getFirstLine(), s.getSecondLine(), order, s, new IteratorWrapper<>(s.stationsIterator()));
			}
		System.err.println("there is at least " + i + " solvable subpaths in first run");
		
		i = 0;
		for (CommonSubpath s : toDo.keySet())
			if (toDo.get(s) == TrackLineOrder.NOT_ENOUGH_INFORMATION
				&& GreedyOrderDecider.orderLines(result, ord, s) != TrackLineOrder.NOT_ENOUGH_INFORMATION)
			{
				i++;
				System.err.println("not_enough_info is no more true");
				final int order = GreedyOrderDecider.orderLines(result, ord, s);
				ord.setOrderOfLinesIfPossible(s.getFirstLine(), s.getSecondLine(), order, s, new IteratorWrapper<>(s.stationsIterator()));
			}
		System.err.println("there is at least " + i + " solvable subpaths in second run");
		
		System.err.println();
		System.err.println("second tail cut");
		TailCutter.cutTails(result, ord, toDo);
		System.err.println("toDo : " + toDo.size() + " common subpaths on " + result.getEdgesNum());
		GreedyOrdererTest_CZE.printTODOmap(toDo);
		GreedyOrdererTest_CZE.printUnsolvedTracks(result, ord);

	}
}