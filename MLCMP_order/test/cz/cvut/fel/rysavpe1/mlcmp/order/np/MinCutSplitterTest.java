/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order.np;

import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.UnderlyingGraph;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
public class MinCutSplitterTest
{
	public MinCutSplitterTest()
	{
	}

	@Test
	public void oneTrackMincut()
	{
		System.err.println("cut one track");
		int i = 0;
		Track t1 = new Track(new Station("a", i++, 0), new Station("b", i++, 0));
		Set<Track> s = new HashSet<>();
		s.add(t1);
		Map<Track, Set> map = new HashMap<>();
		for(Track t : s)
			map.put(t, Collections.EMPTY_SET);
		
		Set<Track> mincut  = MinCutFinder.findMinCut(s, map);
		assertEquals(mincut, s);

		PairSet<Set<Track>> split = MinCutSplitter.splitByMinCut(s, mincut);
		assertEquals(split, new PairSet<Set<Track>>(null, null));
	}

	@Test
	public void twoTrackMincut()
	{
		System.err.println("cut two track");
		int i = 0;
		final Station a = new Station("a", i++, 0);
		final Station b = new Station("b", i++, 0);
		final Station c = new Station("c", i++, 0);
		Track t1 = new Track(a, b);
		Track t2 = new Track(c, b);
		Set<Track> s = new HashSet<>();
		s.add(t1);
		s.add(t2);
		Map<Track, Set> map = new HashMap<>();
		for(Track t : s)
			map.put(t, Collections.EMPTY_SET);
		
		Set<Track> mincut  = MinCutFinder.findMinCut(s, map);
		System.err.println("mincut " + mincut);
		assertEquals(mincut.size(), 1);

		PairSet<Set<Track>> split = MinCutSplitter.splitByMinCut(s, mincut);
		System.err.println("split  " + split);
	}

	@Test
	public void three()
	{
		System.err.println("cut three");
		int i = 0;
		final Station a = new Station("a", i++, Math.random() * 100);
		final Station b = new Station("b", i++, Math.random() * 100);
		final Station c = new Station("c", i++, Math.random() * 100);
		final Station d = new Station("d", i++, Math.random() * 100);
		final Station e = new Station("e", i++, Math.random() * 100);
		final Station f = new Station("f", i++, Math.random() * 100);
		Track t1 = new Track(a, f);
		Track t2 = new Track(a, b);
		Track t3 = new Track(c, b);
		Track t4 = new Track(c, a);
		Track t5 = new Track(f, e);
		Track t6 = new Track(e, d);
		Track t7 = new Track(d, f);
		UnderlyingGraph underlyingGraph = new UnderlyingGraph();
		underlyingGraph.addVertex(a);
		underlyingGraph.addVertex(b);
		underlyingGraph.addVertex(c);
		underlyingGraph.addVertex(d);
		underlyingGraph.addVertex(e);
		underlyingGraph.addVertex(f);
		underlyingGraph.addEdge(t1);
		underlyingGraph.addEdge(t2);
		underlyingGraph.addEdge(t3);
		underlyingGraph.addEdge(t4);
		underlyingGraph.addEdge(t5);
		underlyingGraph.addEdge(t6);
		underlyingGraph.addEdge(t7);
		Map<Track, Set> map = new HashMap<>();
		for(Track t : Arrays.asList(underlyingGraph.getEdges()))
			map.put(t, Collections.EMPTY_SET);
		
		Set<Track> mincut  = MinCutFinder.findMinCut(new HashSet<>(Arrays.asList(underlyingGraph.getEdges())), map);
		System.err.println("mincut " + mincut);
		assertEquals(mincut.size(), 1);
		assertEquals(mincut.iterator().next(), t1);


		HashSet<Track> c1 = new HashSet<>();
		HashSet<Track> c2 = new HashSet<>();
		c1.add(t2);
		c1.add(t3);
		c1.add(t4);
//		for(Track t : new IteratorWrapper<>(a.incidenTracksIterator()))
//			System.err.println("t "+t);
		c2.add(t5);
		c2.add(t6);
		c2.add(t7);
		PairSet<Set<Track>> split = MinCutSplitter.splitByMinCut(new HashSet<>(Arrays.asList(underlyingGraph.getEdges())), mincut);
		assertEquals(new PairSet<Set<Track>>(c1, c2), split);
	}

	@Test
	public void four()
	{
		System.err.println("cut four");
		int i = 0;
		final Station a = new Station("a", i++,  Math.random() * 100);
		final Station b = new Station("b", i++,  Math.random() * 100);
		final Station c = new Station("c", i++,  Math.random() * 100);
		final Station d = new Station("d", i++,  Math.random() * 100);
		final Station e = new Station("e", i++,  Math.random() * 100);
		final Station f = new Station("f", i++,  Math.random() * 100);
		final Station g = new Station("g", i++,  Math.random() * 100);
		final Station h = new Station("h", i++,  Math.random() * 100);
		Track t1 = new Track(b, e);
		Track t2 = new Track(d, g);
		Track t3 = new Track(a, b);
		Track t4 = new Track(b, d);
		Track t5 = new Track(c, d);
		Track t6 = new Track(a, d);
		Track t7 = new Track(c, b);
		Track t8 = new Track(a, c);
		Track t9 = new Track(e, g);
		Track t10 = new Track(e, f);
		Track t11 = new Track(f, h);
		Track t12 = new Track(g, h);
		Track t13 = new Track(g, f);
		Track t14 = new Track(e, h);
		UnderlyingGraph underlyingGraph = new UnderlyingGraph();
		underlyingGraph.addVertex(a);
		underlyingGraph.addVertex(b);
		underlyingGraph.addVertex(c);
		underlyingGraph.addVertex(d);
		underlyingGraph.addVertex(e);
		underlyingGraph.addVertex(f);
		underlyingGraph.addVertex(g);
		underlyingGraph.addVertex(h);
		underlyingGraph.addEdge(t1);
		underlyingGraph.addEdge(t2);
		underlyingGraph.addEdge(t3);
		underlyingGraph.addEdge(t4);
		underlyingGraph.addEdge(t5);
		underlyingGraph.addEdge(t6);
		underlyingGraph.addEdge(t7);
		underlyingGraph.addEdge(t8);
		underlyingGraph.addEdge(t9);
		underlyingGraph.addEdge(t10);
		underlyingGraph.addEdge(t11);
		underlyingGraph.addEdge(t12);
		underlyingGraph.addEdge(t13);
		underlyingGraph.addEdge(t14);
		Map<Track, Set> map = new HashMap<>();
		for(Track t : Arrays.asList(underlyingGraph.getEdges()))
			map.put(t, Collections.EMPTY_SET);
		
		Set<Track> mincut  = MinCutFinder.findMinCut(new HashSet<>(Arrays.asList(underlyingGraph.getEdges())), map);
		System.err.println("mincut 4 " + mincut);
		assertEquals(mincut.size(), 2);
		Iterator<Track> it = mincut.iterator();
		Track x1 = it.next();
		Track x2 = it.next();
		assertEquals(new PairSet<>(x1, x2), new PairSet<>(t1, t2));


		HashSet<Track> c1 = new HashSet<>();
		HashSet<Track> c2 = new HashSet<>();
		c1.add(t3);
		c1.add(t4);
		c1.add(t5);
		c1.add(t6);
		c1.add(t7);
		c2.add(t9);
		c1.add(t8);
		c2.add(t10);
		c2.add(t11);
		c2.add(t12);
		c2.add(t13);
		c2.add(t14);
		PairSet<Set<Track>> split = MinCutSplitter.splitByMinCut(new HashSet<>(Arrays.asList(underlyingGraph.getEdges())), mincut);
		System.err.println("split  "+split);
		assertEquals(new PairSet<Set<Track>>(c1, c2), split);
		//assertTrue(Objects.equals(split, new PairSet<Set<Track>>(c1, c2)));
	}

	@Test
	public void five()
	{
		System.err.println("cut five");
		int i = 0;
		final Station a = new Station("a", i++,  Math.random() * 100);
		final Station b = new Station("b", i++,  Math.random() * 100);
		final Station c = new Station("c", i++,  Math.random() * 100);
		final Station d = new Station("d", i++,  Math.random() * 100);
		final Station e = new Station("e", i++,  Math.random() * 100);
		Track t1 = new Track(a, c);
		Track t2 = new Track(a, b);
		Track t3 = new Track(e, b);
		Track t4 = new Track(b, c);
		Track t5 = new Track(e, d);
		Track t6 = new Track(c, d);
		Track t7 = new Track(c, e);
		Track t8 = new Track(b, d);
		UnderlyingGraph underlyingGraph = new UnderlyingGraph();
		underlyingGraph.addVertex(a);
		underlyingGraph.addVertex(b);
		underlyingGraph.addVertex(c);
		underlyingGraph.addVertex(d);
		underlyingGraph.addVertex(e);
		underlyingGraph.addEdge(t1);
		underlyingGraph.addEdge(t2);
		underlyingGraph.addEdge(t3);
		underlyingGraph.addEdge(t4);
		underlyingGraph.addEdge(t5);
		underlyingGraph.addEdge(t6);
		underlyingGraph.addEdge(t7);
		underlyingGraph.addEdge(t8);
		Map<Track, Set> map = new HashMap<>();
		for(Track t : Arrays.asList(underlyingGraph.getEdges()))
			map.put(t, Collections.EMPTY_SET);
		
		Set<Track> mincut  = MinCutFinder.findMinCut(new HashSet<>(Arrays.asList(underlyingGraph.getEdges())), map);
		System.err.println("mincut 5 " + mincut);
		assertEquals(mincut.size(), 2);
		Iterator<Track> it = mincut.iterator();
		Track x1 = it.next();
		Track x2 = it.next();
		assertEquals(new PairSet<>(x1, x2), new PairSet<>(t1, t2));
						
		HashSet<Track> c1 = new HashSet<>();
		HashSet<Track> c2 = new HashSet<>();
		c1.add(t3);
		c1.add(t4);
		c1.add(t5);
		c1.add(t6);
		c1.add(t7);
		c1.add(t8);
		PairSet<Set<Track>> split = MinCutSplitter.splitByMinCut(new HashSet<>(Arrays.asList(underlyingGraph.getEdges())), mincut);
		assertEquals(split, new PairSet<Set<Track>>(c1, c2));
		//Arrays.asList(underlyingGraph.getEdges())

	}
}