package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.*;

/**
 * TODO tohle napsat
 *
 * @author Petr Ryšavý
 */
public final class GreedyOrderDecider_Strong
{
	private GreedyOrderDecider_Strong()
	{
	}

	public static int orderLines(MLCMP problem, Ordering ordering, CommonSubpath path)
	{
		final Line l1 = path.getFirstLine();
		final Line l2 = path.getSecondLine();
		
		final Station start = path.getFirstStation();
		final Station end = path.getLastStation();

		final Track first = path.getFirstTrack();
		final Track last = path.getLastTrack();

//		System.err.println("l1 "+l1);
//		System.err.println("l2 "+l2);
//		System.err.println("pa "+path.toPathString());
		
		final int startPreferredOrder = getPreferredOrder(l1, l2, start, first, ordering);

		// if you don't have enough information on one side, you don't have enough
		// information at all
		if (startPreferredOrder == NOT_ENOUGH_INFORMATION)
			return NOT_ENOUGH_INFORMATION;

		final int endPreferredOrder = getPreferredOrder(l2, l1, end, last, ordering);

		if (endPreferredOrder == NOT_ENOUGH_INFORMATION)
			return NOT_ENOUGH_INFORMATION;

		// if one side doesn't matter, then it depends only on the second side
		if (startPreferredOrder == NOT_MATTER)
			return endPreferredOrder;
		if (endPreferredOrder == NOT_MATTER)
			return startPreferredOrder;

		// if preferred order is same, than it is our value
		// the only possible values are now PRECEDES or FOLLOWS
		if (startPreferredOrder == endPreferredOrder)
			return startPreferredOrder;
		
		// otherwise they are different, one is FOLLOWS and other PRECEDES
		return NOT_COMPARABLE;
	}

	public static int getPreferredOrder(Line l1, Line l2, Station s, Track t, Ordering ordering)
	{
		if (l1.isEndpoint(s) || l2.isEndpoint(s))
			return NOT_MATTER;

		// here are tracks sorted clockwise so that t is on index 0
		Track[] tracksArr = s.getIncidentTracks(t);
		//Map<Track, Integer> tracksMap = s.getIncidentTracks();

		// index of next l1 line clockwise
		int l1nextClockwise = findNextClockwise(tracksArr, l1);
		int l1nextAnticlockwise = findNextAnticlockwise(tracksArr, l1);
		int l2nextClockwise = findNextClockwise(tracksArr, l2);
		int l2nextAnticlockwise = findNextAnticlockwise(tracksArr, l2);
		
//		System.err.println("l1 "+l1);
//		System.err.println("l2 "+l2);
//		System.err.println("tr "+t);
//		System.err.println("i1 "+l1nextClockwise);
//		System.err.println("i2 "+l2nextClockwise);
//		System.err.println("t1 "+tracksArr[l1nextClockwise]);
//		System.err.println("t2 "+tracksArr[l2nextClockwise]);
//		System.err.println("c1 "+l1.containsTrack(tracksArr[l1nextClockwise]));
//		System.err.println("c2 "+l2.containsTrack(tracksArr[l1nextClockwise]));
//		System.err.println("");

		// if two lines lie on same line that you have already ordered, you can use the order
		if (l1nextClockwise == l2nextClockwise)
			switch (ordering.getOrderOfLines(l1, l2, tracksArr[l1nextClockwise], s))
			{
				case FOLLOWS:
					l1nextClockwise--;
					break;
				case PRECEDES:
					l1nextClockwise++;
					break;
			}
		// similarly anticlockwise
		if (l1nextAnticlockwise == l2nextAnticlockwise)
			switch (ordering.getOrderOfLines(l1, l2, tracksArr[l1nextAnticlockwise], s))
			{
				case FOLLOWS:
					l1nextClockwise--;
					break;
				case PRECEDES:
					l1nextClockwise++;
					break;
			}

		// follows condition
		if (l1nextClockwise < l2nextClockwise && l1nextAnticlockwise < l2nextAnticlockwise)
			return PRECEDES;
		// precedes condition
		if (l1nextClockwise > l2nextClockwise && l1nextAnticlockwise > l2nextAnticlockwise)
			return FOLLOWS;

		// not matter if there are both surrounding tracks contain same line
		if (l1nextClockwise < l2nextClockwise && l1nextAnticlockwise > l2nextAnticlockwise
			|| l1nextClockwise > l2nextClockwise && l1nextAnticlockwise < l2nextAnticlockwise)
			return NOT_MATTER;

		// we have tried all sharp inequalities hence it means that two lines share
		// same track, but they are not ordered yet
		return NOT_ENOUGH_INFORMATION;
	}

	private static int findNextClockwise(Track[] tracksArr, Line l)
	{
		for (int i = 1; i < tracksArr.length; i++)
			if (l.containsTrack(tracksArr[i]))
				return i;
		return -1;
	}

	private static int findNextAnticlockwise(Track[] tracksArr, Line l)
	{
		for (int i = tracksArr.length - 1; i > 0; i--)
			if (l.containsTrack(tracksArr[i]))
				return i;
		return -1;
	}
}
