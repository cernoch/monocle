/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.commonsubpath;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.UnderlyingGraph;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class CycleFinderTest
{
	public CycleFinderTest()
	{
	}

	@Test
	public void testSomeMethod()
	{
		for (int i = 0; i < 100; i++)
		{
			Station s1 = new Station("1", Math.random() * 10000, Math.random() * 10000);
			Station s2 = new Station("2", Math.random() * 10000, Math.random() * 10000);
			Station s3 = new Station("3", Math.random() * 10000, Math.random() * 10000);
			Station s4 = new Station("4", Math.random() * 10000, Math.random() * 10000);
			Station s5 = new Station("5", Math.random() * 10000, Math.random() * 10000);
			Station s6 = new Station("6", Math.random() * 10000, Math.random() * 10000);

			Track t1 = new Track(s1, s2);
			Track t2 = new Track(s2, s3);
			Track t3 = new Track(s3, s4);
			Track t4 = new Track(s1, s4);
			Track t5 = new Track(s6, s2);
			Track t6 = new Track(s5, s3);
			Track t7 = new Track(s2, s4);


			Set<Track> l = new HashSet<>(8);
			l.add(t1);
			l.add(t2);
			l.add(t3);
			l.add(t4);
			l.add(t5);
			l.add(t6);
			l.add(t7);

			ArrayList<Station> s = new ArrayList<>(8);
			s.add(s1);
			s.add(s2);
			s.add(s3);
			s.add(s4);
			s.add(s5);
			s.add(s6);

			UnderlyingGraph ugl = new UnderlyingGraph(s, l);
			MLCMP problem = new MLCMP(ugl, new LinkedList<Line>());

			Collection<List<Track>> resutl = CycleFinder.findCycles(l, problem);
			assertEquals(1,resutl.size());
			List<Track> resl = resutl.iterator().next();
			assertEquals(3, resl.size());
			Track a1 = resl.get(0);
			Track a2 = resl.get(1);
			Track a3 = resl.get(2);
			assertFalse(a1.equals(a2));
			assertFalse(a3.equals(a2));
			assertFalse(a1.equals(a3));
			
			Set<Track> set1 = new HashSet<>(4);
			set1.add(t7);
			set1.add(t1);
			set1.add(t4);
			
			Set<Track> set2 = new HashSet<>(4);
			set2.add(t7);
			set2.add(t3);
			set2.add(t2);
			
			assertTrue((set1.contains(a1) && set1.contains(a2) && set1.contains(a3))||
				(set2.contains(a1) && set2.contains(a2) && set2.contains(a3)));
		}
	}
}