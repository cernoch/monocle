/*
 */

package cz.cvut.fel.rysavpe1.mlcmp.commonsubpath;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class CommonSubpathTest {

    public CommonSubpathTest() {
    }

	@Test
	public void testEquals()
	{
		@SuppressWarnings("unchecked")
		Line l1 = new Line("a line", Collections.EMPTY_LIST);
		Line l2 = new Line("b line", Collections.EMPTY_LIST);
		
		Station a = new Station("a", 0, 0);
		Station b= new Station("b", 1, 0);
		Station c = new Station("c", 2, 0);
		
		List<Track> list = new LinkedList<>();
		list.add(new Track(a, b));
		list.add(new Track(b, c));
		
		List<Track> list2 = new LinkedList<>();
		list2.add(new Track(c, b));
		list2.add(new Track(a, b));
		
		CommonSubpath cs1 = new CommonSubpath(list, l1, l2);
		//Collections.reverse(list);
		CommonSubpath cs2 = new CommonSubpath(list2, l2, l1);
		CommonSubpath cs3 = new CommonSubpath(list2, l1, l2);
		CommonSubpath cs4 = new CommonSubpath(list, l2, l1);
		
		Assert.assertEquals(cs1, cs2);
		Assert.assertEquals(cs1, cs3);
		Assert.assertEquals(cs1, cs4);
		Assert.assertEquals(cs2, cs3);
		Assert.assertEquals(cs2, cs4);
		Assert.assertEquals(cs3, cs4);
	}

	@Test
	public void testHashCode()
	{
		@SuppressWarnings("unchecked")
		Line l1 = new Line("a line", Collections.EMPTY_LIST);
		Line l2 = new Line("b line", Collections.EMPTY_LIST);
		
		Station a = new Station("a", 0, 0);
		Station b= new Station("b", 1, 0);
		Station c = new Station("c", 2, 0);
		
		List<Track> list = new LinkedList<>();
		list.add(new Track(a, b));
		list.add(new Track(b, c));
		
		List<Track> list2 = new LinkedList<>();
		list2.add(new Track(c, b));
		list2.add(new Track(a, b));
		
		CommonSubpath cs1 = new CommonSubpath(list, l1, l2);
		Collections.reverse(list);
		CommonSubpath cs2 = new CommonSubpath(list2, l2, l1);
		CommonSubpath cs3 = new CommonSubpath(list2, l1, l2);
		CommonSubpath cs4 = new CommonSubpath(list, l2, l1);
		
		Assert.assertEquals(cs1.hashCode(), cs2.hashCode());
		Assert.assertEquals(cs1.hashCode(), cs2.hashCode());
		Assert.assertEquals(cs1.hashCode(), cs3.hashCode());
		Assert.assertEquals(cs1.hashCode(), cs4.hashCode());
		Assert.assertEquals(cs2.hashCode(), cs3.hashCode());
		Assert.assertEquals(cs2.hashCode(), cs2.hashCode());
		Assert.assertEquals(cs2.hashCode(), cs4.hashCode());
		Assert.assertEquals(cs3.hashCode(), cs4.hashCode());
	}
	
	@Test
	public void testHashCode2()
	{
		@SuppressWarnings("unchecked")
		Line l1 = new Line("a line", Collections.EMPTY_LIST);
		Line l2 = new Line("b line", Collections.EMPTY_LIST);
		
		Station a = new Station("a", 0, 0);
		Station b= new Station("b", 1, 0);
		Station c = new Station("c", 2, 0);
		
		List<Track> list = new LinkedList<>();
		list.add(new Track(b, c));
		list.add(new Track(b, a));
		
		List<Track> list2 = new LinkedList<>();
		list2.add(new Track(b, a));
		list2.add(new Track(b, c));
		
		CommonSubpath cs1 = new CommonSubpath(list, l1, l2);
		//Collections.reverse(list);
		CommonSubpath cs2 = new CommonSubpath(list2, l2, l1);
		CommonSubpath cs3 = new CommonSubpath(list2, l1, l2);
		CommonSubpath cs4 = new CommonSubpath(list, l2, l1);
		
		Assert.assertEquals(cs1.hashCode(), cs2.hashCode());
		Assert.assertEquals(cs1.hashCode(), cs2.hashCode());
		Assert.assertEquals(cs1.hashCode(), cs3.hashCode());
		Assert.assertEquals(cs1.hashCode(), cs4.hashCode());
		Assert.assertEquals(cs2.hashCode(), cs3.hashCode());
		Assert.assertEquals(cs2.hashCode(), cs2.hashCode());
		Assert.assertEquals(cs2.hashCode(), cs4.hashCode());
		Assert.assertEquals(cs3.hashCode(), cs4.hashCode());
	}

	
	@Test
	public void testEquals2()
	{
		@SuppressWarnings("unchecked")
		Line l1 = new Line("a line", Collections.EMPTY_LIST);
		Line l2 = new Line("b line", Collections.EMPTY_LIST);
		
		Station a = new Station("a", 0, 0);
		Station b= new Station("b", 1, 0);
		Station c = new Station("c", 2, 0);
		
		List<Track> list = new LinkedList<>();
		list.add(new Track(b, c));
		list.add(new Track(b, a));
		
		List<Track> list2 = new LinkedList<>();
		list2.add(new Track(b, a));
		list2.add(new Track(b, c));
		
		CommonSubpath cs1 = new CommonSubpath(list, l1, l2);
		//Collections.reverse(list);
		CommonSubpath cs2 = new CommonSubpath(list2, l2, l1);
		CommonSubpath cs3 = new CommonSubpath(list2, l1, l2);
		CommonSubpath cs4 = new CommonSubpath(list, l2, l1);
		
		cs1.toPathString();
		
		Assert.assertEquals(cs1, cs2);
		Assert.assertEquals(cs1, cs2);
		Assert.assertEquals(cs1, cs3);
		Assert.assertEquals(cs1, cs4);
		Assert.assertEquals(cs2, cs3);
		Assert.assertEquals(cs2, cs2);
		Assert.assertEquals(cs2, cs4);
		Assert.assertEquals(cs3, cs4);
	}

	
	@Test
	public void cycleTest()
	{
		@SuppressWarnings("unchecked")
		Line l1 = new Line("a line", Collections.EMPTY_LIST);
		Line l2 = new Line("b line", Collections.EMPTY_LIST);
		
		Station a = new Station("a", 0, 0);
		Station b= new Station("b", 1, 0);
		Station c = new Station("c", 2, 0);
		Station d = new Station("d", 3, 0);
		
		List<Track> list = new LinkedList<>();
		list.add(new Track(c, a));
		list.add(new Track(b, c));
		list.add(new Track(b, a));
		
		List<Track> list2 = new LinkedList<>();
		list2.add(new Track(b, a));
		list2.add(new Track(b, d));
		list2.add(new Track(d, a));
		
		CommonSubpath cs1 = new CommonSubpath(list, l1, l2);
		//Collections.reverse(list);
		CommonSubpath cs2 = new CommonSubpath(list2, l2, l1);
		CommonSubpath cs3 = new CommonSubpath(list2, l1, l2);
		CommonSubpath cs4 = new CommonSubpath(list, l2, l1);
		
		cs1.toPathString();
		
		Assert.assertFalse(cs1.equals(cs2));
		Assert.assertFalse(cs1.equals(cs3));
		Assert.assertFalse(cs2.equals(cs4));
		Assert.assertFalse(cs3.equals(cs4));
	}
	
	@Test
	public void cycleTest2()
	{
		@SuppressWarnings("unchecked")
		Line l1 = new Line("a line", Collections.EMPTY_LIST);
		Line l2 = new Line("b line", Collections.EMPTY_LIST);
		
		Station a = new Station("a", 0, 0);
		Station b= new Station("b", 1, 0);
		Station c = new Station("c", 2, 0);
		
		List<Track> list = new LinkedList<>();
		list.add(new Track(c, a));
		list.add(new Track(b, c));
		list.add(new Track(b, a));
		
		List<Track> list2 = new LinkedList<>();
		list2.add(new Track(b, a));
		list2.add(new Track(b, c));
		list2.add(new Track(c, a));
		
		CommonSubpath cs1 = new CommonSubpath(list, l1, l2);
		//Collections.reverse(list);
		CommonSubpath cs2 = new CommonSubpath(list2, l2, l1);
		CommonSubpath cs3 = new CommonSubpath(list2, l1, l2);
		CommonSubpath cs4 = new CommonSubpath(list, l2, l1);
		
		cs1.toPathString();
		
		Assert.assertEquals(cs1, cs2);
		Assert.assertEquals(cs1, cs2);
		Assert.assertEquals(cs1, cs3);
		Assert.assertEquals(cs1, cs4);
		Assert.assertEquals(cs2, cs3);
		Assert.assertEquals(cs2, cs2);
		Assert.assertEquals(cs2, cs4);
		Assert.assertEquals(cs3, cs4);
	}

}