/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.commonsubpath;

import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

import static cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpathFinderTest.problem;

/**
 *
 * @author Petr Ryšavý
 */
public class ExtendedCommonSubpathFinderTest
{
	static MLCMP problem;

	@BeforeClass
	public static void beforeClass() throws FileNotFoundException, IOException, GraphReadingException
	{
		BufferedReader in = new BufferedReader(new FileReader("test/graph2.in"));
		PlainTextGraphReader instance = new PlainTextGraphReader(new OutputStreamWriter(System.err));
		problem = instance.readGraph(in);
		//problem.printToStream(new OutputStreamWriter(System.out));
	}

	@Test
	public void testFindCommonSubpath1()
	{
		System.err.println("test1");
		Line l1 = problem.getLine("orange");
		Line l2 = problem.getLine("black");
		Track start = problem.getStoredTrack(new Track(new Station(3, 3), new Station(3, 2)));
		CommonSubpath result = CommonSubpathFinder.findCommonSubpath(problem, l1, l2, start);
		ExtendedCommonSubpath res = ExtendedCommonSubpathFinder.findCommonSubpath(problem, l1, l2, result);
		if (res != null)
		{
			for (CommonSubpath cs : res)
				System.err.println(cs.toPathString());
			for (Track t : new IteratorWrapper<>(res.trackIterator()))
				System.err.println("track " + t);
			for (Station s : new IteratorWrapper<>(res.stationIterator()))
				System.err.println("station " + s);
		}
		else
			System.err.println("null");
		System.err.println("res first line  "+res.getPreferredLine());
	}

	@Test
	public void testFindCommonSubpath2()
	{
		System.err.println("test2");
		Line l1 = problem.getLine("blue");
		Line l2 = problem.getLine("orange");
		Track start = problem.getStoredTrack(new Track(new Station(3, 3), new Station(3, 4)));
		CommonSubpath result = CommonSubpathFinder.findCommonSubpath(problem, l1, l2, start);
		System.err.println("result " + result.toPathString());
		ExtendedCommonSubpath res = ExtendedCommonSubpathFinder.findCommonSubpath(problem, l1, l2, result);
		if (res != null)
		{
			for (CommonSubpath cs : res)
				System.err.println(cs.toPathString());
			for (Track t : new IteratorWrapper<>(res.trackIterator()))
				System.err.println("track " + t);
			for (Station s : new IteratorWrapper<>(res.stationIterator()))
				System.err.println("station " + s);
		}
		else
			System.err.println("null");
		System.err.println("res first line  "+res.getPreferredLine());
	}
}