/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.commonsubpath;

import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Petr Ryšavý
 */
public class CommonSubpathFinderTest
{
	static MLCMP problem;

	public CommonSubpathFinderTest()
	{
	}

	@BeforeClass
	public static void beforeClass() throws FileNotFoundException, IOException, GraphReadingException
	{
		BufferedReader in = new BufferedReader(new FileReader("test/graph2.in"));
		PlainTextGraphReader instance = new PlainTextGraphReader(new OutputStreamWriter(System.err));
		problem = instance.readGraph(in);
		//problem.printToStream(new OutputStreamWriter(System.out));
	}

	@Test
	public void testFindCommonSubpath1()
	{
		Line l1 = problem.getLine("blue");
		Line l2 = problem.getLine("orange");
		Track start = problem.getStoredTrack(new Track(new Station(3, 3), new Station(3, 4)));
		CommonSubpath result = CommonSubpathFinder.findCommonSubpath(problem, l1, l2, start);
		final String res = result.toPathString();
		System.err.println("case 1");
		System.err.println(res);
		final String path = "(I)-(J)-(K)";
		assertTrue(res.equals(path) || res.equals(reversePath(path)));
	}

	@Test
	public void testFindCommonSubpath2()
	{
		Line l1 = problem.getLine("black");
		Line l2 = problem.getLine("orange");
		Track start = problem.getStoredTrack(new Track(new Station(4, 3), new Station(5, 3)));
		CommonSubpath result = CommonSubpathFinder.findCommonSubpath(problem, l1, l2, start);
		final String res = result.toPathString();
		System.err.println("case 2");
		System.err.println(res);
		final String path = "(I)-(M)-(N)-(O)-(P)";
		assertTrue(res.equals(path) || res.equals(reversePath(path)));
	}

	@Test
	public void testFindCommonSubpath3()
	{
		Line l1 = problem.getLine("black");
		Line l2 = problem.getLine("orange");
		Track start = problem.getStoredTrack(new Track(new Station(2, 1), new Station(3, 1)));
		CommonSubpath result = CommonSubpathFinder.findCommonSubpath(problem, l1, l2, start);
		final String res = result.toPathString();
		System.err.println("case 3");
		System.err.println(res);
		final String path = "(G)-(A)-(B)-(C)";
		assertTrue(res.equals(path) || res.equals(reversePath(path)));
	}

	private static String reversePath(String path)
	{
		char[] orig = path.toCharArray();
		char[] reversed = new char[orig.length];

		for (int i = 0, j = reversed.length - 1; i < reversed.length; i++, j--)
			if (orig[i] == '(')
				reversed[j] = ')';
			else if (orig[i] == ')')
				reversed[j] = '(';
			else
				reversed[j] = orig[i];
		
		return new String(reversed);
	}
}