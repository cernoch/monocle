/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpathFinder;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import static cz.cvut.fel.rysavpe1.mlcmp.order.GreedyOrderDecider.findNextClockwise;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import java.util.Collection;
import java.util.HashSet;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.PRECEDES;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.FOLLOWS;

/**
 * The class used for building the dependency graph. It finds dependencies for one
 * particular path.
 *
 * @author Petr Ryšavý
 */
public class DependencyFinder
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private DependencyFinder()
	{
	}

	/**
	 * Find common subpaths on that the lines shall be ordered to find the order.
	 *
	 * @param problem  The MLCMP problem instance.
	 * @param ordering Already known ordering.
	 * @param path     The common subpath.
	 * @return List of dependencies.
	 */
	public static Collection<CommonSubpath> getDependency(MLCMP problem, Ordering ordering, CommonSubpath path)
	{
		final Line l1 = path.getFirstLine();
		final Line l2 = path.getSecondLine();

		final Station start = path.getFirstStation();
		final Station end = path.getLastStation();

		final Track first = path.getFirstTrack();
		final Track last = path.getLastTrack();

		final HashSet<CommonSubpath> set = new HashSet<>();
		findDependencies(problem, l1, l2, start, first, ordering, set);
		findDependencies(problem, l2, l1, end, last, ordering, set);

		return set;
	}

	/**
	 * Finds dependencies on one endpoint of a common subpath.
	 *
	 * @param problem  The problem instance.
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param s        The station to search.
	 * @param t        The track of common subpath incident to {@literal s}.
	 * @param ordering The ordering of lines that is known.
	 * @param set     The list where the subpaths thats order has to be known is
	 *                 stored.
	 */
	private static void findDependencies(MLCMP problem, Line l1, Line l2, Station s, Track t, Ordering ordering, HashSet<CommonSubpath> set)
	{
		if (l1.isEndpoint(s) || l2.isEndpoint(s))
			return;
		
		if(ordering.areLinesCompared(l1, l2, t))
			return;

		// here are tracks sorted clockwise so that t is on index 0
		final Track[] tracksArr = s.getIncidentTracks(t);

		final int l1nextClockwise = findNextClockwise(tracksArr, l1);
		final int l1nextAnticlockwise = GreedyOrderDecider.findNextAnticlockwise(tracksArr, l1);
		final int l2nextClockwise = findNextClockwise(tracksArr, l2);
		final int l2nextAnticlockwise = GreedyOrderDecider.findNextAnticlockwise(tracksArr, l2);

		// if two lines lie on same line that you have already ordered, you can use the order
		if (l1nextClockwise == l2nextClockwise)
			switch (ordering.getOrderOfLines(l1, l2, tracksArr[l1nextClockwise], s))
			{
				case FOLLOWS:
				case PRECEDES:
					break;
				default:
					set.add(CommonSubpathFinder.findCommonSubpath(problem, l1, l2, tracksArr[l1nextClockwise]));
			}
		// similarly anticlockwise,
		// but don't add the dependency twice to the same list - that will happen if
		// l1nextClockwise === l2nextAnticlockwise
		if (l1nextAnticlockwise == l2nextAnticlockwise)
			switch (ordering.getOrderOfLines(l1, l2, tracksArr[l1nextAnticlockwise], s))
			{
				case FOLLOWS:
				case PRECEDES:
					break;
				default:
					set.add(CommonSubpathFinder.findCommonSubpath(problem, l1, l2, tracksArr[l1nextAnticlockwise]));
			}
	}
}
