/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order.np;

import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.utilities.MathUtils;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import cz.cvut.fel.rysavpe1.utilities.RandomAccessMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A class that finds a mincut of a graph. Karger's algorith is a random contraction
 * algorithm for searching the mincut. It is a heuristics.
 *
 * @author Petr Ryšavý
 * @see http://en.wikipedia.org/wiki/Karger%27s_algorithm
 */
public final class KargerAlgorithm
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private KargerAlgorithm()
	{
	}

	/**
	 * Finds a random cut.
	 *
	 * @param graph The graph to cut.
	 * @return The minimal cut.
	 */
	public static Set<Track> contract(Set<Track> graph)
	{
		final int size = MathUtils.pow(graph.size(), 2);
		final Map<Station, Set<Track>> stationMap = new HashMap<>(size * 2);
		final RandomAccessMap<Track, PairSet<Station>> tracksMap = new RandomAccessMap<>(size);

		for (Track t : graph)
		{
			addTrackToStationsMap(stationMap, t);
			tracksMap.put(t, new PairSet<>(t.getVertex1(), t.getVertex2()));
		}

		while (stationMap.size() > 2)
		{
			final Track t = tracksMap.get(MathUtils.random(tracksMap.size() - 1));
			contractTrack(stationMap, tracksMap, t);
		}
		// stationsMap contains only two stations and tracksMap contains the tracks
		// that go between these two stations that represent the mincut
		return tracksMap.keySet();
	}

	/**
	 * Contracst the given track.
	 *
	 * @param stationMap Map from station to set of incident tracks.
	 * @param tracksMap  Map from track to pair of endpoints.
	 * @param t          The track to contract.
	 */
	private static void contractTrack(Map<Station, Set<Track>> stationMap,
		RandomAccessMap<Track, PairSet<Station>> tracksMap,
		Track t)
	{
		final PairSet<Station> trackEnds = tracksMap.remove(t);
		final Station s1 = trackEnds.getValue1();
		final Station s2 = trackEnds.getValue2();

		// now s1 and s2 will be merged into station s1
		// iterate the tracks that go from station s2
		final Set<Track> s1Tracks = stationMap.get(s1);
		for (Track x : stationMap.remove(s2)) // remove s2 from stationMap
			// if track goes between s1 and s2, than it will be ommited
			if (s1Tracks.remove(x))
				tracksMap.remove(x);
			// else it goes from s2 to somewhere else - reconnect the track to s1
			else
			{
				s1Tracks.add(x);
				final PairSet<Station> old = tracksMap.get(x);
				tracksMap.put(x, new PairSet<>(s1, old.getOtherValue(s2)));
			}
	}

	/**
	 * Adds a track to the set of tracks incident with given station.
	 *
	 * @param stationMap The station map.
	 * @param t          The track to add.
	 */
	private static void addTrackToStationsMap(Map<Station, Set<Track>> stationMap, Track t)
	{
		addTrackToStationMap(stationMap, t, t.getVertex1());
		addTrackToStationMap(stationMap, t, t.getVertex2());
	}

	/**
	 * Adds a track to the set of tracks incident with given station.
	 *
	 * @param stationMap The station map.
	 * @param t          Track to add.
	 * @param s          Station where the track shall be added.
	 */
	private static void addTrackToStationMap(Map<Station, Set<Track>> stationMap, Track t, Station s)
	{
		if (!stationMap.containsKey(s))
		{
			final HashSet<Track> set = new HashSet<>();
			set.add(t);
			stationMap.put(s, set);
		}
		else
			stationMap.get(s).add(t);
	}
}