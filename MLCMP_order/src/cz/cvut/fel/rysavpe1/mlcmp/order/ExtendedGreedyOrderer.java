/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.ExtendedCommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.ExtendedCommonSubpathFinder;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Library class that allows to decide the order on the extended common subpath.
 *
 *
 * @author Petr Ryšavý
 */
public class ExtendedGreedyOrderer
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private ExtendedGreedyOrderer()
	{
	}

	/**
	 * Finds the extended common subpaths and then tries to decide the order.
	 *
	 * @param problem      The problem.
	 * @param ordering     The ordering.
	 * @param notSolvedMap The map of not solved common subpaths with preferred order
	 *                     different from precedes or follows.
	 * @return New version of {@code notSolvedMap}.
	 */
	public static Map<CommonSubpath, Integer> order(MLCMP problem, Ordering ordering,
													Map<CommonSubpath, Integer> notSolvedMap)
	{
		final HashMap<CommonSubpath, Integer> replaceMap = new HashMap<>(notSolvedMap.size());
		for (Entry<CommonSubpath, Integer> e : Collections.unmodifiableSet(notSolvedMap.entrySet()))
			if (e.getValue() == TrackLineOrder.NOT_ENOUGH_INFORMATION && !replaceMap.containsKey(e.getKey()))
			{
				CommonSubpath cs = e.getKey();
				ExtendedCommonSubpath ecs = ExtendedCommonSubpathFinder.findCommonSubpath(problem, cs.getFirstLine(), cs.getSecondLine(), cs);
				if (ecs != null)
					ExtendedGreedyOrderDecider.order(problem, ordering, replaceMap, ecs);
			}

		for (Entry<CommonSubpath, Integer> e : replaceMap.entrySet())
		{
			final CommonSubpath key = e.getKey();
			final Integer val = e.getValue();

			if (notSolvedMap.containsKey(key))
				if (val == TrackLineOrder.FOLLOWS || val == TrackLineOrder.PRECEDES)
					notSolvedMap.remove(key);
				else
					notSolvedMap.put(key, val);
		}

		return replaceMap;
	}
}
