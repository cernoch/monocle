/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.graphalgos.KahnAlgorithm;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.SimpleDirectedGraph;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.Vertex;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import cz.cvut.fel.rysavpe1.utilities.ValueIterator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.*;
import java.util.Collections;

/**
 * Resolves the dependency graph.
 *
 * @author Petr Ryšavý
 */
public class NotEnoughInformationResolver
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private NotEnoughInformationResolver()
	{
	}

	/**
	 * Resolves the dependency graphs. Tries to find the topological ordering of the
	 * dependency graph and then solve the common subpaths in given order.
	 *
	 * @param problem      The problem.
	 * @param ordering     The line ordering.
	 * @param notSolvedMap Map of not solved common subpaths.
	 * @param dependencies List of weakly connected dependecy graphs for each pair of
	 *                     lines.
	 * @return New map of unsolved common subpaths.
	 */
	public static Map<CommonSubpath, Integer> resolve(MLCMP problem, Ordering ordering,
		Map<CommonSubpath, Integer> notSolvedMap,
		HashMap<PairSet<Line>, List<SimpleDirectedGraph<CommonSubpath>>> dependencies)
	{
		for (List<SimpleDirectedGraph<CommonSubpath>> graphList : dependencies.values())
			for (SimpleDirectedGraph<CommonSubpath> dependencyGraph : graphList)
			{
				// try to find the topological sort of the graph
				List<Vertex<CommonSubpath>> topSort = KahnAlgorithm.sort(dependencyGraph);

				if (topSort == null)
				{
					resolveGraphWithCycle(problem, ordering, notSolvedMap, dependencyGraph);
					continue;
				}
				// note that we are interested in reversed topological order -
				// if a depends on b, then there is an edge from a to b and a is before
				// a node - we need the order hava reversed
				Collections.reverse(topSort);

				// we have the topological sort, so use it
				for (CommonSubpath cs : new ValueIterator<>(topSort.iterator()))
				{
					// this common subpath is already solved
					if (!notSolvedMap.containsKey(cs) || notSolvedMap.get(cs) != NOT_ENOUGH_INFORMATION)
						// the common subpath is oriented or NOT_MATTER or NOT_COMPARABLE
						continue;

					// the track is not solved so do that
					final int order = GreedyOrderDecider.orderLines(problem, ordering, cs);
					ordering.setOrderOfLinesIfPossible(cs.getFirstLine(), cs.getSecondLine(), order, cs, new IteratorWrapper<>(cs.stationsIterator()));
					if (order == FOLLOWS || order == PRECEDES)
						notSolvedMap.remove(cs);
					else
						notSolvedMap.put(cs, order);
				}
			}

		return notSolvedMap;
	}

	/**
	 * Tries to resolve a graph with dependency cycle. At the moment runs only five
	 * restarts. The better way will be finding of sink vertices.
	 *
	 * @param problem      The problem.
	 * @param ordering     The line ordering.
	 * @param notSolvedMap Map of not solved common subpaths.
	 * @param graph        An dependency graph that contain a cycle.
	 */
	private static void resolveGraphWithCycle(MLCMP problem, Ordering ordering,
		Map<CommonSubpath, Integer> notSolvedMap,
		SimpleDirectedGraph<CommonSubpath> graph)
	{
		for (int i = 0; i < 5; i++)
			for (Vertex<CommonSubpath> v : new IteratorWrapper<>(graph.vertexIterator()))
				if (graph.getOutDegree(v) == i)
				{
					final CommonSubpath cs = v.getValue();

					if (!notSolvedMap.containsKey(cs) || notSolvedMap.get(cs) != NOT_ENOUGH_INFORMATION)
						continue;

					final int order = GreedyOrderDecider.orderLines(problem, ordering, cs);
					ordering.setOrderOfLinesIfPossible(cs.getFirstLine(), cs.getSecondLine(), order, cs, new IteratorWrapper<>(cs.stationsIterator()));
					if (order == FOLLOWS || order == PRECEDES)
						notSolvedMap.remove(cs);
					else
						notSolvedMap.put(cs, order);
				}
	}
}
