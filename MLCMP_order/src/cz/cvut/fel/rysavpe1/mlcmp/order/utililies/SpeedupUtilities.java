/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order.utililies;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.order.GreedyOrderDecider;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import cz.cvut.fel.rysavpe1.mlcmp.utilities.ModelUtilities;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Library class that allows to calculate some values that can probably speedup the
 * program.
 */
public class SpeedupUtilities
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private SpeedupUtilities()
	{
	}

	/**
	 * Returns the lower bound of crossings of given tracks on the given component.
	 *
	 * @param map       The map of tracks to their preferred order.
	 * @param component The component of tracks that we are solving.
	 * @return The lower bound of crossings.
	 */
	public static int getCrossingsLowerBoundOnComponent(Map<CommonSubpath, Integer> map, Set<Track> component)
	{
		int retval = 0;
		for (Entry<CommonSubpath, Integer> e : map.entrySet())
			if (e.getValue() == TrackLineOrder.NOT_COMPARABLE && isWholeInComponent(e.getKey(), component))
				retval++;

		return retval;
	}

	/**
	 * Returns a lower bound for given set of common subpath preferred order.
	 *
	 * @param c The common subpath preferred orders.
	 * @return Number of not comparable tracks in this order.
	 */
	public static int getCrossingsLowerBound(Collection<Integer> c)
	{
		int retval = 0;
		for (Integer i : c)
			if (i == TrackLineOrder.NOT_COMPARABLE)
				retval++;
		return retval;
	}

	/**
	 * Decides whether the particular common subpath is whole part of the component.
	 *
	 * @param cs        The common subpath.
	 * @param component The component.
	 * @return {@literal true} if all tracks of {@literal cs} lie in the component.
	 */
	private static boolean isWholeInComponent(CommonSubpath cs, Set<Track> component)
	{
		for (Track t : cs)
			if (!component.contains(t))
				return false;
		return true;
	}

	/**
	 * Returns more precise lower bound of crossings of given tracks on the given
	 * component.
	 *
	 * @param map       The map of tracks to their preferred order.
	 * @param component The component of tracks that we are solving.
	 * @return The lower bound of crossings.
	 */
	public static int getCrossingsLowerBoundOnComponent(Map<Track, List<CommonSubpath>> csMap,
		Map<CommonSubpath, Integer> map,
		Set<Track> component,
		Ordering ord, MLCMP problem)
	{
//		return getCrossingsLowerBound(map.values());
		Set<Station> stations = ModelUtilities.incidentStations(component);

		int retval = 0;
		// resolve all final orders on the given stations
		for (Station s : stations)
			if (ord.isTotallyOrdered(s))
				retval += CrossingsCounter.countCrossings(ord.getFinalizedOrder(s));
			else
			{
				int debug = 0;
				List<Line> lines = new ArrayList<>(s.countIncidentTracks() * problem.getLinesNum());
				HashSet<PairSet<Line>> nmPairs = new HashSet<>(problem.getLinesNum() * 4);
				for (Track t : new IteratorWrapper<>(s.incidenTracksIterator(true)))
					if (ord.isTotallyOrdered(t))
						for (Line l : new IteratorWrapper<>(ord.finalizedOrderIterator(t, s)))
							lines.add(l);
					else
					{
						// try to handle ends whose preferred order is notMatter
						Line[] tlines = problem.getLineArray(t);
						for (int i = 0; i < tlines.length; i++)
							for (int j = i + 1; j < tlines.length; j++)
								if (!tlines[i].isInline(s) || !tlines[j].isInline(s))
									if (GreedyOrderDecider.getPreferredOrder(tlines[i], tlines[j], s, t, ord) == TrackLineOrder.NOT_MATTER)
									// for each not matter end count once and remember the pair of lines
									// we would then substract one for each of the pair of lines
//									if(getCrossingsLowerBoundOnNotMatterEnd(s, stations, ord, tlines[i], tlines[j]) == 1)
									{
										retval++;
										debug++;
										nmPairs.add(new PairSet<>(tlines[i], tlines[j]));
									}
					}

				Line[] linesA = new Line[lines.size()];
				retval += CrossingsCounter.countCrossings(lines.toArray(linesA));

				// now there shall be -1 for each pair of lines unless we substracted it using countCrossings
				HashSet<Line> linesS = new HashSet<>(lines);
				linesA = new Line[linesS.size()];
				linesS.toArray(linesA);
				for (int i = 0; i < linesA.length; i++)
					for (int j = i + 1; j < linesA.length; j++)
						nmPairs.remove(new PairSet<>(linesA[i], linesA[j]));
//				System.err.println("lines " + lines);
//				System.err.println("nm " + nmPairs);
//				System.err.println("debug " + debug + " nms " + nmPairs.size());
				retval -= nmPairs.size();
			}

		// now add the not comparable pairs
		retval += getCrossingsLowerBound(map.values());

		// tohle je asi nesmysl takhle počítat, můžu zapomenout odečíst jednotku
//		for (Entry<CommonSubpath, Integer> e : map.entrySet())
//		{
//			int order = e.getValue();
//			CommonSubpath cs = e.getKey();
//			if(order == TrackLineOrder.NOT_MATTER)
//			{
//				retval += getCrossingsLowerBoundOnNotMatterEnd(cs.getFirstStation(), stations, ord, cs);
//				retval += getCrossingsLowerBoundOnNotMatterEnd(cs.getLastStation(), stations, ord, cs);
//			}
//			if (order == TrackLineOrder.NOT_COMPARABLE && isWholeInComponent(cs, component))
//				retval++;
//			else if(order == TrackLineOrder.NOT_ENOUGH_INFORMATION)
//			{
//				final Line l1 = cs.getFirstLine();
//				final Line l2 = cs.getSecondLine();
//				final Station firstStation = cs.getFirstStation();
//				final Station lastStation = cs.getLastStation();
//				if(GreedyOrderDecider.getPreferredOrder(l1, l2, firstStation, cs.getFirstTrack(), ord) == TrackLineOrder.NOT_MATTER)
//					retval += getCrossingsLowerBoundOnNotMatterEnd(firstStation, stations, ord, cs);
//				if(GreedyOrderDecider.getPreferredOrder(l1, l2, lastStation, cs.getLastTrack(), ord) == TrackLineOrder.NOT_MATTER)
//					retval += getCrossingsLowerBoundOnNotMatterEnd(firstStation, stations, ord, cs);
//			}
//		}

		return retval;
	}
//
//	private static int getCrossingsLowerBoundOnNotMatterEnd(Station s, Set<Station> stations, Ordering ord, Line l1, Line l2)
//	{
//		if (!stations.contains(s))
//			return 0;
//		// this is bad idea - we would get -1 for this pair
//		if (l1.isEndpoint(s) || l2.isEndpoint(s))
//			return 0;
//		else
//			return 1;
//	}

	public static int getCrossingsLowerBoundOnComponent(Map<CommonSubpath, Integer> map,
		Set<Track> component,
		Ordering ord, MLCMP problem)
	{
		int retval = 0;

		Set<Station> stations = ModelUtilities.incidentStations(component);
		for (Station s : stations)
		{
			Set<Line> slset = problem.getLineSet(s);
			Line[] slA = new Line[slset.size()];
			slset.toArray(slA);

			for (int i = 0; i < slA.length; i++)
				for (int j = i + 1; j < slA.length; j++)
					retval += getCrossingsLowerBound(slA[i], slA[j], s, ord);
		}

		retval += getCrossingsLowerBoundOnComponent(map, component);
		return retval;
	}

	private static int getCrossingsLowerBound(Line l1, Line l2, Station s, Ordering ord)
	{
		int count = 0;

		// at first find the array with lines order, order them in convenient order in
		// such way that first is a definite line

		// line l1 in array stands for l1, same for l2 and null stands for pair of lines l1
		// and l2 whose order is not known
		// int the array the first will be always l1 (if not, we change l1 and l2 names locally or rotate the array)
		ArrayList<Line> al = new ArrayList<>(s.countIncidentTracks() * 2);
		for (Track t : new IteratorWrapper<>(s.incidenTracksIterator(true)))
			if (l1.containsTrack(t))
			{
				// contains both
				if (l2.containsTrack(t))
				{
					final int order = ord.getOrderOfLines(l1, l2, t, s);
					if (order == TrackLineOrder.PRECEDES)
					{
						al.add(l2);
						al.add(l1);
					}
					else if (order == TrackLineOrder.FOLLOWS)
					{
						al.add(l1);
						al.add(l2);
					}
					else
						al.add(null);
				}
				// only l1
				else
					al.add(l1);
			}
			// only l2
			else if (l2.containsTrack(t))
				al.add(l2);

		int first = -1;
		final int alsize = al.size();

		///boolean swap = false;

		for (int i = 0; i < alsize; i++)
		{
			Line l = al.get(i);
			if (l != null)
			{
				//	swap = l == l2;
				first = i;
				break;
			}
		}

		// if there are only nulls in the list, return corresponding value
		if (first == -1)
			return (alsize - 1) / 2;

		int nulls = 0;
		for (Line l : al)
			if (l == null)
				nulls++;

		Line[] bestOrder = new Line[alsize + nulls];

		Line last = null;
		int j = 0;
		for (int i = first; i < alsize; i++)
		{
			final Line ithline = al.get(i);
			if (ithline != null)
				last = bestOrder[j++] = ithline;
			else
			{
				bestOrder[j] = last;
				last = bestOrder[j + 1] = last == l1 ? l2 : l1;
				j += 2;
			}
		}
		for (; j < bestOrder.length; j += 2)
		{
			bestOrder[j] = last;
			last = bestOrder[j+1] = last == l1 ? l2 : l1;
		}
		
//		System.err.println("al "+al);
//		System.err.println("bo "+Arrays.toString(bestOrder));

		return CrossingsCounter.countCrossings(bestOrder);
	}
}