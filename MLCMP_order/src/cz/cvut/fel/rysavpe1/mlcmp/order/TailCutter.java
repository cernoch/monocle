/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.OrderIterator;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This class is used for cutting tails of common subpath. This tails can be found
 * when we already know the order of two edges on some edges of common subpath, but
 * this knowledge is not distributed on whole subpath. This may happen for example
 * when we have NOT_COMPARABLE order. We don't know where to cross the lines, but
 * there is another line that is in optimal solution between them (fist line goes up,
 * second down and this enters between them and then ends on fommon subpath of first
 * two lines) and that prohibites the lines to cross on the beginning of the common
 * subpath.
 *
 * @author Petr Ryšavý
 */
public class TailCutter
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private TailCutter()
	{
	}

	/**
	 * Cuts the tails on problem.
	 *
	 * @param problem      Problem instance.
	 * @param ordering     The ordering of lines already known.
	 * @param notSolvedMap Map of common subpaths that are not already solved and
	 *                     their relative order.
	 */
	public static void cutTails(MLCMP problem, Ordering ordering, Map<CommonSubpath, Integer> notSolvedMap)
	{
		for (Iterator<Entry<CommonSubpath, Integer>> it = notSolvedMap.entrySet().iterator(); it.hasNext();)
		{
			Entry<CommonSubpath, Integer> e = it.next();
			switch (e.getValue())
			{
				case TrackLineOrder.NOT_MATTER:
					if (cutNotMatterTail(e.getKey(), ordering))
						it.remove();
					break;
				case TrackLineOrder.NOT_COMPARABLE:
					if (cutNotComparableTail(e.getKey(), ordering))
						it.remove();
					break;
				case TrackLineOrder.NOT_ENOUGH_INFORMATION:
					if (cutNotEnoughInformationTail(e.getKey(), ordering))
						it.remove();
					break;
			}
		}
	}
	
	private static boolean cutNotEnoughInformationTail(CommonSubpath cs, Ordering ord)
	{
		boolean ordered = true;
		for (Track t : cs)
			ordered &= ord.isTotallyOrdered(t);
		return ordered;
	}

	/**
	 * Cuts tails on common subpath that has {@literal NOT_MATTER} order.
	 *
	 * @param cs  Common subpath.
	 * @param ord The line ordering.
	 * @return {@literal true} if the order is known and we don't need to have this
	 *         common subpath marked as unresolved.
	 */
	private static boolean cutNotMatterTail(CommonSubpath cs, Ordering ord)
	{
		final Line l1 = cs.getFirstLine();
		final Line l2 = cs.getSecondLine();
		for (int i : new IteratorWrapper<>(new OrderIterator(l1, l2, cs, cs.stationsIterator(), ord)))
		{
			if (i == TrackLineOrder.FOLLOWS || i == TrackLineOrder.PRECEDES)
			{
				ord.setOrderOfLinesIfPossible(l1, l2, i, cs, new IteratorWrapper<>(cs.stationsIterator()));
				return true;
			}
		}
		return false;
	}

	/**
	 * Cuts tails on common subpath that has {@literal NOT_COMPARABLE} order.
	 *
	 * @param cs  Common subpath.
	 * @param ord The line ordering.
	 * @return {@literal true} if the order is known and we don't need to have this
	 *         common subpath marked as unresolved.
	 */
	private static boolean cutNotComparableTail(CommonSubpath cs, Ordering ord)
	{
		final int beginning = GreedyOrderDecider.getPreferredOrder(cs.getFirstLine(), cs.getSecondLine(), cs.getFirstStation(), cs.getFirstTrack(), ord);
		final int startCut = cutTail(cs, ord, beginning);
		final CommonSubpath reversed = cs.reverse();
		final int end = GreedyOrderDecider.getPreferredOrder(reversed.getFirstLine(), reversed.getSecondLine(), reversed.getFirstStation(), reversed.getFirstTrack(), ord);
		final int endCut = cutTail(reversed, ord, end);

		if (startCut + endCut == cs.size())
			return true;
		return false;
	}

	/**
	 * Cuts one tail on common subpath that has {@literal NOT_COMPARABLE} order.
	 *
	 * @param cs    Common subpath.
	 * @param ord   The line ordering.
	 * @param order The preffered order of lines at this end of common subpath.
	 * @return The number of edges that has been setted the order.
	 */
	private static int cutTail(CommonSubpath cs, Ordering ord, int order)
	{
		final Line l1 = cs.getFirstLine();
		final Line l2 = cs.getSecondLine();
		int last = -1;
		int i = 0;
		for (int o : new IteratorWrapper<>(new OrderIterator(l1, l2, cs, cs.stationsIterator(), ord)))
		{
			if (o == order)
				last = i;
			i++;
		}

		if (last > 1)
		{
			// we don't have to increment the last index, because it has to significate
			// the first track that is not included in the cutting, but we know that there
			// is the order already setted
			final CommonSubpath path = cs.prefix(last);
			ord.setOrderOfLinesIfPossible(l1, l2, order, path, new IteratorWrapper<>(path.stationsIterator()));
		}
		// cut length is last + 1 - if last is -1, than it is 0, if last with given
		// value has index last, than the length is last+1 as a result of the fact
		// that we count from zero
		return last + 1;
	}
}
