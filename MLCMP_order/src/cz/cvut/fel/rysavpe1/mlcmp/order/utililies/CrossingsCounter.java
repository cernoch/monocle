/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order.utililies;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.util.HashSet;

/**
 * Library class that allows to count crossings in the order of lines.
 */
public final class CrossingsCounter
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private CrossingsCounter()
	{
	}

	/**
	 * Counts crossings in the given order of lines.
	 *
	 * @param arr The array that contains the finalized order od lines.
	 * @return The number of crossings assuming that the {@literal arr} describes order
	 *         of lines in clockwise/anticlockwise order at some vertex.
	 */
	@SuppressWarnings("ManualArrayToCollectionCopy")
	public static int countCrossings(Line[] arr)
	{
		int count = 0;

		final HashSet<Line> lines = new HashSet<>();
		// this is faster than addAll(arrays.aslist())
		for (Line l : arr)
			lines.add(l);
		final Line[] linesA = new Line[lines.size()];
		lines.toArray(linesA);

		for (int i = 0; i < linesA.length; i++)
			for (int j = i + 1; j < linesA.length; j++)
			{
				// now calculate the number of crossings of linesA[i] and linesA[j]
				Line first = null;
				Line last = null;

				for (int k = 0; k < arr.length; k++)
				{
					if (linesA[i] != arr[k] && arr[k] != linesA[j])
						continue;

					if (first == null)
						first = arr[k];

					if (last == linesA[i] && arr[k] == linesA[j])
						count++;

					last = arr[k];
				}

				if (last == linesA[j])
					count--;

				if (first == linesA[i] && last == linesA[i])
					count--;
			}

		return count;
	}

	public static int countCrossings(MLCMP problem, Ordering ord)
	{
		return countCrossings(new IteratorWrapper<>(problem.stationIterator()), ord);
	}

	public static int countCrossings(Iterable<Station> stations, Ordering ord)
	{
		int crossings = 0;
		for (Station station : stations)
			crossings += countCrossings(ord.getFinalizedOrder(station));
		return crossings;
	}
}