package cz.cvut.fel.rysavpe1.mlcmp.order.np;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.order.utililies.SpeedupUtilities;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.FOLLOWS;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.NOT_COMPARABLE;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.NOT_ENOUGH_INFORMATION;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.NOT_MATTER;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.PRECEDES;

/**
 * Library class with static method that enables to find option of order on common
 * subpath that is preferred. That means that methods of this class allows to find an
 * order that seems to be "most likely" the right possible one.
 *
 * @author Petr Ryšavý
 */
public final class PreferredOptionFinder
{
	/**
	 * Private constructor. Don't let anybody to create class instance.
	 */
	private PreferredOptionFinder()
	{
	}

	/**
	 * Returns the order of two lines that seems to be the right one. This method is
	 * similar to that one in {@literal GreedyOrderDecider}, but there is never produced
	 * the output {@literal NOT_ENOUGH_INFORMATION}.
	 *
	 * @param problem  The problem.
	 * @param ordering The given ordering.
	 * @param path     Common subpath of two lines.
	 * @return The order constant given by {@literal TrackLineOrder} class.
	 * @see cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder
	 */
	public static int orderLines(MLCMP problem, Ordering ordering, CommonSubpath path)
	{
		final Line l1 = path.getFirstLine();
		final Line l2 = path.getSecondLine();

		final Station start = path.getFirstStation();
		final Station end = path.getLastStation();

		final Track first = path.getFirstTrack();
		final Track last = path.getLastTrack();

		final int startPreferredOrder = getPreferredOrder(l1, l2, start, first, ordering);
		final int endPreferredOrder = getPreferredOrder(l2, l1, end, last, ordering);

		// if one side doesn't matter, then it depends only on the second side
		// it may be not matter or prefference
		if (startPreferredOrder == NOT_MATTER)
			return endPreferredOrder;
		if (endPreferredOrder == NOT_MATTER)
			return startPreferredOrder;

		// if preferred order is same, than it is our value
		// the only possible values are now PRECEDES or FOLLOWS
		if (startPreferredOrder == endPreferredOrder)
			return startPreferredOrder;

		// otherwise they are different, one is FOLLOWS and other PRECEDES
		return NOT_COMPARABLE;
	}

	/**
	 * Returns the most probable order of two lines on common subpath end.
	 *
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param s        End station of supath.
	 * @param t        Last track of subpath.
	 * @param ordering The ordering already known.
	 * @return The preferred order.
	 */
	private static int getPreferredOrder(Line l1, Line l2, Station s, Track t, Ordering ordering)
	{
		if (l1.isEndpoint(s) || l2.isEndpoint(s))
			return NOT_MATTER;

		// here are tracks sorted clockwise so that t is on index 0
		Track[] tracksArr = s.getIncidentTracks(t);

		// find the line that tends to be first in clockwise order
		Line clockwiseLine = findTendsToBeClockwise(tracksArr, l1, l2, ordering, s);
		if (clockwiseLine == null)
			return NOT_MATTER;
		// and similarly find the line that thends to be first in anticlockwise order
		Line anticlockwiseLine = findTendsToBeAnticlockwise(tracksArr, l1, l2, ordering, s);

		// if both lines are same, then return NOT_MATTER
		if (clockwiseLine == anticlockwiseLine)
			return NOT_MATTER;

		// they are different, if line l1 tends to be in anticlockwise order then
		// the right common subpath preferred order seems to be precedes
		if (anticlockwiseLine == l1)
			return PRECEDES;
		// else return follows
		return FOLLOWS;
	}

	/**
	 * Finds the line that seems to be the first one turning in the clockwise order
	 * to the given track at the given station.
	 *
	 * @param tracksArr Tracks that constains the tracks sorted clockwise. The first
	 *                  track is skipped from the search.
	 * @param l1        The first line.
	 * @param l2        The second line.
	 * @param s         The station to search.
	 * @param ordering  The ordering.
	 * @return The line that seems to be the right one on the clockwise side of the
	 *         track.
	 */
	protected static Line findTendsToBeClockwise(Track[] tracksArr, Line l1, Line l2, Ordering ordering, Station s)
	{
		boolean inverse = false;
		for (int i = 1; i < tracksArr.length; i++)
			if (l1.containsTrack(tracksArr[i]))
				if (l2.containsTrack(tracksArr[i]))
					if (!ordering.areLinesCompared(l1, l2, tracksArr[i]))
						// contains both and they are not compared, hence count the one to the number of the lines
						inverse = !inverse;
					else if (ordering.getOrderOfLines(l1, l2, tracksArr[i], s) == PRECEDES)
						// hence the l2 is closer
						return inverse ? l2 : l1;
					else
						// that means that l1 is closer
						return inverse ? l1 : l2;
				else
					// contains only l1, therefore return l2 or inverse if needed
					return inverse ? l1 : l2;
			// contains l2, therefore return l1 if needed
			else if (l2.containsTrack(tracksArr[i]))
				return inverse ? l2 : l1;
		// we did not find anything, hence return null - the order is unknown on all incident tracks
		return null;
	}

	/**
	 * Finds the line that seems to be the first one turning in the anticlockwise order
	 * to the given track at the given station.
	 *
	 * @param tracksArr Tracks that constains the tracks sorted clockwise. The first
	 *                  track is skipped from the search.
	 * @param l1        The first line.
	 * @param l2        The second line.
	 * @param s         The station to search.
	 * @param ordering  The ordering.
	 * @return The line that seems to be the right one on the clockwise side of the
	 *         track.
	 */
	protected static Line findTendsToBeAnticlockwise(Track[] tracksArr, Line l1, Line l2, Ordering ordering, Station s)
	{
		boolean inverse = false;
		for (int i = tracksArr.length - 1; i > 0; i--)
			if (l1.containsTrack(tracksArr[i]))
				if (l2.containsTrack(tracksArr[i]))
					if (!ordering.areLinesCompared(l1, l2, tracksArr[i]))
						// contains both and they are not compared, hence count the one to the number of the lines
						inverse = !inverse;
					else if (ordering.getOrderOfLines(l1, l2, tracksArr[i], s) == PRECEDES)
						// hence the l1 is closer
						return inverse ? l1 : l2;
					else
						// that means that l2 is closer
						return inverse ? l2 : l1;
				else
					// contains only l1, therefore return l2 or inverse if needed
					return inverse ? l1 : l2;
			// contains l2, therefore return l1 if needed
			else if (l2.containsTrack(tracksArr[i]))
				return inverse ? l2 : l1;
		// we did not find anything, hence return null - the order is unknown on all incident tracks
		return null;
	}
}