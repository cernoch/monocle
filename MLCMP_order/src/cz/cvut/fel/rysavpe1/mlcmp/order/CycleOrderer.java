/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CycleFinder;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import cz.cvut.fel.rysavpe1.utilities.Counter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Library that allows to order lines on cycles of length 3.
 *
 * @author Petr Ryšavý
 */
public final class CycleOrderer
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private CycleOrderer()
	{
	}

	/**
	 * Decide on cycles. Counts the lines that leave the cycle and go outside and
	 * lines that go inside. Then decides the line order such that the number of
	 * crossings is minimized. Works only for cycles of three tracks, otherwise this
	 * counting won't work!!! Also it is required that all lines on the cycle are the
	 * same.
	 *
	 * @param problem   The problem.
	 * @param component The isolated component.
	 * @param lo        The line ordering.
	 * @throws IllegalArgumentException When method {@literal CycleFinder.findCycles}
	 *                                  returns a cycle longer than three edges.
	 */
	public static void decideOnCycles(MLCMP problem, Set<Track> component, Ordering lo)
	{
		Collection<List<Track>> cycles = CycleFinder.findCycles(component, problem);

		for (List<Track> cycle : cycles)
		{
			final Set<Line> lines = canBeDecidedOnCycle(cycle, problem);
			if (lines == null)
				continue;

			final int size = cycle.size();
			if (size != 3)
				throw new IllegalArgumentException("Cannot decide on cycle that has more that three tracks.");

			HashMap<Line, Counter> clockCount = new HashMap<>(size);
			for (Line l : lines)
				clockCount.put(l, new Counter());

			Track prev = cycle.get(size - 1);
			final Station first = cycle.get(0).getCommonStation(prev);
			Station currSt = first;
			for (Track t : cycle)
			{
				Track[] arr = currSt.getIncidentTracks(t);
				boolean clock = true;
				for (int i = 1; i < arr.length; i++)
				{
					if (arr[i].equals(prev))
						clock = false;
					else if (clock)
					{
						for (Entry<Line, Counter> e : clockCount.entrySet())
							if (e.getKey().containsTrack(arr[i]))
								e.getValue().inc();
					}
					else
						for (Entry<Line, Counter> e : clockCount.entrySet())
							if (e.getKey().containsTrack(arr[i]))
								e.getValue().dec();
				}

				prev = t;
				currSt = t.getOtherVertex(currSt);
			}

			List<Entry<Line, Counter>> list = new ArrayList<>(clockCount.entrySet());
			Collections.sort(list, new Comparator<Entry<Line, Counter>>()
			{
				@Override
				public int compare(Entry<Line, Counter> o1, Entry<Line, Counter> o2)
				{
					final int c1 = o1.getValue().getCount();
					final int c2 = o2.getValue().getCount();
					if (c1 > c2)
						return 1;
					else if (c1 < c2)
						return -1;
					return 0;
				}
			});
			Line[] sortedLines = new Line[list.size()];
			int i = 0;
			for (Entry<Line, ?> e : list)
				sortedLines[i++] = e.getKey();

			currSt = first;
			for (Track t : cycle)
			{
				for (i = 1; i < sortedLines.length; i++)
					lo.setOrderOfLines(sortedLines[i - 1], sortedLines[i], TrackLineOrder.FOLLOWS, t, currSt);

				currSt = t.getOtherVertex(currSt);
			}
		}
	}

	/**
	 * Decides whether it is possible to set the order of lines on a cycle.
	 *
	 * @param cycle   The cycle where will be posible decided.
	 * @param problem The problem.
	 * @return The set of lines on the cycle or {@literal null} if it is not possible to
	 *         assume the order.
	 */
	private static Set<Line> canBeDecidedOnCycle(List<Track> cycle, MLCMP problem)
	{
		Iterator<Track> it = cycle.iterator();
		if (!it.hasNext())
			return null;
		final Set<Line> lines = problem.getLineSet(it.next());
		while (it.hasNext())
		{
			final Track t = it.next();
			if (!lines.equals(problem.getLineSet(t)))
				return null;
		}
		return lines;
	}
}