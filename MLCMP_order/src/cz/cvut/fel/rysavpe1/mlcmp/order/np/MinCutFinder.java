/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order.np;

import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.utilities.ModelUtilities;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.jgrapht.alg.StoerWagnerMinimumCut;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

/**
 * Library class for finding the minimal cut. The minimal cuts are cached.
 *
 * @author Petr Ryšavý
 */
public class MinCutFinder
{
	/**
	 * Cache of the result as we will need a minimal cut more times than once.
	 */
	private static Map<Set<Track>, Set<Track>> mincutCache = new HashMap<>();
	private static final MinCutMode MODE = MinCutMode.LOG_MAX_CSNUM_METRICS;

	/**
	 * Don't let anybody to create class instance.
	 */
	private MinCutFinder()
	{
	}

	/**
	 * Finds a minimal cut to the graph using some of the provided algorithms.
	 *
	 * @param graph The graph to cut.
	 * @param map   Map from tracks to the common subpath.
	 * @return The minimal cut with a piece of good luck.
	 */
	@SuppressWarnings("NestedAssignment")
	public static Set<Track> findMinCut(Set<Track> graph, Map<Track, ? extends Collection> map)
	{
		return findMinCut(graph, map, null);
	}

	public static Set<Track> findMinCut(Set<Track> graph, Map<Track, ? extends Collection> map,
										MLCMP problem)
	{
		if (mincutCache.containsKey(graph))
			return mincutCache.get(graph);

		final Set<Track> mincut;

		if (graph.size() == 1)
			mincut = new HashSet<>(graph);
		else
			switch (MODE)
			{
				case KARGER_ALGO:
					mincut = findMinCutKarger(graph, map);
					break;
				case MAX_CSNUM_METRICS:
				case LOG_MAX_CSNUM_METRICS:
				case MIN_CSNUM_METRICS:
					Map<Track, DefaultWeightedEdge> edgeMap = createEdgeMap(graph);
					SimpleWeightedGraph<Station, DefaultWeightedEdge> swg = buildGraph(graph, map, edgeMap);
					setGraphWeights(swg, graph, map, problem, edgeMap);
					Set<Station> cutset = new StoerWagnerMinimumCut<>(swg).minCut();
					mincut = adaptMinCut(graph, cutset);
					break;
				default:
					mincut = null;
			}

		mincutCache.put(graph, mincut);
		return mincut;
	}

	/**
	 * Finds a minimal cut to the graph using the Karger's algorithm with m restarts.
	 * If two mincuts have same number of tracks than the one with lower number of
	 * common subpaths crossing it is choosed. If both characteristics are same than
	 * the cut that splits the problem uniformly is used.
	 *
	 * @param graph The graph to cut.
	 * @param map   Map from tracks to the common subpath.
	 * @return The minimal cut with a piece of good luck.
	 */
	private static Set<Track> findMinCutKarger(Set<Track> graph, Map<Track, ? extends Collection> map)
	{
		final int N_LOOPS = graph.size();
		int minimum = Integer.MAX_VALUE;
		Set<Track> mincut = null;
		int csNum = Integer.MAX_VALUE;
		int splitDiff = Integer.MAX_VALUE;
		for (int i = 0; i < N_LOOPS; i++)
		{
			final Set<Track> found = KargerAlgorithm.contract(graph);
			final int size = found.size();
			int currentCsNum = -1;
			int currentSplitDiff = -1;

			if (size < minimum
				|| (size == minimum && !found.equals(mincut)
				&& ((currentCsNum = commonSubpathsNumber(found, map)) < csNum
				|| (currentCsNum == csNum && (currentSplitDiff = splitDiff(graph, found)) < splitDiff))))
			{
				minimum = size;
				mincut = found;
				csNum = currentCsNum == -1 ? commonSubpathsNumber(mincut, map) : currentCsNum;
				splitDiff = currentSplitDiff == -1 ? splitDiff(graph, mincut) : currentSplitDiff;
			}
		}
		return mincut;
	}

	/**
	 * Returns the numver of common subpaths that go through the cut.
	 *
	 * @param mincut A cut, probably the minimal one.
	 * @param map    Map from tracks to the common subpath.
	 * @return Sum of sizes of the collections in the {@code map} that are values to
	 *         the {@code mincut} tracks.
	 */
	private static int commonSubpathsNumber(Set<Track> mincut, Map<Track, ? extends Collection> map)
	{
		int count = 0;
		for (Track t : mincut)
			count += map.get(t).size();
		return count;
	}

	private static int findMaximalCommonSubpathNumber(Map<Track, ? extends Collection> map)
	{
		int max = -1;
		for (Collection c : map.values())
			if (map.size() > max)
				max = map.size();
		return max;
	}

	/**
	 * Returns the difference between the sizes of both new isolated componets.
	 *
	 * @param graph  The graph.
	 * @param mincut It's cut.
	 * @return The difference between isolated components sizes.
	 */
	private static int splitDiff(Set<Track> graph, Set<Track> mincut)
	{
		PairSet<? extends Collection> split = MinCutSplitter.splitByMinCut(graph, mincut);
		final Collection v1 = split.getValue1();
		final Collection v2 = split.getValue2();
		return Math.abs((v1 == null ? 0 : v1.size()) - (v2 == null ? 0 : v2.size()));
	}

	private static Map<Track, DefaultWeightedEdge> createEdgeMap(Set<Track> graph)
	{
		Map<Track, DefaultWeightedEdge> trackEdgeMap = new HashMap<>(graph.size());
		for (Track t : graph)
			trackEdgeMap.put(t, new DefaultWeightedEdge());
		return trackEdgeMap;
	}

	private static SimpleWeightedGraph<Station, DefaultWeightedEdge> buildGraph(Set<Track> graph, Map<Track, ? extends Collection> map, Map<Track, DefaultWeightedEdge> edgeMap)
	{
		SimpleWeightedGraph<Station, DefaultWeightedEdge> swg = new SimpleWeightedGraph<>(DefaultWeightedEdge.class);
		for (Track t : graph)
		{
			final Station vertex1 = t.getVertex1();
			final Station vertex2 = t.getVertex2();
			swg.addVertex(vertex1);
			swg.addVertex(vertex2);
			swg.addEdge(vertex1, vertex2, edgeMap.get(t));
		}
		return swg;
	}

	private static void setGraphWeights(SimpleWeightedGraph<Station, DefaultWeightedEdge> swg, Set<Track> graph, Map<Track, ? extends Collection> map, MLCMP problem, Map<Track, DefaultWeightedEdge> edgeMap)
	{
		switch (MODE)
		{
			case MAX_CSNUM_METRICS:
				final int maxNCSp1 = findMaximalCommonSubpathNumber(map) + 1;
				for (Track t : graph)
					swg.setEdgeWeight(edgeMap.get(t), maxNCSp1 - map.get(t).size());
				break;
			case LOG_MAX_CSNUM_METRICS:
				final int maxNCSp1_2 = findMaximalCommonSubpathNumber(map) + 1;
				for (Track t : graph)
					swg.setEdgeWeight(edgeMap.get(t), Math.log(maxNCSp1_2 - map.get(t).size())+1);
				break;
			case MIN_CSNUM_METRICS:
				for (Track t : graph)
					swg.setEdgeWeight(edgeMap.get(t), map.get(t).size()+0.001);
				break;
			default:
				throw new IllegalStateException("Unsupported MODE. Implement it or set another mode.");
		}
	}

	private static Set<Track> adaptMinCut(Set<Track> graph, Set<Station> cutset)
	{
		// other set will contains stations from the second part of the mincut
		Set<Station> otherSet = ModelUtilities.incidentStations(graph);
		otherSet.removeAll(cutset);

		// now select only those tracks that cross the mincut
		Set<Track> mincut = new HashSet<>();
		for (Track t : graph)
		{
			final Station s1 = t.getVertex1();
			final Station s2 = t.getVertex2();

			// the usage if(cs.con(s1) && oth.con(s2) || cs.con(s2) && ...)
			// would be slower as we already know that cutset and otherset are mutually exclusive
			if (cutset.contains(s1))
			{
				if (otherSet.contains(s2))
					mincut.add(t);
			}
			else if (cutset.contains(s2) && otherSet.contains(s1))
				mincut.add(t);
		}

		return mincut;
	}

	/**
	 * Clears the cache of the mincuts. It shall be called after each isolated
	 * component is done as the values will be never needed anymore.
	 */
	public static void clearCache()
	{
		mincutCache.clear();
	}

	private static enum MinCutMode
	{
		/** The Karger's algorithm. */
		KARGER_ALGO,
		MAX_CSNUM_METRICS,
		LOG_MAX_CSNUM_METRICS,
		MIN_CSNUM_METRICS;
	};
}