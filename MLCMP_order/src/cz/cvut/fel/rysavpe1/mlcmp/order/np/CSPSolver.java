/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order.np;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.SimpleDirectedGraph;
import cz.cvut.fel.rysavpe1.mlcmp.order.BundleOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.CycleOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.DependencyGraphBuilder;
import cz.cvut.fel.rysavpe1.mlcmp.order.ExtendedGreedyOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.GreedyOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.NotEnoughInformationResolver;
import cz.cvut.fel.rysavpe1.mlcmp.order.TailCutter;
import cz.cvut.fel.rysavpe1.mlcmp.order.utililies.CrossingsCounter;
import cz.cvut.fel.rysavpe1.mlcmp.order.utililies.SpeedupUtilities;
import static cz.cvut.fel.rysavpe1.mlcmp.order.utililies.SpeedupUtilities.getCrossingsLowerBound;
import cz.cvut.fel.rysavpe1.mlcmp.order.utililies.UnsolvedSubpathsFinder;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.IllegalOrderException;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.UndoableOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.UndoableOrdering.OrderHolder;
import cz.cvut.fel.rysavpe1.mlcmp.orderoption.OrderOption;
import cz.cvut.fel.rysavpe1.mlcmp.utilities.ModelUtilities;
import cz.cvut.fel.rysavpe1.utilities.GroupIterator;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

/**
 * Library class.
 *
 * @author Petr Ryšavý
 */
public class CSPSolver
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private CSPSolver()
	{
	}
//	static Result tmpR1;
//	static Result tmpR2;
//	static HashMap<Set<Track>, ResultMemory> memoryMap = new HashMap<>(256);

	/**
	 * Searches state space of each of the isolated components and finds the minimal
	 * number of crossings.
	 *
	 * @param problem    The problem.
	 * @param ordering   The current line ordering.
	 * @param components The isolated components that are in the problem.
	 * @param csMap      Mapping from tracks of the isolated components to not
	 *                   definite common subpaths on that tracks.
	 */
	public static void solve(MLCMP problem, LineOrdering ordering,
		Collection<Set<Track>> components, Map<Track, List<CommonSubpath>> csMap)
	{
		UndoableOrdering uord = new UndoableOrdering(ordering);
		for (Set<Track> s : components)
		{
			CycleOrderer.decideOnCycles(problem, s, uord);
			CSPSolver.solve(problem, uord, s, csMap);
		}
	}

	/**
	 * Searches state space of each one isolated component and finds the minimal
	 * number of crossings. Further it writes the optimum to the ordering.
	 *
	 * @param problem   The problem.
	 * @param ordering  The current line ordering.
	 * @param component The isolated component to search.
	 * @param csMap     Mapping from tracks of the isolated components to not
	 *                  definite common subpaths on that tracks.
	 */
	public static void solve(MLCMP problem, UndoableOrdering ordering,
		Set<Track> component,
		Map<Track, List<CommonSubpath>> csMap)
	{
//		memoryMap.clear();
		Result optimum = findOptimum(problem, ordering, component, csMap);
		if (optimum != null)
			System.err.println("Optimal number of crossings is " + optimum.crossings);
		ordering.storeToRoot(optimum.oh);
		MinCutFinder.clearCache();
	}

	/**
	 * Searches state space of each one isolated component and finds the minimal
	 * number of crossings.
	 *
	 * @param problem   The problem.
	 * @param ordering  The current line ordering.
	 * @param component The isolated component to search.
	 * @param csMap     Mapping from tracks of the isolated components to not
	 *                  definite common subpaths on that tracks.
	 * @return The optimum descriptor.
	 */
	@SuppressWarnings(
		{
		"null", "ConstantConditions"
	})
	private static Result findOptimum(MLCMP problem, final UndoableOrdering ordering,
		Set<Track> component,
		Map<Track, List<CommonSubpath>> csMap)
	{
		Map<CommonSubpath, Integer> toDo;

//		if(ordering.getBreakpointDepth() <= 20)
//			System.err.println("going to solve depth of "+ordering.getBreakpointDepth()+" on comp of "+component.size());

		// try to greedy solve the subproblem as a big one
		if (ordering.pureCSPRequired())
			toDo = UnsolvedSubpathsFinder.allCommonSubpaths(problem, component, ordering);
		else
		{
			toDo = GreedyOrderer.order(problem, component, ordering);
			TailCutter.cutTails(problem, ordering, toDo);
			ExtendedGreedyOrderer.order(problem, ordering, toDo);
			HashMap<PairSet<Line>, List<SimpleDirectedGraph<CommonSubpath>>> graph = DependencyGraphBuilder.getDependencyGraph(problem, ordering, toDo);
			NotEnoughInformationResolver.resolve(problem, ordering, toDo, graph);
			BundleOrderer.order(problem, ordering, toDo);
		}
//		final int optimumLowerBound = SpeedupUtilities.getCrossingsLowerBoundOnComponent(toDo, component);
//		final int optimumLowerBound = SpeedupUtilities.getCrossingsLowerBoundOnComponent(csMap, toDo, component, ordering, problem);
		final int optimumLowerBound = SpeedupUtilities.getCrossingsLowerBoundOnComponent(toDo, component, ordering, problem);

		// find the mincut
		Set<Track> mincut = MinCutFinder.findMinCut(component, csMap);

		// get all common subpaths that you will need to solve in order to solve the mincut
		Set<CommonSubpath> csSet = new HashSet<>();
		for (Track t : mincut)
			for (CommonSubpath cs : csMap.get(t))
				if (toDo.containsKey(cs))
					csSet.add(cs);

		// split the problem into two subcomponents
		final PairSet<Set<Track>> split = MinCutSplitter.splitByMinCut(component, mincut);
		final Set<Track> firstSubcomponent = split.getValue1();
		final Set<Track> secondSubcomponent = split.getValue2();

		// solve the situation when all tracks of the mincut are definite
		if (csSet.isEmpty())
			return solveSubProblems(problem, ordering, firstSubcomponent, secondSubcomponent, mincut, csMap, Integer.MAX_VALUE);

		// now find the possible orders of each of common subpaths that cross the mincut
		Map<CommonSubpath, List<OrderOption>> iterateMap = OrderOptionFinder.findPossibleOrders(problem, ordering, csSet, toDo, mincut);
		final int size = iterateMap.size();
		final List<CommonSubpath> csList = new ArrayList<>(size);
		final List<List<OrderOption>> optionList = new ArrayList<>(size);
		for (Entry<CommonSubpath, List<OrderOption>> e : iterateMap.entrySet())
		{
			csList.add(e.getKey());
			optionList.add(e.getValue());
		}

		Result optimum = Result.DUMMY_RESULT;

//		final ResultMemory firstComponentMemory = resultMemory(firstSubcomponent);
//		final ResultMemory secondComponentMemory = resultMemory(secondSubcomponent);
//		final ResultMemory firstComponentMemory = new ResultMemory();
//		final ResultMemory secondComponentMemory = new ResultMemory();
//		final Map<Track, Integer> componentMap = new HashMap<>(component.size());
//		for (Track t : component)
//			if (mincut.contains(t))
//				componentMap.put(t, 0);
//			else if (firstSubcomponent.contains(t))
//				componentMap.put(t, 1);
//			else
//				componentMap.put(t, 2);

		// NOTE ladění
		int iternum = 0;

		GroupIterator<OrderOption> iterator = new GroupIterator<>(optionList);
		while (iterator.hasNext())
		{
			iternum++;
			ordering.newBreakpoint();
			int i = 0;

//			HashMap<TrackLinePair, Integer> fcStateMap = new HashMap<>(component.size() * problem.getLinesNum() / 3);
//			HashMap<TrackLinePair, Integer> scStateMap = new HashMap<>(component.size() * problem.getLinesNum() / 3);

			try
			{
				final Iterator<OrderOption> option = iterator.next().iterator();
				for (; i < size; i++)
				{
					final CommonSubpath cs = csList.get(i);
					final Line l1 = cs.getFirstLine();
					final Line l2 = cs.getSecondLine();

					OrderOption oo = option.next();
					ordering.setOrderOfLinesIfPossible(l1, l2, oo, cs, new IteratorWrapper<>(cs.stationsIterator()));

//					Iterator<Integer> ooIt = oo.iterator();
//					for (Track t : cs)
//					{
////						Integer comp = componentMap.get(t);
//						if (comp != null)
//							switch (comp)
//							{
//								case 0:
//									final Integer next = ooIt.next();
//									fcStateMap.put(new TrackLinePair(t, l1, l2), next);
//									scStateMap.put(new TrackLinePair(t, l1, l2), next);
//									break;
//								case 1:
//									fcStateMap.put(new TrackLinePair(t, l1, l2), ooIt.next());
//									break;
//								case 2:
//									scStateMap.put(new TrackLinePair(t, l1, l2), ooIt.next());
//									break;
//							}
//					}
				}
			}
			catch (IllegalOrderException ioe)
			{
				// there was a bug at i-th position, so iterete the i-th iterator
				// immmediately - but don't iterate at the last position - otherwise
				// we will skip one (maybe valid) possibility as the last iterator
				// increases automatically
				if (i != size - 1)
					iterator.scrollCurrent(i);
				ordering.undo();
				continue;
			}

			// we have successfully setted the current value - hence try to solve the
			// smaller subproblems - we can still reuse current breakpoint
			try
			{
				Result r = solveSubProblems(problem, ordering, firstSubcomponent,
					secondSubcomponent, mincut, csMap, optimum.crossings
					);
//					,firstComponentMemory.getResult(fcStateMap), secondComponentMemory.getResult(scStateMap));

				if (r == null)
					continue;
				optimum = r;

				if (optimum.crossings < optimumLowerBound)
				{
					// NOTE ladění
//					System.err.println("optimumlowerbound   " + optimumLowerBound);
//					System.err.println("reached             " + optimum.crossings);
					int counted = 0;
					Set<Station> stations = ModelUtilities.incidentStations(component);
					for (Station s : stations)
						counted += CrossingsCounter.countCrossings(ordering.getFinalizedOrder(s));
//					System.err.println("counted             " + counted);
					throw new RuntimeException("optimum is better than lower bound");
				}

				if (optimum.crossings == optimumLowerBound)
				{
					// NOTE ladeni
//					if (ordering.getBreakpointDepth() == 1)
//						System.err.println("We reached the optimum of " + optimumLowerBound + " on component of " + component.size());
					// NOTE ladění
//					System.err.println("reached optimum at iteration " + iternum);
					return optimum;
				}

//				if (firstSubcomponent != null && !firstSubcomponent.isEmpty())
//					firstComponentMemory.saveResult(fcStateMap, tmpR1);
//				if (secondSubcomponent != null && !secondSubcomponent.isEmpty())
//					secondComponentMemory.saveResult(scStateMap, tmpR2);
			}
			catch (IllegalOrderException ioe)
			{
				continue;
			}
			finally
			{
				ordering.undo();
			}
		}

//		System.err.println("null optimum");
		if (optimum == Result.DUMMY_RESULT)
			return null;

		// NOTE ladění
//		if (ordering.getBreakpointDepth() == 0)
//			System.err.println("approx LB " + optimumLowerBound + " reached " + optimum.crossings + " on component of " + component.size());
//		System.err.println("reached optimum at last iter " + iternum);
		return optimum;
	}

	private static Result solveSubProblems(MLCMP problem, UndoableOrdering ordering,
		Set<Track> firstSubcomponent, Set<Track> secondSubcomponent, Set<Track> mincut,
		Map<Track, List<CommonSubpath>> csMap, int crossingsOptimum)
	{
		return solveSubProblems(problem, ordering, firstSubcomponent, secondSubcomponent, mincut, csMap, crossingsOptimum, null, null);
	}

	/**
	 * Searches recursively the isolated subcomponent and finds the minimal number of
	 * crossings. When the number is found, it merges the optima.
	 *
	 * @param problem            The problem.
	 * @param ordering           The current line ordering.
	 * @param firstSubcomponent  The first subcomponent. May be {@literal null}.
	 * @param secondSubcomponent The second subcomponent. May be {@literal null}.
	 * @param mincut             The cut between the subcomponents.
	 * @param csMap              Mapping from tracks of the isolated components to
	 *                           not definite common subpaths on that tracks.
	 * @param crossingsOptimum   The upper bound to the minimal number of crossings.
	 * @return The optimum descriptor or {@literal null} if it is worser than the upper
	 *         bound.
	 */
	@SuppressWarnings(
		{
		"null", "ConstantConditions"
	})
	private static Result solveSubProblems(MLCMP problem, UndoableOrdering ordering,
		Set<Track> firstSubcomponent, Set<Track> secondSubcomponent, Set<Track> mincut,
		Map<Track, List<CommonSubpath>> csMap, int crossingsOptimum, Result r1, Result r2)
	{
//		if (r1 != null || r2 != null)
//			System.err.println("used memory!!!!!!!!!!!!!!!!!");

		final boolean firstComponentValid = firstSubcomponent != null && !firstSubcomponent.isEmpty();
		final boolean secondComponentValid = secondSubcomponent != null && !secondSubcomponent.isEmpty();
		if (firstComponentValid && r1 == null)
		{
			r1 = findOptimum(problem, ordering, firstSubcomponent, csMap);
			if (r1 == null || r1.crossings > crossingsOptimum)
				return null;
		}
		if (secondComponentValid && r2 == null)
		{
			r2 = findOptimum(problem, ordering, secondSubcomponent, csMap);
			if (r2 == null || r2.crossings > crossingsOptimum)
				return null;
		}

//		tmpR1 = r1;
//		tmpR2 = r2;

		OrderHolder oh = null;
		int crossings = 0;
		if (!firstComponentValid && !secondComponentValid)
		{
			// this track is in real only one, as both components are empty
			for (Track t : mincut)
				crossings += CrossingsCounter.countCrossings(ordering.getFinalizedOrder(t.getVertex1()))
					+ CrossingsCounter.countCrossings(ordering.getFinalizedOrder(t.getVertex2()));

			if (crossings < crossingsOptimum)
				oh = ordering.storeTrack(mincut);
		}
		else if (firstComponentValid && secondComponentValid)
		{
			crossings = r1.crossings + r2.crossings;

			if (crossings < crossingsOptimum)
			{
				oh = UndoableOrdering.OrderHolder.concat(r1.oh, r2.oh);
				ordering.storeTracksTo(mincut, oh);
			}
		}
		// hence only one of the result sets is nonempty
		// we have to find out which of the stations that are in mincut don't
		// lie in this component
		else
		{
			Set<Station> stations = ModelUtilities.incidentStations(mincut);//new HashSet<>(mincut.size() * 2);
			//for (Track t : mincut)
			//{
			//	stations.add(t.getVertex1());
			//	stations.add(t.getVertex2());
			//}
			// now remove the stations from the other part
			final Set<Track> componentTracks = firstComponentValid
				? firstSubcomponent
				: secondSubcomponent;
			for (Track t : componentTracks)
			{
				stations.remove(t.getVertex1());
				stations.remove(t.getVertex2());
			}

			crossings = firstComponentValid ? r1.crossings : r2.crossings;

			// OK stations set now contains only stations that we need to add
			// to the crossings - in real this set shall now contain only one
			// station
			for (Station s : stations)
				crossings += CrossingsCounter.countCrossings(ordering.getFinalizedOrder(s));

			if (crossings < crossingsOptimum)
			{
				oh = firstComponentValid ? r1.oh : r2.oh;
				ordering.storeTracksTo(mincut, oh);
			}
		}

		if (crossings < crossingsOptimum)
			return new Result(oh, crossings);
		return null;
	}
	
//	static ResultMemory resultMemory(Set<Track> component)
//	{
//		ResultMemory rm = memoryMap.get(component);
//		if(rm == null)
//		{
//			rm = new ResultMemory();
//			memoryMap.put(component, rm);
//		}
//		return rm;
//	}

	/**
	 * The result wrapper. Stores the optimal ordering and the number of crossings.
	 */
	private static class Result
	{
		/**
		 * The worst possible result.
		 */
		public static final Result DUMMY_RESULT = new Result(null, Integer.MAX_VALUE);
		/**
		 * The optimal order of lines.
		 */
		private OrderHolder oh;
		/**
		 * The optimal number of crossings.
		 */
		private int crossings;

		/**
		 * Creates new result holder.
		 *
		 * @param oh        The optimal order of lines.
		 * @param crossings The optimal number of crossings.
		 */
		public Result(OrderHolder oh, int crossings)
		{
			this.oh = oh;
			this.crossings = crossings;
		}
	}

	private static class TrackLinePair
	{
		private final Track track;
		private final Line l1;
		private final Line l2;

		public TrackLinePair(Track track, Line l1, Line l2)
		{
			this.track = track;
			this.l1 = l1;
			this.l2 = l2;
		}

		public Track getTrack()
		{
			return track;
		}

		public Line getL1()
		{
			return l1;
		}

		public Line getL2()
		{
			return l2;
		}

		@Override
		public int hashCode()
		{
			int hash = 7;
			hash = 43 * hash + Objects.hashCode(this.track);
			hash = 43 * hash + Objects.hashCode(this.l1);
			hash = 43 * hash + Objects.hashCode(this.l2);
			return hash;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final TrackLinePair other = (TrackLinePair) obj;
			if (!Objects.equals(this.track, other.track))
				return false;
			if (!Objects.equals(this.l1, other.l1))
				return false;
			if (!Objects.equals(this.l2, other.l2))
				return false;
			return true;
		}
	}

	private static class ResultMemory
	{
		private final HashMap<HashMap<TrackLinePair, Integer>, Result> memory = new HashMap<>(4);

		public void saveResult(HashMap<TrackLinePair, Integer> map, Result r)
		{
			memory.put(map, r);
		}

		public Result getResult(HashMap<TrackLinePair, Integer> map)
		{
			return memory.get(map);
		}
	}
}
