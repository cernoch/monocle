/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpathFinder;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.IllegalOrderException;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.util.HashSet;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.*;
import java.util.Iterator;

/**
 * Checks the optimalit with respect the common subpath preferred order.
 *
 * @author Petr Ryšavý
 */
public class OptimalityChecker
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private OptimalityChecker()
	{
	}

	/**
	 * Checks whether the common subpath preferred order is respected on the given
	 * problem.
	 *
	 * @param problem  Problem.
	 * @param ordering The final line ordering.
	 * @throws IllegalOrderException It the common subpath preferred order is not
	 *                               respected.
	 */
	public static void checkOptimality(MLCMP problem, Ordering ordering)
	{
		checkOptimality(problem, new IteratorWrapper<>(problem.trackIterator()), ordering);
	}

	/**
	 * Checks whether the common subpath preferred order is respected on the given
	 * problem.
	 *
	 * @param problem  Problem.
	 * @param ordering The line ordering that does not need to be final.
	 * @throws IllegalOrderException It the common subpath preferred order is not
	 *                               respected.
	 */
	public static void checkPartialOptimality(MLCMP problem, Ordering ordering)
	{
		checkPartialOptimality(problem, new IteratorWrapper<>(problem.trackIterator()), ordering);
	}

	/**
	 * Checks whether the common subpath preferred order is respected on the given
	 * set of lines.
	 *
	 * @param problem  Problem.
	 * @param it       The iterable of the tracks.
	 * @param ordering The final line ordering.
	 * @throws IllegalOrderException It the common subpath preferred order is not
	 *                               respected.
	 */
	public static void checkOptimality(MLCMP problem, Iterable<Track> it, Ordering ordering)
	{
		HashSet<CommonSubpath> csSet = new HashSet<>();

		for (Track t : it)
		{
			final Line[] lines = problem.getLineArray(t);
			for (int i = 0; i < lines.length; i++)
				for (int j = i + 1; j < lines.length; j++)
				{
					CommonSubpath subpath = CommonSubpathFinder.findCommonSubpath(problem, lines[i], lines[j], t);

					if (csSet.contains(subpath))
						continue;
					csSet.add(subpath);

					// as the order is supposed to be definite, the returned value is
					// FOLLOWS, PRECEDES, NOT_MATTER or NOT_COMPARABLE
					final int order = GreedyOrderDecider.orderLines(problem, ordering, subpath);
					if (order == FOLLOWS || order == PRECEDES)
						checkDefiniteOrder(ordering, lines[i], lines[j], subpath.iterator(), subpath.stationsIterator(), order);
					else if (order == NOT_MATTER)
						checkNotMatterOrder(ordering, lines[i], lines[j], subpath.iterator(), subpath.stationsIterator());
					else if (order == NOT_COMPARABLE)
						checkNotComparableOrder(ordering, lines[i], lines[j], subpath.iterator(), subpath.stationsIterator());
					else
						throw new IllegalOrderException("The order is not definite.");
				}
		}
	}

	/**
	 * Checks whether the common subpath preferred order is respected on the given
	 * set of lines.
	 *
	 * @param problem  Problem.
	 * @param it       The iterable of the tracks.
	 * @param ordering The line ordering that does not need to be final.
	 * @throws IllegalOrderException It the common subpath preferred order is not
	 *                               respected.
	 */
	public static void checkPartialOptimality(MLCMP problem, Iterable<Track> it, Ordering ordering)
	{
		HashSet<CommonSubpath> csSet = new HashSet<CommonSubpath>();

		for (Track t : it)
		{
			final Line[] lines = problem.getLineArray(t);
			for (int i = 0; i < lines.length; i++)
				for (int j = i + 1; j < lines.length; j++)
				{
					CommonSubpath subpath = CommonSubpathFinder.findCommonSubpath(problem, lines[i], lines[j], t);

					if (csSet.contains(subpath))
						continue;
					csSet.add(subpath);

					// as the order is supposed to be definite, the returned value is
					// FOLLOWS, PRECEDES, NOT_MATTER or NOT_COMPARABLE
					final int order = GreedyOrderDecider.orderLines(problem, ordering, subpath);
					if (order == FOLLOWS || order == PRECEDES)
						checkDefiniteOrder(ordering, lines[i], lines[j], subpath.iterator(), subpath.stationsIterator(), order);
					else if (order == NOT_MATTER)
						checkPartialNotMatterOrder(ordering, lines[i], lines[j], subpath.iterator(), subpath.stationsIterator());
					else if (order == NOT_COMPARABLE)
						checkPartialNotComparableOrder(ordering, lines[i], lines[j], subpath.iterator(), subpath.stationsIterator());
				}
		}
	}

	/**
	 * Checks the order precedes or follows of two lines on a set of tracks.
	 *
	 * @param ordering The ordering.
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param it       Iterator of tracks.
	 * @param sit      Iterator of stations.
	 * @param order    The order that shall lines have.
	 */
	public static void checkDefiniteOrder(Ordering ordering, Line l1, Line l2, Iterator<Track> it,
		Iterator<Station> sit, int order)
	{
		while (it.hasNext())
			if (ordering.getOrderOfLines(l1, l2, it.next(), sit.next()) != order)
				throw new IllegalOrderException("The order is not optimal");
	}

	/**
	 * Checks whether two lines with not matter order do not cross.
	 *
	 * @param ordering The ordering.
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param it       Iterator of tracks.
	 * @param sit      Iterator of stations.
	 */
	public static void checkNotMatterOrder(Ordering ordering, Line l1, Line l2,
		Iterator<Track> it, Iterator<Station> sit)
	{
		if (!it.hasNext())
			return;

		final int order = ordering.getOrderOfLines(l1, l2, it.next(), sit.next());

		if (order != FOLLOWS && order != PRECEDES)
			throw new IllegalOrderException("The order is not definite.");

		if (it.hasNext())
			checkDefiniteOrder(ordering, l1, l2, it, sit, order);
	}

	/**
	 * Checks whether two lines with not matter order do not cross.
	 *
	 * @param ordering The ordering that does not need to be finite.
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param it       Iterator of tracks.
	 * @param sit      Iterator of stations.
	 */
	public static void checkPartialNotMatterOrder(Ordering ordering, Line l1, Line l2,
		Iterator<Track> it, Iterator<Station> sit)
	{
		boolean wasPrecedes = false;
		boolean wasFollows = false;
		while (it.hasNext())
		{
			final int order = ordering.getOrderOfLines(l1, l2, it.next(), sit.next());
			wasPrecedes |= order == FOLLOWS;
			wasFollows |= order == PRECEDES;
			if (wasPrecedes && wasFollows)
				throw new IllegalOrderException("Not matter lines cross.");
		}
	}

	/**
	 * Checks whether two lines with not comparable order cross exactly once.
	 *
	 * @param ordering The ordering.
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param it       Iterator of tracks.
	 * @param sit      Iterator of stations.
	 */
	public static void checkNotComparableOrder(Ordering ordering, Line l1, Line l2,
		Iterator<Track> it, Iterator<Station> sit)
	{
		if (!it.hasNext())
			return;

		int order = ordering.getOrderOfLines(l1, l2, it.next(), sit.next());
		if (order != FOLLOWS && order != PRECEDES)
			throw new IllegalOrderException("The order is not definite.");

		while (it.hasNext())
			if (order != ordering.getOrderOfLines(l1, l2, it.next(), sit.next()))
				break;

		if (it.hasNext())
		{
			order = order == FOLLOWS ? PRECEDES : FOLLOWS;
			checkDefiniteOrder(ordering, l1, l2, it, sit, order);
		}
	}

	/**
	 * Checks whether two lines with not comparable order may cross exactly once.
	 *
	 * @param ordering The ordering that does not need to be finite.
	 * @param l1       First line.
	 * @param l2       Second line.
	 * @param it       Iterator of tracks.
	 * @param sit      Iterator of stations.
	 */
	public static void checkPartialNotComparableOrder(Ordering ordering, Line l1, Line l2,
		Iterator<Track> it, Iterator<Station> sit)
	{
		if (!it.hasNext())
			return;

		int beginorder = ordering.getOrderOfLines(l1, l2, it.next(), sit.next());
		while (beginorder == NOT_COMPARABLE && it.hasNext())
			beginorder = ordering.getOrderOfLines(l1, l2, it.next(), sit.next());
		if (!it.hasNext())
			return;
		if (beginorder != FOLLOWS && beginorder != PRECEDES)
			throw new IllegalOrderException("The order is not definite : " + beginorder);
		final int endorder = beginorder == FOLLOWS ? PRECEDES : FOLLOWS;

		while (it.hasNext())
			if (endorder == ordering.getOrderOfLines(l1, l2, it.next(), sit.next()))
				break;

		while (it.hasNext())
			if (beginorder == ordering.getOrderOfLines(l1, l2, it.next(), sit.next()))
				throw new IllegalOrderException("Not comparable lines cannot cross twice.");
	}
}
