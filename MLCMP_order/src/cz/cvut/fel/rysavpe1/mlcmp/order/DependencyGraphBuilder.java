/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.graphalgos.WeaklyConnectedComponentsSplitter;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.SimpleDirectedGraph;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.SimpleVertex;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.Vertex;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.MathUtils;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Library class that allows to build the dependency graph.
 *
 * @author Petr Ryšavý
 */
public class DependencyGraphBuilder
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private DependencyGraphBuilder()
	{
	}

	/**
	 * Builds the dependency graph of the common subpath preferred order. A common
	 * subpath preferred order depends on order on another common subpath if we don't
	 * have enought enfirmation on the common subpath due to not known order on the
	 * other.
	 *
	 * @param problem      The problem.
	 * @param ordering     The ordering of lines.
	 * @param notSolvedMap Map from not solved tracks to their preferred order.
	 * @return A list for isolated dependency graphs for each pair of lines.
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<PairSet<Line>, List<SimpleDirectedGraph<CommonSubpath>>> getDependencyGraph(MLCMP problem, Ordering ordering, Map<CommonSubpath, Integer> notSolvedMap)
	{
		final HashMap<PairSet<Line>, SimpleDirectedGraph<CommonSubpath>> graphsMap = new HashMap<>(MathUtils.pow(problem.getLinesNum(), 2));

		for (Map.Entry<CommonSubpath, Integer> e : notSolvedMap.entrySet())
			if (e.getValue() == TrackLineOrder.NOT_ENOUGH_INFORMATION)
			{
				final CommonSubpath cs = e.getKey();
				final PairSet<Line> key = new PairSet<>(cs.getFirstLine(), cs.getSecondLine());
				SimpleDirectedGraph<CommonSubpath> graph = graphsMap.get(key);
				if (graph == null)
				{
					graph = new SimpleDirectedGraph<>();
					graphsMap.put(key, graph);
				}

				final Vertex<CommonSubpath> s = new SimpleVertex<>(cs);
				graph.addVertex(s);

				for (CommonSubpath tcs : DependencyFinder.getDependency(problem, ordering, cs))
					graph.addEdge(s, new SimpleVertex<>(tcs));
			}

		final HashMap<PairSet<Line>, List<SimpleDirectedGraph<CommonSubpath>>> retval = new HashMap<>(MathUtils.pow(problem.getLinesNum(), 2));
		for (Map.Entry<PairSet<Line>, SimpleDirectedGraph<CommonSubpath>> e : graphsMap.entrySet())
			retval.put(e.getKey(), WeaklyConnectedComponentsSplitter.wcc(e.getValue()));

		return retval;
	}
}
