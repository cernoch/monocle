/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order.np;

import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Library class that finds the isolated componetns in the problem.
 *
 * @author Petr Ryšavý
 */
public class IsolatedComponentsFinder
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private IsolatedComponentsFinder()
	{
	}

	/**
	 * Finds the isolated components in the problem.
	 *
	 * @param notSolvedTracks Set of not solved tracks.
	 * @return Isolated components from this tracks.
	 */
	public static List<Set<Track>> findIsolatedComponents(Set<Track> notSolvedTracks)
	{
		final List<Set<Track>> components = new LinkedList<>();

		final HashSet<Track> visited = new HashSet<>(notSolvedTracks.size());

		// instead of iterating all tracks in the problem, it will be faster to iterate
		// only the tracks in unsolved common subpaths
		for (Track t : notSolvedTracks)
			if (!visited.contains(t))
				components.add(BFS(t, notSolvedTracks, visited));

		return components;
	}

	/**
	 * Searches one isolated component.
	 *
	 * @param start             The starting track.
	 * @param interestingTracks The tracks we are searching.
	 * @param visited           The viseted tracks.
	 * @return The isolated component.
	 */
	private static Set<Track> BFS(Track start, Set<Track> interestingTracks, Set<Track> visited)
	{
		final Set<Track> component = new HashSet<>(16);
		final Deque<Track> q = new LinkedList<>();
		q.add(start);
		visited.add(start);

		while (!q.isEmpty())
		{
			final Track t = q.remove();
			component.add(t);

			for (Track other : new IteratorWrapper<>(t.getVertex1().incidenTracksIterator()))
				if (interestingTracks.contains(other) && !visited.contains(other))
				{
					visited.add(other);
					q.add(other);
				}
			for (Track other : new IteratorWrapper<>(t.getVertex2().incidenTracksIterator()))
				if (interestingTracks.contains(other) && !visited.contains(other))
				{
					visited.add(other);
					q.add(other);
				}
		}

		return component;
	}
}