/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpathFinder;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import java.util.Iterator;
import java.util.Map;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.*;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.util.Set;

/**
 * Assigns the order on the bundle common subpaths.
 *
 * @author Petr Ryšavý
 */
public class BundleOrderer
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private BundleOrderer()
	{
	}

	/**
	 * Order the lines on bundle common subpaths of order not matter or not
	 * comparable.
	 *
	 * @param problem      The problem.
	 * @param ordering     The ordering.
	 * @param notSolvedMap The map from unsolved common subpaths to their common
	 *                     subpath preferred order.
	 */
	public static void order(MLCMP problem, Ordering ordering,
		Map<CommonSubpath, Integer> notSolvedMap)
	{
		for (Iterator<Map.Entry<CommonSubpath, Integer>> it = notSolvedMap.entrySet().iterator(); it.hasNext();)
		{
			Map.Entry<CommonSubpath, Integer> e = it.next();
			switch (e.getValue())
			{
				case TrackLineOrder.NOT_MATTER:
					if (orderNotMatterSubpath(problem, ordering, e.getKey()))
						it.remove();
					break;
				case TrackLineOrder.NOT_COMPARABLE:
					if (orderNotComparableSubpath(problem, ordering, e.getKey()))
						it.remove();
					break;
			}
		}
	}

	/**
	 * Sets the order on not matter common subpath.
	 *
	 * @param problem  The problem.
	 * @param ordering The ordering.
	 * @param cs       The common subpath.
	 * @return {@literal true} if the lines were ordered, {@literal false} otherwise.
	 */
	private static boolean orderNotMatterSubpath(MLCMP problem, Ordering ordering, CommonSubpath cs)
	{
		if (BundleOrderer.areLinesBundle(cs, problem, ordering)
			|| BundleOrderer.areLinesExtendedBundle(cs, problem, ordering))
		{
			ordering.setOrderOfLinesIfPossible(cs.getFirstLine(), cs.getSecondLine(),
				FOLLOWS, cs, new IteratorWrapper<>(cs.stationsIterator()));
			return true;
		}
		return false;
	}

	/**
	 * Sets the order on not comparable common subpath.
	 *
	 * @param problem  The problem.
	 * @param ordering The ordering.
	 * @param cs       The common subpath.
	 * @return {@literal true} if the lines were ordered, {@literal false} otherwise.
	 */
	private static boolean orderNotComparableSubpath(MLCMP problem, Ordering ordering, CommonSubpath cs)
	{
		if (BundleOrderer.areLinesBundle(cs, problem, ordering))
		{
			ordering.setOrderOfLinesIfPossible(cs.getFirstLine(), cs.getSecondLine(),
				FOLLOWS, cs, new IteratorWrapper<>(cs.stationsIterator()));
			return true;
		}
		return false;
	}

	/**
	 * Decides whether the lines are bundle on the given common subpath. Lines are
	 * bundle on a common subpath if they are bundle on each of its tracks.
	 *
	 * @param cs       The common subpath.
	 * @param problem  The problem.
	 * @param ordering The ordering.
	 * @return {@literal true} if the lines are bundle according the
	 *         {@literal ordering}, {@literal false} otherwise.
	 */
	public static boolean areLinesBundle(CommonSubpath cs, MLCMP problem, Ordering ordering)
	{
		return areLinesBundle(cs.getFirstLine(), cs.getSecondLine(), cs, problem, ordering);
	}

	/**
	 * Decides whether the lines are bundle on the given set of tracks. Lines are
	 * bundle if they are bundle on each of the tracks.
	 *
	 * @param it       The iterator of tracks.
	 * @param l1       The first line.
	 * @param l2       The second line.
	 * @param problem  The curren problem.
	 * @param ordering The ordering.
	 * @return {@literal true} if the lines are bundle according the
	 *         {@literal ordering}, {@literal false} otherwise.
	 */
	public static boolean areLinesBundle(Line l1, Line l2, Iterable<Track> it,
		MLCMP problem, Ordering ordering)
	{
		for (Track t : it)
			if (!areLinesBundle(l1, l2, t, problem, ordering))
				return false;
		return true;
	}

	/**
	 * Decides whether the lines are bundle on the given track. Lines are bundle if
	 * their order to any other line on that track is precedes/follows and that line
	 * is not ordered between them.
	 *
	 * @param t        The track to check.
	 * @param l1       The first line.
	 * @param l2       The second line.
	 * @param problem  The curren problem.
	 * @param ordering The ordering.
	 * @return {@literal true} if the lines are bundle according the
	 *         {@literal ordering}, {@literal false} otherwise.
	 */
	public static boolean areLinesBundle(Line l1, Line l2, Track t,
		MLCMP problem, Ordering ordering)
	{
		if (problem.getLinesNum(t) != 2)
			for (Line l : problem.getLineSet(t))
			{
				if (l.equals(l1) || l.equals(l2))
					continue;
				final Station s = t.getVertex1();
				int order = ordering.getOrderOfLines(l1, l, t, s);
				// the order of third/fourth/... line must be known
				if (order != FOLLOWS && order != PRECEDES)
					return false;
				// and line must be ordered on same side of l1 as well as l2
				if (order != ordering.getOrderOfLines(l2, l, t, s))
					return false;
			}
		return true;
	}

	/**
	 * Decides whether the lines are extended bundle. A set of lines on a not matter
	 * trail is an extended bundle if all lines of that set have their relative order
	 * not matter ad they are bundle to any other line.
	 *
	 * This method is not finished, it works ony for common subpaths of length one at
	 * the moment.
	 *
	 * @param cs       The common subpath.
	 * @param problem  The problem.
	 * @param ordering The line ordering.
	 * @return Whether the lines are extended common subpath on the specified common
	 *         subpath.
	 */
	public static boolean areLinesExtendedBundle(CommonSubpath cs, MLCMP problem, Ordering ordering)
	{
		if (cs.size() != 1)
			return false;

		final Track t = cs.getFirstTrack();
		final Set<Line> lines = problem.getLineSet(t);
		final Station firstSt = cs.getFirstStation();
		final Line l1 = cs.getFirstLine();
		final Line l2 = cs.getSecondLine();

		for (Line other : lines)
			if (other != l1 && other != l2)
			{
				final int order1 = ordering.getOrderOfLines(l1, other, t, firstSt);
				if (order1 != NOT_MATTER && order1 != FOLLOWS && order1 != PRECEDES)
					return false;
				if (order1 != ordering.getOrderOfLines(l2, other, t, firstSt))
					return false;

				final CommonSubpath otherPath1 = CommonSubpathFinder.findCommonSubpath(problem, l1, other, t);
				if (otherPath1.size() != 1)
					return false;
				final CommonSubpath otherPath2 = CommonSubpathFinder.findCommonSubpath(problem, l2, other, t);
				if (otherPath2.size() != 1)
					return false;
			}
		return true;
	}
}
