/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order.utililies;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpathFinder;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import java.util.HashMap;
import java.util.Map;

/**
 * Library class that allows to find all unsolved common subpaths.
 *
 * @author Petr Ryšavý
 */
public final class UnsolvedSubpathsFinder
{
	/**
	 * Gets all unsolved common subpaths on the particular set of tracks. The common
	 * subpath is unsolved if it's preferred order is not precedes nor follows, i.e.
	 * the metod {@literal Ordering.areLinesCompared()} returns false.
	 *
	 * @param problem   The problem.
	 * @param component The set of tracks.
	 * @param ord       The ordering.
	 * @return Set of all indefinite common subpaths and their orders.
	 */
	public static Map<CommonSubpath, Integer> allCommonSubpaths(MLCMP problem, Iterable<Track> component, Ordering ord)
	{
		Map<CommonSubpath, Integer> map = new HashMap<>();
		for (Track t : component)
			if (!ord.isTotallyOrdered(t))
			{
				final Line[] lines = problem.getLineArray(t);
				for (int i = 0; i < lines.length; i++)
					for (int j = i + 1; j < lines.length; j++)
						if (!ord.areLinesCompared(lines[i], lines[j], t))
							map.put(CommonSubpathFinder.findCommonSubpath(problem, lines[i], lines[j], t), ord.getOrderOfLines(lines[i], lines[j], t, t.getVertex1()));
			}
		return map;
	}

	/**
	 * Don't let anybody to create class instance.
	 */
	private UnsolvedSubpathsFinder()
	{
	}
}