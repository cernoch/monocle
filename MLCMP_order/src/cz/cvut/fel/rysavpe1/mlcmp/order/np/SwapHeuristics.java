package cz.cvut.fel.rysavpe1.mlcmp.order.np;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.order.GreedyOrderDecider;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.NOT_MATTER;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Petr Ryšavý
 */
public class SwapHeuristics
{
	/**
	 * Private constructor. Don't let anybody to create class instance.
	 */
	private SwapHeuristics()
	{
	}

	public static void findASolution(Ordering ord, Set<Track> tracks, Set<CommonSubpath> toDo)
	{
		findASolution(ord, tracks, toDo, false);
	}

	public static void findRandomSolution(Ordering ord, Set<Track> tracks, Set<CommonSubpath> toDo)
	{
		findASolution(ord, tracks, toDo, true);
	}

	@SuppressWarnings("NestedAssignment")
	public static void findASolution(Ordering ord, Set<Track> tracks, Set<CommonSubpath> toDo, boolean random)
	{
		if (random)
			ord.assignRandomOrder(tracks);
		else
			ord.assignAnOrder(tracks);

		for (CommonSubpath cs : toDo)
		{
			Iterator<Station> sit = cs.stationsIterator();
			Iterator<Track> tit = cs.iterator();

			boolean[] crossings = new boolean[cs.size() + 1];
			// track t goes from station last to station current
			Station last = sit.next();
			Station current;
			Track t = cs.getFirstTrack();
			final Line firstLine = cs.getFirstLine();
			final Line secondLine = cs.getSecondLine();
			// there can be only PRECEDES, FOLLOWS and NOT_MATTER
			int lastOrder = GreedyOrderDecider.getPreferredOrder(firstLine, secondLine, last, t, ord);
			final int firstOrder = lastOrder;
			int i = 0;
			while (tit.hasNext())
			{
				current = sit.next();
				t = tit.next();

				final int currentOrder = ord.getOrderOfLines(firstLine, secondLine, t, last);
				if (currentOrder != lastOrder)
					crossings[i] = true;

				last = current;
				i++;
				lastOrder = currentOrder;
			}
			final int currentOrder = GreedyOrderDecider.getPreferredOrder(secondLine, firstLine, last, t, ord);
			if(currentOrder != lastOrder && currentOrder != NOT_MATTER)
				crossings[crossings.length - 1] = true;
			if(firstOrder != NOT_MATTER)
				crossings[0] = false;
			
			// now kill the double crossings
			int first = -1;
			int second = -1;
			for(int j = 0; j < crossings.length; j++)
				if(crossings[j])
					if(first == -1)
						first = j;
					else if(second == -1)
						second = j;
					else
					{
						// we found double crossing
						for(i = first; i < second; i++)
							ord.swapLines(cs.get(i), firstLine, secondLine);
						first = second = -1;
					}
			
			if(first == -1)
				continue;
			
			// there is a single crossing - not comparable case
			if(firstOrder != currentOrder && firstOrder != NOT_MATTER && currentOrder != NOT_MATTER)
				continue;
			
			final Station firstStation = cs.getFirstStation();
			final Station lastStation = cs.getLastStation();
			if(firstOrder == NOT_MATTER && firstLine.isInline(firstStation) && secondLine.isInline(firstStation))
			{
				second = first;
				first = 0;
			}
			else if(currentOrder == NOT_MATTER && firstLine.isInline(lastStation) && secondLine.isInline(lastStation))
				second = crossings.length  - 1;	
			else if(firstOrder == NOT_MATTER)
			{
				second = first;
				first = 0;
			}
			else
				second = crossings.length  - 1;	
			
			for(i = first; i < second; i++)
				ord.swapLines(cs.get(i), firstLine, secondLine);
		}
	}
}