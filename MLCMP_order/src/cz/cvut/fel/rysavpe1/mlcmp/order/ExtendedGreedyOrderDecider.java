/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.ExtendedCommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.util.Iterator;
import java.util.Map;
import static cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder.*;

/**
 * Assumes the order on extended common subpaths.
 *
 * @author Petr Ryšavý
 */
public class ExtendedGreedyOrderDecider
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private ExtendedGreedyOrderDecider()
	{
	}

	/**
	 * Orders lines on extended common subpath so that no redundant crossings are
	 * created.
	 *
	 * @param problem   The problem.
	 * @param ordering  The ordering.
	 * @param newValues The map where the now lines order will be saved.
	 * @param path      The path where the order shall be assumed.
	 */
	@SuppressWarnings("empty-statement")
	public static void order(MLCMP problem, Ordering ordering,
		Map<CommonSubpath, Integer> newValues, ExtendedCommonSubpath path)
	{
		if (path.size() <= 1)
			return;

		if (!areLinesBundle(path, problem, ordering))
			return;

		// lines are alone on the whole extendedcommonsubpath, so we can order them
		final Line l1 = path.getPreferredLine();
		final Line l2 = path.getOtherLine();
		int currentOrder = Math.abs(GreedyOrderDecider.getPreferredOrder(l1, l2, path.getLastSubpath().getLastStation(), path.getLastSubpath().getLastTrack(), ordering));
		if (currentOrder == NOT_ENOUGH_INFORMATION)
			return;
		currentOrder = Math.abs(GreedyOrderDecider.getPreferredOrder(l1, l2, path.getFirstSubpath().getFirstStation(), path.getFirstSubpath().getFirstTrack(), ordering));
		if (currentOrder == NOT_ENOUGH_INFORMATION)
			return;
		Iterator<CommonSubpath> it = path.iterator();
		CommonSubpath lastKnown = it.next();
		ordering.setOrderOfLinesIfPossible(l1, l2, currentOrder, lastKnown, new IteratorWrapper<>(lastKnown.stationsIterator()));
		newValues.put(lastKnown, currentOrder);
		boolean edgeBeforeMe = isEdgeBeforeMe(currentOrder, lastKnown, l1, l2, true);
		while (it.hasNext())
		{
			CommonSubpath next = it.next();

			// I'm solving line and I know that there are two edges behind me
			if (currentOrder == NOT_MATTER)
			{
				// TODO možná ty weak pak povedou k tomu, že budu chtít nastavit opačné, ale asi ne ... kdyby tak vím, kde hledat bug
				currentOrder = Math.abs(GreedyOrderDecider.getPreferredOrder(l2, l1, next.getLastStation(), next.getLastTrack(), ordering));
				edgeBeforeMe = isEdgeBeforeMe(currentOrder, next, l1, l2, true);
			}
			else
			{
				boolean nextEdgeSameSite = isEdgeBeforeMe(currentOrder, next, l1, l2, true);
				boolean nextEdgeOtherSite = isEdgeBeforeMe(currentOrder, next, l1, l2, false);

				if (edgeBeforeMe && nextEdgeSameSite && nextEdgeOtherSite)
					currentOrder = NOT_MATTER;
				else if (!edgeBeforeMe) // if there is not edge in front of you, continue
				;
				// edgeBeforeMe is true for sure, so it is ommited
				else if (!nextEdgeSameSite)
				;
				// the edge is on the same site, we have to switch the order
				else
					switch (currentOrder)
					{
						case NOT_MATTER:

							break;
						case FOLLOWS:
							currentOrder = PRECEDES;
							break;
						case PRECEDES:
							currentOrder = FOLLOWS;
							break;
					}
				edgeBeforeMe = nextEdgeSameSite;
			}

			// the last track can be probably already set - it may have some value already
			// and that value can be different from current under some conditions
			if (!next.equals(path.getLastSubpath()) && !ordering.isTotallyOrdered(next.getLastTrack()))
			{
				ordering.setOrderOfLinesIfPossible(l1, l2, currentOrder, next, new IteratorWrapper<>(next.stationsIterator()));
				newValues.put(next, currentOrder);
			}
			// TODO and probably remove the edge
		}

	}

	/**
	 * Decides whether there is an edge before the currently placed common subpath.
	 *
	 * @param currentOrder The current order precedes/follows.
	 * @param cs           The last placed common subpath.
	 * @param preferred    The preferred line.
	 * @param other        The other line.
	 * @param sameSite     Shall we look on the same side where is the preferred line
	 *                     or on the other side.
	 * @return {@literal true} iff there is an edge before the line.
	 */
	private static boolean isEdgeBeforeMe(int currentOrder, CommonSubpath cs, Line preferred, Line other, boolean sameSite)
	{
		if (currentOrder == NOT_MATTER)
			return true;
		else
		{
			Track[] tracks = cs.getLastStation().getIncidentTracks(cs.getLastTrack());
			if (tracks.length == 1)
				return false;
			if (!(currentOrder == FOLLOWS ^ sameSite))
			{
				for (int i = 1; i < tracks.length; i++)
					if (preferred.containsTrack(tracks[i]))
						return false;
					else if (other.containsTrack(tracks[i]))
						return true;
			}
			else if (!(currentOrder == PRECEDES ^ sameSite))
			{
				// at zero is this line, so don't match it
				for (int i = tracks.length - 1; i > 0; i--)
					if (preferred.containsTrack(tracks[i]))
						return false;
					else if (other.containsTrack(tracks[i]))
						return true;

			}
			throw new IllegalArgumentException("unknown order " + currentOrder + " same site is " + sameSite);
		}
	}

	/**
	 * Decides whether the lines are bundle on extended common subpath.
	 * @param path The extended common subpath.
	 * @param problem The problem.
	 * @param ordering The ordering of lines.
	 * @return {@literal true} iff we know that lines are bundle on {@literal path}.
	 */
	private static boolean areLinesBundle(ExtendedCommonSubpath path, MLCMP problem,
		Ordering ordering)
	{
		return BundleOrderer.areLinesBundle(path.getPreferredLine(), path.getOtherLine(),
			new IteratorWrapper<>(path.trackIterator()), problem, ordering);
	}
}
