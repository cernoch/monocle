/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order.np;

import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * Library class that allows to split the isolated component into two.
 *
 * @author Petr Ryšavý
 */
public class MinCutSplitter
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private MinCutSplitter()
	{
	}

	/**
	 * Splits the component into two isolated components using the cut. The returned
	 * components can be both empty as well as null.
	 *
	 * @param graph  The isolated component to split.
	 * @param mincut A cut of this component.
	 * @return Two isolated components given by the cut.
	 */
	public static PairSet<Set<Track>> splitByMinCut(Set<Track> graph, Set<Track> mincut)
	{
		final int size = graph.size() - mincut.size();

		if (size == 0)
			return new PairSet<>(null, null);

		final Set<Track> s1 = new HashSet<>(size);
		final Deque<Track> q = new LinkedList<>();
		q.add(getTrack(graph, mincut));

		// find the first component via BFS
		while (!q.isEmpty())
		{
			final Track t = q.remove();
			s1.add(t);

			for (Track other : new IteratorWrapper<>(t.getVertex1().incidenTracksIterator()))
				if (graph.contains(other) && !s1.contains(other) && !mincut.contains(other))
					q.add(other);
			for (Track other : new IteratorWrapper<>(t.getVertex2().incidenTracksIterator()))
				if (graph.contains(other) && !s1.contains(other) && !mincut.contains(other))
					q.add(other);
		}

		// ... reconstruct the second (iterating over graph will be better as we don't
		// have to iterate over all incident tracks, only that in graph
		final Set<Track> s2 = new HashSet<>(size);
		for (Track t : graph)
			if (!s1.contains(t) && !mincut.contains(t))
				s2.add(t);

		return new PairSet<>(s1, s2);
	}

	/**
	 * Returns a track from the graph that is not part of the cut.
	 *
	 * @param graph  The graph.
	 * @param mincut The cut.
	 * @return A track.
	 */
	private static Track getTrack(Set<Track> graph, Set<Track> mincut)
	{
		for (Track t : graph)
			if (!mincut.contains(t))
				return t;
		return null;
	}
}