/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order.np;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.order.BundleOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.GreedyOrderDecider;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.IllegalOrderException;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.UndoableOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.orderoption.NoCrossingOrderOption;
import cz.cvut.fel.rysavpe1.mlcmp.orderoption.OneCrossingOrderOption;
import cz.cvut.fel.rysavpe1.mlcmp.orderoption.OrderOption;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.ListUtilities;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Library class that allows to find all the possible orders of lines.
 *
 * @author Petr Ryšavý
 */
public class OrderOptionFinder
{
	private static final List<OrderOption> PRECEDES_FOLLOWS_LIST;
	private static final List<OrderOption> FOLLOWS_PRECEDES_LIST;

	static
	{
		List<OrderOption> list = new ArrayList<>(2);
		list.add(NoCrossingOrderOption.FOLLOWS_ORDER_OPTION);
		list.add(NoCrossingOrderOption.PRECEDES_ORDER_OPTION);
		FOLLOWS_PRECEDES_LIST = Collections.unmodifiableList(list);
		List<OrderOption> list2 = new ArrayList<>(2);
		list2.add(NoCrossingOrderOption.PRECEDES_ORDER_OPTION);
		list2.add(NoCrossingOrderOption.FOLLOWS_ORDER_OPTION);
		PRECEDES_FOLLOWS_LIST = Collections.unmodifiableList(list2);
	}
	/**
	 * The possible orders of not matter common subpaths.
	 */
	private static final List<OrderOption> NOT_MATTER_LIST = PRECEDES_FOLLOWS_LIST;

	/**
	 * Don!t let anybody to create the class instance.
	 */
	private OrderOptionFinder()
	{
	}

	/**
	 * Finds all the possible orders for all the common subpaths.
	 *
	 * @param problem      The problem.
	 * @param ordering     The ordering.
	 * @param paths        The common subpaths of which we want to know the possible
	 *                     orders.
	 * @param notSolvedMap The map to their preferred order.
	 * @param tracks       The tracks with the given common subpaths. They belong to
	 *                     the mincut.
	 * @return List of possible orders for each of the common subpaths in
	 *         {@literal paths}.
	 */
	@SuppressWarnings("NestedAssignment")
	public static Map<CommonSubpath, List<OrderOption>> findPossibleOrders(MLCMP problem, UndoableOrdering ordering, Collection<CommonSubpath> paths, Map<CommonSubpath, Integer> notSolvedMap, Set<Track> tracks)
	{
		Map<CommonSubpath, List<OrderOption>> map = new HashMap<>(paths.size());
//		PriorityQueue<OrderEntry> queue = new PriorityQueue<>(paths.size());

		Map<CommonSubpath, Integer> extendedNotSolvedMap = new HashMap<>(notSolvedMap.size());
		for (Entry<CommonSubpath, Integer> e : notSolvedMap.entrySet())
		{
			int order = e.getValue();
			CommonSubpath cs = e.getKey();
			if (order == TrackLineOrder.NOT_ENOUGH_INFORMATION)
				extendedNotSolvedMap.put(cs, PreferredOptionFinder.orderLines(problem, ordering, cs));
			else
				extendedNotSolvedMap.put(cs, order);
		}

		// now try to find an order that won't lead to an error
		HashMap<CommonSubpath, OrderOption> partialSolution = new HashMap<>(paths.size());
		ordering.newBreakpoint();
		try
		{
			// at first set all orders with the most likely order precedes or follows,
			// then try to find appropriate order of not_matter and not_comparable orders
			final int[] currentOrderList = new int[]
			{
				TrackLineOrder.PRECEDES, TrackLineOrder.FOLLOWS,
				TrackLineOrder.NOT_MATTER, TrackLineOrder.NOT_COMPARABLE
			};
			for (int i : currentOrderList)
				for (Entry<CommonSubpath, Integer> e : extendedNotSolvedMap.entrySet())
					if (e.getValue() == i)
					{
						CommonSubpath cs = e.getKey();
						if (TrackLineOrder.isOrderPrecedesOrFollows(i))
							try
							{
								ordering.setOrderOfLines(cs.getFirstLine(), cs.getSecondLine(), i, cs, new IteratorWrapper<>(cs.stationsIterator()));
								partialSolution.put(cs, NoCrossingOrderOption.forOrder(i));
							}
							catch (IllegalOrderException | IllegalStateException ie)
							{
								partialSolution.put(cs, NoCrossingOrderOption.PRECEDES_ORDER_OPTION);
							}
						else
							optionIter:
								// TODO this iteration is not really nicely done - there should be added a breakpoint
								// and if needed, then backtracked
								for (OrderOption oo : findPossibleOrders(problem, ordering, cs, notSolvedMap.get(cs)))
									try
									{
										if (!ordering.isOrderSetPossible(cs.getFirstLine(), cs.getSecondLine(), oo, cs, new IteratorWrapper<>(cs.stationsIterator())))
											continue;
										ordering.setOrderOfLinesIfPossible(cs.getFirstLine(), cs.getSecondLine(), oo, cs, new IteratorWrapper<>(cs.stationsIterator()));
										break optionIter;
									}
									catch (IllegalOrderException | IllegalStateException ie)
									{
										break optionIter;
									}
									finally
									{
										partialSolution.put(cs, oo);
									}
					}
		}
		finally
		{
			ordering.undo();
		}

		for (CommonSubpath cs : paths)
			//			queue.add(new OrderEntry(cs, findPossibleOrders(problem, ordering, cs, notSolvedMap.get(cs))));
			map.put(cs, findPossibleOrders(problem, ordering, cs, notSolvedMap.get(cs), partialSolution.get(cs)));

//		RandomAccessMap<CommonSubpath, List<OrderOption>> map = new RandomAccessMap<>(paths.size());
//		OrderEntry e;
//		while((e = queue.poll()) != null)
//			map.put(e.cs, e.list);

		return map;
	}

	/**
	 * Finds all the possible orders for a common subpaths.
	 *
	 * @param problem  The problem.
	 * @param ordering The ordering.
	 * @param path     The common subpath of which we want to know the possible
	 *                 orders.
	 * @param order    The common subpath preferred order that the {@literal path} has.
	 * @return List of possible orders for the common subpath.
	 */
	public static List<OrderOption> findPossibleOrders(MLCMP problem, Ordering ordering, CommonSubpath path, int order, OrderOption pord)
	{
		switch (order)
		{
			case TrackLineOrder.NOT_MATTER:
				if(pord == NoCrossingOrderOption.PRECEDES_ORDER_OPTION)
					return PRECEDES_FOLLOWS_LIST;
				return FOLLOWS_PRECEDES_LIST;
			case TrackLineOrder.NOT_COMPARABLE:
				final int tracksNum = path.size();
				List<OrderOption> list = new ArrayList<>(tracksNum + 1);
				list.add(NoCrossingOrderOption.FOLLOWS_ORDER_OPTION);
				list.add(NoCrossingOrderOption.PRECEDES_ORDER_OPTION);

				if (tracksNum > 1)
				{
					final Iterator<Track> it = path.iterator();
					final Line l1 = path.getFirstLine();
					final Line l2 = path.getSecondLine();

					// set of bundle lines has to have at least one of its endpoints in the
					// possible crossings point, lines not in bundle must have both
					boolean last = BundleOrderer.areLinesBundle(l1, l2, it.next(), problem, ordering);
					boolean current;
					for (int i = 1; i < tracksNum; i++)
					{
						current = BundleOrderer.areLinesBundle(l1, l2, it.next(), problem, ordering);
						if (!last || last != current)
							list.add(new OneCrossingOrderOption(TrackLineOrder.FOLLOWS, i, tracksNum));
						last = current;
					}
				}
				return ListUtilities.moveToFront(list, pord);
			case TrackLineOrder.NOT_ENOUGH_INFORMATION:
				final Line l1 = path.getFirstLine();
				final Line l2 = path.getSecondLine();
				final int startOrder = GreedyOrderDecider.getPreferredOrder(l1, l2, path.getFirstStation(), path.getFirstTrack(), ordering);
				final int endOrder = GreedyOrderDecider.getPreferredOrder(l2, l1, path.getLastStation(), path.getLastTrack(), ordering);

				final boolean canBeStartPrecedes = startOrder != TrackLineOrder.PRECEDES;
				final boolean canBeStartFollows = startOrder != TrackLineOrder.FOLLOWS;
				final boolean canBeEndPrecedes = endOrder != TrackLineOrder.PRECEDES;
				final boolean canBeEndFollows = endOrder != TrackLineOrder.FOLLOWS;

				final boolean canBePrecedesToFollows = canBeStartPrecedes && canBeEndFollows;
				final boolean canBeFollowsToPrecedes = canBeStartFollows && canBeEndPrecedes;

				final int tracksNumNEI = path.size();
				List<OrderOption> listNEI = new ArrayList<>(tracksNumNEI + 1);

				listNEI.add(NoCrossingOrderOption.PRECEDES_ORDER_OPTION);
				listNEI.add(NoCrossingOrderOption.FOLLOWS_ORDER_OPTION);

				if (tracksNumNEI > 1)
				{
					final Iterator<Track> it = path.iterator();

					// set of bundle lines has to have at least one of its endpoints in the
					// possible crossings point, not bundle lines must have both
					boolean last = BundleOrderer.areLinesBundle(l1, l2, it.next(), problem, ordering);
					boolean current;
					for (int i = 1; i < tracksNumNEI; i++)
					{
						current = BundleOrderer.areLinesBundle(l1, l2, it.next(), problem, ordering);
						if (!last || last != current)
						{
							if (canBePrecedesToFollows)
								listNEI.add(new OneCrossingOrderOption(TrackLineOrder.FOLLOWS, i, tracksNumNEI));
							if (canBeFollowsToPrecedes)
								listNEI.add(new OneCrossingOrderOption(TrackLineOrder.PRECEDES, i, tracksNumNEI));
						}
						last = current;
					}
				}
				return ListUtilities.moveToFront(listNEI, pord);
			default:
				throw new IllegalArgumentException("Unknown line order " + order);
		}
	}

	/**
	 * Finds all the possible orders for a common subpaths.
	 *
	 * @param problem  The problem.
	 * @param ordering The ordering.
	 * @param path     The common subpath of which we want to know the possible
	 *                 orders.
	 * @param order    The common subpath preferred order that the {@literal path} has.
	 * @return List of possible orders for the common subpath.
	 */
	public static List<OrderOption> findPossibleOrders(MLCMP problem, Ordering ordering, CommonSubpath path, int order)
	{
		switch (order)
		{
			case TrackLineOrder.NOT_MATTER:
				return NOT_MATTER_LIST;
			case TrackLineOrder.NOT_COMPARABLE:
				final int tracksNum = path.size();
				List<OrderOption> list = new ArrayList<>(tracksNum + 1);
				list.add(NoCrossingOrderOption.FOLLOWS_ORDER_OPTION);
				list.add(NoCrossingOrderOption.PRECEDES_ORDER_OPTION);

				if (tracksNum > 1)
				{
					final Iterator<Track> it = path.iterator();
					final Line l1 = path.getFirstLine();
					final Line l2 = path.getSecondLine();

					// set of bundle lines has to have at least one of its endpoints in the
					// possible crossings point, not bundle lines must have both
					boolean last = BundleOrderer.areLinesBundle(l1, l2, it.next(), problem, ordering);
					boolean current;
					for (int i = 1; i < tracksNum; i++)
					{
						current = BundleOrderer.areLinesBundle(l1, l2, it.next(), problem, ordering);
						if (!last || last != current)
							list.add(new OneCrossingOrderOption(TrackLineOrder.FOLLOWS, i, tracksNum));
						last = current;
					}
				}
				return list;
			case TrackLineOrder.NOT_ENOUGH_INFORMATION:
				final Line l1 = path.getFirstLine();
				final Line l2 = path.getSecondLine();
				final int startOrder = GreedyOrderDecider.getPreferredOrder(l1, l2, path.getFirstStation(), path.getFirstTrack(), ordering);
				final int endOrder = GreedyOrderDecider.getPreferredOrder(l2, l1, path.getLastStation(), path.getLastTrack(), ordering);

				final boolean canBeStartPrecedes = startOrder != TrackLineOrder.PRECEDES;
				final boolean canBeStartFollows = startOrder != TrackLineOrder.FOLLOWS;
				final boolean canBeEndPrecedes = endOrder != TrackLineOrder.PRECEDES;
				final boolean canBeEndFollows = endOrder != TrackLineOrder.FOLLOWS;

				final boolean canBePrecedesToFollows = canBeStartPrecedes && canBeEndFollows;
				final boolean canBeFollowsToPrecedes = canBeStartFollows && canBeEndPrecedes;

				final int tracksNumNEI = path.size();
				List<OrderOption> listNEI = new ArrayList<>(tracksNumNEI + 1);

				// here we can use the most probable one order
				final int preford = PreferredOptionFinder.orderLines(problem, ordering, path);

				if (preford == TrackLineOrder.PRECEDES)
				{
					listNEI.add(NoCrossingOrderOption.PRECEDES_ORDER_OPTION);
					listNEI.add(NoCrossingOrderOption.FOLLOWS_ORDER_OPTION);
				}
				else
				{
					listNEI.add(NoCrossingOrderOption.FOLLOWS_ORDER_OPTION);
					listNEI.add(NoCrossingOrderOption.PRECEDES_ORDER_OPTION);
				}

				if (tracksNumNEI > 1)
				{
					final Iterator<Track> it = path.iterator();

					// set of bundle lines has to have at least one of its endpoints in the
					// possible crossings point, not bundle lines must have both
					boolean last = BundleOrderer.areLinesBundle(l1, l2, it.next(), problem, ordering);
					boolean current;
					for (int i = 1; i < tracksNumNEI; i++)
					{
						current = BundleOrderer.areLinesBundle(l1, l2, it.next(), problem, ordering);
						if (!last || last != current)
						{
							if (canBePrecedesToFollows)
								listNEI.add(new OneCrossingOrderOption(TrackLineOrder.FOLLOWS, i, tracksNumNEI));
							if (canBeFollowsToPrecedes)
								listNEI.add(new OneCrossingOrderOption(TrackLineOrder.PRECEDES, i, tracksNumNEI));
						}
						last = current;
					}
				}
				return listNEI;
			default:
				throw new IllegalArgumentException("Unknown line order " + order);
		}
	}

	private static final class OrderEntry implements Comparable<OrderEntry>
	{
		private CommonSubpath cs;
		private List<OrderOption> list;
		private int listSize;

		public OrderEntry(CommonSubpath cs, List<OrderOption> list)
		{
			this.cs = cs;
			setList(list);
		}

		@Override
		public int compareTo(OrderEntry o)
		{
			return -(listSize - o.listSize);
		}

		public void setList(List<OrderOption> list)
		{
			this.list = list;
			this.listSize = list.size();
		}
	}
}
