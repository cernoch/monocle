/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.order;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpathFinder;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.TrackLineOrder;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.util.HashMap;
import java.util.Map;

/**
 * A library class that allows to decide the common subpath preferred order.
 *
 * @author Petr Ryšavý
 */
public final class GreedyOrderer
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private GreedyOrderer()
	{
	}

	/**
	 * Orders the lines in the problem.
	 *
	 * @param problem  The problem.
	 * @param ordering The line ordering where the order will be stored.
	 * @return The map of common subpaths with preferred order that is not precedes
	 *         nor follows.
	 */
	public static Map<CommonSubpath, Integer> order(MLCMP problem, Ordering ordering)
	{
		return order(problem, new IteratorWrapper<>(problem.trackIterator()), ordering, true);
	}

	/**
	 * Orders the lines in the problem.
	 *
	 * @param problem  The problem.
	 * @param it       The iterator of tracks where the preferred order shall be
	 *                 calculated.
	 * @param ordering The line ordering where the order will be stored.
	 * @return The map of common subpaths with preferred order that is not precedes
	 *         nor follows.
	 */
	public static Map<CommonSubpath, Integer> order(MLCMP problem, Iterable<Track> it,
		Ordering ordering)
	{
		return order(problem, it, ordering, false);
	}

	/**
	 * Orders the lines in the problem.
	 *
	 * @param problem   The problem.
	 * @param it        The iterator of tracks where the preferred order shall be
	 *                  calculated.
	 * @param ordering  The line ordering where the order will be stored.
	 * @param onlyEmpty If this is {@literal true} than it will be decided only on
	 *                  tracks where the order is {@literal NOT_DETERMINED}.
	 * @return The map of common subpaths with preferred order that is not precedes
	 *         nor follows.
	 */
	public static Map<CommonSubpath, Integer> order(MLCMP problem, Iterable<Track> it,
		Ordering ordering, boolean onlyEmpty)
	{
		final Map<CommonSubpath, Integer> toDo = new HashMap<>();

		trackLoop:
			for (Track t : it)
			{
				if (ordering.isTotallyOrdered(t))
					continue;

				final Line[] lines = problem.getLineArray(t);
				for (int i = 0; i < lines.length; i++)
					for (int j = i + 1; j < lines.length; j++)
						if (!onlyEmpty || ordering.getOrderOfLines(lines[i], lines[j], t, t.getVertex1()) == TrackLineOrder.NOT_DETERMINED)
						{
							if (ordering.isTotallyOrdered(t))
								continue trackLoop;

							CommonSubpath subpath = CommonSubpathFinder.findCommonSubpath(problem, lines[i], lines[j], t);
							final int order = GreedyOrderDecider.orderLines(problem, ordering, subpath);
							ordering.setOrderOfLinesIfPossible(lines[i], lines[j], order, subpath, new IteratorWrapper<>(subpath.stationsIterator()));

							if (order != TrackLineOrder.PRECEDES && order != TrackLineOrder.FOLLOWS)
								toDo.put(subpath, order);
						}
			}

		return toDo;
	}
}
