/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.commonsubpath;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.utilities.ListUtilities;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

/**
 * The common subpath dat structure.
 *
 * @author Petr Ryšavý
 */
public class CommonSubpath implements Iterable<Track>
{
	/**
	 * The list of common tracks.
	 */
	private final List<Track> subpath;
	/**
	 * First line.
	 */
	private final Line l1;
	/**
	 * Second line.
	 */
	private final Line l2;
	/**
	 * The station associated with the first track of the subpath. This station is
	 * endpoint of the subpath.
	 */
	private final Station firstStation;
	/**
	 * The station associated with the last track of the subpath. This station is
	 * endpoint of the subpath.
	 */
	private final Station lastStation;

	/**
	 * Creates new common subpath.
	 *
	 * @param subpath The subpath track.
	 * @param l1      First line.
	 * @param l2      Second line.
	 */
	@SuppressWarnings("NestedAssignment")
	protected CommonSubpath(List<Track> subpath, Line l1, Line l2)
	{
		if (l1 == null || l2 == null)
			throw new NullPointerException("The line cannot be null.");
		if (subpath == null)
			subpath = Collections.emptyList();
		if (l1.equals(l2))
			throw new IllegalArgumentException("Lines on common subpath cannot be same.");

		this.subpath = subpath;
		this.l1 = l1;
		this.l2 = l2;

		if (subpath.isEmpty())
		{
			firstStation = lastStation = null;
			return;
		}

		final Track t0 = subpath.get(0);
		if (subpath.size() == 1)
		{
			firstStation = t0.getVertex1();
			lastStation = t0.getVertex2();
			return;
		}

		firstStation = t0.getOtherVertex(t0.getCommonStation(subpath.get(1)));
		final int lastIndex = subpath.size() - 1;
		final Track last = subpath.get(lastIndex);
		lastStation = last.getOtherVertex(last.getCommonStation(subpath.get(lastIndex - 1)));
	}

	/**
	 * Creates new common subpath long just one edge.
	 *
	 * @param t            The edge.
	 * @param firstStation The starting point of the subpath.
	 * @param l1           First line.
	 * @param l2           Second line.
	 */
	protected CommonSubpath(Track t, Station firstStation, Line l1, Line l2)
	{
		this.subpath = new LinkedList<>();
		this.subpath.add(t);
		this.firstStation = firstStation;
		this.lastStation = t.getOtherVertex(firstStation);
		this.l1 = l1;
		this.l2 = l2;
	}

	/**
	 * Decides whether this path contains the given track.
	 *
	 * @param t The track to search.
	 * @return If this path contains the given track.
	 */
	public boolean contains(Track t)
	{
		return subpath.contains(t);
	}

	/**
	 * Decides whether this path is empty.
	 *
	 * @return {@code true} iff {@code this.size() == 0}.
	 */
	public boolean isEmpty()
	{
		return subpath.isEmpty();
	}

	/**
	 * Gets the track at given index of this subpath.
	 *
	 * @param index Index of the track to search.
	 * @return The track at given index.
	 */
	public Track get(int index)
	{
		return subpath.get(index);
	}

	/**
	 * Returns the index of the given track in this path.
	 *
	 * @param t The track to search.
	 * @return Index of the track.
	 */
	public int indexOf(Track t)
	{
		return subpath.indexOf(t);
	}

	/**
	 * Returns the number of edges of this path.
	 *
	 * @return Length of this path.
	 */
	public int size()
	{
		return subpath.size();
	}

	@Override
	public Iterator<Track> iterator()
	{
		return subpath.iterator();
	}

	/**
	 * Returns first line.
	 *
	 * @return One of the lines.
	 */
	public Line getFirstLine()
	{
		return l1;
	}

	/**
	 * Returns second line.
	 *
	 * @return One of the lines.
	 */
	public Line getSecondLine()
	{
		return l2;
	}

	/**
	 * Returns the string with station names. The format is same as defined in the
	 * textual input, what means {@code (s1)-(s2)-...-(sn)}.
	 *
	 * @return Textual represenation of the path.
	 */
	public String toPathString()
	{
		if (subpath.isEmpty())
			return "";
		if (subpath.size() == 1)
			return '(' + firstStation.getValue() + ")-(" + lastStation.getValue() + ')';

		Station s = firstStation;

		StringBuilder sb = new StringBuilder();
		sb.append('(').append(s.getValue()).append(')');
		for (Track t : subpath)
		{
			s = t.getOtherVertex(s);
			sb.append("-(").append(s.getValue()).append(')');
		}

		return sb.toString();
	}

	/**
	 * Returns the station with that the path begins. If the path is empty, then the
	 * value is null.
	 *
	 * @return First station of the path.
	 */
	public Station getFirstStation()
	{
		return firstStation;
	}

	/**
	 * Returns the station with that the path end. If the path is empty, then the
	 * value is null.
	 *
	 * @return Last station of the path.
	 */
	public Station getLastStation()
	{
		return lastStation;
	}

	/**
	 * Returns the edge with that the path begins. If the path is empty, then the
	 * value is null.
	 *
	 * @return First edge of the path.
	 */
	public Track getFirstTrack()
	{
		if (subpath.isEmpty())
			return null;
		return subpath.get(0);
	}

	/**
	 * Returns the edge with that the path end. If the path is empty, then the value
	 * is null.
	 *
	 * @return Last edge of the path.
	 */
	public Track getLastTrack()
	{
		if (subpath.isEmpty())
			return null;
		return subpath.get(subpath.size() - 1);
	}

	@Override
	public String toString()
	{
		return "CommonSubpath{l1=" + l1 + ", l2=" + l2 + "}\n\tPath : " + toPathString();
	}

	/**
	 * Returns new iterator of stations. The iterator goes from first station to last
	 * and the direction corresponds to list of tracks returned by track iterator.
	 *
	 * @return Stations iterator.
	 */
	public Iterator<Station> stationsIterator()
	{
		return new StationIterator();
	}

	/**
	 * Returns part of this subpath.
	 *
	 * @param firstTrack       Index of first track to be included in this subpath.
	 * @param firstNotIncluded Exclusive index of last track.
	 * @return Part of this path.
	 */
	public CommonSubpath subpath(int firstTrack, int firstNotIncluded)
	{
		if (firstTrack == firstNotIncluded - 1)
			if (firstTrack == 0)
				return new CommonSubpath(subpath.get(0), firstStation, l1, l2);
			else
			{
				final Track t = subpath.get(firstTrack);
				return new CommonSubpath(t, t.getCommonStation(subpath.get(firstTrack - 1)), l1, l2);
			}

		return new CommonSubpath(subpath.subList(firstTrack, firstNotIncluded), l1, l2);
	}

	/**
	 * Returns end of this subpath.
	 *
	 * @param firstTrack Index of first track to be included in this subpath.
	 * @return Part of this path.
	 */
	public CommonSubpath suffix(int firstTrack)
	{
		if (firstTrack == subpath.size() - 1)
		{
			final Track t = subpath.get(firstTrack);
			return new CommonSubpath(t, t.getOtherVertex(lastStation), l1, l2);
		}

		return new CommonSubpath(subpath.subList(firstTrack, subpath.size()), l1, l2);
	}

	/**
	 * Returns beginning of this subpath.
	 *
	 * @param firstNotIncluded Exclusive index of last track.
	 * @return Part of this path.
	 */
	public CommonSubpath prefix(int firstNotIncluded)
	{
		if (firstNotIncluded == 1)
			return new CommonSubpath(subpath.get(0), firstStation, l1, l2);

		return new CommonSubpath(subpath.subList(0, firstNotIncluded), l1, l2);
	}

	/**
	 * Returns part of this subpath.
	 *
	 * @param firstTrack       First track to be included in this subpath.
	 * @param firstNotIncluded Last track (exclusive).
	 * @return Part of this path.
	 */
	public CommonSubpath subpath(Track firstTrack, Track firstNotIncluded)
	{
		return subpath(subpath.indexOf(firstTrack), subpath.indexOf(firstNotIncluded));
	}

	/**
	 * Returns end of this subpath.
	 *
	 * @param firstTrack First track to be included in this subpath.
	 * @return Part of this path.
	 */
	public CommonSubpath suffix(Track firstTrack)
	{
		return suffix(subpath.indexOf(firstTrack));
	}

	/**
	 * Returns beginning of this subpath.
	 *
	 * @param firstNotIncluded Last track (exclusive).
	 * @return Part of this path.
	 */
	public CommonSubpath prefix(Track firstNotIncluded)
	{
		return prefix(subpath.indexOf(firstNotIncluded));
	}

	/**
	 * Returns the copy of this subpath that contains edges in the opposite order.
	 *
	 * @return This subpath in reversed order of edges.
	 */
	public CommonSubpath reverse()
	{
		if (subpath.size() == 1)
			return new CommonSubpath(subpath.get(0), lastStation, l2, l1);
		else
		{
			final List<Track> path = new LinkedList<>(subpath);
			Collections.reverse(path);
			return new CommonSubpath(path, l2, l1);
		}
	}

	/**
	 * Returns a common subpath (not allways different) that wrappes the same path,
	 * but has the given line as the first line:
	 *
	 * @param l1 The line that shall be the first in the returned subpath.
	 * @return Path equal to this, but with given preferred line.
	 */
	public CommonSubpath pathWithGivenLineOrder(Line l1)
	{
		if (this.l1.equals(l1))
			return this;
		else if (this.l2.equals(l1))
			if (subpath.size() == 1)
				return new CommonSubpath(subpath.get(0), firstStation, this.l2, this.l1);
			else
				return new CommonSubpath(subpath, this.l2, this.l1);
		else
			throw new IllegalArgumentException("The line must be in this subpath.");
	}

	@Override
	public int hashCode()
	{
		final int l1HC = Objects.hashCode(this.l1);
		final int l2HC = Objects.hashCode(this.l2);
		final int first = Objects.hashCode(this.firstStation);
		final int last = Objects.hashCode(this.lastStation);
		int hash = 7;
		if (l1HC < l2HC)
		{
			hash = 23 * hash + l1HC;
			hash = 23 * hash + l2HC;
		}
		else
		{
			hash = 23 * hash + l2HC;
			hash = 23 * hash + l1HC;
		}
		if (first < last)
		{
			hash = 23 * hash + first;
			hash = 23 * hash + last;
		}
		else
		{
			hash = 23 * hash + last;
			hash = 23 * hash + first;
		}
		return hash;
	}

	/**
	 * {@inheritDoc }
	 * Returns {@code true} if two subpaths are same. There is allowed one small
	 * inpunctuality, the lines can be swapped or the path can be reversed.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		final CommonSubpath other = (CommonSubpath) obj;
		if (!pathEquals(other))
			return false;

		if (Objects.equals(this.l1, other.l1) && Objects.equals(this.l2, other.l2))
			return true;
		if (Objects.equals(this.l2, other.l1) && Objects.equals(this.l1, other.l2))
			return true;

		return false;
	}

	/**
	 * Checks whether this path described the same path in the graph. This is one of
	 * the conditions required for subpaths to be equal.
	 *
	 * @param other The other common subpath.
	 * @return {@code true} if this path is same as the {@code other} or this path is
	 *         only reverse of the {@code other}. Otherwise returns {@code false}.
	 */
	public boolean pathEquals(CommonSubpath other)
	{
		if (this.subpath.size() != other.subpath.size())
			return false;

		if (Objects.equals(this.firstStation, other.firstStation)
			&& Objects.equals(this.lastStation, other.lastStation))
		{
			// the common subpath can be also a cycle
			if (!subpath.equals(other.subpath)
				&& (!Objects.equals(this.firstStation, this.lastStation)
				|| !ListUtilities.elementsEqualsReversed(this.subpath, other.subpath)))
				return false;

		}
		else if (Objects.equals(this.lastStation, other.firstStation)
			&& Objects.equals(this.firstStation, other.lastStation))
		{
			if (!ListUtilities.elementsEqualsReversed(this.subpath, other.subpath))
				return false;
		}
		else
			return false;

		return true;
	}

	/**
	 * The stations iterator. Iterator wraps the track iterator and retuns stations
	 * one by one.
	 */
	private class StationIterator implements Iterator<Station>
	{
		/**
		 * The wrapped iterator.
		 */
		final Iterator<Track> trackIterator = iterator();
		/**
		 * Last returned station.
		 */
		Station s = null;

		@Override
		public boolean hasNext()
		{
			return trackIterator.hasNext();
		}

		@Override
		@SuppressWarnings("NestedAssignment")
		public Station next()
		{
			if (s == null)
			{
				s = firstStation;
				return s;
			}

			return s = trackIterator.next().getOtherVertex(s);
		}

		/**
		 * This operation is not supported.
		 */
		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("The remove operation is not supported.");
		}
	}
}
