/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.commonsubpath;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalEdgeException;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.util.LinkedList;

/**
 * This class defines static method that allows to find common subpath of two lines.
 *
 * @author Petr Ryšavý
 */
public class CommonSubpathFinder
{
	/**
	 * Don't let anybody to create the class instance.
	 */
	private CommonSubpathFinder()
	{
	}

	/**
	 * Finds the common subpath of two lines. The common subpath of two lines is
	 * longest path such that all innder vertices of this path have degree 2 with
	 * respect to the given two lines. Both lines must contain each of tracks that
	 * belong to this path.
	 *
	 * @param problem The problem where the common subpath shall be searched for.
	 * @param l1      The first line.
	 * @param l2      The second line.
	 * @param start   The track indentifying the common subpath of {@code l1} and
	 *                {@code l2}. This track must contain both lines, otherwise an
	 *                exception is thrown.
	 * @return Common subpath of two lines. This path is never empty and contains at
	 *         leas the {@code start} track.
	 * @throws IllegalEdgeException If the track does not contain any of the lines.
	 */
	public static CommonSubpath findCommonSubpath(MLCMP problem, Line l1, Line l2, Track start)
	{
		if (!l1.containsTrack(start) || !l2.containsTrack(start))
			throw new IllegalEdgeException("The given edge must contain both lines in order to find common subpath.");

		final LinkedList<Track> commonSubpath = new LinkedList<>();
		commonSubpath.add(start);

		findSubpathEdge(problem, l1, l2, start, start.getVertex1(), commonSubpath, true);
		findSubpathEdge(problem, l1, l2, start, start.getVertex2(), commonSubpath, false);

		return new CommonSubpath(commonSubpath, l1, l2);
	}

	/**
	 * Searches recursively for the common subpath.
	 *
	 * @param problem        The problem.
	 * @param l1             The first line.
	 * @param l2             The second line.
	 * @param start          The last track already found added in the common
	 *                       subpath.
	 * @param s              The last found vertex of the subpath. One of the
	 *                       endpoints (that outer) of the {@code start} track.
	 * @param subpath        The subpath already found.
	 * @param addToBeginning Shall be tracks added at the beginning or at the end of
	 *                       the list.
	 */
	private static void findSubpathEdge(MLCMP problem, Line l1, Line l2,
		Track start, Station s,
		LinkedList<Track> subpath, boolean addToBeginning)
	{
		if (!l1.isInline(s) || !l2.isInline(s))
			return;

		// now we know that the station is inline for both of lines
		// hence we need only to find the other track for both lines and find out
		// whether is same
		for (Track t : new IteratorWrapper<>(s.incidenTracksIterator()))
			if (t != start)
				if (l1.containsTrack(t))
					// we found common subpath edge
					if (l2.containsTrack(t))
					{
						if (addToBeginning)
							subpath.addFirst(t);
						else
							subpath.addLast(t);

						// recursively continue
						findSubpathEdge(problem, l1, l2, t, t.getOtherVertex(s), subpath, addToBeginning);
					}
					// we found end of common subpath
					else
						return;
	}
}
