/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.commonsubpath;

import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.ReferencedHashSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Library class that finds a cycle in the graph.
 *
 * @author Petr Ryšavý
 */
public final class CycleFinder
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private CycleFinder()
	{
	}

	/**
	 * Finds all cycles of length of three in the given isolated component.
	 *
	 * @param component The component to search.
	 * @param problem   The problem to that the component belong.
	 * @return The list of all cycles of length 3 in the isolated component.
	 */
	public static Collection<List<Track>> findCycles(Set<Track> component, MLCMP problem)
	{
		ReferencedHashSet<Track> comp = new ReferencedHashSet<>(component);
		List<List<Track>> retval = new ArrayList<>(component.size() / 4);

		outer:
			for (Track t : component)
				if (comp.contains(t))
					for (Track t2 : new IteratorWrapper<>(t.getVertex1().incidenTracksIterator()))
					{
						if (t2.equals(t) || !comp.contains(t2))
							continue;

						final Station common = t.getCommonStation(t2);
						final Track t3 = new Track(t.getOtherVertex(common), t2.getOtherVertex(common));

						if (comp.contains(t3))
						{
							// we found a cycle of three edges, remember it
							List<Track> l = new ArrayList<>(3);
							l.add(t);
							l.add(t2);
							l.add(comp.getStoredRefference(t3));
							retval.add(l);
							comp.remove(t);
							comp.remove(t2);
							comp.remove(t3);
							continue outer;
						}
					}

		return retval;
	}
}