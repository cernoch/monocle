/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.commonsubpath;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * The extended common subpath. A path is an extended common subpath of lines l1 and
 * l2 iff all edges contain l1 and l2 and moreover no inner vertex is the cross
 * vertex for l1. The line l2 can contain cross vertices on this path. Moreover we
 * want this path to be as long as possible.
 *
 * @author Petr Ryšavý
 */
public class ExtendedCommonSubpath implements Iterable<CommonSubpath>
{
	/**
	 * The list of common subpaths that for this path.
	 */
	private LinkedList<CommonSubpath> list;

	/**
	 * Creates new extended common subpaths.
	 *
	 * @param list A list of the subsequent common subpaths that form this extended
	 *             common subpath.
	 */
	protected ExtendedCommonSubpath(List<CommonSubpath> list)
	{
		this(list, list.get(0).getFirstLine());
	}

	/**
	 * Creates new extended common subpaths.
	 *
	 * @param list       A list of the subsequent common subpaths that form this
	 *                   extended common subpath.
	 * @param prefferred The line that has no cross vertices on this extended common
	 *                   subpath.
	 * @throws NullPointerException If the {@code list} is null.
	 * @throws IllegalArgumentException If the {@code list} is empty.
	 */
	protected ExtendedCommonSubpath(List<CommonSubpath> list, Line prefferred)
	{
		if (list == null)
			throw new NullPointerException("List cannot be null");
		if (list.isEmpty())
			throw new IllegalArgumentException("List must contain at least one common subpath.");
		if (list.size() == 1)
		{
			this.list = new LinkedList<>(list);
			return;
		}
		this.list = new LinkedList<>();

		CommonSubpath first = list.get(0);
		Station currentStation = first.getLastStation();
		CommonSubpath current = list.get(1);

		// find the proper direction of first subpath
		if (!currentStation.equals(current.getFirstStation())
			&& !currentStation.equals(current.getLastStation()))
		{
			this.list.add(first.reverse().pathWithGivenLineOrder(prefferred));
			currentStation = first.getLastStation();
		}
		else
			this.list.add(first.pathWithGivenLineOrder(prefferred));

		Iterator<CommonSubpath> it = list.iterator();
		it.next();

		// now add all the subpaths so that they are well oriented
		while (it.hasNext())
		{
			current = it.next();

			if (currentStation.equals(current.getFirstStation()))
			{
				this.list.add(current.pathWithGivenLineOrder(prefferred));
				currentStation = current.getLastStation();
			}
			else if (currentStation.equals(current.getLastStation()))
			{
				this.list.add(current.reverse().pathWithGivenLineOrder(prefferred));
				currentStation = current.getFirstStation();
			}
		}
	}

	@Override
	public Iterator<CommonSubpath> iterator()
	{
		return list.iterator();
	}

	/**
	 * Returns the n-th common subpath.
	 * @param index The index of the common subpath.
	 * @return The desired common subpath.
	 */
	public CommonSubpath get(int index)
	{
		return list.get(index);
	}

	/**
	 * Returns the first common subpath.
	 * @return The first common subpath.
	 */
	public CommonSubpath getFirstSubpath()
	{
		return list.getFirst();
	}

	/**
	 * Returns the last common subpath.
	 * @return The last common subpath.
	 */
	public CommonSubpath getLastSubpath()
	{
		return list.getLast();
	}

	/**
	 * Returns the preferred line.
	 * @return Line l1 from the class comment.
	 */
	public Line getPreferredLine()
	{
		return list.getFirst().getFirstLine();
	}

	/**
	 * Returns the other line.
	 * @return Line l2 from the class comment.
	 */
	public Line getOtherLine()
	{
		return list.getFirst().getSecondLine();
	}

	/**
	 * Returns the number of common subpaths that form this extended common subpath.
	 * @return The length of this extended common subpath.
	 */
	public int size()
	{
		return list.size();
	}

	/**
	 * Returns an iterator over the tracks.
	 * @return Track iterator.
	 */
	public Iterator<Track> trackIterator()
	{
		return new TrackIterator();
	}

	/**
	 * Returns an iterator over the stations.
	 * @return Stations iterator.
	 */
	public Iterator<Station> stationIterator()
	{
		return new StationIterator();
	}

	/**
	 * The tracks iterator.
	 */
	private final class TrackIterator implements Iterator<Track>
	{
		/**
		 * Iterator of the commons subpaths.
		 */
		private Iterator<CommonSubpath> csIterator;
		/**
		 * Iterator over the tracks on the current subpath.
		 */
		private Iterator<Track> trackIterator;

		/**
		 * Creates new iterator.
		 */
		private TrackIterator()
		{
			csIterator = iterator();
			trackIterator = Collections.emptyIterator();
			checkTrackIterator();
		}

		@Override
		public boolean hasNext()
		{
			return trackIterator.hasNext();
		}

		@Override
		public Track next()
		{
			Track t = trackIterator.next();
			checkTrackIterator();
			return t;
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("Not supported operation.");
		}

		/**
		 * Ensures that the track iterator has next element if possible.
		 */
		private void checkTrackIterator()
		{
			if (trackIterator.hasNext())
				return;

			if (csIterator.hasNext())
			{
				trackIterator = csIterator.next().iterator();
				checkTrackIterator();
			}
		}
	}

	/**
	 * The stations iterator. Iterator wraps the track iterator and retuns stations
	 * one by one.
	 */
	private class StationIterator implements Iterator<Station>
	{
		/**
		 * The wrapped iterator.
		 */
		final Iterator<Track> trackIterator = trackIterator();
		/**
		 * Last returned station.
		 */
		Station s = null;

		@Override
		public boolean hasNext()
		{
			return trackIterator.hasNext();
		}

		@Override
		@SuppressWarnings("NestedAssignment")
		public Station next()
		{
			if (s == null)
			{
				s = getFirstSubpath().getFirstStation();
				return s;
			}

			return s = trackIterator.next().getOtherVertex(s);
		}

		/**
		 * This operation is not supported.
		 */
		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("The remove operation is not supported.");
		}
	}
}
