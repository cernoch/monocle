/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.commonsubpath;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalEdgeException;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.util.LinkedList;

/**
 * This class defines static method that allows to find extended common subpath of
 * two lines.
 *
 * @author Petr Ryšavý
 */
public class ExtendedCommonSubpathFinder
{
	/**
	 * Don't let anybody to create the class instance.
	 */
	private ExtendedCommonSubpathFinder()
	{
	}

	/**
	 * Finds the extended common subpath.
	 *
	 * @param problem The problem.
	 * @param l1      The first line.
	 * @param l2      The second line.
	 * @param cs      The starting common subpath.
	 * @return The extended common subpath or {@code null} if there is not a one.
	 * @throws IllegalEdgeException     If {@code cs} is not common subpath of lines.
	 * @throws IllegalArgumentException If the lines are equal.
	 */
	public static ExtendedCommonSubpath findCommonSubpath(MLCMP problem, Line l1, Line l2, CommonSubpath cs)
	{
		if (!l1.containsTrack(cs.getFirstTrack()) || !l2.containsTrack(cs.getFirstTrack()))
			throw new IllegalEdgeException("The given subpath must contain both lines in order to find common subpath.");
		if (l1.equals(l2))
			throw new IllegalArgumentException("Lines cannot be equal.");

		final LinkedList<CommonSubpath> commonSubpath = new LinkedList<>();
		commonSubpath.add(cs);

		Line preferred = null;
		Line other = null;
		if (l1.isInline(cs.getFirstStation()) && l1.isInline(cs.getLastStation()))
		{
			preferred = l1;
			other = l2;
		}
		else if (l2.isInline(cs.getFirstStation()) && l2.isInline(cs.getLastStation()))
		{
			preferred = l2;
			other = l1;
		}

		if (preferred == null)
			return null;

		findSubpathEdge(problem, preferred, other, cs, cs.getFirstStation(), commonSubpath, true);
		findSubpathEdge(problem, preferred, other, cs, cs.getLastStation(), commonSubpath, false);

		return new ExtendedCommonSubpath(commonSubpath, preferred);
	}

	/**
	 * Searches recursively for the extended common subpath.
	 *
	 * @param problem        The problem.
	 * @param l1             The first line.
	 * @param l2             The second line.
	 * @param cs             The last common subpath already found and added in the
	 *                       extended common subpath.
	 * @param s              The last found vertex of the subpath. One of the
	 *                       endpoints (that outer) of the {@code cs} common subpath.
	 * @param subpath        The extended subpath already found.
	 * @param addToBeginning Shall be subpaths added at the beginning or at the end
	 *                       of the list.
	 */
	private static void findSubpathEdge(MLCMP problem, Line l1, Line l2, CommonSubpath cs,
		Station s,
		LinkedList<CommonSubpath> subpath, boolean addToBeginning)
	{
		// for the preferred line must be the station inline, the other can do everything
		if (!l1.isInline(s))
			return;

		// now we know that the station is inline for the preferred line
		// hence we need only to find the other track for both lines and find out
		// whether is same
		for (Track t : new IteratorWrapper<>(s.incidenTracksIterator()))
			if (!cs.contains(t))
				if (l1.containsTrack(t))
					// we found common subpath edge
					if (l2.containsTrack(t))
					{
						final CommonSubpath newPath = CommonSubpathFinder.findCommonSubpath(problem, l1, l2, t);
						if (addToBeginning)
							subpath.addFirst(newPath);
						else
							subpath.addLast(newPath);

						// recursively continue
						if (s.equals(newPath.getFirstStation()))
							findSubpathEdge(problem, l1, l2, newPath, newPath.getLastStation(), subpath, addToBeginning);
						else
							findSubpathEdge(problem, l1, l2, newPath, newPath.getFirstStation(), subpath, addToBeginning);
					}
					// we found end of extended common subpath
					else
						return;
	}
}
