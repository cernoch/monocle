/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.rysavpe1.mlcmp.eval;

import cz.cvut.fel.rysavpe1.mlcmp.input.XMLTouristTrailGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.Test;
import org.junit.Ignore;
import org.xml.sax.SAXException;

/**
 *
 * @author Petr Ryšavý
 */
public class PlainRunTest
{
	public PlainRunTest()
	{
	}

	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void testSomeMethod() throws IOException, GraphReadingException, InterruptedException, SAXException
	{
		//Path p = Paths.get("C:", "Users", "Petr Ryšavý", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
//		Path p = Paths.get("C:", "Users", "Petr Rysavy", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
//		Path p = Paths.get("C:", "Users", "Petr Ryšavý", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
//		Path p = Paths.get("C:", "Users", "Petr Rysavy", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "trams.in");
//		Path p = Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "trams.in");
//		Path p = Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_order", "test", "badGraph.in");
		
//		Path p = Paths.get("C:", "Users", "Petr", "Dropbox", "FEL", "Bakalářská práce", "sources", "MLCMP_input", "test", "touristTrails.in");
		Path p = Paths.get("C:", "Users", "Petr", "Documents", "BP", "czech-republic-latest.osm");

//		PlainRun.run(new PlainTextGraphReader(new OutputStreamWriter(System.out)).readGraph(p), 10000, false);
		PlainRun.run(new XMLTouristTrailGraphReader(new OutputStreamWriter(System.out)).readGraph(p), 10000, false);
	}
}