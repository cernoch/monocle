/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.eval.Statistics;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.SimpleDirectedGraph;
import cz.cvut.fel.rysavpe1.mlcmp.order.BundleOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.DependencyGraphBuilder;
import cz.cvut.fel.rysavpe1.mlcmp.order.ExtendedGreedyOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.GreedyOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.NotEnoughInformationResolver;
import cz.cvut.fel.rysavpe1.mlcmp.order.OptimalityChecker;
import cz.cvut.fel.rysavpe1.mlcmp.order.TailCutter;
import cz.cvut.fel.rysavpe1.mlcmp.order.utililies.UnsolvedSubpathsFinder;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.IllegalOrderException;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.PairSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The part of the program where the greedy part is done.
 *
 * @author Petr Ryšavý
 */
public class GreedyPartThread extends Thread
{
	/**
	 * The problem to solve.
	 */
	private final MLCMP problem;
	/**
	 * Line ordering where the optimum will be saved.
	 */
	private final LineOrdering ordering;
	/**
	 * There will be stored the number of common subpaths that are not solved yet.
	 */
	private int unsolved;
	/**
	 * There will be stored number of tracks that are indefinite.
	 */
	private int indefiniteTracks;
	/**
	 * There will be stored unsolved (by the greedy part) common subpaths.
	 */
	private Map<CommonSubpath, Integer> toDo;

	/**
	 * Creates new thread for solving the greedy part of the program.
	 *
	 * @param problem  The problem to solve.
	 * @param ordering Line ordering where the optimum will be saved.
	 */
	public GreedyPartThread(MLCMP problem, LineOrdering ordering)
	{
		this.problem = problem;
		this.ordering = ordering;
	}

	@Override
	public void run()
	{
		try
		{
			toDo = GreedyOrderer.order(problem, ordering);
			TailCutter.cutTails(problem, ordering, toDo);
//			ExtendedGreedyOrderer.order(problem, ordering, toDo);
			HashMap<PairSet<Line>, List<SimpleDirectedGraph<CommonSubpath>>> graph = DependencyGraphBuilder.getDependencyGraph(problem, ordering, toDo);
			NotEnoughInformationResolver.resolve(problem, ordering, toDo, graph);
			BundleOrderer.order(problem, ordering, toDo);

			OptimalityChecker.checkPartialOptimality(problem, ordering);
		}
		catch (IllegalOrderException ioe)
		{
			System.err.println("gpt ioe");
			ioe.printStackTrace();
			ordering.requirePureCSP();
			toDo = UnsolvedSubpathsFinder.allCommonSubpaths(problem, new IteratorWrapper<>(problem.trackIterator()), ordering);
			//indefiniteTracks = toDo.size();
		}
//		catch(Exception e)
//		{
//			System.err.println("there was gpt exception");
//			e.printStackTrace();
//		}
//		catch(Error e)
//		{
//			
//			System.err.println("there was gpt error");
//			e.printStackTrace();
//		}
		finally
		{
//			System.err.println("gpt fin");
			unsolved = toDo.size();
			indefiniteTracks = 0;
			for (Track t : new IteratorWrapper<>(problem.trackIterator()))
				if (!ordering.isTotallyOrdered(t))
					indefiniteTracks++;
		}
	}

	/**
	 * Returns the statistics from the run.
	 *
	 * @return Map from key to value of the statistics. This map conains integer
	 *         value {@code Statistics.UNSOLVED_CS} and integer value
	 *         {@code Statistics.UNSOLVED_TR}.
	 * @throws IllegalStateException When thread has not finished.
	 */
	public HashMap<String, Object> getStats()
	{
		if (getState() != Thread.State.TERMINATED)
			throw new IllegalStateException("Thread has not completed yet.");

		HashMap<String, Object> statsMap = new HashMap<>(8);
		statsMap.put(Statistics.UNSOLVED_CS, unsolved);
		statsMap.put(Statistics.UNSOLVED_TR, indefiniteTracks);
		return statsMap;
	}

	/**
	 * Retuns the map from common subpath to preferred order on that common subpath.
	 *
	 * @return The map of unsolved common subpaths.
	 * @see cz.cvut.fel.rysavpe1.mlcmp.order.GreedyOrderer#order(cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP, cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering) 
	 */
	public Map<CommonSubpath, Integer> getToDo()
	{
		if (getState() != Thread.State.TERMINATED)
			throw new IllegalStateException("Thread has not completed yet.");

		return toDo;
	}
}
