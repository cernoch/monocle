/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp;

import cz.cvut.fel.rysavpe1.mlcmp.eval.PlainRun;
import cz.cvut.fel.rysavpe1.mlcmp.eval.StatsRun;
import cz.cvut.fel.rysavpe1.mlcmp.eval.StatsRun2;
import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.XMLTouristTrailGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.XMLTramGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.output.PGFSolutionWriter;
import cz.cvut.fel.rysavpe1.mlcmp.output.PlainTextGraphWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Map.Entry;
import org.xml.sax.SAXException;

/**
 * The main class.
 *
 * @author Petr Ryšavý
 */
public class Main
{
	/** Solve operation constant. */
	private static final int SOLVE = 0;
	/** Stats operation constant. */
	private static final int STATS = 1;
	/** To plain operation constant. */
	private static final int TO_PLAIN = 2;
	/** To tex operation constant. */
	private static final int TO_TEX = 3;
	/** Stats operation constant. */
	private static final int STATS2 = 4;
	/** Plain input type constant. */
	private static final int PLAIN = 0;
	/** XML input type constant. */
	private static final int XML = 1;
	/** Tourist trails shall be loaded from XML constant. */
	private static final int TOURIST_TRAILS = 0;
	/** Trams shall be loaded from XML constant. */
	private static final int TRAMS = 1;
	/** Solve command line option. */
	private static final String SOLVE_C = "-solve";
	/** Stats commaind line option. */
	private static final String STATS_C = "-stats";
	/** Stats commaind line option. */
	private static final String STATS2_C = "-stats2";
	/** To plain commaind line option. */
	private static final String TO_PLAIN_C = "-toPlain";
	/** To tex commaind line option. */
	private static final String TO_TEX_C = "-toTEX";
	/** Use plain input commaind line option. */
	private static final String PLAIN_I = "-plain";
	/** Use xml input commaind line option. */
	private static final String XML_I = "-xml";
	/** Time limit commaind line option. */
	private static final String LIMIT_A = "-limit";
	/** The number of runs commaind line option. */
	private static final String N_A = "-n";
	/** The vertices lower bound commaind line option. */
	private static final String VLB_A = "-vlb";
	/** The split commaind line option. This option splits the problem to smaller
	 * isolated components. */
	private static final String SPLIT_A = "-split";
	/** Type of data loaded from xml. Tourist trails or trams network. */
	private static final String XML_CONF_A = "-trails";
	/** The help command line argument. */
	private static final String HELP_C = "-h";
	/** The help command line argument. */
	private static final String HELP_C2 = "--help";
	/** The help command line argument. */
	private static final String HELP_C3 = "-help";
	/** The help command line argument. */
	private static final String HELP_C4 = "/?";
	/** Whether the required command was loaded. */
	static boolean wasCommand = false;
	/** Whether the input type was loaded. */
	static boolean wasInput = false;

	/**
	 * The main running method. Reads the command line options and starts the
	 * program.
	 *
	 * @param args Command line arguments.
	 */
	@SuppressWarnings(
		{
		"CallToThreadDumpStack", "UseOfSystemOutOrSystemErr"
	})
	public static void main(String[] args)
	{
		// there store the options
		Path input = null;
		int limit = 600000;
		int n = 10;
		int vlb = 0;
		boolean split = true;
		int operation = SOLVE;
		int inputType = PLAIN;
		boolean printHelp = false;
		int xmlConfig = TOURIST_TRAILS;

		// read the arguments in random order
		for (int i = 0; i < args.length; i++)
		{
			switch (args[i])
			{
				case SOLVE_C:
					commandLoaded(args[i]);
					operation = SOLVE;
					break;
				case STATS_C:
					commandLoaded(args[i]);
					operation = STATS;
					break;
				case STATS2_C:
					commandLoaded(args[i]);
					operation = STATS2;
					break;
				case TO_PLAIN_C:
					commandLoaded(args[i]);
					operation = TO_PLAIN;
					break;
				case TO_TEX_C:
					commandLoaded(args[i]);
					operation = TO_TEX;
					break;
				case XML_I:
					inputLoaded(args[i]);
					inputType = XML;
					input = readInput(args, ++i);
					break;
				case PLAIN_I:
					inputLoaded(args[i]);
					input = readInput(args, ++i);
					inputType = PLAIN;
					break;
				case LIMIT_A:
					limit = readInteger(args, ++i, 100, Integer.MAX_VALUE);
					break;
				case N_A:
					n = readInteger(args, ++i, 1, 1000);
					break;
				case VLB_A:
					vlb = readInteger(args, ++i, 0, Integer.MAX_VALUE);
					break;
				case SPLIT_A:
					split = readBoolean(args, ++i);
					break;
				case XML_CONF_A:
					xmlConfig = readInteger(args, ++i, TOURIST_TRAILS, TRAMS);
					break;
				case HELP_C:
				case HELP_C2:
				case HELP_C3:
				case HELP_C4:
					printHelp = true;
					break;
				default:
					throwError("Unknown argument : " + args[i]);
					break;
			}
		}

		if (printHelp)
			printHelpAndExit();
		if (!wasInput)
			throwError("Please specify the input.");

		System.err.println("Reading the graph...");

		// Read the graph
		MLCMP bigProblem = null;
		try
		{
			switch (inputType)
			{
				case PLAIN:
					bigProblem = new PlainTextGraphReader(new OutputStreamWriter(System.err)).readGraph(input);
					break;
				case XML:
					switch (xmlConfig)
					{
						case TOURIST_TRAILS:
							bigProblem = new XMLTouristTrailGraphReader(new OutputStreamWriter(System.err)).readGraph(input);
							break;
						case TRAMS:
							bigProblem = new XMLTramGraphReader(new OutputStreamWriter(System.err)).readGraph(input);
							break;
					}
					break;
			}
		}
		catch (IOException ex)
		{
			System.err.println("There was an io error during reading the graph.");
			ex.printStackTrace();
			System.exit(1);
		}
		catch (GraphReadingException | SAXException gre)
		{
			System.err.println("There was an during reading the graph. It seems that the input is corrupted");
			gre.printStackTrace();
			System.exit(1);
		}

		// Do the operation
		try
		{
			switch (operation)
			{
				case SOLVE:
					PlainRun.run(bigProblem, limit, split);
					break;
				case TO_TEX:
					Map<MLCMP, Ordering> map = PlainRun.run(bigProblem, limit, split);
					printToTex(input, map);
					break;
				case STATS:
				case STATS2:
					@SuppressWarnings(
						{
						"null", "ConstantConditions"
					}) String fileName = input.getFileName().toString();
					fileName = fileName.substring(0, fileName.indexOf('.'));
					final Path dir = input.toAbsolutePath().getParent();
					final Path log = checkFile(dir.resolve(fileName + ".log"));
					final Path stats = checkFile(dir.resolve(fileName + ".out"));
					if(operation == STATS)
						StatsRun.runWithStats(bigProblem, log, stats, limit, n, vlb, split);
					else
						StatsRun2.runWithStats(bigProblem, log, stats, limit, n, vlb, split);
					break;
				case TO_PLAIN:
					@SuppressWarnings(
						{
						"null", "ConstantConditions"
					}) String fileName2 = input.getFileName().toString();
					fileName2 = fileName2.substring(0, fileName2.indexOf('.'));
					final Path dir2 = input.toAbsolutePath().getParent();
					final Path outfile = checkFile(dir2.resolve(fileName2 + "_plain.in"));
					if (outfile == null)
						throwError("Cannot write the output file.");
					PlainTextGraphWriter out = new PlainTextGraphWriter(new OutputStreamWriter(System.err));
					out.writeGraph(outfile, bigProblem);
					break;
			}
		}
		catch (IOException ex)
		{
			System.err.println("There was an during writing the output stream. Check whether it is possible to write to the directory.");
			ex.printStackTrace();
			System.exit(1);
		}
		catch (InterruptedException ex)
		{
			System.err.println("Interrupted thread error ...");
			ex.printStackTrace();
			System.exit(1);
		}

	}

	/**
	 * Throws error to the command line and exists with exit value {@code 1}.
	 *
	 * @param error The error message.
	 */
	@SuppressWarnings("UseOfSystemOutOrSystemErr")
	private static void throwError(String error)
	{
		System.err.println(error);
		System.err.println("For usage options rerun with parameter -help.");
		System.exit(1);
	}

	/**
	 * Prints the program help and exits.
	 */
	@SuppressWarnings("UseOfSystemOutOrSystemErr")
	private static void printHelpAndExit()
	{
		System.err.println("MLCMP solver");
		System.err.println("usage:");
		System.err.println("java -jar -Xmx512m mlcmp.jar");
		System.err.println("    [-solve|-stats|-toPlain|-toTEX] <-plain|-xml> <inputfile> [-trails <intval>] [-limit <intval>] [-n <intval>] [-vlb <intval>] [-split <boolval>] [-help]");
		System.err.println();
		System.err.println("-solve      program will solve the input and print the minimal number of crossings");
		System.err.println("-stats      program will solve input and print the stats");
		System.err.println("-toPlain    program will convert graph to plain text");
		System.err.println("-toTEX      program will output the final ordering to tex file compilable with pdflatex");
		System.err.println("-plain      specifies that input will be in plain file");
		System.err.println("-xml        specifies that input will be in xml file");
		System.err.println("-trails     for case of xml input file specifies type of input");
		System.err.println("                0 for tourist trails (default)");
		System.err.println("                1 for trams");
		System.err.println("-limit      time limit for greedy part and for each componet in milisecons");
		System.err.println("                default 10min, valid only for -solve|-stats options");
		System.err.println("-n          number of runs in -stats mode, default 10, max 1000");
		System.err.println("-vlb        minimum number of vertices in problem to be solved");
		System.err.println("                for -stats option to get rid off messy input");
		System.err.println("-split      if true, then the mlcmp problem will be divided into components");
		System.err.println("-help       prints this message");
		System.err.println("If there is an option used multiple times than the last case will be used or an error will be thrown.");
		System.exit(0);
	}

	/**
	 * Checks whether a command was already specified.
	 *
	 * @param input The option argument.
	 */
	private static void commandLoaded(String input)
	{
		if (wasCommand)
			throwError("Command was already specified : " + input);
		wasCommand = true;
	}

	/**
	 * Checks whether the input was already specified.
	 *
	 * @param input The option argument.
	 */
	private static void inputLoaded(String input)
	{
		if (wasInput)
			throwError("The input file already specified : " + input);
		wasInput = true;
	}

	/**
	 * Reads the input file as a given argument.
	 *
	 * @param args Array with program arguments.
	 * @param i    The index of the input file.
	 * @return The path of this file. If the file does not exists or is not readable
	 *         prints error message and stops the program.
	 */
	private static Path readInput(String[] args, int i)
	{
		if (i >= args.length)
			throwError("Please specify the file.");

		final Path path = Paths.get(args[i]);
		if (!Files.exists(path))
			throwError("File does not exist : " + args[i]);
		if (!Files.isReadable(path))
			throwError("Could not read file : " + args[i]);
		return path;
	}

	/**
	 * Reads an integer from the command line arguments.
	 *
	 * @param args     Array with program arguments.
	 * @param i        The index of the desired integer.
	 * @param smallest The smallest admissible integer value.
	 * @param biggest  The biggest admissible integer value.
	 * @return The readed integer. If this integer does not match the specified range
	 *         prints an error message and exists from the program.
	 */
	private static int readInteger(String[] args, int i, int smallest, int biggest)
	{
		if (i >= args.length)
			throwError("Please place an integer value after last argument.");

		int val = 0;
		try
		{
			val = Integer.parseInt(args[i]);
		}
		catch (NumberFormatException nfe)
		{
			throwError("That is not valid integer : " + args[i]);
		}
		if (val < smallest)
			throwError("The value must be at less " + smallest + " : " + val);
		if (val > biggest)
			throwError("The value must be at most " + biggest + " : " + val);
		return val;
	}

	/**
	 * Reads a boolean from command line arguments.
	 *
	 * @param args Array with program arguments.
	 * @param i    The index of the desired boolean.
	 * @return The readed boolean. If the value is not one of
	 *         {@code true}, {@code t}, {@code 1}, {@code false}, {@code f} or
	 *         {@code 0} ignoring the case then prints an error and exists.
	 */
	private static boolean readBoolean(String[] args, int i)
	{
		if (i >= args.length)
			throwError("Please place an boolean value after last argument.");

		if (args[i].equalsIgnoreCase("true") || args[i].equalsIgnoreCase("t") || args[i].equals("1"))
			return true;
		if (args[i].equalsIgnoreCase("false") || args[i].equalsIgnoreCase("f") || args[i].equals("0"))
			return false;

		throwError("Please specify valid boolean input : " + args[i]);
		throw new RuntimeException("Invalid code ...");
	}

	/**
	 * Checks whether the given file is writable. If the file does not exists then it
	 * is created.
	 *
	 * @param path The path to the file.
	 * @return The path or null if the filw is not writable.
	 */
	@SuppressWarnings(
		{
		"CallToThreadDumpStack", "UseOfSystemOutOrSystemErr"
	})
	private static Path checkFile(Path path)
	{
		if (!Files.exists(path))
			try
			{
				Files.createFile(path);
			}
			catch (IOException ex)
			{
				System.err.println("Cannot write the output file. I will use system output instead.");
				ex.printStackTrace();
				return null;
			}
		if (!Files.isWritable(path))
		{
			System.err.println("Cannot write the output file. I will use system output instead.");
			return null;
		}
		return path;
	}

	/**
	 * Prints the graph to a tex file.
	 *
	 * @param input The input filename.
	 * @param map   The solution.
	 * @throws IOException When an error occurs during writing the file.
	 */
	private static void printToTex(Path input, Map<MLCMP, Ordering> map) throws IOException
	{
		String fileName2 = input.getFileName().toString();
		fileName2 = fileName2.substring(0, fileName2.indexOf('.'));
		final Path dir2 = input.toAbsolutePath().getParent();
		@SuppressWarnings("UseOfSystemOutOrSystemErr")
		PGFSolutionWriter out = new PGFSolutionWriter(new OutputStreamWriter(System.err));
		int i = 0;
		for (Entry<MLCMP, Ordering> e : map.entrySet())
		{
			String end = map.size() == 1 ? ".tex" : i++ + ".tex";
			final Path outfile = checkFile(dir2.resolve(fileName2 + end));
			if (outfile == null)
				throwError("Cannot write the output file.");
			out.writeGraph(outfile, e.getKey(), e.getValue());
			try
			{
				final String command = "pdflatex \"" + outfile.toAbsolutePath().toString() + "\" -interaction=nonstopmode -no-file-line-error";
				Runtime.getRuntime().exec(command);
			}
			catch (Exception ex)
			{
				System.err.println("Cannot write the map to pdf. Try to compile alone using pdflatex.");
			}
		}
	}
}