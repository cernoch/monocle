/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp;

import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.order.CycleOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.np.CSPBestEffortSolver;
import cz.cvut.fel.rysavpe1.mlcmp.order.np.CSPSolver;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.UndoableOrdering;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The part of the program where the state space is searched.
 *
 * @author Petr Ryšavý
 * @see cz.cvut.fel.rysavpe1.mlcmp.
 */
public class CSPPartThread extends Thread
{
	/**
	 * The problem to solve.
	 */
	private final MLCMP problem;
	/**
	 * Line ordering where the optimum will be saved.
	 */
	private final UndoableOrdering ordering;
	/**
	 * The map from tracks to the common subpaths going through this track. This map
	 * caches the unsolved tracks.
	 */
	private final Map<Track, List<CommonSubpath>> trackMap;
	/**
	 * The current isolated component to solve.
	 */
	private final Set<Track> component;

	/**
	 * Creates new thread for solving the exponential part.
	 *
	 * @param problem   The problem to solve.
	 * @param ordering  Line ordering where the optimum will be saved.
	 * @param trackMap  The map from tracks to the common subpaths going through this
	 *                  track. This map caches the unsolved tracks.
	 * @param component The current isolated component to solve.
	 */
	public CSPPartThread(MLCMP problem, UndoableOrdering ordering,
		Map<Track, List<CommonSubpath>> trackMap, Set<Track> component)
	{
		this.problem = problem;
		this.ordering = ordering;
		this.trackMap = trackMap;
		this.component = component;
	}

	@Override
	public void run()
	{
		CycleOrderer.decideOnCycles(problem, component, ordering);
		CSPBestEffortSolver.solve(problem, ordering, component, trackMap);
	}

	@Override
	public void interrupt()
	{
//		try
//		{
			super.interrupt();
//			stop();
//			join();
//			ordering.reset();
//		}
//		catch (InterruptedException ex)
//		{
//			Logger.getLogger(CSPPartThread.class.getName()).log(Level.SEVERE, null, ex);
//		}
	}
}
