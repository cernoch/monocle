/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.eval;

import java.util.HashMap;

import static java.lang.Thread.MAX_PRIORITY;

/**
 * Thread that measures the time requirements of an watched thread. Pauses the
 * watched thread periodically after amound given by dividing expected running time
 * by {@code EXPECTED_COUNT}. Runs the garbage collector and then resumes the thread.
 * Returns the running time including the garbage collector and without the garbage
 * collector.
 *
 * @author Petr Ryšavý
 */
public class TimeMeasureThread extends Thread
{
	/**
	 * The expected count of the interruptions of watched thread.
	 */
	private static final int EXPECTED_COUNT = 25;
	/**
	 * The watched thread.
	 */
	private final Thread problemThread;
	/**
	 * Time at the beginning. After thread finished is overwritten with the time
	 * usage including the garbage collector.
	 */
	private long startTime;
	/**
	 * Start of a block.
	 */
	private long blockStart;
	/**
	 * Overall time.
	 */
	private long time;
	/**
	 * The interval for sleeping this thread.
	 */
	private final long sleepInterval;
	/**
	 * Time limit for the target thread to finish.
	 */
	private final long timeLimit;

	/**
	 * Creates the thread that measures the time requirements of a target thread.
	 *
	 * @param problemThread The thread to be watched.
	 * @param expectedTime  Expected running time (in milliseconds) of the thread. If
	 *                      this value is negative or zero then the expected time is
	 *                      assumed to be equal to the time limit.
	 * @param timeLimit     Time limit for thread to finish in milliseconds.
	 */
	public TimeMeasureThread(Thread problemThread, long expectedTime, long timeLimit)
	{
		this.problemThread = problemThread;
		this.timeLimit = timeLimit;
		this.sleepInterval = expectedTime <= 0 ? timeLimit : expectedTime / EXPECTED_COUNT;
		setPriority(MAX_PRIORITY);
	}

	@Override
	@SuppressWarnings(
		{
		"UseOfSystemOutOrSystemErr", "deprecation", "NestedAssignment"
	})
	public void run()
	{
		Runtime r = Runtime.getRuntime();
		r.gc();

		// initialize
		startTime = blockStart = System.currentTimeMillis();
		problemThread.start();

		while (!isInterrupted())
		{
			// sleep or wait the target thread end.
			try
			{
//				System.err.println("ptst "+problemThread.getState());
//				System.err.println("timeLimit "+timeLimit);
				if (timeLimit == -1)
					problemThread.join();
				else
					problemThread.join(sleepInterval);
				
//				System.err.println("ptst "+problemThread.getState());
			}
			catch (InterruptedException ex)
			{
//				System.err.println("The TimeMeasure Thread was interrrupted ...");
				return;
			}

			if (!problemThread.isAlive())
				break;
			problemThread.suspend();
			final long currentTime = System.currentTimeMillis();
			// if it's too late kill the thread and go to bed and sleep soundly until you die
			if (currentTime >= startTime + timeLimit && time != -1)
			{
				time = -1;
				problemThread.interrupt();
				problemThread.stop();
				try
				{
					problemThread.join();
				}
				catch (InterruptedException ex)
				{
				}
				return;
			}

			// ok there is more work to be done - so run the garbage collector
			// and capture the times
			time += currentTime - blockStart;

			r.gc();

			blockStart = System.currentTimeMillis();
			problemThread.resume();
		};

		// we have finished, save the values
		final long completeTime = System.currentTimeMillis();
		time += completeTime - blockStart;
		startTime = completeTime - startTime;
	}

	/**
	 * Returns the statistical data. The data contain
	 * {@code Statistics.COMPLETED_IN_TIME}. Additionaly may contain
	 * {@code Statistics.TIME}, {@code Statistics.TIME_GC} if the thread finished in
	 * time.
	 *
	 * @return Map with the data.
	 * @throws IllegalStateException When thread has not finished.
	 */
	public HashMap<String, Object> getStats()
	{
		if (getState() != Thread.State.TERMINATED)
			throw new IllegalStateException("Thread has not completed yet.");

		HashMap<String, Object> statsMap = new HashMap<>(2);
		final boolean completedInTime = time != -1;
		statsMap.put(Statistics.COMPLETED_IN_TIME, completedInTime);
		if (completedInTime)
		{
			statsMap.put(Statistics.TIME, time);
			statsMap.put(Statistics.TIME_GC, startTime);
		}
		return statsMap;
	}
}
