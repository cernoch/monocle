/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.eval;

import cz.cvut.fel.rysavpe1.mlcmp.CSPPartThread;
import cz.cvut.fel.rysavpe1.mlcmp.GreedyPartThread;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.order.GreedyOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.np.IsolatedComponentsFinder;
import cz.cvut.fel.rysavpe1.mlcmp.order.np.MinCutFinder;
import cz.cvut.fel.rysavpe1.mlcmp.order.utililies.CrossingsCounter;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.UndoableOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.EdgeShortener;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.UnderlyingGraphSplitter;
import cz.cvut.fel.rysavpe1.mlcmp.utilities.TrackMapper;
import cz.cvut.fel.rysavpe1.utilities.DualWriter;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import static cz.cvut.fel.rysavpe1.mlcmp.eval.Statistics.*;

/**
 * Solves the problem given number of times. Cathes the statistics and prints them
 * out. This class is simplified version of {@code StatsRun} and uses best effort
 * solver.
 *
 * The log file contains same output as the command line.
 *
 * The output data file produced by the program with the {@literal -stats2} option
 * contains a list of the MLCMP instances in the loaded data set. The first line of
 * an MLCMP instance contains the measured values from the greedy part in order as
 * follows. All time measurements are in {@literal ms} and all memory measurements
 * are in MB.
 *
 * <ol>
 * <li> Number of tracks in the problem.
 * <li> Number of vertices in the problem.
 * <li> Number of lines in the problem.
 * <li> Number of crossings in the optimal solution or {@literal -1} if it was not
 * found within the time limit.
 * <li> Number of crossings in found solution.
 * <li> Time required for the calculation of the greedy part.
 * <li> Used memory at the beginning.
 * <li> Used memory at the end.
 * <li> Number of indefinite edges in the problem.
 * <li> Number of common subpaths with different preferred order than precedes or
 * follows.
 * </ol>
 *
 * After one blank line follow the measurements of the particular isolated
 * components. Each isolated component is on one line with measurements from
 * following list.
 *
 * <ol>
 * <li> Number of indefinite edges in the isolated component.
 * <li> Number of common subpaths with different preferred order than precedes or
 * follows.
 * <li> Time required for finding the optimum.
 * <li> Used memory at the beginning.
 * <li> Used memory at the end.
 * </ol>
 *
 * @author Petr Ryšavý
 */
public class StatsRun2
{
	/**
	 * Don't let anybody to create the class instance.
	 */
	private StatsRun2()
	{
	}

	/**
	 * Runs the problem and captures the statistics.
	 *
	 * @param bigProblem       The problem.
	 * @param timeLimit        Time limit per one isolated component and per the
	 *                         greedy part.
	 * @param split            Shall be the problem splitted into connected
	 *                         components.
	 * @param logFile          The file where the system output stream will be
	 *                         duplicated.
	 * @param dataFile         File where the statistics data will be saved.
	 * @param nTimes           Počet běhů pro získání statistiky.
	 * @param vertexLoverBound Smallest number of vertices for a problem to be
	 *                         solved.
	 * @throws IOException          When an error occurs during writing the files.
	 * @throws InterruptedException When {@literal Thread.join()} retuns with an
	 *                              exception.
	 */
	public static void runWithStats(MLCMP bigProblem, Path logFile, Path dataFile,
		long timeLimit, int nTimes, int vertexLoverBound, boolean split) throws IOException, InterruptedException
	{
		@SuppressWarnings("UseOfSystemOutOrSystemErr")
		Writer writer = logFile == null
			? new OutputStreamWriter(System.err)
			: new DualWriter(new OutputStreamWriter(System.err), Files.newBufferedWriter(logFile, Charset.defaultCharset()));

		writer.write("Welcome to statistic run :\n");
		writer.write(String.format("original number of stations   : %d\n", bigProblem.getVerticesNum()));
		writer.write(String.format("original number of tracks     : %d\n", bigProblem.getEdgesNum()));

		EdgeShortener.contractTracks(bigProblem);
		writer.write(String.format("contracted number of stations : %d\n", bigProblem.getVerticesNum()));
		writer.write(String.format("contracted number of tracks   : %d\n", bigProblem.getEdgesNum()));
		MLCMP[] problems = split ? UnderlyingGraphSplitter.splitProblem(bigProblem) : new MLCMP[]
		{
			bigProblem
		};
		writer.write(String.format("number of subproblems         : %d\n", problems.length));

		@SuppressWarnings("UseOfSystemOutOrSystemErr")
		Writer dataWriter = dataFile == null
			? new OutputStreamWriter(System.out)
			: Files.newBufferedWriter(dataFile, Charset.defaultCharset());

		problemLoop:
			for (MLCMP problem : problems)
			{
				Map<Set<Track>, Map<String, Object>> sumstats = new HashMap<>();
				if (problem.getVerticesNum() < vertexLoverBound)
					continue;

				writer.write("MLCMP problem :\n");
				writer.write(String.format("\tnumber of stations          : %d\n", problem.getVerticesNum()));
				writer.write(String.format("\tnumber of tracks            : %d\n", problem.getEdgesNum()));
				writer.write('\n');
				writer.flush();

				Map<Set<Track>, List<Map<String, Object>>> stats = new HashMap<>(32);
				boolean solvedAll = true;
				HashMap<String, Object> firstGreedyPartStats = new HashMap<>();

				for (int i = 0; i < nTimes; i++)
				{
					writer.write("Running the greedy part\n");
					MinCutFinder.clearCache();

					List<Map<String, Object>> sts = getStatsList(null, stats, nTimes);

					LineOrdering ord = new LineOrdering(problem);
					GreedyPartThread greedy = new GreedyPartThread(problem, ord);
					TimeLimitThread time = new TimeLimitThread(greedy, timeLimit, false);
					time.start();
					time.join();

					if (ord.pureCSPRequired())
						writer.write("The greedy part was not successful, whole problem must be solved using CSP.\n");

					// from now, greedy part stats
					HashMap<String, Object> st = greedy.getStats();
					st.putAll(time.getStats());
					sts.add(st);

					if (i == 0)
						firstGreedyPartStats.putAll(st);

					if (!((Boolean) st.get(COMPLETED_IN_TIME)))
					{
						writer.write("Sory, but not able to complete the greedy part in time ...\n");
						writer.flush();
						continue problemLoop;
					}

					Map<CommonSubpath, Integer> toDo = greedy.getToDo();
					Map<Track, List<CommonSubpath>> trackMap = TrackMapper.trackMap(toDo.keySet());
					for (Iterator<Track> it = trackMap.keySet().iterator(); it.hasNext();)
						if (ord.isTotallyOrdered(it.next()))
							it.remove();
					UndoableOrdering uord = new UndoableOrdering(ord);
					List<Set<Track>> components = IsolatedComponentsFinder.findIsolatedComponents(trackMap.keySet());

					writer.write("Greedy part done\n");

					for (Set<Track> s : components)
					{
						Station example = s.iterator().next().getVertex1();
						writer.write(String.format(Locale.US, "Going to solve a component of %d tracks, Example point : %f, %f\n", s.size(), example.getY(), example.getX()));
						writer.flush();

						final int unsolvedCS = GreedyOrderer.order(problem, s, uord).size();

						CSPPartThread cspThread = new CSPPartThread(problem, uord, trackMap, s);
						time = new TimeLimitThread(cspThread, timeLimit, false);
						time.start();
						time.join();

						final HashMap<String, Object> compSt = time.getStats();
						sts = getStatsList(s, stats, nTimes);
						compSt.put(UNSOLVED_TR, s.size());
						compSt.put(UNSOLVED_CS, unsolvedCS);
						sts.add(compSt);

						if ((Boolean) compSt.get(Statistics.COMPLETED_IN_TIME))
							writer.write("Component done ...\n");
						else
						{
							writer.write("Unable to finish in time limit ...\n");
							solvedAll = false;
						}
						writer.flush();
					}

					writer.write("Done ...\n\n");
					int crossings = CrossingsCounter.countCrossings(problem, ord);
					writer.write("Total number of crossings : " + crossings + "\n\n");
					firstGreedyPartStats.put(N_CROSSINGS, crossings);
					firstGreedyPartStats.put(SOLVED_ALL, solvedAll);
					if (!solvedAll)
						writer.write("Sorry, but was not able to calculate whole component in time ...\n\n");

					writer.flush();
				}

				firstGreedyPartStats.put(Statistics.N_STATIONS, problem.getVerticesNum());
				firstGreedyPartStats.put(Statistics.N_TRACKS, problem.getEdgesNum());
				firstGreedyPartStats.put(N_LINES, problem.getLinesNum());
				calculateSumStats(firstGreedyPartStats, stats.get(null));
				sumstats.put(null, firstGreedyPartStats);
				for (Entry<Set<Track>, List<Map<String, Object>>> e : stats.entrySet())
				{
					if (e.getKey() == null)
						continue;

					Map<String, Object> firstStats = e.getValue().get(0);
					calculateSumStats(firstStats, e.getValue());
					sumstats.put(e.getKey(), firstStats);
				}

				processStats(sumstats, writer, dataWriter);
				writer.write("\n\n\n\n\n\n");
				writer.flush();
			}
	}

	private static List<Map<String, Object>> getStatsList(Set<Track> key, Map<Set<Track>, List<Map<String, Object>>> stats, int nTimes)
	{
		List<Map<String, Object>> retval = stats.get(key);
		if (retval == null)
		{
			retval = new ArrayList<>(nTimes);
			stats.put(key, retval);
		}
		return retval;
	}

	/**
	 * Calculates the cummulative statistics from all the runs.
	 *
	 * @param st       The map where the statistics will be saved.
	 * @param overTime The statistics that were captured during the runs of the
	 *                 problem.
	 */
	private static void calculateSumStats(Map<String, Object> st, List<Map<String, Object>> overTime)
	{
//		final Map<String, Object> first = overTime.get(0);
//		boolean completed = (Boolean) first.get(COMPLETED_IN_TIME);
//		st.put(COMPLETED_IN_TIME, completed);
//		if (!completed)
//			return;

		long timeSum = 0;
		int timeN = 0;
		for (Map<String, Object> m : overTime)
			if ((Boolean) m.get(COMPLETED_IN_TIME))
				for (Entry<String, Object> e : m.entrySet())
					switch (e.getKey())
					{
						case TIME:
							timeSum += (Long) e.getValue();
							timeN++;
							break;
					}
		if (timeN != 0)
			st.put(TIME, timeSum / timeN);
		else st.put(TIME, new Long(-1));
	}

	/**
	 * Prints the statistics to the output.
	 *
	 * @param sumstats   The statistics for each connected component.
	 * @param logWriter  The log writer.
	 * @param dataWriter The data writer.
	 * @throws IOException When writing to one of the writers caused an exception.
	 */
	@SuppressWarnings("unchecked")
	private static void processStats(Map<Set<Track>, Map<String, Object>> sumstats, Writer logWriter, Writer dataWriter) throws IOException
	{
		logWriter.write("Printing the statistics ...\n");

		Map<String, Object> prob = sumstats.get(null);
		dataWriter.write("*************************** Problem ***************************\n");
		dataWriter.write(String.format(Locale.US, "%8d %8d %3d %6d %6d %10.4f %8.2f %8.2f %6d %6d\n",
			prob.get(N_TRACKS),
			prob.get(N_STATIONS),
			prob.get(N_LINES),
			(Boolean) prob.get(Statistics.SOLVED_ALL) ? prob.get(N_CROSSINGS) : -1,
			prob.get(N_CROSSINGS),
			(Boolean) prob.get(Statistics.COMPLETED_IN_TIME) ? new Long((long) prob.get(TIME)).doubleValue() / 1e6 : -1,
			prob.containsKey(MEMORY_AT_BEGINNING) ? ((Long) prob.get(MEMORY_AT_BEGINNING)).doubleValue() / 1e6 : -1,
			prob.containsKey(MEMORY_AT_END) ? ((Long) prob.get(MEMORY_AT_END)).doubleValue() / 1e6 : -1,
			prob.get(UNSOLVED_TR),
			prob.get(UNSOLVED_CS)));
		dataWriter.write("\n");

		for (Entry<Set<Track>, Map<String, Object>> e : sumstats.entrySet())
			if (e.getKey() != null)
			{
				Map<String, Object> map = e.getValue();
				System.err.println(map);
				dataWriter.write(String.format(Locale.US, "%6d %6d %10.4f %8.2f %8.2f\n",
					map.get(UNSOLVED_TR),
					map.get(UNSOLVED_CS),
					(Boolean) prob.get(Statistics.COMPLETED_IN_TIME) ? new Long((long) map.get(TIME)).doubleValue() / 1e6 : -1,
					map.containsKey(MEMORY_AT_BEGINNING) ? ((Long) map.get(MEMORY_AT_BEGINNING)).doubleValue() / 1e6 : -1,
					map.containsKey(MEMORY_AT_END) ? ((Long) map.get(MEMORY_AT_END)).doubleValue() / 1e6 : -1));
			}
		dataWriter.write("\n");

		dataWriter.write("\n\n");
		dataWriter.flush();
	}
}
