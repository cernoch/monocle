/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.eval;

import cz.cvut.fel.rysavpe1.mlcmp.CSPPartThread;
import cz.cvut.fel.rysavpe1.mlcmp.GreedyPartThread;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.order.GreedyOrderer;
import cz.cvut.fel.rysavpe1.mlcmp.order.np.IsolatedComponentsFinder;
import cz.cvut.fel.rysavpe1.mlcmp.order.np.MinCutFinder;
import cz.cvut.fel.rysavpe1.mlcmp.order.utililies.CrossingsCounter;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.UndoableOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.EdgeShortener;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.UnderlyingGraphSplitter;
import cz.cvut.fel.rysavpe1.mlcmp.utilities.TrackMapper;
import cz.cvut.fel.rysavpe1.utilities.DualWriter;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import static cz.cvut.fel.rysavpe1.mlcmp.eval.Statistics.*;

/**
 * Solves the problem given number of times. Cathes the statistics and prints them
 * out.
 *
 * The log file contains same output as the command line.
 *
 * The output data file produced by the program with the {@literal -stats} option
 * contains a list of the MLCMP instances in the loaded data set. The first line of
 * an MLCMP instance contains the measured values from the greedy part in order as
 * follows. All time measurements are in {@literal ms} and all memory measurements are
 * in MB.
 *
 * <ol>
 * <li> Number of tracks in the problem.
 * <li> Number of vertices in the problem.
 * <li> Number of lines in the problem.
 * <li> Number of crossings in the optimal solution or {@literal -1} if it was not found
 * within the time limit.
 * <li> Time required for the calculation of the greedy part.
 * <li> Time required for the calculation of the greedy part including the garbage
 * collector time.
 * <li> Used memory at the beginning.
 * <li> Used memory at the end.
 * <li> Maximal amount of used memory over time or {@literal -1} if the measurement did
 * not occur.
 * <li> Average amount of used memory over time or {@literal -1} if the measurement did
 * not occur.
 * <li> Maximal amount of used memory over time minus the memory at the beginning or
 * {@literal -1} if the measurement did not occur.
 * <li> Average amount of used memory over time minus the memory at the beginning or
 * {@literal -1} if the measurement did not occur.
 * <li> Time interval between the measurements of the memory.
 * <li> Number of indefinite edges in the problem.
 * <li> Number of common subpaths with different preferred order than precedes or
 * follows.
 * </ol>
 *
 * On the next line follow the measurements of used memory over time. After one blank
 * line follow the measurements of the particular isolated components. Each isolated
 * component is on one line with measurements from following list.
 *
 * <ol>
 * <li>sep0em
 * <li> Number of indefinite edges in the isolated component.
 * <li> Number of common subpaths with different preferred order than precedes or
 * follows.
 * <li> Time required for finding the optimum.
 * <li> Time required for finding the optimum including the garbage collector.
 * <li> Used memory at the beginning.
 * <li> Used memory at the end.
 * <li> Maximal amount of used memory over time or {@literal -1} if the measurement did
 * not occur.
 * <li> Average amount of used memory over time or {@literal -1} if the measurement did
 * not occur.
 * <li> Maximal amount of used memory over time minus the memory at the beginning or
 * {@literal -1} if the measurement did not occur.
 * <li> Average amount of used memory over time minus the memory at the beginning or
 * {@literal -1} if the measurement did not occur.
 * <li> Time interval between the measurements of the memory.
 * </ol>
 *
 * After a blank line follows a list with the measurements of the memory usage over
 * time. If the measurement did not occur, there is {@literal -} symbol. The memory
 * measurements are sorted in the same order as the isolated components.
 *
 * @author Petr Ryšavý
 */
public class StatsRun
{
	/**
	 * Don't let anybody to create the class instance.
	 */
	private StatsRun()
	{
	}

	/**
	 * Runs the problem and captures the statistics.
	 *
	 * @param bigProblem       The problem.
	 * @param timeLimit        Time limit per one isolated component and per the
	 *                         greedy part.
	 * @param split            Shall be the problem splitted into connected
	 *                         components.
	 * @param logFile          The file where the system output stream will be
	 *                         duplicated.
	 * @param dataFile         File where the statistics data will be saved.
	 * @param nTimes           Počet běhů pro získání statistiky.
	 * @param vertexLoverBound Smallest number of vertices for a problem to be
	 *                         solved.
	 * @throws IOException          When an error occurs during writing the files.
	 * @throws InterruptedException When {@literal Thread.join()} retuns with an
	 *                              exception.
	 */
	public static void runWithStats(MLCMP bigProblem, Path logFile, Path dataFile,
		long timeLimit, int nTimes, int vertexLoverBound, boolean split) throws IOException, InterruptedException
	{
		@SuppressWarnings("UseOfSystemOutOrSystemErr")
		Writer writer = logFile == null
			? new OutputStreamWriter(System.err)
			: new DualWriter(new OutputStreamWriter(System.err), Files.newBufferedWriter(logFile, Charset.defaultCharset()));

		writer.write("Welcome to statistic run :\n");
		writer.write(String.format("original number of stations   : %d\n", bigProblem.getVerticesNum()));
		writer.write(String.format("original number of tracks     : %d\n", bigProblem.getEdgesNum()));

		EdgeShortener.contractTracks(bigProblem);
		writer.write(String.format("contracted number of stations : %d\n", bigProblem.getVerticesNum()));
		writer.write(String.format("contracted number of tracks   : %d\n", bigProblem.getEdgesNum()));
		MLCMP[] problems = split ? UnderlyingGraphSplitter.splitProblem(bigProblem) : new MLCMP[]
		{
			bigProblem
		};
		writer.write(String.format("number of subproblems         : %d\n", problems.length));

		@SuppressWarnings("UseOfSystemOutOrSystemErr")
		Writer dataWriter = dataFile == null
			? new OutputStreamWriter(System.out)
			: Files.newBufferedWriter(dataFile, Charset.defaultCharset());

		problemLoop:
			for (MLCMP problem : problems)
			{
				if (problem.getVerticesNum() < vertexLoverBound)
					continue;

				writer.write("MLCMP problem :\n");
				writer.write(String.format("\tnumber of stations          : %d\n", problem.getVerticesNum()));
				writer.write(String.format("\tnumber of tracks            : %d\n", problem.getEdgesNum()));
				writer.write('\n');
				writer.flush();

				Map<Set<Track>, List<Map<String, Object>>> stats = new HashMap<>(32);
				boolean solvedAll = true;

				for (int i = 0; i < nTimes; i++)
				{
					writer.write("Running the greedy part\n");
					MinCutFinder.clearCache();

					List<Map<String, Object>> sts = getStatsList(null, stats, nTimes);
					long approxTime = 0;
					if (!sts.isEmpty())
						approxTime = ((Long) sts.get(sts.size() - 1).get(Statistics.TIME)).longValue();

					LineOrdering ord = new LineOrdering(problem);
					GreedyPartThread greedy = new GreedyPartThread(problem, ord);
					TimeMeasureThread time = new TimeMeasureThread(greedy, approxTime, timeLimit);
					time.start();
					time.join();

					if (ord.pureCSPRequired())
						writer.write("The greedy part was not successful, whole problem must be solved using CSP.\n");

					HashMap<String, Object> st = greedy.getStats();
					st.putAll(time.getStats());
					sts.add(st);
					if (!((Boolean) st.get(COMPLETED_IN_TIME)))
					{
						writer.write("Sory, but not able to complete the greedy part in time ...\n");
						writer.flush();
						continue problemLoop;
					}

					Map<CommonSubpath, Integer> toDo = greedy.getToDo();
					Map<Track, List<CommonSubpath>> trackMap = TrackMapper.trackMap(toDo.keySet());
					for (Iterator<Track> it = trackMap.keySet().iterator(); it.hasNext();)
						if (ord.isTotallyOrdered(it.next()))
							it.remove();
					UndoableOrdering uord = new UndoableOrdering(ord);
					List<Set<Track>> components = IsolatedComponentsFinder.findIsolatedComponents(trackMap.keySet());

					writer.write("Greedy part done\n");

					for (Set<Track> s : components)
					{
						Station example = s.iterator().next().getVertex1();
						writer.write(String.format(Locale.US, "Going to solve a component of %d tracks, Example point : %f, %f\n", s.size(), example.getY(), example.getX()));
						writer.flush();
						sts = getStatsList(s, stats, nTimes);
						if (!sts.isEmpty())
						{
							boolean completed = ((Boolean) sts.get(sts.size() - 1).get(Statistics.COMPLETED_IN_TIME));
							if (!completed)
							{
								writer.write("Skipped component due to time issues.\n");
								continue;
							}
							approxTime = ((Long) sts.get(sts.size() - 1).get(Statistics.TIME)).longValue();
						}

						CSPPartThread cspThread = new CSPPartThread(problem, uord, trackMap, s);
						time = new TimeMeasureThread(cspThread, approxTime, timeLimit);
						time.start();
						time.join();

						st = time.getStats();
						sts.add(st);
						if ((Boolean) st.get(Statistics.COMPLETED_IN_TIME))
							writer.write("Component done ...\n");
						else
						{
							writer.write("Unable to finish in time limit ...\n");
							solvedAll = false;
						}
						writer.flush();
					}

					if (solvedAll)
						writer.write("Done ...\n\n");
					else
						writer.write("Sorry, but was not able to calculate whole component in time ...\n\n");

					writer.flush();
				}

				writer.write("Running the memory statistics ...\n");
				Map<Set<Track>, Map<String, Object>> sumstats = new HashMap<>(stats.size());
				HashMap<String, Object> st = new HashMap<>();
				sumstats.put(null, st);
				st.put(Statistics.N_STATIONS, problem.getVerticesNum());
				st.put(Statistics.N_TRACKS, problem.getEdgesNum());
				st.put(N_LINES, problem.getLinesNum());
				calculateSumStats(st, stats.get(null));

				writer.write("Running the greedy part\n");
				writer.flush();

				List<Map<String, Object>> sts = getStatsList(null, stats, nTimes);
				long approxTime = ((Long) sts.get(sts.size() - 1).get(Statistics.TIME)).longValue();
				LineOrdering ord = new LineOrdering(problem);
				GreedyPartThread greedy = new GreedyPartThread(problem, ord);
				MemusageThread memthr = new MemusageThread(greedy, approxTime);
				memthr.start();
				memthr.join();

				st.putAll(greedy.getStats());
				st.putAll(memthr.getStats());

				Map<CommonSubpath, Integer> toDo = greedy.getToDo();
				Map<Track, List<CommonSubpath>> trackMap = TrackMapper.trackMap(toDo.keySet());
				for (Iterator<Track> it = trackMap.keySet().iterator(); it.hasNext();)
					if (ord.isTotallyOrdered(it.next()))
						it.remove();
				UndoableOrdering uord = new UndoableOrdering(ord);
				List<Set<Track>> components = IsolatedComponentsFinder.findIsolatedComponents(trackMap.keySet());

				writer.write("Greedy part done\n");

				for (Set<Track> s : components)
				{
					Station example = s.iterator().next().getVertex1();
					writer.write(String.format(Locale.US, "Going to solve a component of %d tracks, Example point : %f, %f\n", s.size(), example.getY(), example.getX()));
					writer.flush();
					sts = getStatsList(s, stats, nTimes);
					boolean completed = ((Boolean) sts.get(sts.size() - 1).get(Statistics.COMPLETED_IN_TIME));
					if (!completed)
					{
						writer.write("Skipped component due to time issues.\n");
						continue;
					}
					approxTime = ((Long) sts.get(sts.size() - 1).get(Statistics.TIME)).longValue();

					final int unsolvedCS = GreedyOrderer.order(problem, s, uord).size();

					CSPPartThread cspThread = new CSPPartThread(problem, uord, trackMap, s);
					memthr = new MemusageThread(cspThread, approxTime);
					memthr.start();
					memthr.join();

					Map<String, Object> compStats = memthr.getStats();
					calculateSumStats(compStats, sts);
					compStats.put(UNSOLVED_TR, s.size());
					compStats.put(UNSOLVED_CS, unsolvedCS);
					sumstats.put(s, compStats);
					writer.write("Component done ...\n");
					writer.flush();
				}

				if (solvedAll)
				{
					writer.write("Done ...\n\n");
					int crossings = 0;
					for (Station station : new IteratorWrapper<>(problem.stationIterator()))
						crossings += CrossingsCounter.countCrossings(ord.getFinalizedOrder(station));
					writer.write("Total number of crossings : " + crossings + "\n\n");
					st.put(N_CROSSINGS, crossings);
				}
				else
					writer.write("Sorry, but was not able to calculate whole component in time ...\n\n");

				processStats(sumstats, writer, dataWriter);
				writer.write("\n\n\n\n\n\n");
				writer.flush();
			}
	}

	private static List<Map<String, Object>> getStatsList(Set<Track> key, Map<Set<Track>, List<Map<String, Object>>> stats, int nTimes)
	{
		List<Map<String, Object>> retval = stats.get(key);
		if (retval == null)
		{
			retval = new ArrayList<>(nTimes);
			stats.put(key, retval);
		}
		return retval;
	}

	/**
	 * Calculates the cummulative statistics from all the runs.
	 *
	 * @param st       The map where the statistics will be saved.
	 * @param overTime The statistics that were captured during the runs of the
	 *                 problem.
	 */
	private static void calculateSumStats(Map<String, Object> st, List<Map<String, Object>> overTime)
	{
		final Map<String, Object> first = overTime.get(0);
		boolean completed = (Boolean) first.get(COMPLETED_IN_TIME);
		st.put(COMPLETED_IN_TIME, completed);
		if (!completed)
			return;

		long timeSum = 0;
		long timeGCSum = 0;
		int timeN = 0;
		int timeGCN = 0;
		for (Map<String, Object> m : overTime)
			if ((Boolean) m.get(COMPLETED_IN_TIME))
				for (Entry<String, Object> e : m.entrySet())
					switch (e.getKey())
					{
						case TIME:
							timeSum += (Long) e.getValue();
							timeN++;
							break;
						case TIME_GC:
							timeGCSum += (Long) e.getValue();
							timeGCN++;
							break;
					}
		if (timeN != 0)
			st.put(TIME, timeSum / timeN);
		if (timeGCN != 0)
			st.put(TIME_GC, timeGCSum / timeGCN);
	}

	/**
	 * Prints the statistics to the output.
	 *
	 * @param sumstats   The statistics for each connected component.
	 * @param logWriter  The log writer.
	 * @param dataWriter The data writer.
	 * @throws IOException When writing to one of the writers caused an exception.
	 */
	@SuppressWarnings(
		{
		"unchecked", "unchecked"
	})
	private static void processStats(Map<Set<Track>, Map<String, Object>> sumstats, Writer logWriter, Writer dataWriter) throws IOException
	{
		logWriter.write("Printing the statistics ...\n");

		Map<String, Object> prob = sumstats.get(null);
		dataWriter.write("*************************** Problem ***************************\n");
		dataWriter.write(String.format(Locale.US, "%8d %8d %3d %6d %8d %8d %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %6d %6d %6d\n",
			prob.get(N_TRACKS),
			prob.get(N_STATIONS),
			prob.get(N_LINES),
			prob.containsKey(N_CROSSINGS) ? prob.get(N_CROSSINGS) : -1,
			prob.get(TIME),
			prob.get(TIME_GC),
			prob.containsKey(MEMORY_AT_BEGINNING) ? ((Long) prob.get(MEMORY_AT_BEGINNING)).doubleValue() / 1e6 : -1,
			prob.containsKey(MEMORY_AT_END) ? ((Long) prob.get(MEMORY_AT_END)).doubleValue() / 1e6 : -1,
			prob.containsKey(MEMORY_MAX) ? ((Long) prob.get(MEMORY_MAX)).doubleValue() / 1e6 : -1,
			prob.containsKey(MEMORY_AVG) ? ((Long) prob.get(MEMORY_AVG)).doubleValue() / 1e6 : -1,
			prob.containsKey(MEMORY_ADVANCE_MAX) ? ((Long) prob.get(MEMORY_ADVANCE_MAX)).doubleValue() / 1e6 : -1,
			prob.containsKey(MEMORY_ADVANCE_AVG) ? ((Long) prob.get(MEMORY_ADVANCE_AVG)).doubleValue() / 1e6 : -1,
			prob.get(MEMORY_USAGE_STEP),
			prob.get(UNSOLVED_TR),
			prob.get(UNSOLVED_CS)));
		if (prob.containsKey(MEMORY_USAGE_TIME))
			for (Long mem : (List<Long>) prob.get(MEMORY_USAGE_TIME))
				dataWriter.write(String.format(Locale.US, "%6.3f ", mem.doubleValue() / 1e6));
		dataWriter.write("\n\n");

		for (Entry<Set<Track>, Map<String, Object>> e : sumstats.entrySet())
			if (e.getKey() != null)
			{
				Map<String, Object> map = e.getValue();
				dataWriter.write(String.format(Locale.US, "%6d %6d %8d %8d %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %6d\n",
					map.get(UNSOLVED_TR),
					map.get(UNSOLVED_CS),
					map.get(TIME),
					map.get(TIME_GC),
					map.containsKey(MEMORY_AT_BEGINNING) ? ((Long) map.get(MEMORY_AT_BEGINNING)).doubleValue() / 1e6 : -1,
					map.containsKey(MEMORY_AT_END) ? ((Long) map.get(MEMORY_AT_END)).doubleValue() / 1e6 : -1,
					map.containsKey(MEMORY_MAX) ? ((Long) map.get(MEMORY_MAX)).doubleValue() / 1e6 : -1,
					map.containsKey(MEMORY_AVG) ? ((Long) map.get(MEMORY_AVG)).doubleValue() / 1e6 : -1,
					map.containsKey(MEMORY_ADVANCE_MAX) ? ((Long) map.get(MEMORY_ADVANCE_MAX)).doubleValue() / 1e6 : -1,
					map.containsKey(MEMORY_ADVANCE_AVG) ? ((Long) map.get(MEMORY_ADVANCE_AVG)).doubleValue() / 1e6 : -1,
					map.get(MEMORY_USAGE_STEP)));
			}
		dataWriter.write("\n");

		for (Entry<Set<Track>, Map<String, Object>> e : sumstats.entrySet())
			if (e.getKey() != null)
			{
				Map<String, Object> map = e.getValue();
				if (map.containsKey(MEMORY_USAGE_TIME))
				{
					for (Long mem : (List<Long>) map.get(MEMORY_USAGE_TIME))
						dataWriter.write(String.format(Locale.US, "%6.3f ", mem.doubleValue() / 1e6));
					dataWriter.write('\n');
				}
				else
					dataWriter.write("-\n");
			}
		dataWriter.write("\n\n");
		dataWriter.flush();
	}
}
