/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.eval;

import cz.cvut.fel.rysavpe1.mlcmp.CSPPartThread;
import cz.cvut.fel.rysavpe1.mlcmp.GreedyPartThread;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.order.np.IsolatedComponentsFinder;
import cz.cvut.fel.rysavpe1.mlcmp.order.utililies.CrossingsCounter;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.UndoableOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.EdgeShortener;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.UnderlyingGraphSplitter;
import cz.cvut.fel.rysavpe1.mlcmp.utilities.TrackMapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static cz.cvut.fel.rysavpe1.mlcmp.eval.Statistics.*;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.GraphContractionListener;
import java.util.ArrayList;

/**
 * Solves the problem with output and so on.
 *
 * @author Petr Ryšavý
 */
public class MonocleRun
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private MonocleRun()
	{
	}

	/**
	 * Solves the problem and returns the optimal solution.
	 *
	 * @param bigProblem  The problem.
	 * @param timeLimit   Time limit per one isolated component and per the greedy
	 *                    part.
	 * @param split       Shall be the problem splitted into connected components.
	 * @param enforceStop Whether the run enforces calculation to stop when time is
	 *                    over. This will case Thread.stop() to be called. Use with
	 *                    caution. Recommended is {@code false}.
	 * @param lst		       The listener that shall be informed about contractions.
	 * @return The optimal (maybe partial) solution.
	 * @throws IOException          When there is an error writing the output. Shall
	 *                              not occur as the output is system error.
	 * @throws InterruptedException When {@code join()} method is interrupted.
	 */
	public static Iterable<Output> run(MLCMP bigProblem, long timeLimit, boolean split, boolean enforceStop, GraphContractionListener lst) throws IOException, InterruptedException
	{
		EdgeShortener.contractTracks(bigProblem, lst);
		MLCMP[] problems = split ? UnderlyingGraphSplitter.splitProblem(bigProblem) : new MLCMP[]
		{
			bigProblem
		};
		List<Output> sols = new ArrayList<>(problems.length);

		problemLoop:
			for (MLCMP problem : problems)
			{
				boolean solvedAll = true;

				LineOrdering ord = new LineOrdering(problem);
				GreedyPartThread greedy = new GreedyPartThread(problem, ord);
				TimeLimitThread time = new TimeLimitThread(greedy, timeLimit, enforceStop);
				time.start();
				time.join();

				if (ord.pureCSPRequired())
					System.err.println("The greedy part was not successful, whole problem must be solved using CSP.\n");

				HashMap<String, Object> st = time.getStats();
				if (!((Boolean) st.get(COMPLETED_IN_TIME)))
				{
					sols.add(new Output(false, problem, null, -1));
					continue;
				}

				Map<CommonSubpath, Integer> toDo = greedy.getToDo();
//				System.err.println("st "+st);
//				System.err.println("toDo "+toDo);
//				System.err.println("prob "+problem);
				Map<Track, List<CommonSubpath>> trackMap = TrackMapper.trackMap(toDo.keySet());
				for (Iterator<Track> it = trackMap.keySet().iterator(); it.hasNext();)
					if (ord.isTotallyOrdered(it.next()))
						it.remove();
				UndoableOrdering uord = new UndoableOrdering(ord);
				List<Set<Track>> components = IsolatedComponentsFinder.findIsolatedComponents(trackMap.keySet());

				int comp = 0;

				for (Set<Track> s : components)
				{
					CSPPartThread cspThread = new CSPPartThread(problem, uord, trackMap, s);
					time = new TimeLimitThread(cspThread, timeLimit, enforceStop);
					time.start();
					time.join();

					st = time.getStats();
					solvedAll &= (Boolean) st.get(Statistics.COMPLETED_IN_TIME);
					System.err.println("solved component " + (++comp) + " of " + components.size());
				}

				System.err.println("solved all components");

				int crossings;
//				if (solvedAll) // now we have best effort, we do not need to care
				{
					crossings = CrossingsCounter.countCrossings(problem, ord);
					System.err.println("number of crossings is "+crossings);
					st.put(N_CROSSINGS, crossings);
				}

				sols.add(new Output(solvedAll, problem, ord, crossings));
			}
		return sols;
	}

	public static class Output
	{
		private final boolean optimal;
		private final MLCMP problem;
		private final LineOrdering ordering;
		private final int ncroossings;

		private Output(boolean optimal, MLCMP problem, LineOrdering ordering, int ncroossings)
		{
			this.optimal = optimal;
			this.problem = problem;
			this.ordering = ordering;
			this.ncroossings = ncroossings;
		}

		public boolean isOptimal()
		{
			return optimal;
		}

		public MLCMP getProblem()
		{
			return problem;
		}

		public LineOrdering getOrdering()
		{
			return ordering;
		}

		public int getNcroossings()
		{
			return ncroossings;
		}
	}
}
