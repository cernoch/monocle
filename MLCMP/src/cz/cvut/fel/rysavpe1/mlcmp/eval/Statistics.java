/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.eval;

/**
 * Constants for the statistics data.
 *
 * @author Petr Ryšavý
 */
public class Statistics
{
	/**
	 * Number of unsolved common subpaths in the problem. An integer value.
	 */
	public static final String UNSOLVED_CS = "unscs";
	/**
	 * Number of indefinite tracks in the problem. An integer value.
	 */
	public static final String UNSOLVED_TR = "unstr";
	/**
	 * Memory usage over time. List of long values.
	 */
	public static final String MEMORY_USAGE_TIME = "memintime";
	/**
	 * Step of memory usage over time. Long value in milliseconds.
	 */
	public static final String MEMORY_USAGE_STEP = "memstep";
	/**
	 * Memory usage at the beginning. A long value in bytes.
	 */
	public static final String MEMORY_AT_BEGINNING = "membeg";
	/**
	 * Average memory usage. A long value in bytes.
	 */
	public static final String MEMORY_AVG = "memavg";
	/**
	 * Maximal advance of memory from the beginning. A long value in bytes.
	 */
	public static final String MEMORY_ADVANCE_MAX = "memadvmax";
	/**
	 * Memory usage at the end. A long value in bytes.
	 */
	public static final String MEMORY_AT_END = "memend";
	/**
	 * Maximal memory usage. A long value in bytes.
	 */
	public static final String MEMORY_MAX = "memmax";
	/**
	 * Average advance of memory from the beginning. A long value in bytes.
	 */
	public static final String MEMORY_ADVANCE_AVG = "memadvavg";
	/**
	 * Time spent on running including the garbage collector. A long value in
	 * milliseconds.
	 */
	public static final String TIME_GC = "gctime";
	/**
	 * Whether the thread finished in time. A boolean value.
	 */
	public static final String COMPLETED_IN_TIME = "compl";
	/**
	 * Signalizes that all subcomponents were solved in time.
	 */
	public static final String SOLVED_ALL = "sall";
	/**
	 * Time spent on running without the garbage collector time. A long value in
	 * milliseconds.
	 */
	public static final String TIME = "time";
	/**
	 * Number of tracks in the problem. An integer value.
	 */
	public static final String N_TRACKS = "ntrcks";
	/**
	 * Number of common subpaths in the problem. An integer value.
	 */
	public static final String N_CS = "ncs";
	/**
	 * Number of stations in the problem. An integer value.
	 */
	public static final String N_STATIONS = "nstats";
	/**
	 * Number of crossings in the optimal solution. An integer value.
	 */
	public static final String N_CROSSINGS = "ncross";
	/**
	 * Number of lines in the problem. An integer value.
	 */
	public static final String N_LINES = "lines";

	/**
	 * Don't let anybody to create class instance.
	 */
	private Statistics()
	{
	}
}