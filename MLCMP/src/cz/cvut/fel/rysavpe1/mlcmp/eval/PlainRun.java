/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.eval;

import cz.cvut.fel.rysavpe1.mlcmp.CSPPartThread;
import cz.cvut.fel.rysavpe1.mlcmp.GreedyPartThread;
import cz.cvut.fel.rysavpe1.mlcmp.commonsubpath.CommonSubpath;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.order.np.IsolatedComponentsFinder;
import cz.cvut.fel.rysavpe1.mlcmp.order.utililies.CrossingsCounter;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.LineOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.UndoableOrdering;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.EdgeShortener;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.UnderlyingGraphSplitter;
import cz.cvut.fel.rysavpe1.mlcmp.utilities.TrackMapper;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import static cz.cvut.fel.rysavpe1.mlcmp.eval.Statistics.*;
import cz.cvut.fel.rysavpe1.mlcmp.order.OptimalityChecker;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.IllegalOrderException;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;

/**
 * Solves the problem with output and so on.
 *
 * @author Petr Ryšavý
 */
public class PlainRun
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private PlainRun()
	{
	}

	/**
	 * Solves the problem and returns the optimal solution.
	 *
	 * @param bigProblem The problem.
	 * @param timeLimit  Time limit per one isolated component and per the greedy
	 *                   part.
	 * @param split      Shall be the problem splitted into connected components.
	 * @return The optimal (maybe partial) solution.
	 * @throws IOException          When there is an error writing the output. Shall
	 *                              not occur as the output is system error.
	 * @throws InterruptedException When {@code join()} method is interrupted.
	 */
	public static Map<MLCMP, Ordering> run(MLCMP bigProblem, long timeLimit, boolean split) throws IOException, InterruptedException
	{
		@SuppressWarnings("UseOfSystemOutOrSystemErr")
		Writer writer = new OutputStreamWriter(System.err);

		writer.write("Welcome to MLCMP solver :\n");
		writer.write(String.format("original number of stations   : %d\n", bigProblem.getVerticesNum()));
		writer.write(String.format("original number of tracks     : %d\n", bigProblem.getEdgesNum()));

		EdgeShortener.contractTracks(bigProblem);
		writer.write(String.format("contracted number of stations : %d\n", bigProblem.getVerticesNum()));
		writer.write(String.format("contracted number of tracks   : %d\n", bigProblem.getEdgesNum()));
		MLCMP[] problems = split ? UnderlyingGraphSplitter.splitProblem(bigProblem) : new MLCMP[]
		{
			bigProblem
		};
		Map<MLCMP, Ordering> solsMap = new HashMap<>(problems.length);
		writer.write(String.format("number of subproblems         : %d\n", problems.length));

		problemLoop:
			for (MLCMP problem : problems)
			{
				writer.write("MLCMP problem :\n");
				writer.write(String.format("\tnumber of stations          : %d\n", problem.getVerticesNum()));
				writer.write(String.format("\tnumber of tracks            : %d\n", problem.getEdgesNum()));
				writer.write('\n');
				writer.flush();

				boolean solvedAll = true;
				writer.write("Running the greedy part\n");

				LineOrdering ord = new LineOrdering(problem);
				GreedyPartThread greedy = new GreedyPartThread(problem, ord);
				TimeMeasureThread time = new TimeMeasureThread(greedy, timeLimit, timeLimit);
				time.start();
				time.join();

				if (ord.pureCSPRequired())
					writer.write("The greedy part was not successful, whole problem must be solved using CSP.\n");

				HashMap<String, Object> st = time.getStats();
				if (!((Boolean) st.get(COMPLETED_IN_TIME)))
				{
					writer.write("Sory, but not able to complete the greedy part in time ...");
					writer.flush();
					continue problemLoop;
				}

				Map<CommonSubpath, Integer> toDo = greedy.getToDo();
				Map<Track, List<CommonSubpath>> trackMap = TrackMapper.trackMap(toDo.keySet());
				for (Iterator<Track> it = trackMap.keySet().iterator(); it.hasNext();)
					if (ord.isTotallyOrdered(it.next()))
						it.remove();
				UndoableOrdering uord = new UndoableOrdering(ord);
				List<Set<Track>> components = IsolatedComponentsFinder.findIsolatedComponents(trackMap.keySet());

				writer.write("Greedy part done\n");

				for (Set<Track> s : components)
				{
					Station example = s.iterator().next().getVertex1();
					writer.write(String.format(Locale.US, "Going to solve a component of %d tracks, Example point : %f, %f\n", s.size(), example.getY(), example.getX()));
					writer.flush();

					CSPPartThread cspThread = new CSPPartThread(problem, uord, trackMap, s);
					time = new TimeMeasureThread(cspThread, timeLimit, timeLimit);
					time.start();
					time.join();

					st = time.getStats();
					if ((Boolean) st.get(Statistics.COMPLETED_IN_TIME))
						writer.write("Component done ...\n");
					else
					{
						writer.write("Unable to finish in time limit ...\n");
						solvedAll = false;
					}
					writer.flush();
				}

				if (solvedAll)
				{
					writer.write("Done ...\n\n");
					int crossings = CrossingsCounter.countCrossings(problem, ord);
					writer.write("Total number of crossings : " + crossings + "\n\n");
					st.put(N_CROSSINGS, crossings);

					// check the optimality
					writer.write("Controlling the optimality of found solution.");
					try
					{
						OptimalityChecker.checkOptimality(problem, ord);
					}
					catch (IllegalOrderException ex)
					{
						writer.write("There are some lines that have redundant crossings with respect to preferred order.");
					}
				}
				else
					writer.write("Sorry, but was not able to calculate whole problem in time ...\n\n");

				solsMap.put(problem, ord);
				writer.flush();
			}
		return solsMap;
	}
}
