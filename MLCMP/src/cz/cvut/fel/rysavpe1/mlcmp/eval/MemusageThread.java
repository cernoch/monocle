/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.eval;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Thread that measures the memory usage of an watched thread. Pauses the watched
 * thread periodically after amound given by dividing expected running time by
 * {@code EXPECTED_COUNT}. Runs the garbage collector and then checks the used
 * memory.
 * <p>
 * <b>Warning:</b> This thread uses deprecated {@code java.lang.Thread} API methods.
 * </p>
 *
 * @author Petr Ryšavý
 */
public class MemusageThread extends Thread
{
	/**
	 * The expected count of the interruptions of watched thread.
	 */
	private static final int EXPECTED_COUNT = 25;
	/**
	 * The watched thread.
	 */
	private final Thread problemThread;
	/**
	 * Memory usage at the beginning.
	 */
	private long startUsage;
	/**
	 * Memory usage at the end.
	 */
	private long endUsage;
	/**
	 * The maximal usage during time.
	 */
	private long maximalUsage;
	/**
	 * The average usage over time.
	 */
	private long averageUsage;
	/**
	 * The interval for sleeping this thread.
	 */
	private final long sleepInterval;
	/**
	 * List of memory usage over time.
	 */
	private List<Long> usageInTime;

	/**
	 * Creates the memory watchdog thread.
	 *
	 * @param problemThread The watched thread.
	 * @param expectedTime  The expected running time of the watched thread.
	 */
	public MemusageThread(Thread problemThread, long expectedTime)
	{
		this.problemThread = problemThread;
		this.sleepInterval = Math.max(expectedTime / EXPECTED_COUNT, 1);
		usageInTime = new ArrayList<>((int) ((double) EXPECTED_COUNT * 1.25));
		setPriority(MAX_PRIORITY);
	}

	@Override
	@SuppressWarnings(
		{
		"UseOfSystemOutOrSystemErr", "deprecation"
	})
	public void run()
	{
		Runtime r = Runtime.getRuntime();
		r.gc();
		BigInteger memsum = BigInteger.ZERO;
		int count = 0;
		startUsage = getUsage();

		problemThread.start();

		while (!isInterrupted())
		{
			try
			{
				problemThread.join(sleepInterval);
			}
			catch (InterruptedException ex)
			{
				System.err.println("The Memusage Thread was interrrupted ...");
				return;
			}

			if (problemThread.getState() == Thread.State.TERMINATED)
				break;

			problemThread.suspend();

			r.gc();
			final long currentUsage = getUsage();
			if (currentUsage > maximalUsage)
				maximalUsage = currentUsage;
			usageInTime.add(currentUsage);
			memsum = memsum.add(BigInteger.valueOf(currentUsage));
			count++;

			problemThread.resume();
		}

		r.gc();
		endUsage = getUsage();
		if (count != 0)
			averageUsage = memsum.divide(BigInteger.valueOf(count)).longValue();
	}

	/**
	 * Returns the statistical data. The data contain {@code Statistics.MEMORY_AT_BEGINNING}, {@code Statistics.MEMORY_AT_END},
	 * {@code Statistics.MEMORY_USAGE_STEP}. Additionaly may contain
	 * {@code Statistics.MEMORY_USAGE_TIME}, {@code Statistics.MEMORY_AVG}, {@code Statistics.MEMORY_MAX}, {@code Statistics.MEMORY_ADVANCE_AVG}
	 * and {@code Statistics.MEMORY_ADVANCE_MAX} if the measurements were not
	 * successful.
	 *
	 * @return Map with the data.
	 * @throws IllegalStateException When thread has not finished.
	 */
	public HashMap<String, Object> getStats()
	{
		if (getState() != Thread.State.TERMINATED)
			throw new IllegalStateException("Thread has not completed yet.");

		HashMap<String, Object> statsMap = new HashMap<>(8);
		statsMap.put(Statistics.MEMORY_AT_BEGINNING, startUsage);
		statsMap.put(Statistics.MEMORY_AT_END, endUsage);
		statsMap.put(Statistics.MEMORY_USAGE_STEP, sleepInterval);
		if (averageUsage != 0)
		{
			statsMap.put(Statistics.MEMORY_USAGE_TIME, usageInTime);
			statsMap.put(Statistics.MEMORY_AVG, averageUsage);
			statsMap.put(Statistics.MEMORY_MAX, maximalUsage);
			statsMap.put(Statistics.MEMORY_ADVANCE_AVG, averageUsage - startUsage);
			statsMap.put(Statistics.MEMORY_ADVANCE_MAX, maximalUsage - startUsage);
		}
		return statsMap;
	}

	/**
	 * Returns the current memory usage.
	 *
	 * @return Total memory minus free memory.
	 */
	public static long getUsage()
	{
		Runtime r = Runtime.getRuntime();
		return r.totalMemory() - r.freeMemory();
	}
}
