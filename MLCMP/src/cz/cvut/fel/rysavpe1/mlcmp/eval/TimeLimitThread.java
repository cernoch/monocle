/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.eval;

import java.util.HashMap;

/**
 *
 * @author Petr Ryšavý
 */
public class TimeLimitThread extends Thread
{
	/**
	 * The watched thread.
	 */
	private final Thread problemThread;
	private final long timeLimit;
	/**
	 * This time is measured in nanos.
	 */
	private long time;
	private boolean enforceStop;
	private long startUsage;
	private long endUsage;

	public TimeLimitThread(Thread problemThread, long timeLimit)
	{
		this(problemThread, timeLimit, true);
	}

	public TimeLimitThread(Thread problemThread, long timeLimit, boolean enforceStop)
	{
		this.problemThread = problemThread;
		this.timeLimit = timeLimit;
		this.enforceStop = enforceStop;
	}

	@Override
	public void run()
	{
		// initialize
		final long startTime = System.currentTimeMillis();
		final long nanoStartTime = System.nanoTime();
		startUsage = MemusageThread.getUsage();
		problemThread.start();

		// sleep or wait the target thread end.
		try
		{
			if (timeLimit == -1)
				problemThread.join();
			else
				problemThread.join(timeLimit);
		}
		catch (InterruptedException ex)
		{
			return;
		}

		if (problemThread.isAlive())
		{
			time = -1;
			stopProblemThread();
			try
			{
				problemThread.join();
			}
			catch (InterruptedException ex)
			{
			}
			return;
		}

		endUsage = MemusageThread.getUsage();
		time = System.nanoTime() - nanoStartTime;
	}

	private void stopProblemThread()
	{
		problemThread.interrupt();
		if (enforceStop)
			problemThread.stop();
	}

	/**
	 * Returns the statistical data. The data contain
	 * {@code Statistics.COMPLETED_IN_TIME}. Additionaly may contain
	 * {@code Statistics.TIME} if the thread finished in time. This time is measured
	 * in nanoseconds.
	 *
	 * @return Map with the data.
	 * @throws IllegalStateException When thread has not finished.
	 */
	public HashMap<String, Object> getStats()
	{
		if (getState() != Thread.State.TERMINATED)
			throw new IllegalStateException("Thread has not completed yet.");

		HashMap<String, Object> statsMap = new HashMap<>(2);
		statsMap.put(Statistics.COMPLETED_IN_TIME, time != -1);
		if (time != -1)
			statsMap.put(Statistics.TIME, time);
		statsMap.put(Statistics.MEMORY_AT_BEGINNING, startUsage);
		statsMap.put(Statistics.MEMORY_AT_END, endUsage);
		return statsMap;
	}
}
