package cz.cvut.fel.rysavpe1.utilities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class NotNullIterableTest {

    public NotNullIterableTest() {
    }

	@Test
	public void testSomeMethod()
	{
		List<Integer> l = new ArrayList<>();
		l.add(1);
		l.add(null);
		l.add(5);
		l.add(-1);
		l.add(null);
		l.add(null);
		l.add(2);
		l.add(null);
		Iterator<Integer> it = new NotNullIterable<>(l).iterator();
		assertEquals(new Integer(1), it.next());
		assertEquals(new Integer(5), it.next());
		assertEquals(new Integer(-1), it.next());
		assertEquals(new Integer(2), it.next());
		assertFalse(it.hasNext());
	}
	
	@Test
	public void testSomeMethod2()
	{
		List<Integer> l = new ArrayList<>();
		l.add(1);
		Iterator<Integer> it = new NotNullIterable<>(l).iterator();
		assertEquals(new Integer(1), it.next());
		assertFalse(it.hasNext());
	}

}