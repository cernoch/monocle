/*
 */
package cz.cvut.fel.rysavpe1.utilities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class GroupIteratorTest
{
	
	public GroupIteratorTest()
	{
	}

	@Test
	public void testSomeMethod()
	{
		int size = 10;
		int n = 4;
		List<Integer> lst = new ArrayList<Integer>(size);
		for (int i = 0; i < size; i++)
			lst.add(i);
		List<List<Integer>> list = new LinkedList<>();
		for(int i = 0; i < n; i++)
			list.add(lst);
		
		GroupIterator<Integer> gi = new GroupIterator<>(list);
		for(int i = 0; i < MathUtils.pow(10, n); i++)
		{
			System.err.println("i  : "+i);
			List<Integer> xl = gi.next();
			int x = 0;
			for(int j = 0; j < n; j++)
				x = x*10 + xl.get(j);
			assertEquals(x, i);
		}
		
				
		gi = new GroupIterator<>(list);
		for(int i = 0; i < MathUtils.pow(10, n); i++)
		{
			System.err.println("i  : "+i);
			if((i % 57) == 0)
			{
				final int deg = MathUtils.random(n);
				System.err.println("deg  "+deg);
				gi.scrollCurrent(deg);
				final int targ = MathUtils.pow(10, n-deg - 1);
				i++;
				while((i % targ) != 0)
					i++;
				i--;
				continue;
			}
				
			List<Integer> xl = gi.next();
			int x = 0;
			for(int j = 0; j < n; j++)
				x = x*10 + xl.get(j);
			assertEquals(x, i);
		}

	}
	
	@Test
	public void test2()
	{
		int size = 3;
		int n = 3;
		List<Character> lst = new ArrayList<>(size);
		lst.add('a');
		lst.add('b');
		lst.add('c');
		List<Character> lst2 = new ArrayList<>(size);
		lst2.add('d');
		lst2.add('f');
		List<Character> lst3 = new ArrayList<>(size);
		lst3.add('g');
		lst3.add('h');
		lst3.add('i');
		lst3.add('j');
		List<List<Character>> list = new LinkedList<>();
		list.add(lst);
		list.add(lst2);
		list.add(lst3);
		
		GroupIterator<Character> gi = new GroupIterator<>(list);
		assertEquals("adg", toString(gi.next()));
		assertEquals("adh", toString(gi.next()));
		assertEquals("adi", toString(gi.next()));
		assertEquals("adj", toString(gi.next()));
		assertEquals("afg", toString(gi.next()));
		assertEquals("afh", toString(gi.next()));
		gi.scrollCurrent(1);
		assertEquals("bdg", toString(gi.next()));
		gi.scrollCurrent(2);
//		assertEquals("bdh", toString(gi.next()));
		assertEquals("bdi", toString(gi.next()));
		gi.scrollCurrent(0);
		assertEquals("cdg", toString(gi.next()));
		assertEquals("cdh", toString(gi.next()));
		gi.scrollCurrent(1);
		assertEquals("cfg", toString(gi.next()));
		assertEquals("cfh", toString(gi.next()));
		gi.scrollCurrent(0);
		assertFalse(gi.hasNext());
	}
	
	private static String toString(List<Character> lst)
	{
		StringBuilder sb = new StringBuilder(lst.size());
		for(Character ch : lst)
			sb.append(ch);
		return sb.toString();
	}
}