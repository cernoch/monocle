package cz.cvut.fel.rysavpe1.utilities;

import java.util.Iterator;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class RandomAccessMapTest {

    public RandomAccessMapTest() {
    }

	@Test
	public void testIterator()
	{
		RandomAccessMap<Integer, Character> map = new RandomAccessMap<>(4);
		map.put(1, 'a');
		map.put(2, 'B');
		map.put(3, 'c');
		map.put(4, 'D');
		Iterator<Map.Entry<Integer, Character>> iterator = map.entrySet().iterator();
		Map.Entry<Integer, Character> e = iterator.next();
		assertEquals(e.getKey(), (Integer)1);
		assertEquals(e.getValue(), (Character)'a');
		e = iterator.next();
		assertEquals(e.getKey(), (Integer)2);
		assertEquals(e.getValue(), (Character)'B');
		e = iterator.next();
		assertEquals(e.getKey(), (Integer)3);
		assertEquals(e.getValue(), (Character)'c');
		e = iterator.next();
		assertEquals(e.getKey(), (Integer)4);
		assertEquals(e.getValue(), (Character)'D');
	}

}