/*
 */

package cz.cvut.fel.rysavpe1.utilities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class MathUtilsTest {

    public MathUtilsTest() {
    }

	@Test
	public void testPow()
	{
		assertEquals(81, MathUtils.pow(3, 4));
		assertEquals(1, MathUtils.pow(3, 0));
		assertEquals(1024, MathUtils.pow(2, 10));
	}

}