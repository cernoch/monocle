/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.utilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Set;

/**
 * Library class that contains various set utilites.
 *
 * @author Petr Ryšavý
 */
public final class SetUtilities
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private SetUtilities()
	{
	}

	public static <T> Set<T> newHashSet(T element)
	{
		HashSet<T> hs = new HashSet<>();
		hs.add(element);
		return hs;
	}
	
	public static <T> Set<T> newHashSet(T element, int initialCapacity)
	{
		HashSet<T> hs = new HashSet<>(initialCapacity);
		hs.add(element);
		return hs;
	}
}