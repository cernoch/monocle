/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.utilities;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This class defines an iterable that wraps up an array. This might be useful when
 * an object want to grant acces on reading the array elements, but not array itself.
 * Using the iterable is more faster than copying the array.
 *
 * @param <T> The type of values in the array.
 * @author Petr Ryšavý
 */
public class BackwardArrayIterable<T> implements Iterable<T>
{
	/**
	 * The wrapped array.
	 */
	private final T[] array;

	/**
	 * Creates new array iterable that wraps the given array.
	 *
	 * @param array Array to wrap.
	 */
	public BackwardArrayIterable(final T[] array)
	{
		this.array = array;
	}

	@Override
	public Iterator<T> iterator()
	{
		return new Iterator<T>()
		{
			/**
			 * Index to the array.
			 */
			int index = array.length - 1;

			@Override
			public boolean hasNext()
			{
				return index > -1;
			}

			@Override
			public T next()
			{
				if (index < 0)
					throw new NoSuchElementException("Reached end of the array.");

				return array[index--];
			}

			@Override
			public void remove()
			{
			}
		};
	}
}
