/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.utilities;

import java.util.Iterator;

/**
 * Iterator of values.
 *
 * @param <T> The type of values stored in {@code ValueContainer}.
 */
public class ValueIterator<T> implements Iterator<T>, Iterable<T>
{
	/**
	 * Iterator of values.
	 */
	private Iterator<? extends ValueContainer<T>> it;

	/**
	 * Creates new iterator of values.
	 *
	 * @param it Iterator of values that will be returned.
	 */
	public ValueIterator(Iterator<? extends ValueContainer<T>> it)
	{
		this.it = it;
	}

	@Override
	public boolean hasNext()
	{
		return it.hasNext();
	}

	@Override
	public T next()
	{
		return it.next().getValue();
	}

	@Override
	public void remove()
	{
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Iterator<T> iterator()
	{
		return this;
	}
}
