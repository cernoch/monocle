package cz.cvut.fel.rysavpe1.utilities;


import java.util.Iterator;

/**
 *
 * @author Petr Ryšavý
 */
public class NotNullIterable<T> implements Iterable<T>
{
	private final Iterable<T> it;
	
	public NotNullIterable(Iterable<T> it)
	{
		this.it = it;
	}

	@Override
	public Iterator<T> iterator()
	{
		return new NotNullIterator();
	}
	
	private class NotNullIterator implements Iterator<T>
	{
		private T next = null;
		Iterator<T> iter;
		
		private NotNullIterator()
		{
			iter = it.iterator();
			scrollNext();
		}

		@Override
		public boolean hasNext()
		{
			return next != null;
		}

		@Override
		public T next()
		{
			T tmp = next;
			scrollNext();
			return tmp;
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("Not supported.");
		}
		
		private void scrollNext()
		{
			if(iter.hasNext())
				next = iter.next();
			else
				next = null;
			
			while(iter.hasNext() && next == null)
				next = iter.next();
		}
		
	}
}
