/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.utilities;

import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.RandomAccess;

/**
 * A set that allows constant time access to the particular value as well as to the
 * n-th element.
 * <p>
 * <b>Warning:</b> The values order is not preserved when calling the remove methods.
 * </p>
 *
 * @param <E> The element type.
 * @author Petr Ryšavý
 */
public class RandomAccessSet<E> extends AbstractSet<E> implements RandomAccess
{
	/**
	 * List of ordered keys.
	 */
	private final List<E> list;
	/**
	 * Map from keys to indices to the list.
	 */
	private final Map<E, Integer> map;

	/**
	 * Creates new random access set.
	 */
	public RandomAccessSet()
	{
		list = new ArrayList<>();
		map = new HashMap<>();
	}

	/**
	 * Creates new set with the given capacity.
	 *
	 * @param initialCapacity The initial capacity of the list and the map.
	 */
	public RandomAccessSet(int initialCapacity)
	{
		list = new ArrayList<>(initialCapacity);
		map = new HashMap<>(initialCapacity);
	}

	/**
	 * Creates new set with the given capacity and load factor.
	 *
	 * @param initialCapacity The initial capacity of the list and the map.
	 * @param loadFactor      The load factor of the hash map.
	 */
	public RandomAccessSet(int initialCapacity, float loadFactor)
	{
		list = new ArrayList<>(initialCapacity);
		map = new HashMap<>(initialCapacity, loadFactor);
	}

	/**
	 * Creates new set with given values. The values will be inserted in order of
	 * iterator of entry set of {@code m}.
	 *
	 * @param set The set with values.
	 */
	public RandomAccessSet(Collection<? extends E> set)
	{
		final int size = set.size();
		list = new ArrayList<>(size);
		map = new HashMap<>(size);

		int i = 0;
		for (E e : set)
		{
			list.add(e);
			map.put(e, i++);
		}
	}

	@Override
	public boolean isEmpty()
	{
		return list.isEmpty();
	}

	@Override
	@SuppressWarnings("element-type-mismatch")
	public boolean contains(Object o)
	{
		return map.containsKey(o);
	}

	@Override
	public boolean add(E e)
	{
		if (map.containsKey(e))
			return false;

		map.put(e, list.size());
		list.add(e);
		return true;
	}

	/**
	 * {@inheritDoc }
	 * Calling this method will move the last element to the gap.
	 */
	@Override
	@SuppressWarnings("element-type-mismatch")
	public boolean remove(Object o)
	{
		if (!map.containsKey(o))
			return false;
		return remove(map.get(o).intValue()) != null;
	}

	/**
	 * Removes the element at the given index. Calling this method will move the last
	 * element to the gap.
	 *
	 * @param index Index of the value to remove.
	 * @return The element at given index,
	 */
	public E remove(int index)
	{
		final int size = list.size();
		if (index >= size || index < 0)
			return null;

		final E toRemove = list.get(index);
		final E last = list.remove(size - 1);
		map.remove(toRemove);

		if (index != size - 1)
		{
			map.put(last, index);
			list.set(index, last);
		}

		return toRemove;
	}

	@Override
	public void clear()
	{
		list.clear();
		map.clear();
	}

	@Override
	public Iterator<E> iterator()
	{
		return list.iterator();
	}

	@Override
	public int size()
	{
		return list.size();
	}

	/**
	 * Gets the element at the given position. If {@code remove} method was not
	 * called then the returned value was added to the map at {@code index} position.
	 *
	 * @param index The position.
	 * @return The element at this position.
	 */
	public E get(int index)
	{
		return list.get(index);
	}

	/**
	 * Gets the index of the given element. If {@code remove} method was not called
	 * then the returned value was added to the map at {@code index} position.
	 *
	 * @param e The element to find.
	 * @return The index of that element or {@code -1} if the element is not in the
	 *         set.
	 */
	public int indexOf(E e)
	{
		if (map.containsKey(e))
			return map.get(e);
		return -1;
	}

	@Override
	public int hashCode()
	{
		int hash = 5;
		hash = 97 * hash + Objects.hashCode(this.list);
		return hash;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		final RandomAccessSet<E> other = (RandomAccessSet<E>) obj;
		if (!Objects.equals(this.list, other.list))
			return false;
		if (!Objects.equals(this.map, other.map))
			return false;
		return true;
	}
}
