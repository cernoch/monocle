/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.utilities;

import java.util.Arrays;
import java.util.List;

/**
 * Utilities that can be usefull when manipulating with arrays.
 *
 * @author Petr Ryšavý
 */
public final class ArrayUtilities
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private ArrayUtilities()
	{
		
	}
	
	/**
	 * Copies two dimensional array.
	 * @param array Array to copy.
	 * @return The copy.
	 */
	public static int[][] deepCopyOf(int[][] array)
	{
		final int[][] copy = new int[array.length][];
		for(int i = 0; i < array.length; i++)
			copy[i] = Arrays.copyOf(array[i], array[i].length);
		return copy;
	}
	
	public static <T> T[] toReversedArray(List<T> lst, T[] arr)
	{
		final int size = lst.size();
		for(int i = size -1, j = 0; i > 0 && j < size; i--, j++)
			arr[j] = lst.get(i);
		return arr;
	}
}
