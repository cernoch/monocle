/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.utilities;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.RandomAccess;
import java.util.Set;

/**
 * A map that allows constant time access to the particular value as well as to the
 * n-th element.
 * <p>
 * <b>Warning:</b> The values order is not preserved when calling the remove methods.
 * </p>
 *
 * @param <K> The key type.
 * @param <V> The value type.
 * @author Petr Ryšavý
 */
public class RandomAccessMap<K, V> extends AbstractMap<K, V> implements RandomAccess
{
	/**
	 * List of ordered keys.
	 */
	private final List<K> list;
	/**
	 * Map from keys to values.
	 */
	private final Map<K, Holder> map;
	/**
	 * The entry set.
	 */
	private Set<Map.Entry<K, V>> entrySet = null;

	/**
	 * Creates new map.
	 */
	public RandomAccessMap()
	{
		list = new ArrayList<>();
		map = new HashMap<>();
	}

	/**
	 * Creates new map with given capacity.
	 *
	 * @param initialCapacity The initial capacity.
	 */
	public RandomAccessMap(int initialCapacity)
	{
		list = new ArrayList<>(initialCapacity);
		map = new HashMap<>(initialCapacity);
	}

	/**
	 * Creates new map with given capacity and given load factor of the hash map.
	 *
	 * @param initialCapacity The initial capacity.
	 * @param loadFactor      Load factor of the map.
	 */
	public RandomAccessMap(int initialCapacity, float loadFactor)
	{
		list = new ArrayList<>(initialCapacity);
		map = new HashMap<>(initialCapacity, loadFactor);
	}

	/**
	 * Creates new map with given values. The values will be inserted in order of
	 * iterator of entry set of {@code m}.
	 *
	 * @param m The map with values.
	 */
	public RandomAccessMap(Map<? extends K, ? extends V> m)
	{
		final int size = m.size();
		list = new ArrayList<>(size);
		map = new HashMap<>(size);

		int i = 0;
		for (Map.Entry<? extends K, ? extends V> e : m.entrySet())
		{
			final K key = e.getKey();
			list.add(key);
			map.put(key, new Holder(i++, e.getValue()));
		}
	}

	@Override
	public boolean isEmpty()
	{
		return list.isEmpty();
	}

	@Override
	public boolean containsKey(Object key)
	{
		return map.containsKey(key);
	}

	@Override
	public V put(K key, V value)
	{
		final Holder oldValue = map.get(key);

		if (oldValue == null)
		{
			map.put(key, new Holder(list.size(), value));
			list.add(key);
			return null;
		}
		else
		{
			final V retval = oldValue.value;
			oldValue.value = value;
			return retval;
		}
	}

	/**
	 * {@inheritDoc }
	 * Calling this method will move the last element to the gap.
	 *
	 * @param o The key to remove.
	 */
	@Override
	@SuppressWarnings("element-type-mismatch")
	public V remove(Object o)
	{
		if (!map.containsKey(o))
			return null;
		return remove(map.get(o).index);
	}

	/**
	 * Removes the element at the given index. Calling this method will move the last
	 * element to the gap.
	 *
	 * @param index Index of the value to remove.
	 * @return The original value stored at given index.
	 */
	public V remove(int index)
	{
		final int lastIndex = list.size() - 1;
		if (index > lastIndex || index < 0)
			return null;

		final K last = list.remove(lastIndex);
		final V retval = map.remove(index == lastIndex ? last : list.get(index)).value;

		if (index != lastIndex)
		{
			map.get(last).index = index;
			list.set(index, last);
		}

		return retval;
	}

	@Override
	public void clear()
	{
		list.clear();
		map.clear();
	}

	@Override
	public Set<Map.Entry<K, V>> entrySet()
	{
		if (entrySet == null)
			entrySet = new EntrySet();
		return entrySet;
	}

	@Override
	public int size()
	{
		return list.size();
	}

	/**
	 * Gets the key at the given position. If {@code remove} method was not called
	 * then the returned value was added to the map at {@code index} position.
	 *
	 * @param index The position.
	 * @return The key at this position.
	 */
	public K get(int index)
	{
		return list.get(index);
	}

	/**
	 * Gets the index of the given key. If {@code remove} method was not called then
	 * the returned value was added to the map at {@code index} position.
	 *
	 * @param e The key.
	 * @return The index of that key or {@code -1} if the key is not in the map.
	 */
	public int indexOf(K e)
	{
		if (map.containsKey(e))
			return map.get(e).index;
		return -1;
	}

	@Override
	public V get(Object key)
	{
		final Holder h = map.get(key);
		return h == null ? null : h.value;
	}

	@Override
	public boolean containsValue(Object value)
	{
		for (Holder h : map.values())
			if (h.value.equals(value))
				return true;
		return false;
	}

	@Override
	public int hashCode()
	{
		int hash = 5;
		hash = 97 * hash + Objects.hashCode(this.list);
		return hash;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		final RandomAccessMap<K, V> other = (RandomAccessMap<K, V>) obj;
		if (!Objects.equals(this.list, other.list))
			return false;
		if (!Objects.equals(this.map, other.map))
			return false;
		return true;
	}

	/**
	 * Returns the entry corresponding to the given key.
	 *
	 * @param key The key.
	 * @return The entry or {@code null} if it was not found.
	 */
	final RandomAccessMap.Entry getEntry(K key)
	{
		if (map.containsKey(key))
			return new Entry(key);
		return null;
	}

	/**
	 * The wrapper to the value and index to the array.
	 */
	private final class Holder
	{
		/**
		 * The index.
		 */
		private int index;
		/**
		 * The value.
		 */
		private V value;

		/**
		 * Creates new wrapper to these two informations.
		 *
		 * @param index The index.
		 * @param value The value.
		 */
		public Holder(int index, V value)
		{
			this.index = index;
			this.value = value;
		}
	}

	/**
	 * The entry set backed to this map.
	 */
	private final class EntrySet extends AbstractSet<Map.Entry<K, V>>
	{
		@Override
		public Iterator<Map.Entry<K, V>> iterator()
		{
			return new EntryIterator();
		}

		@Override
		public boolean contains(Object o)
		{
			if (!(o instanceof Map.Entry))
				return false;
			@SuppressWarnings("unchecked")
			Map.Entry<K, V> e = (Map.Entry<K, V>) o;
			@SuppressWarnings("unchecked")
			Entry candidate = getEntry(e.getKey());
			return candidate != null && candidate.equals(e);
		}

		@Override
		public boolean remove(Object o)
		{
			if (!(o instanceof Map.Entry))
				return false;
			return remove(((Map.Entry) o).getKey());
		}

		@Override
		public int size()
		{
			return list.size();
		}

		@Override
		public void clear()
		{
			RandomAccessMap.this.clear();
		}
	}

	/**
	 * The entry. The changes to the entry are backed to the map.
	 */
	private class Entry implements Map.Entry<K, V>
	{
		/**
		 * The key.
		 */
		private final K key;

		/**
		 * Creates new entry.
		 *
		 * @param key The key of the entry.
		 */
		private Entry(K key)
		{
			this.key = key;
		}

		@Override
		public K getKey()
		{
			return key;
		}

		@Override
		public V getValue()
		{
			return get(key);
		}

		@Override
		public V setValue(V value)
		{
			return put(key, value);
		}

		@Override
		public int hashCode()
		{
			int hash = 3;
			hash = 41 * hash + Objects.hashCode(this.key);
			return hash;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (!(obj instanceof Map.Entry))
				return false;
			Map.Entry e = (Map.Entry) obj;
			return Objects.equals(getKey(), e.getKey()) && Objects.equals(getValue(), e.getValue());
		}
	}

	/**
	 * Iterator of the entries in the map. Again backed to the map.
	 */
	private final class EntryIterator implements Iterator<Map.Entry<K, V>>
	{
		/**
		 * The iterator of the keys.
		 */
		private final Iterator<K> it;
		/**
		 * Last key that was removed.
		 */
		private K lastKey;

		private EntryIterator()
		{
			it = list.iterator();
			lastKey = null;
		}

		@Override
		public boolean hasNext()
		{
			return it.hasNext();
		}

		@Override
		public Entry next()
		{
			lastKey = it.next();
			return new Entry(lastKey);
		}

		@Override
		public void remove()
		{
			if (lastKey == null)
				throw new NoSuchElementException("Method remove can be called once after next method.");
			RandomAccessMap.this.remove(lastKey);
			lastKey = null;
		}
	}
}
