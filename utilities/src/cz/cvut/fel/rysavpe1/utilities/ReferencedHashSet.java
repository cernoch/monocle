/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.utilities;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * This class defines a hash set that can be used for getting the optimal object
 * placed into the set.
 *
 * This can be handy if two objects are equal (by calling {@code Object.equals}), but
 * you want to get refference to the original one. The reason might be for example
 * that this component contains some variables that are not used by {@code equals}
 * method.
 *
 * @param <E> The type of stored values.
 * @author Petr Ryšavý
 */
public class ReferencedHashSet<E> extends AbstractSet<E> implements Set<E>
{
	/**
	 * Map that stores the values and the original instances.
	 */
	private final HashMap<E, E> map;

	/**
	 * Creates new referenced hash set object.
	 */
	public ReferencedHashSet()
	{
		map = new HashMap<>();
	}

	/**
	 * Creates new referenced hash set object.
	 * 
	 * @param c The initial set of values stored in hashset.
	 */
	public ReferencedHashSet(Collection<? extends E> c)
	{
		map = new HashMap<>(c.size());
		for(E e : c)
			map.put(e, e);
	}

	@Override
	public Iterator<E> iterator()
	{
		return map.keySet().iterator();
	}

	@Override
	public int size()
	{
		return map.size();
	}

	@Override
	public boolean isEmpty()
	{
		return map.isEmpty();
	}

	@Override
	@SuppressWarnings("element-type-mismatch")
	public boolean contains(Object o)
	{
		return map.containsKey(o);
	}

	@Override
	public boolean add(E e)
	{
		return map.put(e, e) == null;
	}

	@Override
	@SuppressWarnings("element-type-mismatch")
	public boolean remove(Object o)
	{
		return map.remove(o) == o;
	}

	@Override
	public void clear()
	{
		map.clear();
	}

	/**
	 * Returns the original object that was placed into this set.
	 *
	 * @param o The object to search.
	 * @return The original value, {@code null} when the object {@code o} cannot be
	 *         found.
	 */
	@SuppressWarnings("element-type-mismatch")
	public E getStoredRefference(Object o)
	{
		return map.get(o);
	}
}
