/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Iterator that iterates over all permutations of several iterator values. It has
 * wrapped i1, i2, ..., in iterators and iterates all |i1|*|i2|*...*|in| possible
 * combinations of the iterators. If the iterators ix are numbers from zero to nine
 * then this simple counts the decimal numbers with n digits including the leading
 * zeros.
 *
 * @param <T> The type over that it is iterated.
 * @author Petr Ryšavý
 */
public class GroupIterator<T> implements Iterator<List<T>>
{
	/**
	 * List of iterables ix.
	 */
	private final List<? extends Iterable<T>> iterables;
	/**
	 * Current list of iterators.
	 */
	private final ArrayList<Iterator<T>> iterators;
	/**
	 * The current value. This value will be returned after next call of {@code next}
	 * method. The {@code current} and {@code retval} means something different. They
	 * must be divided unless calling {@code scrollCurrent} from outside will change
	 * the list returned by {@code next} method.
	 */
	private final ArrayList<T> current;
	/**
	 * The last value returned by the {@code next} method.
	 */
	private final ArrayList<T> retval;
	/**
	 * The unmodifiable view to the {@code retval}.
	 */
	private final List<T> retvalView;
	/**
	 * Number of iterators over which it is iterated.
	 */
	private final int size;
	/**
	 * Is there a next element.
	 */
	private boolean hasNext;

	/**
	 * Creates new group iterator.
	 *
	 * @param iterables The values over that it will be iterated.s
	 */
	public GroupIterator(List<? extends Iterable<T>> iterables)
	{
		this.iterables = iterables;
		size = iterables.size();
		this.iterators = new ArrayList<>(size);
		Iterator<? extends Iterable<T>> it = iterables.iterator();
		for (int i = 0; i < size; i++)
			iterators.add(i, it.next().iterator());
		hasNext = size != 0;
		for (Iterator i : iterators)
			hasNext &= i.hasNext();
		if (hasNext)
		{
			current = new ArrayList<>(size);
			Iterator<Iterator<T>> it2 = iterators.iterator();
			for (int i = 0; i < size; i++)
				current.add(i, it2.next().next());
			retval = new ArrayList<>(size);
			for (int i = 0; i < size; i++)
				retval.add(null); // in order to set method work properly
			retvalView = Collections.unmodifiableList(retval);
		}
		else
		{
			current = null;
			retval = null;
			retvalView = null;
		}
	}

	@Override
	public boolean hasNext()
	{
		return hasNext;
	}

	@Override
	public List<T> next()
	{
		if (!hasNext)
			throw new NoSuchElementException("Reached end of all possible iterations.");

		updateRetval();
		scrollCurrentSafe(size - 1);

		return retvalView;
	}

	@Override
	public void remove()
	{
		throw new UnsupportedOperationException("Operation remove() is not supported.");
	}

	/**
	 * Scrolls the iterator at given position to next value. All iterators after that
	 * will be set to initial value. Iterator with index 0 changes slow and iterator
	 * with index {@code size - 1} is the fastest.
	 *
	 * @param position The index of scrolled iterator. It corresponds to the position
	 *                 in {@code iterables} from constructor.
	 * @throws IndexOutOfBoundsException When the position is less than zero or
	 *                                   greater or equal than the number of
	 *                                   iterators.
	 */
	public void scrollCurrent(int position)
	{
		if (position < 0 || position >= size)
			throw new IndexOutOfBoundsException("Position out of bounds : " + position);

		scrollCurrentSafe(position);
	}

	/**
	 * Does same as the {@code scrollCurrent} but without checking.
	 *
	 * @param position The index of scrolled iterator.
	 */
	private void scrollCurrentSafe(int position)
	{
		// find the next position we need to update
		while (position != -1 && !iterators.get(position).hasNext())
			position--;

		if (position == -1)
		{
			hasNext = false;
			return;
		}

		current.set(position, iterators.get(position).next());

		// now update all further iterators
		for (position = position + 1; position < size; position++)
		{
			Iterator<T> curr = iterables.get(position).iterator();
			iterators.set(position, curr);
			current.set(position, curr.next());
		}
	}

	/**
	 * Updates the retval by copying from the current.
	 */
	private void updateRetval()
	{
		for (int i = 0; i < size; i++)
			retval.set(i, current.get(i));
	}
}
