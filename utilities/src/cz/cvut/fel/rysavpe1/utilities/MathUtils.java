/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.utilities;

import java.util.Random;

/**
 * Some math utilities.
 *
 * @author Petr Ryšavý
 */
public class MathUtils
{
	private static Random randomGenerator = null;

	/**
	 * Don't let anybody to create class instance.
	 */
	private MathUtils()
	{
	}

	/**
	 * Stores the unsigned integer value into integer value. The values bigger than
	 * {@code Integer.MAX_VALUE} are saved as
	 * {@code longValue - 2 * Integer.MAX_VALUE}.
	 *
	 * @param value String value to read.
	 * @return The integer value.
	 * @throws NumberFormatException If the value is more two times bigger than
	 *                               {@code Integer.MAX_VALUE} or {@code value} is
	 *                               not textual representaion of number.
	 */
	public static int parseUnsignedInt(String value)
	{
		final long longValue = Long.parseLong(value);
		if (longValue > 2 * ((long) Integer.MAX_VALUE))
			throw new NumberFormatException("The value is too big for unsigned integer : " + value);
		if (longValue > Integer.MAX_VALUE)
			return (int) (longValue - 2 * Integer.MAX_VALUE);
		return (int) longValue;
	}

	/**
	 * Calculates an power of integer.
	 *
	 * @param a The base.
	 * @param b The power.
	 * @return a^b
	 */
	public static int pow(int a, int b)
	{
		if (b < 0)
			throw new IllegalArgumentException("Cannot calculate integer negative power. Use Math.pow() instead.");

		int result = 1;
		while (b-- > 0)
			result *= a;
		return result;
	}

	/**
	 * Returns a random value withing the given range.
	 *
	 * @param n The max value.
	 * @return Integer within range 0 (inclusive) to n (exclusive).
	 * @see java.util.Random#nextInt(int)
	 */
	public static int random(int n)
	{
		checkRNG();
		return randomGenerator.nextInt(n);
	}

	/**
	 * Initializes the random generator if needed.
	 */
	private static void checkRNG()
	{
		if (randomGenerator == null)
			randomGenerator = new Random();
	}
}
