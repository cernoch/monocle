/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.utilities;

/**
 * The counter utility.
 *
 * @author Petr Ryšavý
 */
public class Counter implements Cloneable, Comparable<Counter>
{
	/**
	 * Current count.
	 */
	private int c = 0;
	/**
	 * The increment.
	 */
	private int inc = 1;

	/**
	 * Creates new counter with increment 1 and defuault value 0.
	 */
	public Counter()
	{
	}

	/**
	 * Creates new counter with increment 1.
	 *
	 * @param c	The initial value of the counter.
	 */
	public Counter(int c)
	{
		this.c = c;
	}

	/**
	 * Creates new counter with desired values.
	 *
	 * @param c	  The initial value of the counter.
	 * @param inc The increments.
	 */
	public Counter(int c, int inc)
	{
		this.c = c;
		this.inc = inc;
	}

	/**
	 * Increments the counter value.
	 */
	public synchronized void inc()
	{
		c += inc;
	}

	/**
	 * Decrements the counter value.
	 */
	public synchronized void dec()
	{
		c -= inc;
	}

	/**
	 * Increments the counter value by the given increment. The increment is only for
	 * one use.
	 *
	 * @param increment	The increment.
	 */
	public synchronized void incBy(int increment)
	{
		c += increment;
	}

	/**
	 * Decrements the counter value by the given increment. The decrement is only for
	 * one use.
	 *
	 * @param decrement	The decrement.
	 */
	public synchronized void decBy(int decrement)
	{
		c -= decrement;
	}

	/**
	 * Sets new increment that will be used in future by decrease and increase
	 * methods.
	 *
	 * @param inc	The increment.
	 */
	public synchronized void setIncrement(int inc)
	{
		this.inc = inc;
	}

	/**
	 * Returns the current increment.
	 *
	 * @return	The current increment.
	 */
	public synchronized int getIncrement()
	{
		return inc;
	}

	/**
	 * Returns the counter value.
	 *
	 * @return	The current count.
	 */
	public synchronized int getCount()
	{
		return c;
	}

	/**
	 * Sets the count.
	 *
	 * @param c	New count.
	 */
	public synchronized void setCount(int c)
	{
		this.c = c;
	}

	/**
	 * Returns the couter value.
	 *
	 * @return	Current count.
	 */
	public synchronized int value()
	{
		return c;
	}

	/**
	 * Compares the counter value with zero.
	 *
	 * @return Is the count zero?
	 */
	public boolean isZero()
	{
		return c == 0;
	}

	@Override
	public int compareTo(Counter o)
	{
		int c2 = o.getCount();

		if (c2 > c)
			return -1;
		else if (c2 < c)
			return 1;
		else
			return 0;
	}

	@Override
	public synchronized Counter clone()
	{
		return new Counter(c, inc);
	}

	/**
	 * Two counters are equal when they have the same count and the same increment.
	 * {@inheritDoc }
	 */
	@Override
	public synchronized boolean equals(Object obj)
	{
		if (obj.getClass() != this.getClass())
			return false;
		Counter temp = (Counter) obj;
		return c == temp.getCount() && inc == temp.getIncrement();
	}

	@Override
	public synchronized int hashCode()
	{
		int hash = 3;
		hash = 53 * hash + this.c;
		hash = 53 * hash + this.inc;
		return hash;
	}

	@Override
	public synchronized String toString()
	{
		return "Counter: [ count: " + c + " ];";
	}
}
