/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.utilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Set;

/**
 * Library class that contains various list utilites.
 *
 * @author Petr Ryšavý
 */
public final class ListUtilities
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private ListUtilities()
	{
	}

	/**
	 * Checks whether two lists contains same elements, but in the reversed order.
	 * That means that last of first list is equal to first etc.
	 *
	 * @param <T> The type of values stored in the list.
	 * @param l1  First list.
	 * @param l2  Second list.
	 * @return Whether lists contain same values in reversed order.
	 */
	public static <T> boolean elementsEqualsReversed(List<T> l1, List<T> l2)
	{
		Iterator e1 = l1.iterator();
		ListIterator e2 = l2.listIterator();
		final int size = l1.size();
		for (int i = 0; i < size; i++)
			e2.next();

		for (int i = 0; i < size; i++)
			if (!Objects.equals(e1.next(), e2.previous()))
				return false;

		return true;
	}

	/**
	 * Stores the iterator values to a list.
	 *
	 * @param <T> The type of elements.
	 * @param it  Iterator of values.
	 * @return List with the values from the iterator.
	 */
	public static <T> List<T> toList(Iterator<? extends T> it)
	{
		final List<T> list = new ArrayList<>();
		while (it.hasNext())
			list.add(it.next());
		return list;
	}
	
	public static <T> List<T> moveToFront(List<T> list, T obj)
	{
		list.remove(obj);
		list.add(0, obj);
		return list;
	}
}