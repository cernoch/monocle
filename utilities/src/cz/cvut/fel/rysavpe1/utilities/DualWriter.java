/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.utilities;

import java.io.IOException;
import java.io.Writer;

/**
 * Writer that duplicates the output into two streams. What is written to this writer
 * is atomatically written to both of the underlying streams.
 *
 * @author Petr Ryšavý
 */
public class DualWriter extends Writer
{
	/**
	 * The first stream.
	 */
	private final Writer w1;
	/**
	 * The seciond stream.
	 */
	private final Writer w2;

	/**
	 * Creates new writer.
	 *
	 * @param w1 The first stream.
	 * @param w2 The seciond stream.
	 */
	public DualWriter(Writer w1, Writer w2)
	{
		if (w1 == null || w2 == null)
			throw new NullPointerException("Wrapped writer cannot be null.");

		this.w1 = w1;
		this.w2 = w2;
	}

	@Override
	public void write(char[] cbuf, int off, int len) throws IOException
	{
		w1.write(cbuf, off, len);
		w2.write(cbuf, off, len);
	}

	@Override
	public void flush() throws IOException
	{
		w1.flush();
		w2.flush();
	}

	@Override
	public void close() throws IOException
	{
		w1.close();
		w2.close();
	}
}
