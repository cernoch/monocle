/*
 */

package cz.cvut.fel.rysavpe1.graphalgos;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.SimpleDirectedGraph;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.SimpleVertex;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.Vertex;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class WeaklyConnectedComponentsSplitterTest {

    public WeaklyConnectedComponentsSplitterTest() {
    }

	@Test
	public void testSomeMethod()
	{
		Vertex<String> a = new SimpleVertex<>("a");
		Vertex<String> b = new SimpleVertex<>("b");
		Vertex<String> c = new SimpleVertex<>("c");
		Vertex<String> d = new SimpleVertex<>("d");
		Vertex<String> e = new SimpleVertex<>("e");
		Vertex<String> f = new SimpleVertex<>("f");
		Vertex<String> g = new SimpleVertex<>("g");
		
		SimpleDirectedGraph<String> graph = new SimpleDirectedGraph<>();
		graph.addVertex(a);
		graph.addVertex(b);
		graph.addVertex(c);
		graph.addVertex(d);
		graph.addVertex(e);
		graph.addVertex(f);
		graph.addVertex(g);
		
		graph.addEdge(a, d);
		graph.addEdge(a, b);
		graph.addEdge(d, c);
		graph.addEdge(b, c);
		graph.addEdge(e, f);
		
//		for(Vertex<String> v : new IteratorWrapper<>(graph.incidentVerticesIterator(c)))
//			System.err.println("vertex "+v);
		
		List<SimpleDirectedGraph<String>> list = WeaklyConnectedComponentsSplitter.wcc(graph);
		
		for(SimpleDirectedGraph<String> gr : list)
		{
			System.err.println("graph");
			System.err.println("vertices " + gr.getVerticesNum());
			System.err.println("vertices " + Arrays.toString(gr.getVertices()));
			System.err.println("edges    " + gr.getEdgesNum());
			System.err.println("edges    " + Arrays.toString(gr.getEdges()));
			System.err.println("topsort  " + KahnAlgorithm.sort(gr));
			System.err.println("---------");
		}
	}
	
	@Test
	public void testSomeMethod2()
	{
		Vertex<String> a = new SimpleVertex<>("a");
		Vertex<String> b = new SimpleVertex<>("b");
		Vertex<String> c = new SimpleVertex<>("c");
		Vertex<String> d = new SimpleVertex<>("d");
		Vertex<String> e = new SimpleVertex<>("e");
		Vertex<String> f = new SimpleVertex<>("f");
		Vertex<String> g = new SimpleVertex<>("g");
		Vertex<String> i = new SimpleVertex<>("i");
		Vertex<String> h = new SimpleVertex<>("h");
		Vertex<String> j = new SimpleVertex<>("j");
		Vertex<String> k = new SimpleVertex<>("k");
		Vertex<String> l = new SimpleVertex<>("l");
		
		SimpleDirectedGraph<String> graph = new SimpleDirectedGraph<>();
		graph.addVertex(a);
		graph.addVertex(b);
		graph.addVertex(c);
		graph.addVertex(d);
		graph.addVertex(e);
		graph.addVertex(f);
		graph.addVertex(g);
		graph.addVertex(i);
		graph.addVertex(h);
		graph.addVertex(j);
		graph.addVertex(k);
		graph.addVertex(l);
		graph.addVertex(l);
		graph.addVertex(l);
		
		graph.addEdge(a, d);
		graph.addEdge(a, b);
		graph.addEdge(d, c);
		graph.addEdge(b, c);
		graph.addEdge(e, f);
		graph.addEdge(d, i);
		graph.addEdge(h, b);
		graph.addEdge(j, k);
		graph.addEdge(k, l);
		graph.addEdge(l, j);
		
//		for(Vertex<String> v : new IteratorWrapper<>(graph.incidentVerticesIterator(c)))
//			System.err.println("vertex "+v);
		
		List<SimpleDirectedGraph<String>> list = WeaklyConnectedComponentsSplitter.wcc(graph);
		
		for(SimpleDirectedGraph<String> gr : list)
		{
			System.err.println("graph");
			System.err.println("vertices " + gr.getVerticesNum());
			System.err.println("vertices " + Arrays.toString(gr.getVertices()));
			System.err.println("edges    " + gr.getEdgesNum());
			System.err.println("edges    " + Arrays.toString(gr.getEdges()));
			System.err.println("topsort  " + KahnAlgorithm.sort(gr));
			System.err.println("---------");
		}
	}

}