/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.graphalgos;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.DirectedGraph;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.Vertex;
import cz.cvut.fel.rysavpe1.utilities.Counter;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This algorithm finds the topological ordering of an directed graph if this
 * ordering exists.
 *
 * @author Petr Ryšavý
 * @see http://en.wikipedia.org/wiki/Topological_sorting
 */
public class KahnAlgorithm
{
	/**
	 * Finds the topological ordering of a graph if this ordering exists.
	 *
	 * @param <T>   The type of values stored in vertices.
	 * @param graph The directed graph where the topological orderging shall be
	 *              found.
	 * @return The topological ordering or {@code null} if this ordering does not
	 *         exist.
	 */
	public static <T> List<Vertex<T>> sort(DirectedGraph<T, ? extends Object> graph)
	{
		final int verticesNum = graph.getVerticesNum();
		final List<Vertex<T>> list = new ArrayList<>(verticesNum);
		final Set<Vertex<T>> set = new HashSet<>(verticesNum);
		final HashMap<Vertex<T>, Counter> degreeMap = new HashMap<>(verticesNum);

		for (Vertex<T> vertex : new IteratorWrapper<>(graph.vertexIterator()))
			degreeMap.put(vertex, new Counter());
		for (Vertex<T> head : degreeMap.keySet())
			for (final Vertex<T> tail : new IteratorWrapper<>(graph.incidentVerticesIterator(head)))
				degreeMap.get(tail).inc();
		for (Vertex<T> vertex : degreeMap.keySet())
			if (degreeMap.get(vertex).isZero())
				set.add(vertex);

		while (!set.isEmpty())
		{
			final Vertex<T> n = set.iterator().next();
			set.remove(n);
			list.add(n);
			for (final Vertex<T> m : new IteratorWrapper<>(graph.incidentVerticesIterator(n)))
			{
				Counter deg = degreeMap.get(m);
				deg.dec();
				if (deg.isZero())
					set.add(m);
			}
		}

		for (Counter i : degreeMap.values())
			if (!i.isZero())
				return null;

		return list;
	}

	/**
	 * Don't let anybody to create class instance.
	 */
	private KahnAlgorithm()
	{
	}
}