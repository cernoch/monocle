/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.graphalgos;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.InductableGraph;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.SimpleUndirectedGraph;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.Vertex;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Finds the weakly connected components of an graph.
 *
 * @author Petr Ryšavý
 */
public class WeaklyConnectedComponentsSplitter
{
	/**
	 * Finds the connected components of the graph.
	 *
	 * @param <T>   Type of the data stored with the graph vertices.
	 * @param <G>   The class of the graph.
	 * @param graph The graph to be splitted.
	 * @return List of the weakly connected components of the original graph.
	 */
	@SuppressWarnings(
		{
		"unchecked"
	})
	public static <T, G extends InductableGraph<T, ? extends Object>> List<G> wcc(G graph)
	{
		final List<G> values = new LinkedList<>();
		final int verticesNum = graph.getVerticesNum();
		final HashSet<Vertex<T>> visited = new HashSet<>(verticesNum);
		final HashSet<Vertex<T>> currentComponent = new HashSet<>(verticesNum);

		G g;
		if (graph.isDirected())
			g = (G) new SimpleUndirectedGraph<>(graph);
		else
			g = graph;

		for (Vertex<T> v : new IteratorWrapper<>(g.vertexIterator()))
			if (!visited.contains(v))
			{
				currentComponent.clear();
				BFS(g, v, visited, currentComponent);
				values.add((G) graph.inducedGraph(currentComponent));
			}

		return values;
	}

	/**
	 * Runs the BFS to the current component.
	 *
	 * @param <T>     Type of the data stored with the graph vertices.
	 * @param <G>     The class of the graph.
	 * @param graph   The graph to be splitted.
	 * @param s       The starting vertex of the search.
	 * @param visited The set of already visited vertices.
	 * @param current The hash set where the whole connected component shall be
	 *                added.
	 */
	private static <T, G extends InductableGraph<T, ?>> void BFS(G graph, Vertex<T> s, HashSet<Vertex<T>> visited, HashSet<Vertex<T>> current)
	{
		final LinkedList<Vertex<T>> q = new LinkedList<>();
		q.add(s);
		visited.add(s);
		current.add(s);

		while (!q.isEmpty())
		{
			final Vertex<T> v = q.remove();
			for (Vertex<T> u : graph.getIncidentVertices(v))
				if (!visited.contains(u))
				{
					visited.add(u);
					q.add(u);
					current.add(u);
				}
		}
	}

	/**
	 * Don't let anybody to create the class instance.
	 */
	WeaklyConnectedComponentsSplitter()
	{
	}
}