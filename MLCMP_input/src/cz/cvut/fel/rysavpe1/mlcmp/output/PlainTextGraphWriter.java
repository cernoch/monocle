/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.output;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

/**
 * The class that provides static method that prints the underlying graph in such
 * format that the it can be loaded using the {@code PlainTextGraphReader}.
 *
 * @author Petr Ryšavý
 * @see cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader
 */
public class PlainTextGraphWriter
{
	/**
	 * Output in that will be writen warnings.
	 */
	private final Writer errOut;
	/**
	 * Format for writing the decimal number inputs.
	 */
	private static final DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance(Locale.US);
	/**
	 * Regular expression for double value.
	 */
	private static final String DOUBLE_REGEX = "\\d+(\\.\\d+)?";
	/**
	 * Regular expression that can be used for checking whether the station name was
	 * generated automatically.
	 */
	private static final String GENERIC_STATION_NAME = "Station \\[" + DOUBLE_REGEX + ", " + DOUBLE_REGEX + "\\]";
	/**
	 * Index used for generating names of currently unnamed stations.
	 */
	private static int genericIndex = 0;

	static
	{
		format.setMaximumFractionDigits(Integer.MAX_VALUE);
	}

	/**
	 * Creates new graph writer.
	 *
	 * @param errOut Output for warnings.
	 */
	public PlainTextGraphWriter(Writer errOut)
	{
		this.errOut = errOut;
	}

	/**
	 * Writes graph to the given file.
	 *
	 * @param file    The target file.
	 * @param problem The problem to write.
	 * @throws IOException If the file is not writable or there is an error writing
	 *                     it.
	 */
	public void writeGraph(Path file, MLCMP problem) throws IOException
	{
		try (Writer br = Files.newBufferedWriter(file, Charset.forName("utf-8")))
		{
			writeGraph(br, problem);
		}
	}

	/**
	 * Writes the problem into the given output.
	 *
	 * @param out     The output stream.
	 * @param problem The problem to write.
	 * @throws IOException When output cannot be written because of an io exception.
	 */
	public void writeGraph(Writer out, MLCMP problem) throws IOException
	{
		final int stationsNum = problem.getVerticesNum();
		final HashMap<Station, String> stationMap = new HashMap<>(stationsNum);
		// Sometimes may happen that two stations have similar names that are returned
		// by wordString as one same - so prevent this
		final HashSet<String> usedNames = new HashSet<>(stationsNum);
		for (Station s : new IteratorWrapper<>(problem.stationIterator()))
		{
			writeStation(out, s, stationMap, usedNames);
			out.write('\n');
		}

		out.write('\n');
		for (Line l : new IteratorWrapper<>(problem.lineIterator()))
			writeLine(out, l, stationMap);

		out.flush();
	}

	/**
	 * Writes the station.
	 *
	 * @param out          The output.
	 * @param s            The station.
	 * @param map          Map in that will be the station name stored.
	 * @param stationNames Set of already used nongeneric station names. That will be
	 *                     useful if two names will be same after removing
	 *                     diacritics.
	 * @throws IOException When there was and IO error.
	 */
	private void writeStation(Writer out, Station s, HashMap<Station, String> map, HashSet<String> stationNames) throws IOException
	{
		out.write('(');
		final String name = s.getValue();
		String stationName;

		if (name.matches(GENERIC_STATION_NAME))
			stationName = "S" + genericIndex++;
		else
		{
			stationName = wordString(name);
			if (stationNames.contains(stationName))
				stationName = stationName + genericIndex++;
			else
				stationNames.add(stationName);
		}

		out.write(stationName);
		out.write(")=");
		out.write(format.format(s.getX()));
		out.write(", ");
		out.write(format.format(s.getY()));

		map.put(s, stationName);
	}

	/**
	 * Writes the line.
	 *
	 * @param out The output stream.
	 * @param l   The line to write.
	 * @param map Map that contains stations names.
	 * @throws IOException When there was and IO error.
	 */
	private void writeLine(Writer out, Line l, HashMap<Station, String> map) throws IOException
	{
		final String lineName = wordString(l.getName());

		for (Track t : l)
		{
			out.write(lineName);
			out.write(' ');
			writeTrack(out, t, map);
			out.write('\n');
		}
	}

	/**
	 * Writes a track in the output.
	 *
	 * @param out The output stream.
	 * @param t   The track to write.
	 * @param map Map that contains stations names.
	 * @throws IOException When there was and IO error.
	 */
	private void writeTrack(Writer out, Track t, HashMap<Station, String> map) throws IOException
	{
		out.write('(');
		out.write(map.get(t.getVertex1()));
		out.write(")-(");
		out.write(map.get(t.getVertex2()));
		out.write(')');
	}

	/**
	 * Removes the special characters so that the output is in form
	 * {@code [a-zA-Z]\\w*}. If the diacritics cannot be removed that the character
	 * is removed at all. If there will be no character left, than the returned value
	 * will be generic name.
	 *
	 * @param str The string to remove diacritics.
	 * @return The word string created from {@code str}.
	 * @throws IOException When there was and IO error.
	 */
	private String wordString(String str) throws IOException
	{
		StringBuilder sb = new StringBuilder(str.length());
		for (char ch : str.toCharArray())
			if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z' || ch >= '0' && ch <= '9')
				sb.append(ch);
			else if (ch == 'á')
				sb.append('a');
			else if (ch == 'č')
				sb.append('c');
			else if (ch == 'ď')
				sb.append('d');
			else if (ch == 'ě' || ch == 'é')
				sb.append('e');
			else if (ch == 'í')
				sb.append('i');
			else if (ch == 'ň')
				sb.append('n');
			else if (ch == 'ó')
				sb.append('o');
			else if (ch == 'ř')
				sb.append('r');
			else if (ch == 'š')
				sb.append('s');
			else if (ch == 'ť')
				sb.append('t');
			else if (ch == 'ů' || ch == 'ú')
				sb.append('u');
			else if (ch == 'ž')
				sb.append('z');

		if (sb.length() == 0)
			return "S" + genericIndex++;

		char ch0 = sb.charAt(0);
		if (ch0 >= '0' && ch0 <= '9')
			sb.insert(0, 'S');

		return sb.toString();
	}
}
