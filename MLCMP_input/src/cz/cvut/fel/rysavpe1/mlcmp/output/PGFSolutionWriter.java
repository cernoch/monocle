/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.output;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.utilities.ModelUtilities;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.MathUtils;
import java.awt.Color;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Outputs the solution to a TeX file that can be compiled with pdfLaTeX. The output
 * file contains the solution as a tikzpicture. This output is only informative and
 * is not intended as an proper output from the program. It also may contain bugs and
 * there is no warranty that the output does 100% hold the ordering. Also it may be
 * sometimes unreadable especially if there is a large area.
 *
 * @author Petr Ryšavý
 */
public class PGFSolutionWriter
{
	/**
	 * Output in that will be writen warnings.
	 */
	private final Writer errOut;
	/**
	 * Format for writing the decimal number inputs.
	 */
	private static final DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance(Locale.US);

	static
	{
		format.setMaximumFractionDigits(Integer.MAX_VALUE);
	}

	/**
	 * Creates new graph writer.
	 *
	 * @param errOut Output for warnings.
	 */
	public PGFSolutionWriter(Writer errOut)
	{
		this.errOut = errOut;
	}

	/**
	 * Writes the solution to a given file.
	 *
	 * @param file    The target file.
	 * @param problem The problem.
	 * @param lo      The solution.
	 * @throws IOException When file is not writable or there is an error writing it.
	 */
	public void writeGraph(Path file, MLCMP problem, Ordering lo) throws IOException
	{
		try (Writer br = Files.newBufferedWriter(file, Charset.forName("utf-8")))
		{
			writeGraph(br, problem, lo);
		}
	}

	/**
	 * Writes the solution to the given stream.
	 *
	 * @param out     The output stream.
	 * @param problem The problem.
	 * @param lo      The solution.
	 * @throws IOException When calling the stream methods.
	 */
	public void writeGraph(Writer out, MLCMP problem, Ordering lo) throws IOException
	{
		final double[] scale = getScale(problem);

		out.write("\\documentclass{article}\n");
		out.write("\\usepackage{tikz}\n");
		out.write("\\usepackage{xcolor}\n");
		out.write("\\usepackage[active,tightpage]{preview}\n");
		out.write("\\usetikzlibrary{calc}\n");
		out.write("\\PreviewEnvironment{tikzpicture}\n\n");

		Map<Line, String> colors = generateColors(out, problem.lineIterator());

		out.write("\\begin{document}\n");
		out.write("\\begin{tikzpicture}\n");

		for (Track t : new IteratorWrapper<>(problem.trackIterator()))
			writeTrack(out, t, lo, scale, colors);

		out.write("\\end{tikzpicture}\n");
		out.write("\\end{document}\n");
	}

	;

	/**
	 * Returns the scale of the coordinates such that the pdfLaTeX will be able to plot it.
	 * The first value is the axis scale, second the X center and third the Y center. At first
	 * subtract the center and then multiply by scale to get scaled coordinate.
	 * @param problem The problem.
	 * @return A 3*1 double vector with scale description.
	 */
	@SuppressWarnings("NestedAssignment")
	private double[] getScale(MLCMP problem)
	{
		// maximální rozah může být 3m, ie 300
		double minDist = Double.MAX_VALUE;
		double tmp;
		double maxX = Double.MIN_VALUE;
		double maxY = Double.MIN_VALUE;
		double minX = Double.MAX_VALUE;
		double minY = Double.MAX_VALUE;
		for (Track t : new IteratorWrapper<>(problem.trackIterator()))
		{
			final Station vertex1 = t.getVertex1();
			final Station vertex2 = t.getVertex2();
			maxX = Math.max(maxX, vertex1.getX());
			minX = Math.min(minX, vertex1.getX());
			maxX = Math.max(maxX, vertex2.getX());
			minX = Math.min(minX, vertex2.getX());
			maxY = Math.max(maxY, vertex1.getY());
			minY = Math.min(minY, vertex1.getY());
			maxY = Math.max(maxY, vertex2.getY());
			minY = Math.min(minY, vertex2.getY());

			if ((tmp = ModelUtilities.stationDistance(vertex1, vertex2)) < minDist)
				minDist = tmp;
		}

		double scale = minDist > 1 ? 1 : 1 / minDist;
		if (scale * Math.max(maxX - minX, maxY - minY) > 100)
			scale = 100 / Math.max(maxX - minX, maxY - minY);
		return new double[]
		{
			scale,
			(maxX + minX) / 2,
			(maxY + minY) / 2
		};
	}

	/**
	 * Writes the track to output stream.
	 *
	 * @param out    Output stream.
	 * @param t      The track to write.
	 * @param lo     Ordering of the track.
	 * @param scale  The scale by {@literal getScale} method.
	 * @param colors The colormap of lines.
	 * @throws IOException When calling the stream methods.
	 */
	private void writeTrack(Writer out, Track t, Ordering lo, double[] scale, Map<Line, String> colors) throws IOException
	{
		final Station vertex1 = t.getVertex1();
		final double x1 = (vertex1.getX() - scale[1]) * scale[0];
		final double y1 = (vertex1.getY() - scale[2]) * scale[0];
		final double ang1rad = t.getVertex1Angle() + Math.PI / 2;
		final int ang1 = (int) Math.toDegrees(ang1rad);
		final Station vertex2 = t.getVertex2();
		final double x2 = (vertex2.getX() - scale[1]) * scale[0];
		final double y2 = (vertex2.getY() - scale[2]) * scale[0];
		final double ang2rad = t.getVertex2Angle() + Math.PI / 2;
		final int ang2 = (int) Math.toDegrees(ang2rad);

		if (lo.isTotallyOrdered(t))
		{
			int i = 0;
			Iterator<Line> it = lo.finalizedOrderIterator(t, vertex1);
			while (it.hasNext())
			{
				out.write(String.format(Locale.US,
										//to [in=%d, out=%d]
										"  \\draw[color=%s,line width=1pt] ($(%.2f,%.2f) + (%.2fpt, %.2fpt)$) to [in=%d, out=%d] ($(%.2f,%.2f) + (%.2fpt, %.2fpt)$);\n",
										colors.get(it.next()),
										x1, y1,
										i * Math.sin(ang1rad), -i * Math.cos(ang1rad),
										ang2, ang1,
										x2, y2,
										//										i * Math.sin(ang1rad), -i * Math.cos(ang1rad)));
										-i * Math.sin(ang2rad), i * Math.cos(ang2rad)));
				i++;
			}

		}
		else
			out.write(String.format(Locale.US,
									"  \\draw[color=black, ,line width=3pt] (%.2f,%.2f) to [in=%d, out=%d] (%.2f,%.2f);\n",
									x1, y1,
									ang2, ang1,
									x2, y2));
	}

	/**
	 * Generates the colormap of the lines. If the lines names are color names then
	 * they will be used, otherwise a random color will be used. The names will be
	 * writen to stream as tikz defined color.
	 *
	 * @param out          The output stream.
	 * @param lineIterator Iterator of lines.
	 * @return The colormap.
	 * @throws IOException When calling the writer methods.
	 */
	private Map<Line, String> generateColors(Writer out, Iterator<Line> lineIterator) throws IOException
	{
		HashMap<Line, Color> map = new HashMap<>(16);
		while (lineIterator.hasNext())
		{
			final Line l = lineIterator.next();
			switch (l.getName())
			{
				case "black":
					map.put(l, Color.BLACK);
					break;
				case "blue":
					map.put(l, Color.BLUE);
					break;
				case "cyan":
					map.put(l, Color.CYAN);
					break;
				case "gray":
					map.put(l, Color.GRAY);
					break;
				case "green":
					map.put(l, Color.GREEN);
					break;
				case "magenta":
					map.put(l, Color.MAGENTA);
					break;
				case "orange":
					map.put(l, Color.ORANGE);
					break;
				case "pink":
					map.put(l, Color.PINK);
					break;
				case "red":
					map.put(l, Color.RED);
					break;
				case "yellow":
					map.put(l, Color.YELLOW);
					break;
				default:
					map.put(l, randomColor());
					break;
			}
		}

		HashMap<Line, String> retval = new HashMap<>(map.size());
		int i = 0;
		for (Entry<Line, Color> e : map.entrySet())
		{
			final String genname = "gencolor" + i;
			retval.put(e.getKey(), genname);
			final Color col = e.getValue();
			out.write("\\definecolor{gencolor" + i + "}{RGB}{" + col.getRed() + ',' + col.getGreen() + ',' + col.getBlue() + "}\n");
			i++;
		}

		return retval;
	}

	/**
	 * Generates a random color.
	 *
	 * @return Random color.
	 */
	private Color randomColor()
	{
		return new Color(MathUtils.random(255), MathUtils.random(255), MathUtils.random(255));
	}
}
