/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import java.util.Collection;
import java.util.Map;

/**
 * Java interface that is used for creating lines from data loaded from OSM XML.
 *
 * @author Petr Ryšavý
 */
public interface LineFactory
{
	/**
	 * Gets the line name from the values that were loaded from the XML file.
	 *
	 * @param values Map of properties loaded from XML.
	 * @return The line name. The {@code null} value can be returned if such line is
	 *         not desired to be loaded.
	 */
	public String lineName(Map<String, String> values);

	/**
	 * Decides whether the given tag is interesting for the station. If true, then
	 * the tag will be assigned to the {@code values} parameter of {@code createLine}
	 * method.
	 *
	 * @param key Key name.
	 * @return {@code true} if tag is required, {@code false} otherwise.
	 */
	public boolean readTag(String key);

	/**
	 * Creates line with specified name and collection of tracks.
	 *
	 * @param tracks   Collection of tracks.
	 * @param lineName Line name returned by {@code lineName} method.
	 * @return New line.
	 */
	public Line createLine(Collection<Track> tracks, String lineName);

	/**
	 * Decides whether way role is interesting for line. For example tram line can
	 * contain stations in OSM XML file defined as way inside the relation tag. But
	 * the stations are not part of the tracks, we are interested in and this method
	 * is check that can protect us from loading them.
	 *
	 * @param role The role of way in relation.
	 * @return {@code true} if the way shall be loaded, {@code false} otherwise.
	 */
	public boolean acceptedMemberRole(String role);
}