/**
 * Copyright (c) 2010 Radomír Černoch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input.osm;

import cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory.LineFactory;
import cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory.XMLConfig;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.UnderlyingGraph;
import cz.cvut.fel.rysavpe1.utilities.SetUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.openstreetmap.partrksplitter.input.Monitor;
import org.openstreetmap.partrksplitter.input.XmlParser;
import org.xml.sax.SAXException;

/**
 * Manager for loading map from a file
 *
 * @author Radomír Černoch
 */
public class DataLoader
{
	/**
	 * Configuration that provides factory.
	 */
	private final XMLConfig config;
	/**
	 * Path to file with OSM XML.
	 */
	private final Path file;
	/**
	 * Shall be monitors enabled.
	 */
	private boolean enableMonitors;
	/**
	 * Phase progress of the load. If the loader is not in desired phase then
	 * {@code IllegalStateException} is thrown.
	 */
	private int phase;
	/**
	 * The loaded problem.
	 */
	private MLCMP problem;
	/**
	 * Line factory returned by {@code config}.
	 *
	 * @see #config
	 */
	private LineFactory lineFactory;
	/**
	 * Output for logging. If this stream is not specified, then standard error
	 * output is used.
	 */
	private PrintWriter out;

	/**
	 * Initializes the data loader
	 *
	 * @param file   file with the map in OSM-XML format from which all the data will
	 *               be loaded
	 * @param config configuration with patterns and groups, which trigger ways to be
	 *               loaded
	 */
	public DataLoader(Path file, XMLConfig config)
	{
		this.file = file;
		this.config = config;
		phase = 0;
	}

	/**
	 * Enables the monitors during the loading.
	 *
	 * @param enable Shall be monitors enabled.
	 */
	public void enableMonitors(boolean enable)
	{
		this.enableMonitors = enable;
	}

	/**
	 * Sets the logging output stream.
	 *
	 * @param out Output sream.
	 */
	public void setOutput(Writer out)
	{
		if (out instanceof PrintWriter)
			this.out = (PrintWriter) out;
		else
			this.out = new PrintWriter(out);
	}
	/**
	 * Set of ways, which were loaded during {@link #phase1()}.
	 *
	 * <p>They serve as "trigger ways" for {@link #phase2()}, which loads nodes for
	 * this set of ways.</p>
	 */
	private Collection<Integer> waysFromPhase1;
	/**
	 * Set of stations, which were loaded during {@link #phase2()}.
	 *
	 * <p>They serve as "trigger nodes" for {@link #phase3()}, which loads nodes for
	 * this set of ways.</p>
	 */
	private Collection<Integer> stationsFromPhase2;
	/**
	 * Map from line name into set of way ids.
	 */
	private Map<String, Set<Integer>> linesMap;
	private Map<String, Set<Integer>> linesUnmodMap;
	/**
	 * Map from line id into set of node ids.
	 */
	private Map<Integer, List<Integer>> waysMap;
	/**
	 * Map from node id directly to the station id.
	 */
	private Map<Integer, Station> stationsMap;
	private Map<Station, Integer> stationsReverseMap;
	private Map<Integer, Station> replaceStationMap;
	private Collection<Set<Integer>> equivallentStations;

	/**
	 * Starts the given parser and optionally prints progress bar
	 *
	 * @param parser the parser to run
	 * @throws SAXException if XML file contains syntax errors
	 * @throws IOException  if the file is unreadable
	 */
	private void runParser(XmlParser parser) throws SAXException, IOException
	{
		final Monitor monitor = new Monitor(parser, out);

		try
		{
			if (enableMonitors)
				monitor.start();

			parser.parse(file);

		}
		finally
		{
			monitor.terminate();
		}
	}

	/**
	 * Loads all ways, which belong to one of the groups defined in the config
	 *
	 * @throws SAXException if XML file contains syntax errors
	 * @throws IOException  if the file is unreadable
	 */
	public void phase1() throws SAXException, IOException
	{
		if (phase != 0)
			throw new IllegalStateException("Phase1 can be called only as first call.");
		phase = 1;

		lineFactory = config.getLineFactory();
		RelationParser parser = new RelationParser(lineFactory);
		runParser(parser);
		waysFromPhase1 = parser.getWaysToLoadIds();
		linesMap = parser.getLoadedRelations();

		if (enableMonitors && out != null)
			out.println("Phase 1 done.");

		phase = 2;
	}

	/**
	 * Loads nodes, which belong to ways loaded during {@link #phase1()}
	 *
	 * @throws SAXException if XML file contains syntax errors
	 * @throws IOException  if the file is unreadable
	 */
	public void phase2() throws SAXException, IOException
	{
		if (phase != 2)
			throw new IllegalStateException("Phase2 can be called only after phase1.");
		phase = 3;

		WayParser parser = new WayParser(waysFromPhase1);
		runParser(parser);
		waysMap = parser.getLoadedWays();
		stationsFromPhase2 = parser.getStationsToRead();

		if (enableMonitors && out != null)
			out.println("Phase 2 done.");

		phase = 4;
	}

	/**
	 * Loads positions of all loaded nodes
	 *
	 * @throws SAXException if XML file contains syntax errors
	 * @throws IOException  if the file is unreadable
	 */
	public void phase3() throws SAXException, IOException
	{
		if (phase != 4)
			throw new IllegalStateException("Phase3 can be called only after phase2.");
		phase = 5;

		StationParser parser = new StationParser(stationsFromPhase2, config.getStationFactory());
		runParser(parser);
		stationsMap = parser.getStationsMap();

		if (enableMonitors && out != null)
			out.println("Phase 3 done.");

		phase = 6;
	}

	/**
	 * Constructs the graph from data loaded during the phases1-3.
	 */
	@SuppressWarnings("UnusedAssignment")
	public void constructGraph()
	{
		if (phase != 6)
			throw new IllegalStateException("Graph can be constructed only after phases 1-3.");
		phase = 7;

		final UnderlyingGraph graph = new UnderlyingGraph();
		Map<Integer, Station> replaceMap = new HashMap<>();
		// at first add all stations to the graph
		for (Entry<Integer, Station> e : stationsMap.entrySet())
		{
			Station s = e.getValue();
			// Czech trails are a little bit buggy - there are nodes with same coordinate
			if (!graph.contains(s))
				graph.addVertex(s);
			else
				replaceMap.put(e.getKey(), graph.getStoredStation(s));
		}

		final LinkedList<Line> lines = new LinkedList<>();
		final ArrayList<Track> lineTracks = new ArrayList<>(1024);

		// pas through all lines
		for (String lineName : linesMap.keySet())
		{
			// and go through all ways of that line
			final Set<Integer> ways = linesMap.get(lineName);

			for (Integer wayId : ways)
			{
				// get the nodes list
				final List<Integer> way = waysMap.get(wayId);
				// bad thig - corrupted XML that contains refference to way that is not specified
				if (way == null)
				{
					if (out != null)
						out.println("Found refference to way that is not specified in the file : " + wayId);
					continue;
				}

				final int size = way.size();

				if (size <= 1)
				{
					if (out != null)
						out.println("Illegal way - no or one node.");
					continue;
				}

				// now create tracks and paste them into line
				final Integer first = way.get(0);
				Station u = replaceMap.containsKey(first) ? replaceMap.get(first) : stationsMap.get(first);
				for (int i = 1; i < size; i++)
				{
					final Integer id = way.get(i);
					final Station v = replaceMap.containsKey(id) ? replaceMap.get(id) : stationsMap.get(id);
					// we don't want tracks that have both endpoints same
					if (u.equals(v))
						continue;
					Track t = new Track(u, v);

					// don't try to add to graph track if it already exists
					if (graph.contains(t))
						t = graph.getStoredTrack(t);
					else
						graph.addEdge(t);

					// and add track to current line
					lineTracks.add(t);
					u = v;
				}
			}

			// remember the just created line
			lines.add(lineFactory.createLine(lineTracks, lineName));
			lineTracks.clear();
		}

		problem = new MLCMP(graph, lines);
		replaceStationMap = replaceMap;

		if (enableMonitors && out != null)
			out.println("Graph constructed.");

		phase = 8;
	}

	public Map<Station, Integer> getStationToIdMap()
	{
		if (phase != 8)
			throw new IllegalStateException("Graph must be constructed before.");
		
		if (stationsReverseMap == null)
		{
			Map<Station, Integer> map = new HashMap<>(stationsMap.size());
			for (Entry<Integer, Station> e : stationsMap.entrySet())
				map.put(e.getValue(), e.getKey());
			stationsReverseMap = Collections.unmodifiableMap(map);
		}
		return stationsReverseMap;
	}
	
	public Collection<Set<Integer>> getEquivallentStations()
	{
		if (phase != 8)
			throw new IllegalStateException("Graph must be constructed before.");
		
		Map<Station, Set<Integer>> equivallenceMap = new HashMap<>();
		getStationToIdMap();
		if(equivallentStations == null)
		{
			for(Entry<Integer, Station> e : replaceStationMap.entrySet())
			{
				final Station s = e.getValue();
				Set<Integer> stationSet = equivallenceMap.remove(s);
				if(stationSet == null)
					stationSet = SetUtilities.newHashSet(stationsReverseMap.get(s));
				stationSet.add(e.getKey());
				equivallenceMap.put(s, stationSet);
			}
			equivallentStations = Collections.unmodifiableCollection(equivallenceMap.values());
		}
		return equivallentStations;
	}
	
	public Map<String, Set<Integer>> getLineToWaysIdsMap()
	{
		if (phase != 8)
			throw new IllegalStateException("Graph must be constructed before.");
		
		if(linesUnmodMap == null)
		{
			Map<String, Set<Integer>> map = new HashMap<>(linesMap.size());
			for(Entry<String, Set<Integer>> e : linesMap.entrySet())
				map.put(e.getKey(), Collections.unmodifiableSet(e.getValue()));
			linesUnmodMap = Collections.unmodifiableMap(map);
		}
		return linesUnmodMap;
	}

	/**
	 * Returns the loaded problem.
	 *
	 * @return The problem.
	 */
	public MLCMP getProblem()
	{
		if (phase != 8)
			throw new IllegalStateException("Graph must be constructed before.");
		return problem;
	}
}
