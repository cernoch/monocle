package cz.cvut.fel.rysavpe1.mlcmp.input.osm;

import cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory.LineFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import static cz.cvut.fel.rysavpe1.mlcmp.input.osm.OSMXML.*;
import static cz.cvut.fel.rysavpe1.utilities.MathUtils.parseUnsignedInt;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Parser of {@code relation} tag. Relation tag is used for specifiing that set of
 * ways in map belong together, for example that ways belong to red tourist trail.
 *
 * @author Radomír Černoch
 */
public class RelationParser extends OneUseXMLParser
{
	/**
	 * The id of currently handled way. Some tourist trail ways are not part of any
	 * relation. {@code null} if no way is currently beeing loaded.
	 */
	private Integer wayId = null;
	/**
	 * The id of currently handled relation. {@code null} if no relation is currently
	 * beeing loaded.
	 */
	private Integer relId = null;
	/**
	 * Map from line name to set of ways that belong to this line.
	 */
	private final Map<String, Set<Integer>> lines;
	/**
	 * List of ways that are loaded as part of current relation. List contains
	 * temporaly data, but instance is used more times.
	 */
	private final List<Integer> wayRef;
	/**
	 * Set of ways that shall be loaded in next phase. Each of this ways is part of
	 * some relation that is loaded.
	 */
	private final Set<Integer> waysToLoadIds;
	/**
	 * Map of loaded properties that belong to current relation. Map contains
	 * temporaly data, but instance is used more times.
	 */
	private final Map<String, String> tagMap;
	/**
	 * Factory that is responsible of coordinating which data shall be loaded from
	 * XML file. This class defines if the loaded graph will contain tourist trails
	 * or public transportation tracks.
	 */
	private final LineFactory lf;

	/**
	 * Creates new relation parser.
	 *
	 * @param lf The factory responsible of coordinating which data shall be loaded
	 *           from XML file.
	 */
	public RelationParser(LineFactory lf)
	{
		this.lf = lf;
		lines = new HashMap<>();
		wayRef = new ArrayList<>(128);
		tagMap = new HashMap<>(32);
		waysToLoadIds = new HashSet<>(1024);
	}

	@Override
	public void startElement(String uri, String localName,
		String qName, Attributes attrs) throws SAXException
	{
		/*
		 * Hitting a member of a relation
		 * => note down the members (if relation becomes interesting)
		 */
		if (relId != null && localName.equals(MEMBER_TAG))
		{
			if (attrs.getValue(TYPE_ATTR) != null
				&& attrs.getValue(TYPE_ATTR).equals(WAY_TYPE)
				&& lf.acceptedMemberRole(attrs.getValue(ROLE_ATTR)))
				wayRef.add(parseUnsignedInt(attrs.getValue(REF_ATTR)));

			return;
		}

		/*
		 * Spotting a tag triggering a group-membership
		 * => remember all groups, which are matched by the relation.
		 */
		if ((relId != null || wayId != null) && localName.equals(TAG_TAG))
		{
			final String key = attrs.getValue(KEY_ATTR);

			if (lf.readTag(key))
				tagMap.put(key, attrs.getValue(VALUE_ATTR));
			return;
		}

		/*
		 * Start of a "way" definition => note down the id.
		 */
		if (localName.equals(WAY_TAG))
		{
			wayId = parseUnsignedInt(attrs.getValue(ID_ATTR));
			return;
		}

		/*
		 * Start of a "relation" definition => note down the id.
		 */
		if (localName.equals(RELATION_TAG))
			relId = parseUnsignedInt(attrs.getValue(ID_ATTR));
	}

	@Override
	public void endElement(String uri, String localName,
		String qName) throws SAXException
	{

		/*
		 * Hitting the end of a relation
		 * => remember all its members and reinit
		 */
		if (localName.equals(RELATION_TAG))
		{
			final String lineName = lf.lineName(tagMap);
			if (lineName != null)
			{
				Set<Integer> ways = lines.get(lineName);
				if (ways == null)
				{
					ways = new HashSet<>(wayRef.size() << 4);
					lines.put(lineName, ways);
				}

				ways.addAll(wayRef);
				waysToLoadIds.addAll(wayRef);
			}

			relId = null;
			wayRef.clear();
			tagMap.clear();
			return;
		}

		/*
		 * Hitting the end of a way
		 * => remember all its groups
		 */
		if (localName.equals(WAY_TAG))
		{
			final String lineName = lf.lineName(tagMap);
			if (lineName != null)
			{
				Set<Integer> ways = lines.get(lineName);
				if (ways == null)
				{
					ways = new HashSet<>(16);
					lines.put(lineName, ways);
				}

				ways.add(wayId);
				waysToLoadIds.add(wayId);
			}

			wayId = null;
			tagMap.clear();
		}
	}

	/**
	 * Returns the set of ways that shall be loaded in next phase. Set contains id of
	 * each way that is part of desired relations and that shall be loaded into
	 * graph.
	 *
	 * @return Set of ids of ways that will be loaded.
	 */
	public Set<Integer> getWaysToLoadIds()
	{
		if (!isWholeWorkDone())
			throw new IllegalStateException("The file wasn't parsed yet.");

		return Collections.unmodifiableSet(waysToLoadIds);
	}

	/**
	 * Return map from line name given by LineFactory to set of ids of ways that
	 * belong to the specified line.
	 *
	 * @return Map with ids of each line.
	 */
	public Map<String, Set<Integer>> getLoadedRelations()
	{
		if (!isWholeWorkDone())
			throw new IllegalStateException("The file wasn't parsed yet.");

		return Collections.unmodifiableMap(lines);
	}
}
