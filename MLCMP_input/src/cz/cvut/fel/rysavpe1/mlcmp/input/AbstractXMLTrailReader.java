/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input;

import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.input.osm.DataLoader;
import cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory.XMLConfig;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Path;
import org.xml.sax.SAXException;

/**
 * Class that defines abstract class that can read underlying graph from XML OSM
 * file.
 *
 * @author Petr Ryšavý
 */
public abstract class AbstractXMLTrailReader
{
	/**
	 * Output in that will be writen warnings and monitor status.
	 */
	private final Writer out;

	/**
	 * Creates new instance.
	 *
	 * @param out The output stream where shall be logged the progress.
	 */
	public AbstractXMLTrailReader(Writer out)
	{
		this.out = out;
	}

	/**
	 * Reads graph from given XML file.
	 *
	 * @param file The XML file with graph specification.
	 * @return The underlying graph.
	 * @throws IOException           When file reading occurs an error.
	 * @throws GraphReadingException When the graph is badly specified.
	 * @throws SAXException          When XML is not valid.
	 */
	public abstract MLCMP readGraph(Path file) throws IOException, GraphReadingException, SAXException;

	/**
	 * Reads graph from given XML file with given format.
	 *
	 * @param file The XML file with graph specification.
	 * @param con  The configuration that enables to parse objects from data.
	 * @return The underlying graph.
	 * @throws IOException           When file reading occurs an error.
	 * @throws SAXException          When XML is not valid.
	 */
	@SuppressWarnings("UseOfSystemOutOrSystemErr")
	protected MLCMP readGraph(Path file, XMLConfig con) throws SAXException, IOException
	{
		DataLoader dl = new DataLoader(file, con);
		dl.enableMonitors(true);
		if (out == null)
			dl.setOutput(new PrintWriter(System.err));
		else
			dl.setOutput(out);

		dl.phase1();
		dl.phase2();
		dl.phase3();
		dl.constructGraph();

		return dl.getProblem();
	}
}
