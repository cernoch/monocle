/**
 * Copyright (c) 2010 Radomír Černoch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input.osm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.xml.sax.*;
import static cz.cvut.fel.rysavpe1.mlcmp.input.osm.OSMXML.*;
import static cz.cvut.fel.rysavpe1.utilities.MathUtils.parseUnsignedInt;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Radomír Černoch
 */
public class WayParser extends OneUseXMLParser
{
	/**
	 * The id of currently parsed way.
	 */
	private Integer wayId = null;
	/**
	 * The list of way ids that contains all ways that shall be read.
	 */
	private final Collection<Integer> triggers;
	/**
	 * List of nodes in current way.
	 */
	private final List<Integer> nodes;
	/**
	 * Map from way id to list of way nodes ids.
	 */
	private final Map<Integer, List<Integer>> loadedWays;
	/**
	 * Set of stations that shall be read.
	 */
	private final Set<Integer> stationsToRead;

	/**
	 * Creates new way parser.
	 * @param interestingWays Set of stations that shall be read. 
	 */
	public WayParser(Collection<Integer> interestingWays)
	{
		this.triggers = interestingWays;
		nodes = new ArrayList<>(64);
		loadedWays = new HashMap<>(interestingWays.size());
		this.stationsToRead = new HashSet<>(1024);
	}

	@Override
	public void startElement(String uri, String localName,
							 String qName, Attributes attrs) throws SAXException
	{
		// if you are in a way and you found a node refference tag, then read the node
		if (wayId != null && localName.equals(NODE_REF_TAG))
		{
			nodes.add(parseUnsignedInt(attrs.getValue(REF_ATTR)));
			return;
		}

		// the beginning of line can ve only iff there is no current way
		if (wayId == null && localName.equals(WAY_TAG))
		{
			wayId = parseUnsignedInt(attrs.getValue(ID_ATTR));
			if (!triggers.contains(wayId))
				wayId = null;
		}
	}

	@Override
	public void endElement(String uri, String localName,
						   String qName) throws SAXException
	{
		// we found the end of way
		if (wayId != null && localName.equals(WAY_TAG))
		{
			// get the refference to the list of nodes of this way
			List<Integer> way = loadedWays.get(wayId);
			if(way == null)
			{
				way = new ArrayList<>(nodes.size());
				loadedWays.put(wayId, way);
			}

			// add all nodes
			way.addAll(nodes);
			stationsToRead.addAll(nodes);

			// and do the housekeeping
			wayId = null;
			nodes.clear();
		}
	}

	/**
	 * Returns the map that describes the loaded ways.
	 * 
	 * @return Map from way id to list of nodes ids.
	 */
	public Map<Integer, List<Integer>> getLoadedWays()
	{
		if (!isWholeWorkDone())
			throw new IllegalStateException("The file wasn't parsed yet.");

		return Collections.unmodifiableMap(loadedWays);
	}
	
	/**
	 * Returns the set of node ids that shall be read in next phase.
	 * 
	 * @return Set of ids of nodes that shall be readed.
	 */
	public Set<Integer> getStationsToRead()
	{
		if (!isWholeWorkDone())
			throw new IllegalStateException("The file wasn't parsed yet.");

		return Collections.unmodifiableSet(stationsToRead);
	}
}
