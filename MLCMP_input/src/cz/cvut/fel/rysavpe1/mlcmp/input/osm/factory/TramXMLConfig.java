/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory;

/**
 * Configuration of {@code DataLoader} that is needed for loading of tram trails
 * graph.
 *
 * @author Petr Ryšavý
 */
public class TramXMLConfig implements XMLConfig
{
	/**
	 * The instance of this singleton.
	 */
	private static TramXMLConfig instance = null;

	/**
	 * Don't let anybody to create instance of this class.
	 */
	private TramXMLConfig()
	{
	}

	/**
	 * Retrieve the instance of this class.
	 *
	 * @return The singleton instance.
	 */
	public static TramXMLConfig getInstance()
	{
		if (instance == null)
			instance = new TramXMLConfig();

		return instance;
	}

	@Override
	public StationFactory getStationFactory()
	{
		return DefaultStationFactory.getInstance();
	}

	@Override
	public LineFactory getLineFactory()
	{
		return TramLineFactory.getInstance();
	}
}
