/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input;

import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory.TramXMLConfig;
import cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory.XMLConfig;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import org.xml.sax.SAXException;

/**
 * Reader that enables read train network from OSM file into underlying graph.
 *
 * @author Petr Ryšavý
 * @see TramXMLConfig
 */
public class XMLTramGraphReader extends AbstractXMLTrailReader
{
	/**
	 * Creates new graph reader.
	 *
	 * @param out Output for warnings.
	 */
	public XMLTramGraphReader(Writer out)
	{
		super(out);
	}

	@SuppressWarnings("UseOfSystemOutOrSystemErr")
	@Override
	public MLCMP readGraph(Path file) throws IOException, GraphReadingException, SAXException
	{
		XMLConfig con = TramXMLConfig.getInstance();
		return super.readGraph(file, con);
	}
}
