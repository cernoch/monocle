/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import java.util.Collection;
import java.util.Map;
import static cz.cvut.fel.rysavpe1.mlcmp.input.osm.OSMXML.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Line factory that is responsible of loading tram lines graph.
 *
 * @author Petr Ryšavý
 */
public class TramLineFactory implements LineFactory
{
	/**
	 * Set with tags that shall be loaded.
	 */
	private static final Set<String> INTERESTING_TAGS = new HashSet<>(8);

	static
	{
		INTERESTING_TAGS.add(NAME_KEY);
		INTERESTING_TAGS.add(ROUTE_KEY);
		INTERESTING_TAGS.add(TYPE_KEY);
		INTERESTING_TAGS.add(REF_KEY);
	}
	/**
	 * The instance of this singleton.
	 */
	private static TramLineFactory instance = null;

	/**
	 * Don't let anybody to create instance of this class.
	 */
	private TramLineFactory()
	{
	}

	/**
	 * Retrieve the instance of this class.
	 *
	 * @return The singleton instance.
	 */
	public static TramLineFactory getInstance()
	{
		if (instance == null)
			instance = new TramLineFactory();

		return instance;
	}

	@Override
	public String lineName(Map<String, String> values)
	{
		final String type = values.get(TYPE_KEY);
		if (type == null)
			return null;

		final String route = values.get(ROUTE_KEY);
		if (type.equals(ROUTE_VALUE) && route != null && route.equals(TRAM_VALUE))
			if (values.containsKey(REF_KEY))
				return "Tram " + values.get(REF_KEY);
			else
			{
				String name = values.get(NAME_KEY);
				// some trains don't have the ref tag, so try to use name instead
				// note that trams have multiple instances in the OSM XML - for
				// example one line consists from at least two instances, one from
				// A to B and second from B to A
				if (name.matches("Tram \\d+:.*"))
					name = name.substring(0, name.indexOf(':'));
				return name;
			}

		return null;
	}

	@Override
	public boolean readTag(String key)
	{
		return INTERESTING_TAGS.contains(key);
	}

	@Override
	public Line createLine(Collection<Track> tracks, String lineName)
	{
		return new Line(lineName, tracks);
	}

	@Override
	public boolean acceptedMemberRole(String role)
	{
		return role != null && role.isEmpty();
	}
}
