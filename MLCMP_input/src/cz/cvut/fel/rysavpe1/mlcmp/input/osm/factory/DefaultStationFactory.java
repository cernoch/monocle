/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory;

import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import java.util.HashMap;
import static cz.cvut.fel.rysavpe1.mlcmp.input.osm.OSMXML.*;
import cz.cvut.fel.rysavpe1.utilities.ReferencedHashSet;
import java.util.HashSet;

/**
 * Factory that creates station using node attributes lon, lat and name tag of the
 * node.
 *
 * @author Petr Ryšavý
 */
public class DefaultStationFactory implements StationFactory
{
	/**
	 * The instance of this singleton.
	 */
	private static DefaultStationFactory instance = null;

	/**
	 * Don't let anybody to create instance of this class.
	 */
	private DefaultStationFactory()
	{
	}

	/**
	 * Retrieve the instance of this class.
	 *
	 * @return The singleton instance.
	 */
	public static DefaultStationFactory getInstance()
	{
		if (instance == null)
			instance = new DefaultStationFactory();

		return instance;
	}

	@Override
	public boolean readTag(String key)
	{
		return key.equals(NAME_KEY);
	}

	@Override
	public Station createStation(HashMap<String, String> values)
	{
		final double x = Double.parseDouble(values.get(LONGITUDE_ATTR));
		final double y = Double.parseDouble(values.get(LATITUDE_ATTR));
		if (values.containsKey(NAME_KEY))
			return new Station(values.get(NAME_KEY), x, y);
		else
			return new Station(x, y);
	}
}
