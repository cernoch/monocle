/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input;

import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.LineFormatException;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.NodeFormatException;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.UndefinedStationException;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.UnderlyingGraph;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is a reader that reads the input from the plain text file.
 *
 * The input must have following format. The input can contain trailing spaces.
 * <ul>
 * <li>
 * List of node shotcuts definitions. Each node is written on single line in form
 * <code>(<i>node_name</i>)=<i>x_coord</i>, <i>y_coord</i></code>. Both coordinates
 * shall be writen with decimal point as dot
 * <code>.</code>. Node name can be upper or lowercase letters a-z and numbers. Two
 * nodes with same coordinates will produce a warning and they will be merged.
 * </li>
 * <li> Empty line </li>
 * <li>
 * List of edges. The edges can be grouped into paths. Each line begins with name of
 * the line folowed by list of nodes splitted by
 * <code>-</code>. Each node shall be in form
 * <code>(<i>node_name</i>)</code> or
 * <code>(<i>x_coord</i>, <i>y_coord</i>)</code>. One color can be on more than one
 * lines of the input. Using not defined node name will produce an error. The edges
 * are not allowed to cross each other in any other point than node. The line name
 * can contain numbers and uppercase or lowercase letters a-z.
 * </li>
 * </ul>
 *
 * Example of input:
 * <pre>
 * (A)=0,0
 * (X)=4,2
 * (Y)=7,2
 *
 * yellow (A)-(2,2)-(X)-(Y)-(11,0)
 * green  (1,7)-(3.5, 4.2)-(X)-(Y)
 * green  (Y)-(7,6)
 * green  (Y)-(10,5)
 * </pre>
 *
 * @author Petr Ryšavý
 */
public class PlainTextGraphReader
{
	/**
	 * Output in that will be writen warnings.
	 */
	private final Writer errOut;
	/**
	 * Group of node name.
	 */
	private static final String NODE_NAME_GROUP = "nodeName";
	/**
	 * Group capturing line name.
	 */
	private static final String LINE_NAME_GROUP = "lineName";
	/**
	 * Group capturing set of line edges.
	 */
	private static final String LINE_DEF_GROUP = "line";
	/**
	 * Group capturing x-coordinate of node.
	 */
	private static final String X_COORD_GROUP = "xCoord";
	/**
	 * Group capturing y-coordinate of node.
	 */
	private static final String Y_COORD_GROUP = "yCoord";
	/**
	 * Regular expression for double value.
	 */
	private static final String DOUBLE_REGEX = "\\d+(\\.\\d+)?";
	/**
	 * Numerical node definition.
	 */
	private static final String NUMERICAL_NODE = "(?<" + X_COORD_GROUP
		+ '>' + DOUBLE_REGEX + ")\\s*,\\s*(?<" + Y_COORD_GROUP + '>' + DOUBLE_REGEX + ")";
	/**
	 * Textual node definition.
	 */
	private static final String TEXTUAL_NODE = "[a-zA-Z]\\w*";
	/**
	 * Regex that matches node definition.
	 */
	private static final String NODE_DEF_REGEX = "^\\s*\\((?<" + NODE_NAME_GROUP
		+ ">" + TEXTUAL_NODE + ")\\)\\s*=\\s*" + NUMERICAL_NODE + "\\s*$";
	/**
	 * Regex for matching node in edge list.
	 */
	private static final String NODE_REGEX = '(' + NUMERICAL_NODE + '|' + TEXTUAL_NODE + ')';
	/**
	 * Regex for matching line with edge list.
	 */
	private static final String LINE_DEF_REGEX = "^\\s*(?<" + LINE_NAME_GROUP
		+ ">" + TEXTUAL_NODE + ")\\s+(?<" + LINE_DEF_GROUP + ">\\(" + NODE_REGEX
		+ "\\)(\\s*\\-\\s*\\("
		+ NODE_REGEX.replace(X_COORD_GROUP, X_COORD_GROUP + '1').replace(Y_COORD_GROUP, Y_COORD_GROUP + '1')
		+ "\\))+)\\s*$";
	/**
	 * Pattern for matching the nodes.
	 */
	//@SuppressWarnings("MalformedRegexp") // netbeans are confused with group name literals
	private static final Pattern NODE_DEF_PATTERN = Pattern.compile(NODE_DEF_REGEX);
	/**
	 * Pattern for matching the line edges.
	 */
	//@SuppressWarnings("MalformedRegexp")
	private static final Pattern LINE_DEF_PATTERN = Pattern.compile(LINE_DEF_REGEX);
	/**
	 * Pattern for searching nodes in the line definition.
	 */
	private static final Pattern NODE_SEARCH_PATTERN = Pattern.compile(NODE_REGEX);
	/**
	 * Pattern for searching nodes in the line definition.
	 */
	@SuppressWarnings("MalformedRegexp")
	private static final Pattern NUMERICAL_NODE_PATTERN = Pattern.compile(NUMERICAL_NODE);
	/**
	 * Format for reading the decimal number inputs.
	 */
	private static final DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance(Locale.US);

	/**
	 * Creates new graph reader.
	 *
	 * @param errOut Output for warnings.
	 */
	public PlainTextGraphReader(Writer errOut)
	{
		this.errOut = errOut;
	}

	/**
	 * Reads graph from given file.
	 *
	 * @param file The file where the graph is sotored.
	 * @return The ungerlying graph and lines.
	 * @throws IOException           When file is not readable or simillar error
	 *                               occurs.
	 * @throws GraphReadingException When input is badly formatted.
	 */
	public MLCMP readGraph(Path file) throws IOException, GraphReadingException
	{
		return readGraph(Files.newBufferedReader(file, Charset.forName("utf-8")));
	}
	
	public MLCMP readGraph(Reader in) throws IOException, GraphReadingException
	{
		return readGraph(new BufferedReader(in));
	}

	/**
	 * Reads the graph from the input.
	 *
	 * @param in The input stream.
	 * @return The underlying graph.
	 * @throws IOException           When calling input stream methods.
	 * @throws GraphReadingException When input is badly formatted.
	 */
	@SuppressWarnings("MalformedRegexp")
	public MLCMP readGraph(BufferedReader in) throws IOException, GraphReadingException
	{
		// Init the data structures
		UnderlyingGraph graph = new UnderlyingGraph();
		HashMap<String, Station> nameMap = new HashMap<>();
		Matcher mat;
		String line = null;

		// at first read the nodes
		while (true)
		{
			try
			{
				line = in.readLine();
				if (line == null)
					break;
				if (line.matches("\\s*"))
					continue;
				mat = NODE_DEF_PATTERN.matcher(line);
				if (!mat.matches())
					break;

				final String node = mat.group(NODE_NAME_GROUP);
				final Double x = format.parse(mat.group(X_COORD_GROUP)).doubleValue();
				final Double y = format.parse(mat.group(Y_COORD_GROUP)).doubleValue();
				Station s = new Station(node, x, y);
				if (graph.contains(s))
					errOut.append("Warning : Station at " + x + ", " + y + " has multiple name definitions.");
				else
					graph.addVertex(s);
				nameMap.put(node, s);
			}
			catch (ParseException ex)
			{
				// This exception won't arise as we check the format with the regex
				errOut.append("Unexpected ParseException ...");
			}
		}

		if (line != null && !line.matches(LINE_DEF_REGEX))
			throw new NodeFormatException("Found badly formatted node : " + line);

		HashMap<String, LinkedList<Track>> lines = new HashMap<>(16);

		// now read the edges
		while (true)
		{
			try
			{
				if (line == null)
					break;
				if (line.matches("\\s*"))
				{
					line = in.readLine();
					continue;
				}

				mat = LINE_DEF_PATTERN.matcher(line);
				if (!mat.matches())
					throw new LineFormatException("Found badly formatted line : " + line);

				// get or create the line
				String name = mat.group(LINE_NAME_GROUP);
				LinkedList<Track> l;
				if (lines.containsKey(name))
					l = lines.get(name);
				else
				{
					l = new LinkedList<>();
					lines.put(name, l);
				}

				// the string with the edges deginition
				String edges = mat.group(LINE_DEF_GROUP);

				Matcher nodesMat = NODE_SEARCH_PATTERN.matcher(edges);
				nodesMat.find();
				Station last = parseStation(edges.substring(nodesMat.start(), nodesMat.end()), nameMap);
				if (!graph.contains(last))
					graph.addVertex(last);
				else
					last = graph.getStoredStation(last);

				while (nodesMat.find())
				{
					Station next = parseStation(edges.substring(nodesMat.start(), nodesMat.end()), nameMap);
					if (!graph.contains(next))
						graph.addVertex(next);
					else
						next = graph.getStoredStation(next);

					Track t = new Track(last, next);
					if (!graph.contains(t))
						graph.addEdge(t);
					else
						t = graph.getStoredTrack(t);

					// add track to line
					l.add(t);

					last = next;
				}


				line = in.readLine();
			}
			catch (ParseException ex)
			{
				// This exception won't arise as we check the format with the regex
				errOut.append("Unexpected ParseException ...");
			}
		}

		// TODO dvě čáry se nesmí křížit, to není kontrolované

		Line[] linesA = new Line[lines.size()];
		int i = 0;
		for (Map.Entry<String, LinkedList<Track>> e : lines.entrySet())
			linesA[i++] = new Line(e.getKey(), e.getValue());
		return new MLCMP(graph, linesA);
	}

	/**
	 * Parses a single station.
	 *
	 * @param station The string with station to parse.
	 * @param nameMap The map from station names to the stations that were loaded.
	 * @return The new station if it is new definition or the station that
	 *         corresponds to given station string.
	 * @throws UndefinedStationException When {@code station} is only in form
	 *                                   {@code (stname)} and the {@code stname} is
	 *                                   not a station from {@code nameMap}.
	 * @throws IllegalArgumentException  When the {@code station} does not match
	 *                                   {@code TEXTUAL_NODE} nor
	 *                                   {@code NUMERICAL_NODE_PATTERN}.
	 */
	private Station parseStation(String station, HashMap<String, Station> nameMap) throws UndefinedStationException, ParseException
	{
		if (station.matches(TEXTUAL_NODE))
		{
			final Station s = nameMap.get(station);
			if (s == null)
				throw new UndefinedStationException("Trying to access undefined station : " + station);
			return s;
		}
		else
		{
			Matcher mat = NUMERICAL_NODE_PATTERN.matcher(station);

			if (!mat.matches())
				throw new IllegalArgumentException("The input is not a station definition.");

			final Double x = format.parse(mat.group(X_COORD_GROUP)).doubleValue();
			final Double y = format.parse(mat.group(Y_COORD_GROUP)).doubleValue();
			Station s = new Station(x, y);
			return s;
		}
	}
}
