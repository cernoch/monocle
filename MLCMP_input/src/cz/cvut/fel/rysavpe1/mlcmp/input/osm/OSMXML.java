/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input.osm;

/**
 * The constants that can be found in XML generated by OpenStreetMap.
 *
 * @author Petr Ryšavý
 * @see http://wiki.openstreetmap.org/wiki/OSM_XML
 */
public class OSMXML
{
	/**
	 * The name of tag that defines node. Node is a point.
	 *
	 * @see #LATITUDE_ATTR
	 * @see #LONGITUDE_ATTR
	 */
	public static final String NODE_TAG = "node";
	/**
	 * Name of tag that defines a way. Way is a set of nodes.
	 */
	public static final String WAY_TAG = "way";
	/**
	 * Name of tag that defines relation.
	 */
	public static final String RELATION_TAG = "relation";
	/**
	 * Name of tag that is used inside a way and that defines refference to a node.
	 * Each of node is a point in that way.
	 *
	 * @see #REF_ATTR
	 */
	public static final String NODE_REF_TAG = "nd";
	/**
	 * Name of tag that is used to add some additional information to node, way or
	 * relation. This tag has two options key and value.
	 *
	 * @see #KEY_ATTR
	 * @see #VALUE_ATTR
	 */
	public static final String TAG_TAG = "tag";
	/**
	 * Tag that defines member of the relation. Member is mostly a way, which is
	 * specified by type attribute.
	 *
	 * @see #TYPE_ATTR
	 * @see #RELATION_TAG
	 */
	public static final String MEMBER_TAG = "member";
	/**
	 * Name of attribute that is used for storing id.
	 */
	public static final String ID_ATTR = "id";
	/**
	 * Attribute of node that specifies latitude.
	 *
	 * @see #NODE_TAG
	 */
	public static final String LATITUDE_ATTR = "lat";
	/**
	 * Attribute of node that specifies longitude.
	 *
	 * @see #NODE_TAG
	 */
	public static final String LONGITUDE_ATTR = "lon";
	/**
	 * The key of the tag attribute.
	 *
	 * @see #TAG_TAG
	 */
	public static final String KEY_ATTR = "k";
	/**
	 * The value of the tag attribute.
	 *
	 * @see #TAG_TAG
	 */
	public static final String VALUE_ATTR = "v";
	/**
	 * The refference to another object id.
	 *
	 * @see #NODE_REF_TAG
	 */
	public static final String REF_ATTR = "ref";
	/**
	 * Tag that specifies whether relation member is way or anything other.
	 *
	 * @see #MEMBER_TAG
	 * @see #WAY_TYPE
	 */
	public static final String TYPE_ATTR = "type";
	/**
	 * Tag that specifies role of relation member.
	 *
	 * @see #MEMBER_TAG
	 */
	public static final String ROLE_ATTR = "role";
	/**
	 * The name tag key.
	 */
	public static final String NAME_KEY = "name";
	/**
	 * The type tag key. Type is for example route.
	 */
	public static final String TYPE_KEY = "type";
	/**
	 * The route key tag. Route can be for example tram.
	 */
	public static final String ROUTE_KEY = "route";
	/**
	 * The ref key is used as tag together with tram. The value is number that
	 * describes the train number.
	 */
	public static final String REF_KEY = "ref";
	/**
	 * Says that type of relation member is way.
	 *
	 * @see #TYPE_ATTR
	 */
	public static final String WAY_TYPE = "way";
	/**
	 * Value that specifies that way or relation is route type.
	 *
	 * @see #TYPE_KEY
	 */
	public static final String ROUTE_VALUE = ROUTE_KEY;
	/**
	 * Value specifying that route is tram.
	 *
	 * @see #ROUTE_KEY
	 */
	public static final String TRAM_VALUE = "tram";

	/**
	 * Don't let anybody to create class instance.
	 */
	private OSMXML()
	{
	}
}
