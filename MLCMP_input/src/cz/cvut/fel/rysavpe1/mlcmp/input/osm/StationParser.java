/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input.osm;

import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import java.util.HashMap;
import org.openstreetmap.partrksplitter.input.XmlParser;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import static cz.cvut.fel.rysavpe1.mlcmp.input.osm.OSMXML.*;
import static cz.cvut.fel.rysavpe1.utilities.MathUtils.parseUnsignedInt;
import cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory.StationFactory;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * Parser of the station object. Station maps to node object in map.
 * @author Petr Ryšavý
 */
public class StationParser extends OneUseXMLParser
{
	/**
	 * The id of current station. {@code null} signalizes that no station is in
	 * process.
	 */
	private Integer stationId;
	/**
	 * Hash map with stations loaded yet.
	 */
	private final HashMap<Integer, Station> loadedStationsMap;
	/**
	 * The set of station ids that shall be readed.
	 */
	private final Collection<Integer> stationsToRead;
	/**
	 * The set of key-value tags of current station.
	 */
	private final HashMap<String, String> tags;
	/**
	 * The factory that will create the station.
	 */
	private final StationFactory sf;

	/**
	 * Creates new station parser.
	 *
	 * @param stationsToRead Ids of nodes that shall be read.
	 * @param sf             The station factory.
	 */
	public StationParser(Collection<Integer> stationsToRead, StationFactory sf)
	{
		this.stationsToRead = stationsToRead;
		this.sf = sf;

		tags = new HashMap<>(32);
		loadedStationsMap = new HashMap<>(stationsToRead.size());
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attr) throws SAXException
	{
		// now we have the tag attribute, that describes the station
		// try to find the name of the station, if it exists
		if (stationId != null && localName.equals(TAG_TAG))
		{
			final String key = attr.getValue(KEY_ATTR);
			if (sf.readTag(key))
				tags.put(key, attr.getValue(VALUE_ATTR));

			return;
		}

		// we found a new tag, hence read it
		if (localName.equals(NODE_TAG))
		{
			int id = parseUnsignedInt(attr.getValue("id"));
			if (stationsToRead.contains(id))
			{
				stationId = id;
				tags.put(LATITUDE_ATTR, attr.getValue(LATITUDE_ATTR));
				tags.put(LONGITUDE_ATTR, attr.getValue(LONGITUDE_ATTR));
			}
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException
	{
		// if you are in a node and it is closed, then you have already read the whole tag
		if (stationId != null && localName.equals(NODE_TAG))
		{
			final Station station = sf.createStation(tags);
			loadedStationsMap.put(stationId, station);

			tags.clear();
			stationId = null;
		}
	}

	/**
	 * Returns the map of readed stations.
	 *
	 * <b>Note that the returned map is unmodifiable as the output is shared.</b>
	 *
	 * @return Unmodifiable map with readed stations.
	 */
	public Map<Integer, Station> getStationsMap()
	{
		if (!isWholeWorkDone())
			throw new IllegalStateException("The file wasn't parsed yet.");

		return Collections.unmodifiableMap(loadedStationsMap);
	}
}
