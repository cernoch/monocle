/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input.osm;

import org.openstreetmap.partrksplitter.input.XmlParser;
import org.xml.sax.SAXException;

/**
 * Defines an XML parser that can be used only to parse one XML file. This might be
 * useful if the parser is connected with some additional information connected with
 * the specified instance or the return value is connected with the parser.
 *
 * @author Petr Ryšavý
 */
public class OneUseXMLParser extends XmlParser
{
	/**
	 * Signalizes that the parser has parsed whole file.
	 */
	private boolean wholeWorkDone;

	/**
	 * Creates new instance.
	 */
	public OneUseXMLParser()
	{
		wholeWorkDone = false;
	}

	@Override
	public void startDocument() throws SAXException
	{
		if (wholeWorkDone)
			throw new IllegalStateException("The parser was already used.");

		super.startDocument();
	}

	@Override
	public void endDocument() throws SAXException
	{
		super.endDocument(); //To change body of generated methods, choose Tools | Templates.
		wholeWorkDone = true;
	}

	/**
	 * Whether the parser has read whole document.
	 *
	 * @return {@code true} if the document was read till end, false
	 *         {@code otherwise}.
	 */
	public boolean isWholeWorkDone()
	{
		return wholeWorkDone;
	}
}
