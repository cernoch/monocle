/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Line factory that is used for loading tourist trails in Czech Republic.
 *
 * @author Petr Ryšavý
 */
public class TouristTrailLineFactory implements LineFactory
{
	/**
	 * The mapping from tourist trails properties in OSM XML to track names.
	 */
	private static final Map<String, String> TRAILS_SET = new HashMap<>(8);

	static
	{
		TRAILS_SET.put("kct_yellow", "yellow");
		TRAILS_SET.put("kct_red", "red");
		TRAILS_SET.put("kct_green", "green");
		TRAILS_SET.put("kct_blue", "blue");
	}
	/**
	 * The instance of this singleton.
	 */
	private static TouristTrailLineFactory instance = null;

	/**
	 * Don't let anybody to create instance of this class.
	 */
	private TouristTrailLineFactory()
	{
	}

	/**
	 * Retrieve the instance of this class.
	 *
	 * @return The singleton instance.
	 */
	public static TouristTrailLineFactory getInstance()
	{
		if (instance == null)
			instance = new TouristTrailLineFactory();

		return instance;
	}

	/**
	 * Checks whether there is a property with key from the {@code TRAILS_SET} array.
	 * That means checks for {@code kct_color} properties.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public String lineName(Map<String, String> values)
	{
		for (String key : TRAILS_SET.keySet())
			if (values.containsKey(key))
				return TRAILS_SET.get(key);
		return null;
	}

	/**
	 * {@inheritDoc}
	 * @return {@code true} if the key is in form {@code kct_color}.
	 */
	@Override
	public boolean readTag(String key)
	{
		return TRAILS_SET.containsKey(key);
	}

	/**
	 * Just calls the line constructor.
	 * {@inheritDoc}
	 */
	@Override
	public Line createLine(Collection<Track> tracks, String lineName)
	{
		return new Line(lineName, tracks);
	}

	/**
	 * {@inheritDoc }
	 * @return {@code true}
	 */
	@Override
	public boolean acceptedMemberRole(String role)
	{
		return true;
		//return role != null && role.isEmpty();
	}
}
