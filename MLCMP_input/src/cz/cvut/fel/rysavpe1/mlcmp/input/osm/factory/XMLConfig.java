/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory;

/**
 * Defines configuration of XML parser.
 * @author Petr Ryšavý
 */
public interface XMLConfig
{
	/**
	 * Returns the station factory.
	 * @return Station factory.
	 */
	public StationFactory getStationFactory();
	
	/**
	 * Returns the line factory.
	 * @return Line factory.
	 */
	public LineFactory getLineFactory();
}