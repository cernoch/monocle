/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.input.osm.factory;

import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import java.util.HashMap;

/**
 * Java interface that defines factory for creating stations.
 *
 * @author Petr Ryšavý
 */
public interface StationFactory
{
	/**
	 * Decides whether the given tag is interesting for the station. If true, then
	 * the tag will be assigned to the {@code values} parameter of
	 * {@code createStation} method.
	 *
	 * @param key Key name.
	 * @return {@code true} if tag is required, {@code false} otherwise.
	 */
	public boolean readTag(String key);

	/**
	 * Creates station from given parameters.
	 *
	 * @param values List of key-value options of the station.
	 * @return New station with given properties.
	 */
	public Station createStation(HashMap<String, String> values);
}