/**
 * Copyright (c) 2010 Radomír Černoch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.openstreetmap.partrksplitter.input;

import java.io.*;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Filereader, which remembers how much has been read
 *
 * @author Radomír Černoch
 */
public class CountingFileReader extends Reader
{
	private final BufferedReader in;
	private long pos = 0;
	private long tot = 0;

	/**
	 * Opens the given file with given charset.
	 *
	 * @param input The input file.
	 * @param ch    Desired charset.
	 * @throws FileNotFoundException When file don't exist.
	 * @throws IOException           An IO exception.
	 */
	public CountingFileReader(Path input, Charset ch) throws FileNotFoundException, IOException
	{
		if (ch == null)
			ch = Charset.defaultCharset();

		in = Files.newBufferedReader(input, ch);
		tot = Files.size(input);
	}

	/**
	 * Opens the given file with given charset.
	 *
	 * @param input The input file.
	 * @param ch    Desired charset.
	 * @throws FileNotFoundException When file don't exist.
	 * @throws IOException           An IO exception.
	 */
	public CountingFileReader(File input, Charset ch) throws FileNotFoundException, IOException
	{
		this(input.toPath(), ch);
	}

	/**
	 * Opens the given file with given charset.
	 *
	 * @param filename The name of input file.
	 * @param ch       Desired charset.
	 * @throws FileNotFoundException When file don't exist.
	 * @throws IOException           An IO exception.
	 */
	public CountingFileReader(String filename, Charset ch) throws FileNotFoundException, IOException
	{
		this(new File(filename), ch);
	}

	@Override
	public int read() throws IOException
	{
		int o = in.read();
		if (o >= 0)
			pos++;
		return o;
	}

	@Override
	public int read(CharBuffer target) throws IOException
	{
		int o = in.read(target);
		if (o >= 0)
			pos += o;
		return o;
	}

	@Override
	public int read(char[] cbuf, int offset, int length) throws IOException
	{
		int o = in.read(cbuf, offset, length);
		if (o >= 0)
			pos += o;
		return o;
	}

	@Override
	public int read(char[] cbuf) throws IOException
	{
		int o = in.read(cbuf);
		if (o >= 0)
			pos += o;
		return o;
	}

	/**
	 * Returns the percent value of how much was readed from file.
	 *
	 * @return The part of file already loaded.
	 */
	public double getPercentRead()
	{
		return (100.0 * pos) / tot;
	}

	@Override
	public void close() throws IOException
	{
		in.close();
	}
}
