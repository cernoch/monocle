/**
 * Copyright (c) 2010 Radomír Černoch
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
package org.openstreetmap.partrksplitter.input;

import java.io.PrintWriter;
import java.text.DecimalFormat;

/**
 * Prints status of {@link XmlParser} to the given print writer.
 *
 * @author Radomír Černoch
 */
public class Monitor extends Thread {

    private XmlParser parser = null;
    private boolean running = true;
    private static final int size = 70;
    private static final DecimalFormat format
            = new DecimalFormat();
	private final PrintWriter out;

	/**
	 * Creates new instance.
	 * @param parser Parser.
	 * @param out Output.
	 */
	@SuppressWarnings("UseOfSystemOutOrSystemErr")
    public Monitor(XmlParser parser, PrintWriter out) {
		if(out == null)
			out = new PrintWriter(System.err);
		
        this.parser = parser;
		this.out = out;
        setName("Monitor for '" + parser.getFileName() + "'");
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);
        format.setMaximumIntegerDigits(2);
        format.setMinimumIntegerDigits(2);
    }

	/**
	 * Terminates the thread.
	 */
    public void terminate() {
        running = false;
        for (int i=0; i<size+11; i++)
            out.print(" ");
        out.println();
    }

    private void printStatus(double percent) {
        if (parser == null) return;

        final long tPoz = Math.round( size * percent / 100.0 );

        out.print("[");

        for (int i=0; i<tPoz; i++)     out.print("=");
        for (long i=tPoz; i<size; i++) out.print(" ");

        out.print("] ");

        out.print(format.format(percent));
        out.print("%");
		out.println();
		out.flush();
    }

    @Override
    public void run() {
        while (running) {
            printStatus(parser.getPercentRead());
            try { sleep(200); } catch (InterruptedException ex) {}
        }
    }
}
