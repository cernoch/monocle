/**
 * Copyright (c) 2010 Radomír Černoch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.openstreetmap.partrksplitter.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

/**
 * @author Radomír Černoch
 */
public class XmlParser extends DefaultHandler
{
	private Path file;
	private CountingFileReader fileReader;
	/**
	 * The name of encoding group in declaration regular expression.
	 */
	private static final String ENCODING_GROUP = "encgrp";
	/**
	 * The XML declaration regular expression. It may sometimes accept not valid
	 * declaration, because it does not require the version number. The valid
	 * declaration is not accepted if it does not contain the encoding parameter.
	 */
	private static final String HEAD_ENCODING_REGEX = "\\<\\?xml .*encoding\\=[\"\'](?<" + ENCODING_GROUP + ">.*)[\"\'].*\\?\\>";

	/**
	 * Parses the given XML file.
	 *
	 * @param file File to read.
	 * @throws SAXException Problem with XML.
	 * @throws IOException  Problem with IO.
	 */
	public void parse(Path file) throws SAXException, IOException
	{
		this.file = file;
		XMLReader reader = XMLReaderFactory.createXMLReader();
		reader.setContentHandler(this);
		final Charset ch = getXMLCharset(file);
		fileReader = new CountingFileReader(file, ch);
		reader.setErrorHandler(this);
		reader.parse(new InputSource(fileReader));
	}

	/**
	 * Returns the percent value of how much was readed from file.
	 *
	 * @return The part of file already loaded.
	 */
	public double getPercentRead()
	{
		if (fileReader == null)
			return 0;
		return fileReader.getPercentRead();
	}

	/**
	 * Returns the file name.
	 * @return Name of loaded file.
	 */
	public String getFileName()
	{
		if (file != null)
			return file.toString();
		else
			return "XML file";
	}

	/**
	 *
	 * @param file
	 * @return
	 * @throws IOException
	 * @see http://xmlwriter.net/xml_guide/xml_declaration.shtmlm
	 */
	public Charset getXMLCharset(Path file) throws IOException
	{
		try (BufferedReader br = Files.newBufferedReader(file, Charset.defaultCharset()))
		{
			final String head = br.readLine();
			// typical head is in form
			// <?xml version="1.0" encoding="UTF-8"?>
			// hence use regex to find the encoding
			// the declaration must be placed at first line at first character
			// for details see http://xmlwriter.net/xml_guide/xml_declaration.shtml
			final Matcher mat = Pattern.compile(HEAD_ENCODING_REGEX).matcher(head);
			if (!mat.matches())
				return Charset.defaultCharset();

			// better try to chatch the exception - the method isSupported may throw also
			// an exception, hence we have to check it at all
			return Charset.forName(mat.group(ENCODING_GROUP));
		}
		catch (IllegalCharsetNameException | UnsupportedCharsetException ex)
		{
			return Charset.defaultCharset();
		}
	}
}
