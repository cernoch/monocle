/*
 */

package cz.cvut.fel.rysavpe1.mlcmp.output;

import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class PlainTextGraphWriterTest {

    public PlainTextGraphWriterTest() {
    }

	@Test
	public void testSomeMethod() throws FileNotFoundException, IOException, GraphReadingException
	{
		BufferedReader in = new BufferedReader(new FileReader("test/graph1.in"));
		final OutputStreamWriter serr = new OutputStreamWriter(System.err);
		PlainTextGraphReader instance = new PlainTextGraphReader(serr);
		MLCMP result = instance.readGraph(in);
		PlainTextGraphWriter out = new PlainTextGraphWriter(serr);
		out.writeGraph(serr, result);
		serr.flush();
	}

}