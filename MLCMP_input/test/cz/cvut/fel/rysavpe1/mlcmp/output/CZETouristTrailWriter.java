/*
 */

package cz.cvut.fel.rysavpe1.mlcmp.output;

import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.XMLTouristTrailGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.xml.sax.SAXException;

/**
 *
 * @author Petr Ryšavý
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class CZETouristTrailWriter {

    public CZETouristTrailWriter() {
    }

	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void testSomeMethod() throws FileNotFoundException, IOException, GraphReadingException, SAXException
	{
		//BufferedReader in = new BufferedReader(new FileReader("test/graph1.in"));
		final OutputStreamWriter serr = new OutputStreamWriter(System.err);
		Path file = Paths.get("C:", "Users", "Petr", "Documents", "BP", "czech-republic-latest.osm");
		XMLTouristTrailGraphReader instance = new XMLTouristTrailGraphReader(serr);
		MLCMP result = instance.readGraph(file);
		
		BufferedWriter outWriter = Files.newBufferedWriter(Paths.get("C:", "Users", "Petr", "Documents", "BP", "aTouristTrailsNC.in"), Charset.forName("utf-8"));
		PlainTextGraphWriter out = new PlainTextGraphWriter(serr);
		out.writeGraph(outWriter, result);
		outWriter.flush();
		outWriter.close();
	}

}