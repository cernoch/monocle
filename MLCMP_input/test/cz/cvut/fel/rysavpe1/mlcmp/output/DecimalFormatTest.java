/*
 */

package cz.cvut.fel.rysavpe1.mlcmp.output;

import java.text.DecimalFormat;
import java.util.Locale;
import org.junit.Test;

/**
 *
 * @author Petr Ryšavý
 */
public class DecimalFormatTest
{
	
	
	public DecimalFormatTest()
	{
		
	}
	
	@Test
	public void	test()
	{
		DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance(Locale.US);
		System.err.println("max dig "+format.getMaximumFractionDigits());
		format.setMaximumFractionDigits(Integer.MAX_VALUE);
		System.out.println(format.format(1.0/3));
		System.out.println(format.format(1.0/2));
	}
}
