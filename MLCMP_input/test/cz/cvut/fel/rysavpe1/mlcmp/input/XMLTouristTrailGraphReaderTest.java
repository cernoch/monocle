/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.input;

import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Petr Ryšavý
 */
public class XMLTouristTrailGraphReaderTest
{
	public XMLTouristTrailGraphReaderTest()
	{
	}

	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void testReadGraph() throws Exception
	{
		System.out.println("readGraph");
		Path file = Paths.get("C:", "Users", "Petr", "Documents", "BP", "map.osm");
		System.out.println("file:"+file);
		System.out.println("file:"+file.toFile());
		XMLTouristTrailGraphReader instance = new XMLTouristTrailGraphReader(new OutputStreamWriter(System.err));
		MLCMP result = instance.readGraph(file);
		//result.printToStream(new OutputStreamWriter(System.out));
		
	}
}