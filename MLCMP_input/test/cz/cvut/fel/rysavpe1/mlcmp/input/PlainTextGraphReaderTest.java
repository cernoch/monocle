package cz.cvut.fel.rysavpe1.mlcmp.input;

import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Petr Ryšavý
 */
public class PlainTextGraphReaderTest
{
	public PlainTextGraphReaderTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of readGraph method, of class PlainTextGraphReader.
	 */
	@Test
	public void testReadGraph() throws Exception
	{
		BufferedReader in = new BufferedReader(new FileReader("test/graph1.in"));
		PlainTextGraphReader instance = new PlainTextGraphReader(new OutputStreamWriter(System.err));
		MLCMP result = instance.readGraph(in);
		result.printToStream(new OutputStreamWriter(System.out));
	}
}