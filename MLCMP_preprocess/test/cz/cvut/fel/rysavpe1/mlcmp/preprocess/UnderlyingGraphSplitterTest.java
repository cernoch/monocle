/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.preprocess;

import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
public class UnderlyingGraphSplitterTest
{
	public UnderlyingGraphSplitterTest()
	{
	}

	@Test
	public void testSplitProblem() throws FileNotFoundException, IOException, GraphReadingException
	{
		BufferedReader in = new BufferedReader(new FileReader("test/graph3.in"));
		PlainTextGraphReader instance = new PlainTextGraphReader(new OutputStreamWriter(System.err));
		System.err.println("Reading");
		MLCMP result = instance.readGraph(in);
		System.err.println("Reading done");
		//EdgeShortener.contractTracks(result);
		//System.err.println("Contracting");
		//result.printToStream(new OutputStreamWriter(System.out));
		System.out.println("splitProblem");
		MLCMP[] arr = UnderlyingGraphSplitter.splitProblem(result);
		for(MLCMP m : arr)
			m.printToStream(new OutputStreamWriter(System.out));
	}
}