/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.preprocess;

import cz.cvut.fel.rysavpe1.mlcmp.input.PlainTextGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Petr Ryšavý
 */
public class EdgeShortenerTest
{
	public EdgeShortenerTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of contractTracks method, of class EdgeShortener.
	 */
	@Test
	public void testContractTracks() throws IOException, GraphReadingException
	{
		BufferedReader in = new BufferedReader(new FileReader("test/graph2.in"));
		PlainTextGraphReader instance = new PlainTextGraphReader(new OutputStreamWriter(System.err));
		System.err.println("Reading");
		MLCMP result = instance.readGraph(in);
		System.err.println("Reading done");
		EdgeShortener.contractTracks(result);
		System.err.println("Contracting");
		result.printToStream(new OutputStreamWriter(System.out));
	}
}