/*
 */

package cz.cvut.fel.rysavpe1.mlcmp.preprocess;

import cz.cvut.fel.rysavpe1.mlcmp.input.XMLTouristTrailGraphReader;
import cz.cvut.fel.rysavpe1.mlcmp.input.exception.GraphReadingException;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;

/**
 *
 * @author Petr Ryšavý
 */
public class CZETouristTrailTest
{
	
	@Ignore("Replace the path to the graph with correct one.")
	@Test
	public void runTest() throws IOException, GraphReadingException, SAXException
	{
		System.out.println("readGraph");
		Path file = Paths.get("C:", "Users", "Petr", "Documents", "BP", "czech-republic-latest.osm");
		XMLTouristTrailGraphReader instance = new XMLTouristTrailGraphReader(new OutputStreamWriter(System.err));
		MLCMP result = instance.readGraph(file);
		final int origTracks = result.getEdgesNum();
		final int origStations = result.getVerticesNum();
		MLCMP[] probArr = UnderlyingGraphSplitter.splitProblem(result);
		int newTracks = 0;
		int newStations = 0;
		
		for(MLCMP problem : probArr)
		{
			EdgeShortener.contractTracks(problem);
			//problem.printToStream(new OutputStreamWriter(System.out));
			newTracks += problem.getEdgesNum();
			newStations += problem.getVerticesNum();
		}
		
		System.err.println("----------------------------------------------");
		System.err.println("----------------------------------------------");
		System.err.println("problems : "+probArr.length);
		System.err.println("              vertices     edges");
		System.err.println("originallly   "+origStations+"   "+origTracks);
		System.err.println("now           "+newStations+"   "+newTracks);
		System.err.println("----------------------------------------------");
		System.err.println("----------------------------------------------");
	}
}

	