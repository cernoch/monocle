/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.preprocess;

import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;

/**
 *
 * @author Petr Ryšavý
 */
public class ContractionEvent // we do not use EventObject as parent as EventObject is designed for GUI events
{
	private Station removedStation;
	private Track firstTrack;
	private Track secondTrack;
	private Track newTrack;

	public ContractionEvent(Station removedStation, Track firstTrack, Track secondTrack, Track newTrack)
	{
		final boolean firstToSecond = firstTrack.isIncidentWith(newTrack.getVertex1());

		this.removedStation = removedStation;
		if (firstToSecond)
		{
			this.firstTrack = firstTrack;
			this.secondTrack = secondTrack;
		}
		else
		{
			this.firstTrack = firstTrack;
			this.secondTrack = secondTrack;
		}
		this.newTrack = newTrack;
	}

	public Station getRemovedStation()
	{
		return removedStation;
	}

	public Track getFirstTrack()
	{
		return firstTrack;
	}

	public Track getSecondTrack()
	{
		return secondTrack;
	}

	public Track getNewTrack()
	{
		return newTrack;
	}
}
