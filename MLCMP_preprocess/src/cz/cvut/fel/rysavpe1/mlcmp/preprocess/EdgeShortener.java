/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.preprocess;

import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import java.util.Iterator;

/**
 * This utility is used to contract the edges.
 *
 * @author Petr Ryšavý
 */
public class EdgeShortener
{
	/**
	 * Don't let anybody to create the class instance.
	 */
	public EdgeShortener()
	{
	}

	/**
	 * Contracts the tracks in the given problem.
	 *
	 * @param problem The problem where the tracks shall be contracted.
	 * @see
	 * cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP#canBeStationContracted(cz.cvut.fel.rysavpe1.mlcmp.model.Station)
	 * @see
	 * cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP#contractStationIfPossible(cz.cvut.fel.rysavpe1.mlcmp.model.Station)
	 */
	public static void contractTracks(MLCMP problem)
	{
		Station[] stations = problem.getVertices();
		for (Station s : stations)
			problem.contractStationIfPossible(s);
	}

	/**
	 * Contracts the tracks in the given problem.
	 *
	 * @param problem  The problem where the tracks shall be contracted.
	 * @param listener The listener that will be informed about the changes. May be
	 *                 {@code null} as well.
	 * @see
	 * cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP#canBeStationContracted(cz.cvut.fel.rysavpe1.mlcmp.model.Station)
	 * @see
	 * cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP#contractStationIfPossible(cz.cvut.fel.rysavpe1.mlcmp.model.Station)
	 */
	public static void contractTracks(MLCMP problem, GraphContractionListener listener)
	{
		if (listener == null)
		{
			contractTracks(problem);
			return;
		}

		Station[] stations = problem.getVertices();
		for (Station s : stations)
		{
			if(!problem.canBeStationContracted(s))
				continue;
			
			Iterator<Track> it = s.incidenTracksIterator();
			final Track t1 = it.next();
			final Track t2 = it.next();
			Track t = problem.contractStation(s);

			if (t != null)
			{
				listener.edgeContracted(new ContractionEvent(s, t1, t2, t));
			}
		}
	}
}
