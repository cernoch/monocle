/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.preprocess;

import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.UnderlyingGraph;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * This utility is able to split the MLCMP problem into independent subproblems.
 *
 * This means that the underlying graph is splitted into connected components. If
 * there is a station without incident edge, the station is removed from the problem.
 * This can by done by naming a node without using it in any line. The edges don't
 * have this problem as they can be defined only within a line.
 *
 * @author Petr Ryšavý
 */
public class UnderlyingGraphSplitter
{
	/**
	 * Don't let anybody to create the class instance.
	 */
	private UnderlyingGraphSplitter()
	{
	}

	/**
	 * Splits the problem into set of independent subproblems. The problems are
	 * independent as they form two distinct connected compoents.
	 *
	 * @param problem The problem to split.
	 * @return The array of new problems.
	 */
	public static MLCMP[] splitProblem(MLCMP problem)
	{
		final int n = problem.getVerticesNum();
		final ArrayList<Station> currStations = new ArrayList<>(n);
		final HashSet<Track> currTracks = new HashSet<>(problem.getEdgesNum());
		final HashSet<Station> visited = new HashSet<>(n);
		final Station[] stations = problem.getVertices();
		final Line[] lines = problem.getLines();
		final LinkedList<MLCMP> problemList = new LinkedList<>();

		ArrayList<LinkedList<Track>> currLinesAL = new ArrayList<>();
		for (int j = 0; j < lines.length; j++)
			currLinesAL.add(new LinkedList<Track>());
		LinkedList<Line> currLines = new LinkedList<>();

		for (int i = 0; i < n; i++)
			// this is the only place to check whether a station is not connected
			// ady other
			if (!visited.contains(stations[i]) && problem.getDegree(stations[i]) != 0)
			{
				currStations.clear();
				currTracks.clear();
				for (LinkedList<Track> trLL : currLinesAL)
					trLL.clear();
				currLines.clear();

				BFS(stations[i], problem, currStations, currTracks, visited);

				// now the problem is encoded by the sets currStations and currTracks
				// we have to only reconstruct the lines
				for (Track t : currTracks)
					for (int j = 0; j < lines.length; j++)
						if (lines[j].containsTrack(t))
							currLinesAL.get(j).add(t);

				for (int j = 0; j < lines.length; j++)
					if (!currLinesAL.get(j).isEmpty())
						currLines.add(new Line(lines[j].getName(), currLinesAL.get(j)));

				problemList.add(new MLCMP(new UnderlyingGraph(currStations, currTracks), currLines));
			}

		MLCMP[] problems = new MLCMP[problemList.size()];
		return problemList.toArray(problems);
	}

	/**
	 * Searchs the graph in order to find strictly connected components.
	 *
	 * @param start       Start station.
	 * @param problem     Problem to search.
	 * @param stationList List in that will be stored the list of stations of the
	 *                    current connected component.
	 * @param trackList   List where will be stored the list of tracks of the current
	 *                    connected component.
	 * @param visited     The array with visited nodes. All nodes in same connected
	 *                    component as {@code start} must be marked as unvisited,
	 *                    otherwise the method won't work.
	 */
	private static void BFS(Station start, MLCMP problem, List<Station> stationList,
							Set<Track> trackList, HashSet<Station> visited)
	{
		final LinkedList<Station> queue = new LinkedList<>();
		queue.add(start);
		visited.add(start);

		while (!queue.isEmpty())
		{
			final Station curr = queue.removeFirst();
			stationList.add(curr);

			for (Track t : problem.getIncidentEdges(curr, false))
				// we have to add the track if the track was not searched yet
				if (!trackList.contains(t))
				{
					trackList.add(t);

					final Station otherVertex = t.getOtherVertex(curr);
					// the track was not searched
					if (!visited.contains(otherVertex))
					{
						visited.add(otherVertex);
						queue.add(otherVertex);
					}
				}
		}
	}
}
