/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
public class TrackTest
{
	public TrackTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of getVertex1 method, of class Track.
	 */
//	@Test
//	public void testGetVertex1()
//	{
////		System.out.println("getVertex1");
////		Track instance = null;
////		Station expResult = null;
////		Station result = instance.getVertex1();
////		assertEquals(expResult, result);
////		// TODO review the generated test code and remove the default call to fail.
////		fail("The test case is a prototype.");
//	}
//
//	/**
//	 * Test of getVertex2 method, of class Track.
//	 */
//	@Test
//	public void testGetVertex2()
//	{
////		System.out.println("getVertex2");
////		Track instance = null;
////		Station expResult = null;
////		Station result = instance.getVertex2();
////		assertEquals(expResult, result);
////		// TODO review the generated test code and remove the default call to fail.
////		fail("The test case is a prototype.");
//	}
//
//	/**
//	 * Test of isIncidentWith method, of class Track.
//	 */
//	@Test
//	public void testIsIncidentWith()
//	{
////		System.out.println("isIncidentWith");
////		Vertex v = null;
////		Track instance = null;
////		boolean expResult = false;
////		boolean result = instance.isIncidentWith(v);
////		assertEquals(expResult, result);
////		// TODO review the generated test code and remove the default call to fail.
////		fail("The test case is a prototype.");
//	}
//
//	/**
//	 * Test of getOtherVertex method, of class Track.
//	 */
//	@Test
//	public void testGetOtherVertex()
//	{
////		System.out.println("getOtherVertex");
////		Vertex v = null;
////		Track instance = null;
////		Vertex expResult = null;
////		Vertex result = instance.getOtherVertex(v);
////		assertEquals(expResult, result);
////		// TODO review the generated test code and remove the default call to fail.
////		fail("The test case is a prototype.");
//	}
//
//	/**
//	 * Test of getValue method, of class Track.
//	 */
//	@Test
//	public void testGetValue()
//	{
////		System.out.println("getValue");
////		Track instance = null;
////		Void expResult = null;
////		Void result = instance.getValue();
////		assertEquals(expResult, result);
////		// TODO review the generated test code and remove the default call to fail.
////		fail("The test case is a prototype.");
//	}

	/**
	 * Test of getVertex1Angle method, of class Track.
	 */
	@Test
	public void testGetuAngle()
	{
		System.out.println("getuAngle");
		
		Station u = new Station(0, 0);
		
		Station v = new Station(0, 1);
		Track instance = new Track(u, v);
		double expResult = 2*Math.PI;
		double result = instance.getVertex1Angle();
		assertEquals(expResult, result, 0.0);
		
		v = new Station(1, 1);
		instance = new Track(u, v);
		expResult = 7.0/4*Math.PI;
		result = instance.getVertex1Angle();
		assertEquals(expResult, result, 10e-15);
		
		v = new Station(-1, 0);
		instance = new Track(u, v);
		expResult = 1.0/2*Math.PI;
		result = instance.getVertex1Angle();
		assertEquals(expResult, result, 10e-15);
		
		v = new Station(0, -1);
		instance = new Track(u, v);
		expResult = Math.PI;
		result = instance.getVertex1Angle();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getVertex2Angle method, of class Track.
	 */
	@Test
	public void testGetvAngle()
	{
		System.out.println("getvAngle");
		
		Station u = new Station(0, 0);
		
		Station v = new Station(0, 1);
		Track instance = new Track(u, v);
		double expResult = Math.PI;
		double result = instance.getVertex2Angle();
		assertEquals(expResult, result, 0.0);
		
		v = new Station(1, 1);
		instance = new Track(u, v);
		expResult = 3.0/4*Math.PI;
		result = instance.getVertex2Angle();
		assertEquals(expResult, result, 10e-15);
		
		v = new Station(-1, 0);
		instance = new Track(u, v);
		expResult = 3.0/2*Math.PI;
		result = instance.getVertex2Angle();
		assertEquals(expResult, result, 10e-15);
		
		v = new Station(0, -1);
		instance = new Track(u, v);
		expResult = 2*Math.PI;
		result = instance.getVertex2Angle();
		assertEquals(expResult, result, 0.0);
	}
	
	@Test
	public void testEquals()
	{
		Station u = new Station(0, 0);
		Station v = new Station(1, 1);
		Track t1 = new Track(u, v);
		Track t2 = new Track(v, u);
		assertTrue(t1.equals(t2));
	}
}
