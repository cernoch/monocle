/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import java.util.Arrays;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Rysavy
 */
public class SimpleDirectedGraphTest
{
	static SimpleDirectedGraph<String> graph;
	
	public SimpleDirectedGraphTest()
	{
	}

	@BeforeClass
	public static void before()
	{
		Vertex<String> a = new SimpleVertex<>("a");
		Vertex<String> b = new SimpleVertex<>("b");
		Vertex<String> c = new SimpleVertex<>("c");
		Vertex<String> d = new SimpleVertex<>("d");
		Vertex<String> e = new SimpleVertex<>("e");
		Vertex<String> f = new SimpleVertex<>("f");
		Vertex<String> g = new SimpleVertex<>("g");

		graph = new SimpleDirectedGraph<>();
		graph.addVertex(a);
		graph.addVertex(b);
		graph.addVertex(c);
		graph.addVertex(d);
		graph.addVertex(e);
		graph.addVertex(f);
		graph.addVertex(g);

		graph.addEdge(a, d);
		graph.addEdge(a, b);
		graph.addEdge(d, c);
		graph.addEdge(b, c);
		graph.addEdge(e, f);
	}

	@Test
	public void testOutDegree()
	{
		assertEquals(2, graph.getOutDegree(new SimpleVertex<>("a")));
		assertEquals(1, graph.getOutDegree(new SimpleVertex<>("b")));
		assertEquals(0, graph.getOutDegree(new SimpleVertex<>("c")));
		assertEquals(0, graph.getOutDegree(new SimpleVertex<>("g")));
	}
	
	@Test
	public void getIncidentVertices()
	{
		System.err.println(Arrays.toString(graph.getIncidentVertices(new SimpleVertex<>("a"))));
		System.err.println(Arrays.toString(graph.getIncidentVertices(new SimpleVertex<>("b"))));
		System.err.println(Arrays.toString(graph.getIncidentVertices(new SimpleVertex<>("c"))));
	}
}