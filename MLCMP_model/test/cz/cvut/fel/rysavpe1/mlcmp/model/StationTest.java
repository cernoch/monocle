/*
 */
package cz.cvut.fel.rysavpe1.mlcmp.model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Ryšavý
 */
public class StationTest
{
	public StationTest()
	{
	}

	@Test
	public void testIncidentTracks()
	{
		Station s0 = new Station("s0", 0, 0);
		Station s1 = new Station("s1", 0, 1);
		Station s2 = new Station("s2", 1, 0);
		Station s3 = new Station("s3", 0, -1);
		Station s4 = new Station("s4", -1, 0);
		final Track t1 = new Track(s0, s1);

		s0.addTrack(t1);
		final Track t2 = new Track(s0, s2);
		s0.addTrack(t2);
		final Track t3 = new Track(s0, s3);
		s0.addTrack(t3);
		final Track t4 = new Track(s0, s4);
		s0.addTrack(t4);

		Track[] exp = new Track[]
		{
			t1, t2,t3,t4
		};
		assertArrayEquals(exp, s0.getIncidentTracks(true));
	}
	
	@Test
	public void testIncidentPivot()
	{
		Station s0 = new Station("s0", 0, 0);
		Station s1 = new Station("s1", 0, 1);
		Station s2 = new Station("s2", 1, 0);
		Station s3 = new Station("s3", 0, -1);
		Station s4 = new Station("s4", -1, 0);
		final Track t1 = new Track(s0, s1);

		s0.addTrack(t1);
		final Track t2 = new Track(s0, s2);
		s0.addTrack(t2);
		final Track t3 = new Track(s0, s3);
		s0.addTrack(t3);
		final Track t4 = new Track(s0, s4);
		s0.addTrack(t4);

		Track[] exp = new Track[]
		{
			t2,t3,t4, t1
		};
		assertArrayEquals(exp, s0.getIncidentTracks(t2));
	}
	@Test
	public void testIncidentPivot2()
	{
		Station s0 = new Station("s0", 0, 0);
		Station s1 = new Station("s1", 0, 1);
		Station s2 = new Station("s2", 1, 0);
		Station s3 = new Station("s3", 0, -1);
		Station s4 = new Station("s4", -1, 0);
		final Track t1 = new Track(s0, s1);

		s0.addTrack(t1);
		final Track t2 = new Track(s0, s2);
		s0.addTrack(t2);
		final Track t3 = new Track(s0, s3);
		s0.addTrack(t3);
		final Track t4 = new Track(s0, s4);
		s0.addTrack(t4);

		Track[] exp = new Track[]
		{
			t4, t1,t2,t3
		};
		assertArrayEquals(exp, s0.getIncidentTracks(t4));
	}
}
