/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.utilities;

import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Utilities library.
 *
 * @author Petr Ryšavý
 */
public final class ModelUtilities
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private ModelUtilities()
	{
	}

	/**
	 * Calculates eucledian distance of two stations.
	 *
	 * @param v1 First station.
	 * @param v2 Second station.
	 * @return The eucleidian distance.
	 */
	public static double stationDistance(Station v1, Station v2)
	{
		return Math.hypot(v1.getX() - v2.getX(), v1.getY() - v2.getY());
	}

	/**
	 * Returs the set of stations incident to the given set of tracks.
	 *
	 * @param tracks The tracks.
	 * @return Set of stations such that there is at least one track in
	 *         {@code tracks} whose enpoint is the particular station.
	 */
	public static Set<Station> incidentStations(Collection<Track> tracks)
	{
		Set<Station> set = new HashSet<>(tracks.size() * 2);
		for(Track t : tracks)
		{
			set.add(t.getVertex1());
			set.add(t.getVertex2());
		}
		return set;
	}
}