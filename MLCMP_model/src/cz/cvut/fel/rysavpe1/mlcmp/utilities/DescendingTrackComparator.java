/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.utilities;

import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalEdgeException;
import java.util.Comparator;

/**
 * Comparator which sorts the edges in descending order of angle at a station. The
 * tracks are sorted clockwise.
 *
 * @author Petr Ryšavý
 */
public final class DescendingTrackComparator implements Comparator<Track>
{
	/**
	 * The station at which the tracks are sorted. The compared tracks must be
	 * incident with this vertex.
	 */
	private final Station s;

	/**
	 * Creates new comparator.
	 *
	 * @param s The station at which we sort the tracks.
	 */
	public DescendingTrackComparator(Station s)
	{
		this.s = s;
	}

	@Override
	public int compare(Track t1, Track t2)
	{
		final double t1Angle = t1.getVertexAngle(s);
		final double t2Angle = t2.getVertexAngle(s);

		if (t1Angle == t2Angle)
			throw new IllegalEdgeException("Two edges cannot leave a vertex in the same direction.");

		// negative value if t1 shall be before t2
		return t1Angle > t2Angle ? -1 : 1;
	}
}
