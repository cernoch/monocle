/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.utilities;

import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Library class that allows to map track to list of common subpaths through this
 * track.
 *
 * @author Petr Ryšavý
 */
public final class TrackMapper
{
	/**
	 * Don't let anybody to create class instance.
	 */
	private TrackMapper()
	{
	}

	/**
	 * Maps the tracks to the T.
	 *
	 * @param <T> The iterable of tracks to that the tracks will be matched. The
	 *            {@code CommonSubpath} is good example of a such iterable.
	 * @param set The set of iterables to that will be mapped.
	 * @return Map from track to the iterables.
	 */
	public static <T extends Iterable<? extends Track>> Map<Track, List<T>> trackMap(Set<T> set)
	{
		// assume that most of the set values have exactly one track included -
		// hash table won't grow many times
		final HashMap<Track, List<T>> map = new HashMap<>(set.size());
		for (T it : set)
			for (Track t : it)
				if (map.containsKey(t))
					map.get(t).add(it);
				else
					map.put(t, newList(it));
		return map;
	}

	/**
	 * Returns new list of given type.
	 *
	 * @param <T>   The new list.
	 * @param value The value to store into the list.
	 * @return A new list with given value.
	 */
	private static <T extends Object & Iterable<? extends Track>> List<T> newList(T value)
	{
		final LinkedList<T> ll = new LinkedList<>();
		ll.add(value);
		return ll;
	}
}