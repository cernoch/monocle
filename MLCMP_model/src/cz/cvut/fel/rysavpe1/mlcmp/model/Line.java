/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalEdgeException;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;

/**
 * The object that represents the line as set of edges.
 *
 * @author Petr Ryšavý
 */
public class Line implements Iterable<Track>
{
	/**
	 * The name of the line.
	 */
	private final String name;
	/**
	 * The set of tracks in this line.
	 */
	private final HashSet<Track> tracks;

	/**
	 * Creates new line in the graph.
	 *
	 * @param name   The name of the line.
	 * @param tracks The tracks in this line.
	 */
	public Line(String name, Collection<Track> tracks)
	{
		this.name = name;
		if(name == null)
			throw new NullPointerException("The line name cannot be null");
		if (tracks != null)
			this.tracks = new HashSet<>(tracks);
		else
			this.tracks = new HashSet<>();
	}

	/**
	 * Returns the name of the line.
	 *
	 * @return The name of the line.
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Decides whether a given station is terminal station of this line.
	 *
	 * This method is preferred to calculating the station degree and comparing it
	 * with one, because this method may finish earlier.
	 *
	 * @param s Station to probably be terminal.
	 * @return Whether {@code s} is terminal.
	 */
	public boolean isEndpoint(Station s)
	{
		int count = 0;
		for (Track t : new IteratorWrapper<>(s.incidenTracksIterator()))
			if (tracks.contains(t))
			{
				count++;
				if (count == 2)
					return false;
			}
		if (count == 1)
			return true;
		return false;
	}

	/**
	 * Decides whether a given station is inline station of this line. The station is
	 * inline iff it's degree with respect to this line is equals to 2.
	 *
	 * This method is preferred to calculating the station degree and comparing it
	 * with two, because this method may finish earlier.
	 *
	 * @param s Station to probably be inline.
	 * @return Whether {@code s} is inline.
	 */
	public boolean isInline(Station s)
	{
		int count = 0;
		for (Track t : new IteratorWrapper<>(s.incidenTracksIterator()))
			if (tracks.contains(t))
			{
				count++;
				if (count == 3)
					return false;
			}
		if (count == 2)
			return true;
		return false;
	}

	/**
	 * Calculates the degree of a station with respect to this line.
	 *
	 * @param s Station at which we shall the degree calculate.
	 * @return The {@code deg_l s}.
	 */
	public int getStationDegree(Station s)
	{
		int count = 0;
		for (Track t : new IteratorWrapper<>(s.incidenTracksIterator()))
			if (tracks.contains(t))
				count++;
		return count;
	}

	/**
	 * Adds a track to the line.
	 *
	 * @param t The track to add.
	 */
	protected void addTrack(Track t)
	{
		if (tracks.contains(t))
			throw new IllegalEdgeException("The edge already exists.");

		tracks.add(t);
	}

	/**
	 * Removes the track from the line.
	 *
	 * @param t Track to remove.
	 */
	protected void removeTrack(Track t)
	{
		if (!tracks.contains(t))
			throw new IllegalEdgeException("The edge to remove doesn't exist in the graph.");

		tracks.remove(t);
	}

	/**
	 * Decides whether this line contains the specified track.
	 *
	 * @param t The track which can be possibly part of this line.
	 * @return Whether this line contains this track.
	 */
	public boolean containsTrack(Track t)
	{
		return tracks.contains(t);
	}

	@Override
	public Iterator<Track> iterator()
	{
		return tracks.iterator();
	}

	/**
	 * Finds out whether this line is an empty graph.
	 *
	 * @return True of this line doesn't contain any track.
	 */
	public boolean isEmpty()
	{
		return tracks.isEmpty();
	}

	@Override
	public int hashCode()
	{
		return Objects.hashCode(this.name);
	}

	/**
	 * Two lines are equal if they have the same name and contain the same set of
	 * tracks.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		// this enhances the speed if lines are of the same instance
		if (this == obj)
			return true;
		final Line other = (Line) obj;
		if (!Objects.equals(this.name, other.name))
			return false;
		if (!Objects.equals(this.tracks, other.tracks))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "Line (" + name + ") with " + tracks.size() + " tracks";
	}

	/**
	 * Prints the line to given output.
	 *
	 * @param out The output.
	 * @throws IOException Exception thrown by the writer {@code out}.
	 */
	public void printToStream(Writer out) throws IOException
	{
		out.write("Line : ");
		out.write(name);
		out.write('\n');
		out.write(Integer.toString(tracks.size()));
		out.write(" tracks\n\nTracks:\n");

		for (Track t : tracks)
		{
			out.write(t.toString());
			out.write('\n');
		}
		out.flush();
	}
}
