/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.Edge;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.MutableGraph;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.UndirectedEdge;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.UndirectedGraph;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.Vertex;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalEdgeException;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalVertexException;
import cz.cvut.fel.rysavpe1.utilities.ReferencedHashSet;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**
 * The underlying graph.
 *
 * @author Petr Ryšavý
 */
public class UnderlyingGraph extends UndirectedGraph<String, Void> implements MutableGraph<String, Void>
{
	/**
	 * The set of tracks in the graph.
	 */
	private ReferencedHashSet<Track> tracks;
	/**
	 * The set of stations in the graph.
	 */
	private ReferencedHashSet<Station> stations;

	/**
	 * Creates new underlying graph object.
	 */
	public UnderlyingGraph()
	{
		tracks = new ReferencedHashSet<>();
		stations = new ReferencedHashSet<>();
	}

	/**
	 * Crates new underlying graph object with the given stations and tracks. The
	 * stations are modified, hence they cannot be longer used in any other graph.
	 *
	 * @param stations The stations to be in the graph.
	 * @param tracks   The tracks in the graph.
	 */
	public UnderlyingGraph(Collection<Station> stations, Collection<Track> tracks)
	{
		this();

		for (Station s : stations)
		{
			s.clearTrackList();
			addVertex(s);
		}
		for (Track t : tracks)
			addEdge(t);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Track[] getEdges()
	{
		Track[] arr = new Track[tracks.size()];
		return  tracks.toArray(arr);
	}

	@Override
	public Track[] getIncidentEdges(Vertex<String> v)
	{
		if (!(v instanceof Station))
			throw new IllegalArgumentException("The vertex must be a station.");

		return ((Station) v).getIncidentTracks(false);
	}

	/**
	 * Creates an array of edges incident with given station.
	 *
	 * @param v               The station with that the edges shall be incident.
	 * @param guaranteeSorted If {@code true}, than the returned array will be
	 *                        sorted, otherwise the tracks can be sorted randomly.
	 * @return The array with incident edges.
	 */
	public Track[] getIncidentEdges(Station v, boolean guaranteeSorted)
	{
		return v.getIncidentTracks(guaranteeSorted);
	}

	@Override
	public Track randomEdge()
	{
		return getEdges()[((int) Math.random() * tracks.size())];
	}

	@Override
	public Station[] getVertices()
	{
		Station[] stationsA = new Station[stations.size()];
		return stations.toArray(stationsA);
	}

	@Override
	public Station randomVertex()
	{
		return getVertices()[((int) Math.random() * stations.size())];
	}

	@Override
	public final void addEdge(Edge<Void> e)
	{
		if (!(e instanceof Track))
			throw new IllegalArgumentException("The edge must be instance of Track.");
		final Track t = (Track) e;

		final Station u = t.getVertex1();
		final Station v = t.getVertex2();
		if (stations.getStoredRefference(u) != u || stations.getStoredRefference(v) != v)
			throw new IllegalEdgeException("The added edge must be between two vertices of the graph: "+e);

		// check whether the edge already exists or there is already an edge with same
		// direction - that would be problem as the edges won't be comparable

		// but checking the angle already does the Station
		for (Track tx : u.getIncidentTracks(false))
			if (tx.getOtherVertex(u).equals(v))
				throw new IllegalEdgeException("The edge already exists: "+tx);

		// everything seems to be OK, then add the edge
		tracks.add(t);
		u.addTrack(t);
		v.addTrack(t);
	}

	@Override
	public void removeEdge(Edge<Void> e)
	{
		if (!(e instanceof Track))
			throw new IllegalArgumentException("The edge must be instance of Track.");
		final Track t = (Track) e;

		if (!tracks.contains(t))
			throw new IllegalEdgeException("The edge to remove doesn't exist in the graph.");

		// everything is OK, then remove the edge
		final Station u = t.getVertex1();
		final Station v = t.getVertex2();
		tracks.remove(t);
		u.removeTrack(t);
		v.removeTrack(t);
	}

	@Override
	public final void addVertex(Vertex<String> v)
	{
		if (!(v instanceof Station))
			throw new IllegalArgumentException("The edge must be instance of Station.");
		Station s = (Station) v;

		if (stations.contains(s))
			throw new IllegalVertexException("The vertex already exists.");

		stations.add(s);
	}

	@Override
	public void removeVertex(Vertex<String> v)
	{
		if (!(v instanceof Station))
			throw new IllegalArgumentException("The edge must be instance of Station.");
		Station s = (Station) v;

		if (!stations.contains(s))
			throw new IllegalEdgeException("The vertex to remove must exist.");

		stations.remove(s);
		for (Track t : s.getIncidentTracks(false))
		{
			t.getOtherVertex(s).removeTrack(t);
			tracks.remove(t);
		}
	}

	@Override
	public MutableGraph<String, Void> inducedGraph(Collection<Vertex<String>> vertices)
	{
		UnderlyingGraph inducedG = new UnderlyingGraph();
		HashMap<Station, Station> oldToNewStation = new HashMap<>((int) (vertices.size() * 1.5));

		// at first check whether the input is valid and clone all the stations
		// also note that the new stations must be copy of the original in order to
		// have the graph encapsulated
		for (Vertex<String> v : vertices)
		{
			if (!(v instanceof Station))
				throw new IllegalArgumentException("Underlying graph can consist only from stations.");
			final Station s = (Station) v;
			if (!stations.contains(s))
				throw new IllegalVertexException("The vertex is not part of the graph.");

			final Station newS = new Station(s.getValue(), s.getX(), s.getY());
			inducedG.stations.add(newS);
			oldToNewStation.put(s, newS);
		}

		HashSet<Vertex> doneVertices = new HashSet<>();
		// more effective than searching all the edges will be the BFS
		for (Vertex<String> ver : vertices)
		{
			final Station s = (Station) ver;
			doneVertices.add(s);

			// if the track goes between two vertices of the vertices set and also
			// we haven't added it already, add a copy of the track
			Track[] incidentTracks = s.getIncidentTracks(false);
			for (Track t : incidentTracks)
			{
				final Vertex otherVertex = t.getOtherVertex(s);
				if (vertices.contains(otherVertex) && !doneVertices.contains(otherVertex))
				{
					final Station u = oldToNewStation.get(t.getVertex1());
					final Station v = oldToNewStation.get(t.getVertex2());
					inducedG.tracks.add(new Track(u, v, t.getVertex1Angle(), t.getVertex2Angle()));
					u.addTrack(t);
					v.addTrack(t);
				}
			}
		}

		return inducedG;
	}

	/**
	 * Decides whether the graph contains given station. Two stations are equall if
	 * they are lying on same position.
	 *
	 * @param s The station to search.
	 * @return True if there is a station at given position, false otherwise.
	 */
	public boolean contains(Station s)
	{
		return stations.contains(s);
	}

	/**
	 * Decides whether the graph contains given track. Two tracks are equall if they
	 * share their endpoints.
	 *
	 * @param t The track to search.
	 * @return True if there is a same track, false otherwise.
	 */
	public boolean contains(Track t)
	{
		return tracks.contains(t);
	}

	/**
	 * Returns the station instance which is connected with this graph.
	 *
	 * @param s The station to search.
	 * @return The stored instance.
	 */
	public Station getStoredStation(Station s)
	{
		return stations.getStoredRefference(s);
	}

	/**
	 * Returns the track which is connected with this graph.
	 *
	 * @param t The track to search.
	 * @return The stored instance.
	 */
	public Track getStoredTrack(Track t)
	{
		return tracks.getStoredRefference(t);
	}

	/**
	 * Prints the graph to given output.
	 *
	 * @param out The output.
	 * @throws IOException Exception thrown by the writer {@code out}.
	 */
	public void printToStream(Writer out) throws IOException
	{
		out.write("Underlying graph\n");
		out.write(Integer.toString(stations.size()));
		out.write(" stations\n");
		out.write(Integer.toString(tracks.size()));
		out.write(" tracks\n\nStations:\n");

		for (Station s : stations)
		{
			out.write(s.toString());
			out.write('\n');
		}
		out.write("\nTracks:\n");
		for (Track t : tracks)
		{
			out.write(t.toString());
			out.write('\n');
		}
		out.flush();
	}

	@Override
	public String toString()
	{
		return "UnderlyingGraph{" + "tracks=" + tracks + ", stations=" + stations + '}';
	}

	@Override
	public int getDegree(Vertex<String> v)
	{
		if (!(v instanceof Station))
			throw new IllegalArgumentException("The vertex must be a station.");

		return ((Station) v).countIncidentTracks();
	}

	@Override
	public int getVerticesNum()
	{
		return stations.size();
	}

	@Override
	public int getEdgesNum()
	{
		return tracks.size();
	}

	/**
	 * Gets the iterator of the stations.
	 * @return Stations iterator.
	 */
	public Iterator<Station> stationIterator()
	{
		return stations.iterator();
	}

	/**
	 * Gets the iterator of the tracks.
	 * @return Tracks iterator.
	 */
	public Iterator<Track> trackIterator()
	{
		return tracks.iterator();
	}

	@Override
	public Iterator<? extends Vertex<String>> vertexIterator()
	{
		return stationIterator();
	}

	@Override
	public Vertex<String>[] getIncidentVertices(Vertex<String> v)
	{
		return getIncidentVertices(v);
	}

	@Override
	public Iterator<? extends UndirectedEdge<Void>> edgeIterator()
	{
		return trackIterator();
	}
}
