/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import java.util.Objects;

/**
 * Simple vertex implementation.
 *
 * @param <T> Type of value stored together with vertex.
 * @author Petr Ryšavý
 */
public class SimpleVertex <T> implements Vertex<T>
{
	/**
	 * The value.
	 */
	private final T value;
	
	/**
	 * Creates new vertex.
	 * @param value The value to store.
	 */
	public SimpleVertex(T value)
	{
		this.value = value;
	}
	
	/**
	 * Creates newe vertex.
	 * 
	 * @param v The vertex whose value will be used.
	 */
	public SimpleVertex(Vertex<T> v)
	{
		this(v.getValue());
	}

	@Override
	public T getValue()
	{
		return value;
	}

	@Override
	public int hashCode()
	{
		return Objects.hashCode(this.value);
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final SimpleVertex other = (SimpleVertex) obj;
		return Objects.equals(this.value, other.value);
	}

	@Override
	public String toString()
	{
		return "SimpleVertex [" + value + ']';
	}
}
