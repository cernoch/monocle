/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import java.util.Collection;

/**
 * This interface defines and graph from that the indiced graph can be calcualted.
 *
 * @param <T> They type of values stored with the vertices.
 * @param <S> They type of values stored with the edges.
 * @author Petr Ryšavý
 */
public interface InductableGraph<T, S> extends Graph<T, S>
{
	/**
	 * Computes the graph induced by given set of vertices.
	 *
	 * @param vertices Set of vertices.
	 * @return The induced graph.
	 */
	public InductableGraph<T, S> inducedGraph(Collection<Vertex<T>> vertices);
}