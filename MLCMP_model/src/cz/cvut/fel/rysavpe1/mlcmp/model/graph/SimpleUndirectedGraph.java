/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalEdgeException;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalVertexException;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import cz.cvut.fel.rysavpe1.utilities.MathUtils;
import cz.cvut.fel.rysavpe1.utilities.ReferencedHashSet;
import cz.cvut.fel.rysavpe1.utilities.ValueIterator;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Simple graph implementation. The graph stores values only within vertices, the
 * edges are left void values. This class is not synchronized.
 *
 * @param <T> The type of values stored within vertices.
 * @author Petr Ryšavý
 */
public class SimpleUndirectedGraph<T> extends SimpleAbstractUndirectedMutableGraph<T> implements Iterable<T>
{
	/**
	 * The adjacency list.
	 */
	private HashMap<Vertex<T>, Set<Vertex<T>>> adj;
	/**
	 * Number of vertices.
	 */
	private int verticesNum;
	/**
	 * Number of edges.
	 */
	private int edgesNum;

	/**
	 * Creates new graph instance.
	 */
	public SimpleUndirectedGraph()
	{
		this.adj = new HashMap<>();
		verticesNum = 0;
		edgesNum = 0;
	}

	/**
	 * Creates new undirected graph.
	 * @param graph The graph to copy in.
	 */
	@SuppressWarnings("unchecked")
	public SimpleUndirectedGraph(Graph<T, ?> graph)
	{
		this();
		ReferencedHashSet<Vertex<T>> hs = new ReferencedHashSet<>();

		for (Vertex<T> v : new IteratorWrapper<>(graph.vertexIterator()))
		{
			final Vertex<T> w = new SimpleVertex<>(v);
			addVertex(w);
			hs.add(w);
		}
		for (Edge e : new IteratorWrapper<>(graph.edgeIterator()))
			if (e instanceof DirectedEdge)
			{
				final DirectedEdge f = (DirectedEdge) e;
				addEdge(hs.getStoredRefference(f.getHead()), hs.getStoredRefference(f.getTail()));
			}
			else if (e instanceof UndirectedEdge)
			{
				final UndirectedEdge f = (UndirectedEdge) e;
				addEdge(hs.getStoredRefference(f.getVertex1()), hs.getStoredRefference(f.getVertex2()));
			}
			else
				throw new IllegalEdgeException("Unknown edge type.");
	}

	/**
	 * Creates new instance with the given data.
	 *
	 * @param adj The adjacency list. This map is used as inner map, hence
	 *            modification to this map will lead to changes in the graph. But
	 *            note that this modifications may cause errors in bad number of
	 *            edges or vertices or may lead to an exception.
	 */
	public SimpleUndirectedGraph(HashMap<Vertex<T>, Set<Vertex<T>>> adj)
	{
		this.adj = adj;
		verticesNum = 0;
		edgesNum = 0;
		for (Set l : adj.values())
			edgesNum += l.size();
	}

	@Override
	public UndirectedEdge<Void> randomEdge()
	{
		int index = MathUtils.random(edgesNum);
		for (Entry<Vertex<T>, Set<Vertex<T>>> e : adj.entrySet())
		{
			final Set<Vertex<T>> s = e.getValue();
			final int size = s.size();
			if (size < index)
				index -= size;
			else
			{
				// we have the starting vertex, so get the final
				for (Vertex<T> t : s)
					if (index == 0)
						return new SimpleUndirectedEdge<>(e.getKey(), t);
					else
						index--;
			}
		}
		
		// just for compiler
		throw new RuntimeException("This exception shall never arise ...");
	}

	@Override
	@SuppressWarnings("unchecked")
	public final Vertex<T>[] getIncidentVertices(Vertex<T> v)
	{
		final Set<Vertex<T>> incidentList = adj.get(v);
		if (incidentList == null)
			throw new IllegalVertexException("The vertex is not in the graph.");
		return toArray(incidentList);
	}

	@Override
	public final Vertex<T>[] getVertices()
	{
		final Set<Vertex<T>> col = adj.keySet();
		return toArray(col);
	}

	@Override
	public final int getVerticesNum()
	{
		return verticesNum;
	}

	@Override
	public final int getEdgesNum()
	{
		return edgesNum;
	}

	@Override
	public Vertex<T> randomVertex()
	{
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public final void addVertex(Vertex<T> v)
	{
		if (adj.containsKey(v))
			return;

		final Set<Vertex<T>> list = newAdjacencySet();
		adj.put(v, list);
		verticesNum++;
	}

	@Override
	public void removeVertex(Vertex<T> v)
	{
		Set<Vertex<T>> set = adj.remove(v);
		if (set != null)
		{
			verticesNum--;
			edgesNum -= set.size();
			for (Vertex<T> u : set)
				adj.get(u).remove(v);
		}
	}

	@Override
	public SimpleUndirectedGraph<T> inducedGraph(Collection<Vertex<T>> vertices)
	{
		SimpleUndirectedGraph<T> graph = new SimpleUndirectedGraph<>();
		for (Vertex<T> v : vertices)
		{
			checkVertex(v);
			graph.addVertex(v);
		}

		for (Vertex<T> v : vertices)
			for (Vertex<T> u : adj.get(v))
				if (graph.adj.containsKey(u))
					graph.addEdge(v, u);

		return graph;
	}

	@Override
	public final Iterator<Vertex<T>> vertexIterator()
	{
		return adj.keySet().iterator();
	}

	/**
	 * Returns an iterator of vertices incident to particular vertex.
	 * @param v A vertex.
	 * @return Iterator of inicident vertices.
	 */
	public final Iterator<Vertex<T>> incidentVerticesIterator(Vertex<T> v)
	{
		checkVertex(v);
		return Collections.unmodifiableSet(adj.get(v)).iterator();
	}

	/**
	 * Pass values from set to new array.
	 *
	 * @param set List of vertices.
	 * @return Array of same vertices.
	 */
	@SuppressWarnings("unchecked")
	private Vertex<T>[] toArray(final Set<Vertex<T>> set)
	{
		Vertex[] arr = new Vertex[set.size()];
		return set.toArray(arr);
	}

	@Override
	public final void addEdge(Vertex<T> u, Vertex<T> v)
	{
		addConditionally(u);
		addConditionally(v);

		adj.get(u).add(v);
		adj.get(v).add(u);
		edgesNum++;
	}

	@Override
	public void removeEdge(Vertex<T> u, Vertex<T> v)
	{
		final Set<Vertex<T>> list = adj.get(u);
		if (list != null)
		{
			if (list.remove(v))
			{
				edgesNum--;
				adj.get(v).remove(u);
			}
		}
	}

	/**
	 * Creates new adjacency set and sets it's capacity.
	 *
	 * @return New adjacency set.
	 */
	private Set<Vertex<T>> newAdjacencySet()
	{
		return new HashSet<>(edgesNum / (verticesNum + 1) + 2);
	}

	/**
	 * Checks whether the graph contains particular vertex.
	 *
	 * @param v Vertex to search.
	 * @throws IllegalVertexException {@code true} if vertex is not part of this
	 *                                graph.
	 */
	protected final void checkVertex(Vertex<T> v) throws IllegalVertexException
	{
		if (!adj.containsKey(v))
			throw new IllegalVertexException("The vertex is not in the graph.");
	}

	/**
	 * Adds vertex if it is not already in this graph.
	 *
	 * @param v Vertex to add.
	 */
	protected final void addConditionally(Vertex<T> v)
	{
		if (!adj.containsKey(v))
			addVertex(v);
	}

	@Override
	public Iterator<T> iterator()
	{
		return new ValueIterator<>(adj.keySet().iterator());
	}

	@Override
	public int getDegree(Vertex<T> v)
	{
		Set<Vertex<T>> incidentList = adj.get(v);
		if (incidentList == null)
			throw new IllegalVertexException("The vertex is not in the graph.");
		return incidentList.size();
	}
}
