/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import java.util.Iterator;

/**
 * The undirected graph abstract class.
 *
 * @param <T> Type of values assigned to the vertices.
 * @param <S> Type of values assigned to the edges.
 * @author Petr Ryšavý
 */
public abstract class UndirectedGraph<T, S> implements Graph<T, S>
{
	@Override
	public abstract UndirectedEdge<S>[] getEdges();

	@Override
	public abstract UndirectedEdge<S>[] getIncidentEdges(Vertex<T> v);

	@Override
	public abstract UndirectedEdge<S> randomEdge();

	@Override
	public abstract Iterator<? extends UndirectedEdge<S>> edgeIterator();

	@Override
	public boolean isDirected()
	{
		return false;
	}
}
