/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.EdgeNotFoundException;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalEdgeException;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalVertexException;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.VertexNotFoundException;
import java.util.Collection;
import java.util.Set;

/**
 * Interface that defines methods for modifying graph.
 *
 * @param <T> Type of values assigned to the vertices.
 * @param <S> Type of values assigned to the edges.
 * @author Petr Ryšavý
 */
public interface MutableGraph<T, S> extends Graph<T, S>, InductableGraph<T, S>
{
	/**
	 * Adds new edge between two existing vertices.
	 *
	 * @param e New edge.
	 * @throws IllegalEdgeException When at least one of the endpoints of edge is not
	 *                                 part of the graph, ege exists in non multigraph
	 *                                 or any similar reason.
	 */
	public void addEdge(Edge<S> e);

	/**
	 * Adds new edge between two existing vertices.
	 *
	 * @param e New edge.
	 * @throws EdgeNotFoundException When the edge is not in the graph.
	 */
	public void removeEdge(Edge<S> e);

	/**
	 * Adds new vertex to the graph.
	 *
	 * @param v New vertex.
	 * @throws IllegalVertexException When vertex is already in the graph.
	 */
	public void addVertex(Vertex<T> v);

	/**
	 * Removes the given vertex from the graph as well as all incident edges.
	 *
	 * @param v The vertex to remove.
	 * @throws VertexNotFoundException When vertex is not in the graph.
	 */
	public void removeVertex(Vertex<T> v);
}