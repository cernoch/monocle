/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.Vertex;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalLineException;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalVertexException;
import cz.cvut.fel.rysavpe1.utilities.IteratorWrapper;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

/**
 * A class that wraps the MLCMP problem instance. The instance contains the
 * underlying graph and the line set.
 *
 * @author Petr Ryšavý
 */
public class MLCMP
{
	/**
	 * The underlying graph.
	 */
	private UnderlyingGraph graph;
	/**
	 * The set of lines on the graph.
	 */
	private LinkedList<Line> lines;

	/**
	 * Creates a new Metro Line Crossing Minimization Problem.
	 *
	 * @param graph The underlying graph.
	 * @param lines The set of lines in the graph.
	 */
	public MLCMP(UnderlyingGraph graph, Collection<Line> lines)
	{
		this.graph = graph;
		this.lines = new LinkedList<>();

		for (Line l : lines)
		{
			for (Track t : l)
				if (!graph.contains(t))
					throw new IllegalLineException("Found a line with track that is not in the graph.");
			this.lines.add(l);
		}
	}

	/**
	 * Creates a new Metro Line Crossing Minimization Problem.
	 *
	 * @param graph The underlying graph.
	 * @param lines The set of lines in the graph.
	 */
	public MLCMP(UnderlyingGraph graph, Line[] lines)
	{
		this(graph, Arrays.asList(lines));
	}

	/**
	 * Removes the given track from the problem. The track is removed from lines as
	 * well as from the underlying graph.
	 *
	 * @param t Track to be removed.
	 */
	public void removeTrack(Track t)
	{
		graph.removeEdge(t);
		for (Line l : lines)
			if (l.containsTrack(t))
				l.removeTrack(t);
	}

	/**
	 * Removes the given track from the problem. The track is removed from lines as
	 * well as from the underlying graph.
	 *
	 * @param t Track to be removed.
	 * @return Set of lines that were going throug this track.
	 */
	public Collection<Line> removeTrack2(Track t)
	{
		graph.removeEdge(t);
		LinkedHashSet<Line> linesSet = new LinkedHashSet<>();
		for (Line l : lines)
			if (l.containsTrack(t))
			{
				l.removeTrack(t);
				linesSet.add(l);
			}
		return linesSet;
	}

	/**
	 * Adds new track to the problem.
	 *
	 * @param t     The new track.
	 * @param lines The set of lines that cross this track. Can be also {@code null}.
	 * @throws IllegalLineException When the given line is not in the list.
	 */
	public void addTrack(Track t, Collection<Line> lines)
	{
		if (lines != null)
			for (Line l : lines)
				if (!this.lines.contains(l))
					throw new IllegalLineException("The line that shall go through this track is not in the lines list");

		graph.addEdge(t);
		if (lines != null)
			for (Line l : lines)
				l.addTrack(t);
	}

	/**
	 * Prints the problem to given output. At first this method prints the graph and
	 * then prints the lines.
	 *
	 * @param out The output.
	 * @throws IOException Exception thrown by the writer {@code out}.
	 */
	public void printToStream(Writer out) throws IOException
	{
		graph.printToStream(out);
		out.write("---------------\n");
		for (Line l : lines)
		{
			l.printToStream(out);
			out.write("---------------\n");
		}
	}

	/**
	 * Adds new station to the problem.
	 *
	 * @param s The station to be added.
	 */
	public void addStation(Station s)
	{
		graph.addVertex(s);
	}

	/**
	 * Removes station from the problem. It also removes all incident tracks from
	 * lines as well as from the underlying graph.
	 *
	 * @param s The station to be removed.
	 */
	public void removeStation(Station s)
	{
		graph.removeVertex(s);
		for (Track t : s.getIncidentTracks(false))
			for (Line l : lines)
				l.removeTrack(t);
	}

	/**
	 * Retruns the edges set.
	 *
	 * @return Array that contains all edges.
	 */
	public Track[] getEdges()
	{
		return graph.getEdges();
	}

	/**
	 * Returns the set of track incident with the specified station.
	 *
	 * @param s The station to search.
	 * @return The set of tracks incident with {@code s}.
	 */
	public Track[] getIncidentEdges(Station s)
	{
		return graph.getIncidentEdges(s);
	}

	/**
	 * Returns the set of edges incident with the given station.
	 *
	 * @param v               The station to search.
	 * @param guaranteeSorted If {@code true}, than the returned array will be
	 *                        sorted, otherwise the tracks can be sorted randomly.
	 * @return The array with incident edges.
	 */
	public Track[] getIncidentEdges(Station v, boolean guaranteeSorted)
	{
		return graph.getIncidentEdges(v, guaranteeSorted);
	}

	/**
	 * Returns the set of all vertices in the problem.
	 *
	 * @return Set of vertices.
	 */
	public Station[] getVertices()
	{
		return graph.getVertices();
	}

	/**
	 * Calculates the problem induced by a set of vertices. The induced problem
	 * consists from underlying graph induced by this set of vertices and lines that
	 * contain edges in this new graph. No outher eges are added to the new set of
	 * lines.
	 *
	 * @param stations The set of stations.
	 * @return New problem given by the set of stations.
	 */
	public MLCMP inducedProblem(Collection stations)
	{
		@SuppressWarnings("unchecked")
		UnderlyingGraph newGraph = (UnderlyingGraph) graph.inducedGraph(new HashSet<Vertex<String>>(stations));

		LinkedHashMap<Line, Line> linesMap = new LinkedHashMap<>();
		for (Line l : lines)
			linesMap.put(l, new Line(l.getName(), null));

		for (Track t : newGraph.getEdges())
			for (Entry<Line, Line> e : linesMap.entrySet())
				if (e.getKey().containsTrack(t))
					e.getValue().addTrack(t);

		LinkedList<Line> newLines = new LinkedList<>();
		for (Entry<Line, Line> e : linesMap.entrySet())
			if (!e.getValue().isEmpty())
				newLines.add(e.getValue());
		return new MLCMP(newGraph, newLines);
	}

	/**
	 * Decides whether problem contains given station.
	 *
	 * @param s Station to search.
	 * @return Returns {@code true} if underlying graph contains
	 *         {@code s}, {@code false} otherwise.
	 */
	public boolean contains(Station s)
	{
		return graph.contains(s);
	}

	/**
	 * Decides whether problem contains given track.
	 *
	 * @param t Track to search.
	 * @return Returns {@code true} if underlying graph contains
	 *         {@code t}, {@code false} otherwise.
	 */
	public boolean contains(Track t)
	{
		return graph.contains(t);
	}

	/**
	 * Returns the vertex degree. That is number of edges incident with station
	 * {@code s}.
	 *
	 * @param s Station to search.
	 * @return The station degree.
	 */
	public int getDegree(Station s)
	{
		return graph.getDegree(s);
	}

	/**
	 * Returns the set of lines that cross the given track.
	 *
	 * @param t Track to search.
	 * @return The set of incident lines.
	 */
	public Set<Line> getLineSet(Track t)
	{
		// TODO tohle by chtělo cachovat ...
		LinkedHashSet<Line> lineSet = new LinkedHashSet<>();
		for (Line l : lines)
			if (l.containsTrack(t))
				lineSet.add(l);
		return lineSet;
	}

	/**
	 * Returns the set of lines that goes through the given station or end at this
	 * station.
	 *
	 * @param s Station to search.
	 * @return The set of incident lines.
	 */
	public Set<Line> getLineSet(Station s)
	{
		HashSet<Line> set = new HashSet<>(lines.size());
		for (Track t : new IteratorWrapper<>(s.incidenTracksIterator()))
			set.addAll(getLineSet(t));
		return set;
	}

	/**
	 * Returns the number of lines that pass through a track.
	 *
	 * @param t The track to count the lines on it.
	 * @return The number of lines that go through the track {@code t}.
	 */
	public int getLinesNum(Track t)
	{
		// TODO tohle by chtělo cachovat ... z getLineSet
		int count = 0;
		for (Line l : lines)
			if (l.containsTrack(t))
				count++;
		return count;
	}

	/**
	 * Returns an array with lines that pass through the given track.
	 *
	 * @param t Track to search.
	 * @return The set of incident lines.
	 */
	public Line[] getLineArray(Track t)
	{
		Collection<Line> lineSet = getLineSet(t);
		Line[] arr = new Line[lineSet.size()];
		return lineSet.toArray(arr);
	}

	/**
	 * Decides whether this station can be removed from the graph, i.e. if the edges
	 * can be contracted.
	 *
	 * A station can be contracted if and only if following conditions hold:
	 * <ul>
	 * <li> The station is of degree 2. </li>
	 * <li> There is no line that ends in this station. </li>
	 * <li> The contraction won't produce parallel edges. This may arise in a cycle
	 * </li>
	 * </ul>
	 *
	 * @param s The station which shall be contracted.
	 * @return True if the station can be contracted.
	 */
	public boolean canBeStationContracted(Station s)
	{
		if (graph.getDegree(s) != 2)
			return false;

		// TODO možná cachovat celé nebo využít cache getLineSet
		for (Line l : lines)
			if (l.isEndpoint(s))
				return false;

		final Track[] tA = s.getIncidentTracks(false);
		final Station u = tA[0].getOtherVertex(s);
		final Station v = tA[1].getOtherVertex(s);

		return !graph.contains(new Track(u, v));
	}

	/**
	 * Contracts the problem at the given station, if it is possible. If not, then a
	 * exception is thrown.
	 *
	 * @param s Station, where the problem shall be contracted.
	 * @throws IllegalVertexException When mehod {@code canBeStationContracted}
	 *                                returns {@code false}.
	 * @see #canBeStationContracted(cz.cvut.fel.rysavpe1.mlcmp.model.Station)
	 * @return The new track that skips this station.
	 */
	public Track contractStation(Station s)
	{
		if (!canBeStationContracted(s))
			throw new IllegalVertexException("The station cannot be contracted : " + s);

		return contractStationSafe(s);
	}

	/**
	 * Contracts the problem at given station if possible.
	 *
	 * @param s Station, where the problem shall be contracted.
	 * @return The new track that skips this station or {@code null} when the problem
	 *         cannot be contracted at this station.
	 */
	public Track contractStationIfPossible(Station s)
	{
		if (canBeStationContracted(s))
			return contractStationSafe(s);
		return null;
	}

	/**
	 * Contracts the problem at given station, for which returns
	 * {@code canBeStationContracted} returns {@code true}. If the problem cannot be
	 * contracted at that station, the method will be buggy.
	 *
	 * @param s Station, where the problem shall be contracted.
	 * @return The new track that skips this station.
	 */
	private Track contractStationSafe(Station s)
	{
		final Track[] tA = s.getIncidentTracks(false);
		final Track t1 = tA[0];
		final Track t2 = tA[1];

		graph.removeEdge(t1);
		graph.removeEdge(t2);
		final Station u = t1.getOtherVertex(s);
		final Station v = t2.getOtherVertex(s);
		final Track contractedTrack = new Track(u, v, t1.getVertexAngle(u), t2.getVertexAngle(v));
		graph.addEdge(contractedTrack);
		graph.removeVertex(s);

		// get line set is used, cause it may be cached sometimes - maybe it will
		// be faster than going through all of the lines
		for (Line l : getLineSet(t1))
		{
			l.removeTrack(t1);
			l.removeTrack(t2);
			l.addTrack(contractedTrack);
		}

		return contractedTrack;
	}

	/**
	 * Returns the number of stations in the graph.
	 *
	 * @return The number of vertices.
	 */
	public int getVerticesNum()
	{
		return graph.getVerticesNum();
	}

	/**
	 * Returns the number of tracks in the graph.
	 *
	 * @return The number of edges.
	 */
	public int getEdgesNum()
	{
		return graph.getEdgesNum();
	}

	/**
	 * Returns the number of lines in the graph.
	 *
	 * @return The number of lines.
	 */
	public int getLinesNum()
	{
		return lines.size();
	}

	/**
	 * Returns the set of all lines in the problem.
	 *
	 * @return The array of lines.
	 */
	public Line[] getLines()
	{
		@SuppressWarnings("LocalVariableHidesMemberVariable")
		Line[] lines = new Line[this.lines.size()];
		return this.lines.toArray(lines);
	}

	/**
	 * Returns new iterator of lines.
	 *
	 * @return Iterator of problem lines.
	 */
	public Iterator<Line> lineIterator()
	{
		return Collections.unmodifiableList(lines).iterator();
	}

	/**
	 * Gets the iterator of the stations.
	 *
	 * @return Stations iterator.
	 */
	public Iterator<Station> stationIterator()
	{
		return graph.stationIterator();
	}

	/**
	 * Gets the iterator of the tracks.
	 *
	 * @return Tracks iterator.
	 */
	public Iterator<Track> trackIterator()
	{
		return graph.trackIterator();
	}

	/**
	 * Finds a line for name. Runs linear search in lines.
	 *
	 * @param name Name of line to find.
	 * @return The line object or null if the line wasn't found.
	 */
	public Line getLine(String name)
	{
		for (Line l : lines)
			if (l.getName().equals(name))
				return l;
		return null;
	}

	/**
	 * Returns the station instance which is connected with this graph. This means
	 * that the returned station is equal to searched station, but is also part of
	 * the graph.
	 *
	 * @param s The station to search.
	 * @return The stored instance.
	 */
	public Station getStoredStation(Station s)
	{
		return graph.getStoredStation(s);
	}

	/**
	 * Returns the track which is connected with this graph. This means that the
	 * returned track is equal to searched track, but is also part of the graph.
	 *
	 * @param t The track to search.
	 * @return The stored instance.
	 */
	public Track getStoredTrack(Track t)
	{
		return graph.getStoredTrack(t);
	}
}
