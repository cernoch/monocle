/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalVertexException;
import java.util.Objects;

/**
 * Simple implementation of undirected edge.
 *
 * @param <V> The value stored in vertices.
 * @author Petr Ryšavý
 */
public class SimpleUndirectedEdge<V> implements UndirectedEdge<Void>
{
	/**
	 * The first vertex.
	 */
	private final Vertex<V> u;
	/**
	 * The second vertex.
	 */
	private final Vertex<V> v;

	/**
	 * Creates new edge.
	 * @param u One of the endpoints.
	 * @param v Second of the endpoints.
	 */
	public SimpleUndirectedEdge(Vertex<V> u, Vertex<V> v)
	{
		this.u = u;
		this.v = v;
	}

	@Override
	public boolean isIncidentWith(Vertex v)
	{
		return v.equals(u) || v.equals(v);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Vertex<V> getOtherVertex(Vertex v)
	{
		if (v.equals(u))
			return v;
		else if (v.equals(v))
			return u;
		throw new IllegalVertexException("The vertex is not incident with the given edge.");
	}

	@Override
	public Void getValue()
	{
		return null;
	}

	@Override
	public int hashCode()
	{
		// two equal tracks must return same hash code - ie hash code of edge uv
		// must be same as hash code of vu - hence the codes must be returned in the
		// same order
		final int hashu = Objects.hashCode(this.u);
		final int hashv = Objects.hashCode(this.v);
		if (hashu >= hashv)
			return 23 * hashu + hashv;
		else
			return 23 * hashv + hashu;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		final SimpleUndirectedEdge<V> other = (SimpleUndirectedEdge<V>) obj;
		if (Objects.equals(this.u, other.u) && Objects.equals(this.v, other.v)
			|| Objects.equals(this.u, other.v) && Objects.equals(this.v, other.u))
			return true;
		return false;
	}

	@Override
	public String toString()
	{
		return "directedEdge{" + "head=" + u + ", tail=" + v + '}';
	}

	@Override
	public Vertex getVertex1()
	{
		return u;
	}

	@Override
	public Vertex getVertex2()
	{
		return v;
	}
}
