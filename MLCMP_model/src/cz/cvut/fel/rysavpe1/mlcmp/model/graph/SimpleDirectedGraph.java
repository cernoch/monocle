/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalVertexException;
import cz.cvut.fel.rysavpe1.utilities.ValueIterator;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Simple graph implementation. The graph stores values only within vertices, the
 * edges are left void values. This class is not synchronized.
 *
 * @param <T> The type of values stored within vertices.
 * @author Petr Ryšavý
 */
public class SimpleDirectedGraph<T> extends SimpleAbstractDirectedMutableGraph<T> implements Iterable<T>
{
	/**
	 * The adjacency list.
	 */
	private HashMap<Vertex<T>, Set<Vertex<T>>> adj;
	/**
	 * Number of vertices.
	 */
	private int verticesNum;
	/**
	 * Number of edges.
	 */
	private int edgesNum;

	/**
	 * Creates new graph instance.
	 */
	public SimpleDirectedGraph()
	{
		this.adj = new HashMap<>();
		verticesNum = 0;
		edgesNum = 0;
	}

	/**
	 * Creates new instance with the given data.
	 *
	 * @param adj The adjacency list. This map is used as inner map, hence
	 *            modification to this map will lead to changes in the graph. But
	 *            note that this modifications may cause errors in bad number of
	 *            edges or vertices or may lead to an exception.
	 */
	public SimpleDirectedGraph(HashMap<Vertex<T>, Set<Vertex<T>>> adj)
	{
		this.adj = adj;
		verticesNum = 0;
		edgesNum = 0;
		for (Set l : adj.values())
			edgesNum += l.size();
	}

	@Override
	public DirectedEdge<Void> randomEdge()
	{
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	@SuppressWarnings("unchecked")
	public final Vertex<T>[] getIncidentVertices(Vertex<T> v)
	{
		final Set<Vertex<T>> incidentList = adj.get(v);
		if (incidentList == null)
			throw new IllegalVertexException("The vertex is not in the graph.");
		return toArray(incidentList);
	}

	@Override
	public int getInDegree(Vertex<T> v)
	{
		checkVertex(v);
		int degree = 0;
		for (Set<Vertex<T>> l : adj.values())
			for (Vertex u : l)
				if (u.equals(v))
					degree++;
		return degree;
	}

	@Override
	public final int getOutDegree(Vertex<T> v)
	{
		Set<Vertex<T>> incidentList = adj.get(v);
		if (incidentList == null)
			throw new IllegalVertexException("The vertex is not in the graph.");
		return incidentList.size();
	}

	@Override
	public final Vertex<T>[] getVertices()
	{
		final Set<Vertex<T>> col = adj.keySet();
		return toArray(col);
	}

	@Override
	public final int getVerticesNum()
	{
		return verticesNum;
	}

	@Override
	public final int getEdgesNum()
	{
		return edgesNum;
	}

	@Override
	public Vertex<T> randomVertex()
	{
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void addVertex(Vertex<T> v)
	{
		if (adj.containsKey(v))
			return;

		final Set<Vertex<T>> list = newAdjacencySet();
		adj.put(v, list);
		verticesNum++;
	}

	@Override
	public void removeVertex(Vertex<T> v)
	{
		if (adj.remove(v) != null)
			verticesNum--;
		for(Set<Vertex<T>> set : adj.values())
			if(set.remove(v))
				edgesNum--;
	}

	@Override
	public SimpleDirectedGraph<T> inducedGraph(Collection<Vertex<T>> vertices)
	{
		SimpleDirectedGraph<T> graph = new SimpleDirectedGraph<>();
		for(Vertex<T> v : vertices)
		{
			checkVertex(v);
			graph.addVertex(v);
		}
		
		for(Vertex<T> v : vertices)
			for(Vertex<T> u : adj.get(v))
				if(graph.adj.containsKey(u))
					graph.addEdge(v, u);
		
		return graph;
	}

	@Override
	public final Iterator<Vertex<T>> vertexIterator()
	{
		return adj.keySet().iterator();
	}

	@Override
	public final Iterator<Vertex<T>> incidentVerticesIterator(Vertex<T> v)
	{
		checkVertex(v);
		return Collections.unmodifiableSet(adj.get(v)).iterator();
	}

	/**
	 * Pass values from set to new array.
	 * @param set List of vertices.
	 * @return Array of same vertices.
	 */
	@SuppressWarnings("unchecked")
	private Vertex<T>[] toArray(final Set<Vertex<T>> set)
	{
		Vertex[] arr = new Vertex[set.size()];
		return set.toArray(arr);
	}

	@Override
	public void addEdge(Vertex<T> head, Vertex<T> tail)
	{
		addConditionally(head);
		addConditionally(tail);

		adj.get(head).add(tail);
		edgesNum++;
	}

	@Override
	public void removeEdge(Vertex<T> head, Vertex<T> tail)
	{
		final Set<Vertex<T>> list = adj.get(head);
		if (list != null)
			if (list.remove(tail))
				edgesNum--;
	}

	/**
	 * Creates new adjacency set and sets it's capacity.
	 *
	 * @return New adjacency set.
	 */
	private Set<Vertex<T>> newAdjacencySet()
	{
		return new HashSet<>(edgesNum / (verticesNum + 1) + 2);
	}

	/**
	 * Checks whether the graph contains particular vertex.
	 *
	 * @param v Vertex to search.
	 * @throws IllegalVertexException {@code true} if vertex is not part of this
	 *                                graph.
	 */
	protected final void checkVertex(Vertex<T> v) throws IllegalVertexException
	{
		if (!adj.containsKey(v))
			throw new IllegalVertexException("The vertex is not in the graph.");
	}

	/**
	 * Adds vertex if it is not already in this graph.
	 * @param v Vertex to add.
	 */
	protected final void addConditionally(Vertex<T> v)
	{
		if (!adj.containsKey(v))
			addVertex(v);
	}

	@Override
	public Iterator<T> iterator()
	{
		return new ValueIterator<>(adj.keySet().iterator());
	}

}
