/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.VertexNotFoundException;

/**
 * Definition of undirected edge.
 *
 * @param <T> Type of values stored together with the edge.
 * @author Petr Ryšavý
 */
public interface UndirectedEdge<T> extends Edge<T>
{
	/**
	 * Returns one of the vertices incident with the edge.
	 *
	 * @return First vertex incident to the edge.
	 */
	public Vertex getVertex1();

	/**
	 * Returns one of the vertices incident with the edge.
	 *
	 * @return Second vertex incident to the edge.
	 */
	public Vertex getVertex2();

	/**
	 * Decides whether a vertex is incident with the vertex.
	 *
	 * @param v Vertex to be incident.
	 * @return Is {@code v} endpoint of this edge?
	 */
	public boolean isIncidentWith(Vertex v);

	/**
	 * Finds the second vertex of the edge. If this edge contains vertices {@code u}
	 * and {@code v}, then returns {@code u}. If this edge doesn't contain vertex
	 * {@code v}, then it throws an error.
	 *
	 * @param v First vertex of the edge.
	 * @return The second endpoint of the edge.
	 * @throws VertexNotFoundException When vertex {@code v} is not incident with
	 *                                    this edge.
	 */
	public Vertex getOtherVertex(Vertex v);
}