/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalVertexException;
import java.util.Objects;

/**
 * Simple implementation of directed edge.
 *
 * @param <V> The value stored in vertices.
 * @author Petr Ryšavý
 */
public class SimpleDirectedEdge<V> implements DirectedEdge<Void>
{
	/**
	 * The edge head.
	 */
	private Vertex<V> head;
	/**
	 * The edge tail.
	 */
	private Vertex<V> tail;

	/**
	 * Creates new edge object.
	 * 
	 * @param head Edge start vertex.
	 * @param tail Edge end vertex.
	 */
	public SimpleDirectedEdge(Vertex<V> head, Vertex<V> tail)
	{
		this.head = head;
		this.tail = tail;
	}

	@Override
	public Vertex<V> getHead()
	{
		return head;
	}

	@Override
	public Vertex<V> getTail()
	{
		return tail;
	}

	@Override
	public boolean isIncidentWith(Vertex v)
	{
		return v.equals(head) || v.equals(tail);
	}

	@Override
	public boolean isHead(Vertex v)
	{
		return v.equals(head);
	}

	@Override
	public boolean isTail(Vertex v)
	{
		return v.equals(tail);
	}

	@Override
	public SimpleDirectedEdge<V> antiParallelEdge()
	{
		return new SimpleDirectedEdge<>(tail, head);
	}

	@Override
	public Vertex<V> getOtherVertex(Vertex v)
	{
		if(v.equals(head))
			return tail;
		else if(v.equals(tail))
			return head;
		throw new IllegalVertexException("The vertex is not incident with the given edge.");
	}

	@Override
	public Void getValue()
	{
		return null;
	}

	@Override
	public int hashCode()
	{
		int hash = 7;
		hash = 97 * hash + Objects.hashCode(this.head);
		hash = 97 * hash + Objects.hashCode(this.tail);
		return hash;
	}

	/**
	 * The edges are same if they have same endpoints and same orientation.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		final SimpleDirectedEdge<V> other = (SimpleDirectedEdge<V>) obj;
		if (!Objects.equals(this.head, other.head))
			return false;
		if (!Objects.equals(this.tail, other.tail))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "directedEdge{" + "head=" + head + ", tail=" + tail + '}';
	}
}
