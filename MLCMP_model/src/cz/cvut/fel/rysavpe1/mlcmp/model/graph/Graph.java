/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import java.util.Iterator;

/**
 * This inderface defines a general graph.
 *
 * @param <T> Type of values assigned to the vertices.
 * @param <S> Type of values assigned to the edges.
 * @author Petr Ryšavý
 */
public interface Graph<T, S>
{
	/**
	 * Returns set of vertices in the graph.
	 *
	 * @return Set of vertices.
	 */
	public Vertex<T>[] getVertices();

	/**
	 * Returns the number of vertices in the graph.
	 *
	 * @return The number of vertices.
	 */
	public int getVerticesNum();

	/**
	 * Returns set of edges in the graph.
	 *
	 * @return Set of edges.
	 */
	public Edge<S>[] getEdges();

	/**
	 * Returns number of edges in the graph.
	 *
	 * @return The number of edges.
	 */
	public int getEdgesNum();

	/**
	 * Returns the iterator of vertices.
	 *
	 * @return Vertices iterator.
	 */
	public Iterator<? extends Vertex<T>> vertexIterator();

	/**
	 * Returns the iterator of edges in the problem.
	 * @return Iterator of edges.
	 */
	public Iterator<? extends Edge<S>> edgeIterator();

	/**
	 * Returns set of edges incident with the choosen vertex.
	 *
	 * @param v Vertex.
	 * @return Set of edges incident with {@code v}.
	 */
	public Edge<S>[] getIncidentEdges(Vertex<T> v);

	/**
	 * Returns set the degreeof choosen vertex.
	 *
	 * @param v Vertex.
	 * @return The {@code deg(v)}.
	 */
	public int getDegree(Vertex<T> v);

	/**
	 * Returns random vertex.
	 *
	 * @return A vertex.
	 */
	public Vertex<T> randomVertex();

	/**
	 * Returns random edge.
	 *
	 * @return A edge.
	 */
	public Edge<S> randomEdge();

	/**
	 * Decides whether a graph is directed or not.
	 *
	 * @return Is graph directed?
	 */
	public boolean isDirected();

	/**
	 * Returns the set of vertices incident to the particular one.
	 *
	 * @param v The vertex from that the incident vertices shall be searched.
	 * @return The set of vertices that are edpoints of edges going from {@code v}.
	 */
	public abstract Vertex<T>[] getIncidentVertices(Vertex<T> v);
}