/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalVertexException;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.UndirectedEdge;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.Vertex;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalEdgeException;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.VertexNotFoundException;
import java.util.Objects;

/**
 * The one edge in the underlying graph. This class is also suitable to represent a
 * contracted set of incident edges that form a path.
 *
 * @author Petr Ryšavý
 */
public class Track implements UndirectedEdge<Void>
{
	/**
	 * First station.
	 */
	private final Station u;
	/**
	 * Second station.
	 */
	private final Station v;
	/**
	 * The anti-clockwise oriented angle (0, 2*pi&gt; at which the track leaves the
	 * first station.
	 */
	private final double uAngle;
	/**
	 * The anti-clockwise oriented angle (0, 2*pi&gt; at which the track leaves the
	 * second station.
	 */
	private final double vAngle;

	/**
	 * Creates a track between two stations. The ingoing angles are automatically
	 * calculated from the stations positions.
	 *
	 * @param u First station.
	 * @param v Second station.
	 */
	@SuppressWarnings("LeakingThisInConstructor")
	public Track(Station u, Station v)
	{
		if (u == null || v == null)
			throw new NullPointerException("The edge endpoints cannot be null.");
		if (u.equals(v))
			throw new IllegalArgumentException("The trivial cycle is not allowed in the MLCMP graph.");

		this.u = u;
		this.v = v;

		final double xDiff = v.getX() - u.getX();
		final double yDiff = v.getY() - u.getY();

		if (xDiff == 0)
		{
			if (yDiff == 0)
				throw new IllegalVertexException("The edge endpoints have same coordinates.");
			else if (yDiff > 0)
				uAngle = 2 * Math.PI;
			else
				uAngle = Math.atan2(xDiff, -yDiff) + Math.PI;
		}
		else
			uAngle = Math.atan2(xDiff, -yDiff) + Math.PI;

		if (uAngle > Math.PI)
			vAngle = uAngle - Math.PI;
		else
			vAngle = uAngle + Math.PI;
	}

	/**
	 * Creates a track between two stations leawing at given angles.
	 *
	 * @param u      First station.
	 * @param v      Second station.
	 * @param uAngle The angle at which the track leaves the first station.
	 * @param vAngle The angle at which the track leaves the second station.
	 */
	@SuppressWarnings("LeakingThisInConstructor")
	public Track(Station u, Station v, double uAngle, double vAngle)
	{
		this.u = u;
		this.v = v;
		this.uAngle = uAngle;
		this.vAngle = vAngle;
	}

	/**
	 * Returns one of the stations incident with the track.
	 *
	 * @return First station incident to the track.
	 */
	@Override
	public Station getVertex1()
	{
		return u;
	}

	/**
	 * Returns one of the stations incident with the track.
	 *
	 * @return Second station incident to the track.
	 */
	@Override
	public Station getVertex2()
	{
		return v;
	}

	@Override
	public boolean isIncidentWith(Vertex v)
	{
		if (v.equals(u) || v.equals(this.v))
			return true;
		return false;
	}

	/**
	 * Checks whether track is incident with another one. I.e. checks whether they
	 * share a common station.
	 *
	 * @param t The track to check.
	 * @return {@code true} iff this track ends at same station as {@code t}.
	 */
	public boolean isIncidentWith(Track t)
	{
		if (t.u.equals(u) || t.u.equals(v) || t.v.equals(u) || t.v.equals(v))
			return true;
		return false;
	}

	@Override
	public Station getOtherVertex(Vertex v)
	{
		if (v.equals(u))
			return this.v;
		else if (v.equals(this.v))
			return u;
		else
			throw new VertexNotFoundException("The vertex is not incident with this edge.");
	}

	/**
	 * Calculates the angle at which the track leaves the given station.
	 *
	 * @param s One of the endpoints of this track.
	 * @return The angle at which the track leaves the station {@code s}.
	 * @throws VertexNotFoundException If the {@code s} is not incident with this
	 *                                 track.
	 */
	public double getVertexAngle(Station s)
	{
		if (s.equals(u))
			return uAngle;
		else if (s.equals(v))
			return vAngle;
		else
			throw new VertexNotFoundException("The vertex is not incident with this edge.");
	}

	/**
	 * Calculates the angle at which the track leaves the other station.
	 *
	 * @param s One of the endpoints of this track.
	 * @return The angle at which the track leaves the other station.
	 * @throws VertexNotFoundException If the {@code s} is not incident with this
	 *                                 track.
	 */
	public double getOtherVertexAngle(Station s)
	{
		if (s.equals(u))
			return vAngle;
		else if (s.equals(v))
			return uAngle;
		else
			throw new VertexNotFoundException("The vertex is not incident with this edge.");
	}

	@Override
	public Void getValue()
	{
		return null;
	}

	/**
	 * Returns the angle at which the track leaves the station returned by method
	 * {@code getVertex1}.
	 *
	 * @return Angle at which the track leaves vertex.
	 */
	public double getVertex1Angle()
	{
		return uAngle;
	}

	/**
	 * Returns the angle at which the track leaves the station returned by method
	 * {@code getVertex2}.
	 *
	 * @return Angle at which the track leaves vertex.
	 */
	public double getVertex2Angle()
	{
		return vAngle;
	}

	/**
	 * Returns station that has this track with common with given track.
	 *
	 * @param t A track incident to this track.
	 * @return Station that is incident to both tracks, this and the {@code t} track.
	 * @throws IllegalEdgeException If this track is not incident with {@code t}.
	 */
	public Station getCommonStation(Track t)
	{
		if (this.v.equals(t.v) || this.v.equals(t.u))
			return this.v;
		else if (this.u.equals(t.u) || this.u.equals(t.v))
			return this.u;
		else
			throw new IllegalEdgeException("The track does not have common vertex with this track.");
	}

	@Override
	public int hashCode()
	{
		// two equal tracks must return same hash code - ie hash code of edge uv
		// must be same as hash code of vu - hence the codes must be returned in the
		// same order
		final int hashu = Objects.hashCode(this.u);
		final int hashv = Objects.hashCode(this.v);
		if (hashu >= hashv)
			return 23 * hashu + hashv;
		else
			return 23 * hashv + hashu;
	}

	/**
	 * Two tracks are equal if they have same enpoints not matter the order.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Track other = (Track) obj;
		if (this.v.equals(other.v) && this.u.equals(other.u)
			|| this.v.equals(other.u) && this.u.equals(other.v))
			return true;
		return false;
	}

	@Override
	public String toString()
	{
		return "Track{" + "u=" + u.getValue() + ", v=" + v.getValue() + '}';
	}
}
