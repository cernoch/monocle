/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalEdgeException;
import cz.cvut.fel.rysavpe1.mlcmp.model.graph.Vertex;
import cz.cvut.fel.rysavpe1.mlcmp.utilities.DescendingTrackComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 * The station.
 *
 * @author Petr Ryšavý
 */
public class Station implements Vertex<String>
{
	/**
	 * The name of the station.
	 */
	private final String value;
	/**
	 * The x coordinate of the station.
	 */
	private final double x;
	/**
	 * The y coordinate of the station.
	 */
	private final double y;
	/**
	 * List of edges that enter this vertex.
	 */
	private ArrayList<Track> tracks;
	/**
	 * Inidicates whether a {@code tracks} list is sorted.
	 */
	private boolean sorted = false;

	/**
	 * Creates new station with given name and position.
	 *
	 * @param value The name of the station.
	 * @param x     The x coordinate of the position.
	 * @param y     The y coordinate of the position.
	 */
	public Station(String value, double x, double y)
	{
		if (value == null)
			throw new NullPointerException("The station name cannot be null. Please use the other constructor.");

		this.value = value;
		this.x = x;
		this.y = y;
		tracks = new ArrayList<>(8);
	}

	/**
	 * Creates new station with generic name and given position.
	 *
	 * @param x The x coordinate of the position.
	 * @param y The y coordinate of the position.
	 */
	public Station(double x, double y)
	{
		this.value = new StringBuilder("Station [").append(x).append(", ").append(y).append(']').toString();
		this.x = x;
		this.y = y;
		tracks = new ArrayList<>(8);
	}

	/**
	 * Returns the name of the station.
	 *
	 * @return The station name.
	 */
	@Override
	public String getValue()
	{
		return value;
	}

	/**
	 * Gets the x-coordinate of the station.
	 *
	 * @return The x position.
	 */
	public double getX()
	{
		return x;
	}

	/**
	 * Gets the y-coordinate of the station.
	 *
	 * @return The y position.
	 */
	public double getY()
	{
		return y;
	}

	/**
	 * Creates an array of edges incident with this station.
	 *
	 * @param guaranteeSorted If {@code true}, than the returned array will be
	 *                        sorted, otherwise the tracks can be sorted randomly.
	 * @return The array with incident edges.
	 */
	public Track[] getIncidentTracks(boolean guaranteeSorted)
	{
		if (guaranteeSorted && !sorted)
			sortTracks();

		Track[] trackArray = new Track[tracks.size()];
		return tracks.toArray(trackArray);
	}

	/**
	 * Creates an array of edges incident with this station. The array is always
	 * sorted and the tracks are sorted clockwise from the {@code pivot} track. The
	 * {@code pivot} is always at the first position in returned array.
	 *
	 * @param pivot The track that specifies the direction from that are other tracks
	 *              taken in clockwise direction.
	 * @return The array with incident edges.
	 */
	public Track[] getIncidentTracks(Track pivot)
	{
		if (!sorted)
			sortTracks();

		final int trackIndex = tracks.indexOf(pivot);
		if (trackIndex == -1)
			throw new IllegalEdgeException("The pivot track must be incident with the given station.");

		Track[] trackArray = new Track[tracks.size()];
		final int size = tracks.size();
		int i, j;
		for (i = 0, j = trackIndex; j < size; i++, j++)
			trackArray[i] = tracks.get(j);
		for (j = 0; i < size; i++, j++)
			trackArray[i] = tracks.get(j);
		return trackArray;
	}

	/**
	 * Returns map that maps each track to index of that track in ordering.
	 *
	 * @return The map from tracks to order.
	 */
	public Map<Track, Integer> getIncidentTracks()
	{
		if (!sorted)
			sortTracks();

		final int n = tracks.size();
		final Map<Track, Integer> sortMap = new LinkedHashMap<>(n);
		for (int i = 0; i < n; i++)
			sortMap.put(tracks.get(i), i);
		return sortMap;
	}

	/**
	 * Returns iterator of tracks incident with this station.
	 *
	 * @return The iterator of incident tracks.
	 */
	public Iterator<Track> incidenTracksIterator()
	{
		return tracks.iterator();
	}

	/**
	 * Returns the iterator of incident tracks.
	 *
	 * @param guaranteeSorted Whether the tracks shall be sorted in the clockwise
	 *                        order.
	 * @return The iterator of incident tracks.
	 */
	public Iterator<Track> incidenTracksIterator(boolean guaranteeSorted)
	{
		if (guaranteeSorted && !sorted)
			sortTracks();

		return tracks.iterator();
	}

	/**
	 * Returns how many edges are incident with this stations.
	 *
	 * @return The count of incident edges.
	 */
	public int countIncidentTracks()
	{
		return tracks.size();
	}

	/**
	 * Adds track to the set of tracks incident with this station.
	 *
	 * <i>This method is called automatically from {@code Graph} when adding the
	 * {@code Track} into the graph.</i>
	 *
	 * @param t Track to add.
	 */
	protected void addTrack(Track t)
	{
		if (tracks.contains(t))
			return;
		if (!t.isIncidentWith(this))
			throw new IllegalEdgeException("The added edge must be incident with this vertex.: "+t + " is not incident with "+this);

		final double trackAngle = t.getVertexAngle(this);
		for (Track t2 : tracks)
			if (t2.getVertexAngle(this) == trackAngle)
				throw new IllegalEdgeException("The added edge leaves the node in same direction as one of existing edges: "+t + " at station "+this);

		tracks.add(t);
		sorted = false;
	}

	/**
	 * Removes this track from the station. Calling this on nonincident edge doesn't
	 * cause any error.
	 *
	 * @param t Track to remove.
	 * @return Value given by {@code ArrayList.remove()}.
	 * @see java.util.List#remove(java.lang.Object) For return value.
	 */
	protected boolean removeTrack(Track t)
	{
		return tracks.remove(t);
	}

	/**
	 * Sorts the tracks in clockwise order.
	 */
	protected void sortTracks()
	{
		if (sorted)
			return;

		Collections.sort(tracks, new DescendingTrackComparator(this));
		sorted = true;
	}

	/**
	 * Two stations are equal if they have same location.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (!(obj instanceof Station))
			return false;
		Station s = (Station) obj;
		return s.x == x && s.y == y;
	}

	@Override
	public int hashCode()
	{
		int hash = 3;
		hash = 23 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
		hash = 23 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
		return hash;
	}

	@Override
	public String toString()
	{
		return "Station " + value + ": x=" + x + ", y=" + y;
	}

	/**
	 * Clears the list of tracks.
	 *
	 * @see #addTrack(cz.cvut.fel.rysavpe1.mlcmp.model.Track)
	 */
	void clearTrackList()
	{
		tracks.clear();
	}
}
