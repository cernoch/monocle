/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import java.util.Iterator;

/**
 * The directed graph abstract class.
 *
 * @param <T> Type of values assigned to the vertices.
 * @param <S> Type of values assigned to the edges.
 * @author Petr Ryšavý
 */
public abstract class DirectedGraph<T, S> implements Graph<T, S>
{
	@Override
	public abstract DirectedEdge<S>[] getEdges();

	@Override
	public abstract DirectedEdge<S>[] getIncidentEdges(Vertex<T> v);

	@Override
	public abstract DirectedEdge<S> randomEdge();

	/**
	 * Returns the indegree of particular vertex.
	 *
	 * @param v The vertex.
	 * @return Number of edges with starting vertex {@code v}.
	 */
	public abstract int getInDegree(Vertex<T> v);

	/**
	 * Returns the outegree of particular vertex.
	 *
	 * @param v The vertex.
	 * @return Number of edges with ending at vertex {@code v}.
	 */
	public abstract int getOutDegree(Vertex<T> v);

	/**
	 * Returns ther iterator of vertices incident to the particular one.
	 *
	 * @param v The vertex to search from.
	 * @return Iterator of vertices returned in array of {@code getIncidentVertices}
	 *         method. Calling this method is often more memory friendly.
	 * @see #getIncidentVertices(cz.cvut.fel.rysavpe1.mlcmp.model.graph.Vertex) 
	 */
	public abstract Iterator<Vertex<T>> incidentVerticesIterator(Vertex<T> v);

	/**
	 * Returns the iterator of edges in the problem.
	 * @return Iterator of edges.
	 */
	@Override
	public abstract Iterator<? extends DirectedEdge<S>> edgeIterator();

	@Override
	public boolean isDirected()
	{
		return true;
	}

	@Override
	public int getDegree(Vertex<T> v)
	{
		return getInDegree(v) + getOutDegree(v);
	}
}
