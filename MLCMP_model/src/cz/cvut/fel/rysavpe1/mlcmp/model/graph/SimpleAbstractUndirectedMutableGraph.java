/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

/**
 * The simple graph implementation with mutability property.
 *
 * @param <T> The type of values stored in vertices.
 * @author Petr Ryšavý
 */
public abstract class SimpleAbstractUndirectedMutableGraph<T> extends SimpleAbstractUndirectedGraph<T> implements MutableGraph<T, Void>
{
	@Override
	@SuppressWarnings("unchecked")
	public void addEdge(Edge<Void> e)
	{
		checkUndirected(e);
		final UndirectedEdge<Void> f = (UndirectedEdge<Void>) e;
		addEdge(f.getVertex1(), f.getVertex2());
	}

	@Override
	@SuppressWarnings("unchecked")
	public void removeEdge(Edge<Void> e)
	{
		checkUndirected(e);
		final UndirectedEdge<Void> f = (UndirectedEdge<Void>) e;
		addEdge(f.getVertex1(), f.getVertex2());
	}

	/**
	 * Adds a new undirected edge to the graph.
	 * @param u One of the edge endpoints.
	 * @param v Second edge endpoint.
	 */
	public abstract void addEdge(Vertex<T> u, Vertex<T> v);

	/**
	 * Removes the edge from the graph.
	 * @param u One of the edge endpoints.
	 * @param v Second edge endpoint.
	 */
	public abstract void removeEdge(Vertex<T> u, Vertex<T> v);
}
