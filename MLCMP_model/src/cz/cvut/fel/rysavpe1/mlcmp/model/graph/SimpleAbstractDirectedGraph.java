/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalEdgeException;
import java.util.Collections;
import java.util.Iterator;

/**
 * Abstract implementation of directed graph with values assigned to vertices.
 *
 * @param <T> The type of values connected with vertices.
 * @author Petr Ryšavý
 */
public abstract class SimpleAbstractDirectedGraph<T> extends DirectedGraph<T, Void>
{
	@Override
	public DirectedEdge<Void>[] getEdges()
	{
		final int edges = getEdgesNum();
		@SuppressWarnings("unchecked")
		SimpleDirectedEdge<T>[] arr = new SimpleDirectedEdge[edges];
		int i = 0;
		for (Vertex<T> v : getVertices())
			for (Vertex<T> u : getIncidentVertices(v))
				arr[i++] = new SimpleDirectedEdge<>(v, u);
		return arr;
	}

	@Override
	public DirectedEdge<Void>[] getIncidentEdges(Vertex<T> v)
	{
		final int edges = getEdgesNum();
		@SuppressWarnings("unchecked")
		SimpleDirectedEdge<T>[] arr = new SimpleDirectedEdge[edges];
		int i = 0;
		for (Vertex<T> u : getIncidentVertices(v))
			arr[i++] = new SimpleDirectedEdge<>(v, u);
		return arr;
	}

	/**
	 * Checks whether a particular edge is directed.
	 *
	 * @param e The edge to check.
	 * @throws IllegalEdgeException Is thrown when {@code e} is not instance of
	 *                              {@code DirectedEdge}.
	 * @see DirectedEdge
	 */
	protected final void checkDirected(Edge<Void> e) throws IllegalEdgeException
	{
		if (!(e instanceof DirectedEdge))
			throw new IllegalEdgeException("Edges in directed graph must be directed.");
	}

	/**
	 * {@inheritDoc }
	 * Calling this method is not memory friendly, because the edges are created one
	 * by one.
	 */
	@Override
	public Iterator<SimpleDirectedEdge<T>> edgeIterator()
	{
		return new EdgeIterator();
	}

	/**
	 * The iterator of edges.
	 */
	protected class EdgeIterator implements Iterator<SimpleDirectedEdge<T>>
	{
		/**
		 * Iterator of edge head.
		 */
		private final Iterator<Vertex<T>> headIterator;
		/**
		 * Current edge head.
		 */
		private Vertex<T> current;
		/**
		 * Iterator of tail vertex.
		 */
		private Iterator<Vertex<T>> tailIterator;

		/**
		 * Creates new instance and allocates head and tail iterators.
		 */
		@SuppressWarnings("unchecked")
		public EdgeIterator()
		{
			headIterator = (Iterator<Vertex<T>>) vertexIterator();
			tailIterator = Collections.emptyIterator();
			checkTailIterator();
		}

		@Override
		public boolean hasNext()
		{
			return tailIterator.hasNext();
		}

		@Override
		public SimpleDirectedEdge<T> next()
		{
			SimpleDirectedEdge<T> edge = new SimpleDirectedEdge<>(current, tailIterator.next());
			checkTailIterator();
			return edge;
		}

		/**
		 * This method is not supported.
		 */
		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("Not supported.");
		}

		/**
		 * Finds next head if the tail iterator is gone.
		 */
		private void checkTailIterator()
		{
			if (tailIterator.hasNext())
				return;

			// tail iterator is empty
			if (headIterator.hasNext())
			{
				current = headIterator.next();
				tailIterator = incidentVerticesIterator(current);
				checkTailIterator();
			}
		}
	}
}
