/*
 * Copyright (c) 2013 Petr Ryšavý <rysavpe1@fel.cvut.cz>
 *
 * This file is part of MLCMP.
 * 
 * MLCMP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MLCMP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MLCMP.  If not, see <http ://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.rysavpe1.mlcmp.model.graph;

import cz.cvut.fel.rysavpe1.mlcmp.model.graph.exception.IllegalEdgeException;
import cz.cvut.fel.rysavpe1.utilities.ArrayIterable;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Abstract implementation of directed graph with values assigned to vertices.
 *
 * @param <T> The type of values connected with vertices.
 * @author Petr Ryšavý
 */
public abstract class SimpleAbstractUndirectedGraph<T> extends UndirectedGraph<T, Void>
{
	@Override
	public SimpleUndirectedEdge<T>[] getEdges()
	{
		final int edges = getEdgesNum();
		@SuppressWarnings("unchecked")
		SimpleUndirectedEdge<T>[] arr = new SimpleUndirectedEdge[edges];
		HashSet<SimpleUndirectedEdge<T>> set = new HashSet<>(edges);
		for (Vertex<T> v : getVertices())
			for (Vertex<T> u : getIncidentVertices(v))
				set.add(new SimpleUndirectedEdge<>(u, v));
		return set.toArray(arr);
	}

	@Override
	public SimpleUndirectedEdge<T>[] getIncidentEdges(Vertex<T> v)
	{
		final int edges = getEdgesNum();
		@SuppressWarnings("unchecked")
		SimpleUndirectedEdge<T>[] arr = new SimpleUndirectedEdge[edges];
		int i = 0;
		for (Vertex<T> u : getIncidentVertices(v))
			arr[i++] = new SimpleUndirectedEdge<>(v, u);
		return arr;
	}

	/**
	 * Checks whether a particular edge is undirected.
	 *
	 * @param e The edge to check.
	 * @throws IllegalEdgeException Is thrown when {@code e} is not instance of
	 *                              {@code UnirectedEdge}.
	 * @see DirectedEdge
	 */
	protected final void checkUndirected(Edge<Void> e) throws IllegalEdgeException
	{
		if (!(e instanceof UndirectedEdge))
			throw new IllegalEdgeException("Edges in undirected graph must be undirected.");
	}

	/**
	 * {@inheritDoc }
	 * Calling this method is not memory friendly, because the edges are created one
	 * by one.
	 */
	@Override
	public Iterator<SimpleUndirectedEdge<T>> edgeIterator()
	{
		return new ArrayIterable<>(getEdges()).iterator();
	}
}
